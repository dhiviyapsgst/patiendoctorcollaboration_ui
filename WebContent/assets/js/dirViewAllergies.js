(function () {
	'use strict';
	var App = angular.module('app');
	App.directive('dirViewAllergies', dirViewAllergies);
	dirViewAllergies.$inject = ['$rootScope', '$compile'];

	function dirViewAllergies($rootScope, $compile) {
		return {
			restrict: 'EA',
			templateUrl: 'assets/views/allergies/ViewAllergies.html',
			controller: 'ViewAllergiesController',
			replace: true,
			scope: true,
			link: function (scope, element) {
			}

		};
	}

})();

App
	.controller(
		'ViewAllergiesController',
		[
			'$scope',
			'$rootScope',
			'$http',
			'$localStorage',
			'$state',
			'$stateParams',
			'$window',
			'$timeout',
			function ViewAllergiesController($scope, $rootScope,
				$http, $localStorage, $state, $stateParams,
				$window, $timeout) {

				// On clicking Allergies tab - Display Allergies by Date - for first block
				$scope.num = [1];
				getAllergies = '/ihspdc/rest/displayActiveAllergies?patientId='
				getAllergies += $stateParams.id;
				$http.get(getAllergies).then(function (response, err) {
					if (!err) {
						console.log(response);
						$scope.displayActiveAllergy = response.data;
					} else {
						console.log('Error in displayActiveAllergy');
					}
				});
				getInactiveAllergy = '/ihspdc/rest/displayInactiveAllergies?patientId='
				getInactiveAllergy += $stateParams.id;
				$http.get(getInactiveAllergy)
					.then(function (response, err) {
						if (!err) {
							$scope.displayInactiveAllergy = response.data;
						} else {
							console.log('Error in displayInactiveAllergy');
						}
					});


				var validationOfProfileSection = function (onLoad, formId, callback) {

					var contextScope = this;
					contextScope.onLoad = onLoad;
					var validator = $(formId).validate({
						ignore: [],
						errorClass: 'help-block animated fadeInDown',
						errorElement: 'div',
						errorPlacement: function (error, e) {
							if (!contextScope.onLoad) {
								jQuery(e).parents('.form-group > div').append(error);
							}
						},
						highlight: function (e) {
							var elem = jQuery(e);
							if (!contextScope.onLoad) {
								elem.closest('.form-group').removeClass('has-error').addClass('has-error');
								elem.closest('.help-block').remove();
							}
						},
						success: function (e) {
							var elem = jQuery(e);
							elem.closest('.form-group').removeClass('has-error');
							elem.closest('.help-block').remove();
						},
						rules: {
							'allergen': {
								required: true,
							},
							'patientReaction': {
								required: true,

							},
							'onSetDate': {
								required: true

							}
						},
						messages: {
							'allergen': {
								required: 'allergen cannot be blank',

							},
							'patientReaction': 'patientReaction  cannot be blank',
							'onSetDate': 'onSetDate cannot be blank'
						}
					});
					if (contextScope.onLoad) {
						$(formId).valid();
					}
					else if ($(formId).valid()) {
						callback(null, validator);
					}
				};

				$scope.throwErrorMsg = function (msg) {
					$rootScope.hideAlert();
					$("html, body").animate({ scrollTop: 0 }, 200);
					$rootScope.errMsg = true;
					$rootScope.errorMsg = msg;
					jQuery("#errId").focus();
					jQuery("#errId").show();
				}
				$scope.throwSuccessMsg = function (msg) {
					$rootScope.hideAlert();
					$("html, body").animate({ scrollTop: 0 }, 200);
					$rootScope.sucMsg = true;
					$rootScope.successMsg = msg;
					jQuery("#successId").focus();
					jQuery("#successId").show();
				}

				var allergyValidated = false;

				$scope.allergyValidation = function (allergy) {
					$rootScope.hideAlert();
					if (!allergy.allergen || !allergy.allergyType || !allergy.severity || !allergy.onsetDate) {
						$scope.throwErrorMsg("Mandatory fields are missing");
					}
					else {
						allergyValidated = true;
						$('#saveModalofallergy').modal('show');
					}
				}

				//save allergies code
				$scope.saveAllergies = function (AllergyInfo) {
					if (allergyValidated) {
						console.log("AllergyInfo");
						console.log(AllergyInfo);
						finalAllergies = [];
						TempAllergy = {};
						TempAllergy.patient = {};
						TempAllergy.allergenId = AllergyInfo.allergenId;
						TempAllergy.allergen = AllergyInfo.allergen;
						TempAllergy.allergyType = AllergyInfo.allergyType;
						TempAllergy.reaction = AllergyInfo.reaction;
						TempAllergy.severity = AllergyInfo.severity;
						TempAllergy.onsetDate = AllergyInfo.onsetDate;
						TempAllergy.notes = AllergyInfo.notes;
						if (!AllergyInfo.status) {
							TempAllergy.status = "active";
						}
						else {
							TempAllergy.status = AllergyInfo.status;
						}
						TempAllergy.endDate = AllergyInfo.endDate;
						TempAllergy.patient.opCode = $stateParams.id;
						finalAllergies.push(TempAllergy);

						$http({
							url: '/ihspdc/rest/saveallergy',
							data: finalAllergies,
							method: 'POST',
							headers: {
								'Content-Type': 'application/json'
							}
						}).then(function (response) {
							$scope.getActiveAllergies();
							$scope.getInactiveAllergies();
							$('#addallergies').hide();
							$('#addallergiestrigger').show();
							$rootScope.hideAlert();
							if (allergyModifier) {
								allergyModifier = false;
								$scope.throwSuccessMsg("Allergy has been successfully "+modifyOperation);
								modifyOperation = "";
							} else {
								$scope.throwSuccessMsg("Allergy has been successfully saved");
							}
						}, function (error) {
							console.log("error");
						}
						);
					}
				}//end os save allergy

				$scope.opencollapse = function () {
					$('#addallergies').show();
					$('#addallergiestrigger').hide();
					var onLoad = true;
					var formId = "#allergy";
					var callback = null;
					var success = 0;
					validationOfProfileSection(onLoad, formId, function (err, response) {
						if (!err) {
							console.log("success");
						}
					});
					$scope.allergies = {}
				}

				$scope.closecollapse = function () {
					$scope.allergies = {};
					$('#addallergies').hide();
					$('#addallergiestrigger').show();
					$('.form-group').removeClass('has-error');
					$('.help-block').remove();
				}


				var allergyModifier = false;
				var modifyOperation = "";
				$scope.setInactive = function () {
					allergyModifier = true;
					modifyOperation = "inactivated";
					allergyValidated = true;
					$scope.saveAllergies($scope.toMakethisInActive);
				}

				$scope.allergyIfExist = function (nameOfAllergy) {
					var allAllergies = $scope.displayActiveAllergy.concat($scope.displayInactiveAllergy);
					angular.forEach(allAllergies,function(allergy){
						if(allergy.allergen.toUpperCase() == nameOfAllergy.toUpperCase()){
							$rootScope.hideAlert();
							$scope.throwErrorMsg("Allergy already exist");
						} 
					});
				}

				$scope.setActive = function () {
					allergyModifier = true;
					modifyOperation = "activated";
					allergyValidated = true;
					$scope.saveAllergies($scope.toMakethisActive);
				}

				//inactive allergy function
				$scope.displayInactiveAllergy = [];
				$scope.inactiveAllergy = function (Allergy, index) {
					Allergy.endDate = (moment(new Date()).format("DD-MM-YYYY"));
					Allergy.status = "inactive";
					$scope.toMakethisInActive = Allergy;
					$scope.expand = true;
				}

				$scope.activeAllergy = function (InAllergy, index) {
					InAllergy.onsetDate = (moment(new Date()).format("DD-MM-YYYY"));
					InAllergy.status = "active";
					$scope.toMakethisActive = InAllergy;
				}


				// Display Allergies on show details
				$scope.getActiveAllergies = function () {
					getAllergies = '/ihspdc/rest/displayActiveAllergies?patientId='
					getAllergies += $stateParams.id;
					$http.get(getAllergies)
						.then(function (response, err) {
							if (!err) {
								$scope.displayActiveAllergy = response.data;
							} else {
								console.log('Error');
							}
						});
				}

				$scope.getInactiveAllergies = function () {
					getInactiveAllergy = '/ihspdc/rest/displayInactiveAllergies?patientId='
					getInactiveAllergy += $stateParams.id;
					console.log("Entered inactive");
					$http.get(getInactiveAllergy)
						.then(function (response, err) {
							if (!err) {
								$scope.displayInactiveAllergy = response.data;
							} else {
								console.log('Error');
							}
						});
				}

			}

		]);
