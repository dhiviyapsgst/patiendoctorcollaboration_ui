(function () {
	'use strict';
	var App = angular.module('app');
	App.directive('dirViewVitals', dirViewVitals);
	dirViewVitals.$inject = ['$rootScope', '$compile'];

	function dirViewVitals($rootScope, $compile) {
		return {
			restrict: 'EA',
			templateUrl: 'assets/views/vitals/ViewVitals.html',
			controller: 'ViewVitalsController',
			replace: true,
			scope: true,
			link: function (scope, element) {
			}

		};
	}

})();

App.controller(
	'ViewVitalsController',
	[
		'$scope',
		'$rootScope',
		'$http',
		'$localStorage',
		'$state',
		'$stateParams',
		'$window', '$timeout', 'patientService',
		function ViewVitalsController($scope, $rootScope,
			$http, $localStorage, $state, $stateParams,
			$window, $timeout, patientService) {


			var validationOfProfileSection = function (onLoad, formId, callback) {

				var contextScope = this;
				contextScope.onLoad = onLoad;
				var validator = $(formId).validate({
					ignore: [],
					errorClass: 'help-block animated fadeInDown',
					errorElement: 'div',
					errorPlacement: function (error, e) {
						if (!contextScope.onLoad) {
							jQuery(e).parents('.form-group > div').append(error);
						}
					},
					highlight: function (e) {
						var elem = jQuery(e);
						if (!contextScope.onLoad) {
							elem.closest('.form-group').removeClass('has-error').addClass('has-error');
							elem.closest('.help-block').remove();
						}
					},
					success: function (e) {
						var elem = jQuery(e);
						elem.closest('.form-group').removeClass('has-error');
						elem.closest('.help-block').remove();
					},
					rules: {
						'vitalsdate': {
							required: true,
						},

					},
					messages: {
						'vitalsdate': {
							required: 'Date cannot be blank',

						},

					}
				});
				if (contextScope.onLoad) {
					$(formId).valid();
				}
				else if ($(formId).valid()) {
					callback(null, validator);
				}
			};

			$scope.opencollapse = function () {

				$scope.TotalVitals = {};
				$('#addvitals').show();
				$('#addvitalstrigger').hide();
				var onLoad = true;
				var formId = "#vitals_form";
				var callback = null;
				var success = 0;
				$(function () {
					validationOfProfileSection(onLoad, formId, function (err, response) {
						if (!err) {
							console.log("success");
						}
					});
				});


			}

			$scope.closecollapse = function () {
				$rootScope.hideAlert();
				document.getElementById("vitals_form").reset();
				document.getElementById("vitals_Date").reset();
				$scope.TotalVitals = {};
				$('#addvitals').hide();
				$('#addvitalstrigger').show();
				$('.form-group').removeClass('has-error');
				$('.help-block').remove();
			}
			// Start of Vitals and LabTests
			// On Load Vital tab

			//get recent dates
			$scope.displayVital = {};
			getVitalDates = '/ihspdc/rest/displayVitalDates?patientId='
			getVitalDates += $stateParams.id;
			$scope.formattedVitalDate = [];
			$scope.loadBtn = false;
			$http
				.get(getVitalDates)
				.then(
					function (response, err) {
						if (!err) {
							console.log(response);
							$scope.Vitaldates = response.data;
							if (response.data.length > 5) {
								$scope.loadBtn = true;
							} else {
								$scope.loadBtn = false;
							}
							for (i = 0; i < $scope.Vitaldates.length; i++) {
								$scope.formattedVitalDate.push(moment($scope.Vitaldates[i], 'DD-MM-YYYY').format('MMM DD,YYYY'));
							}
							// Display Vitals by Date
							// for first block
							if ($scope.Vitaldates.length > 0) {
								$scope.getVitalsByDate(0);
							}
						}
					});


			$scope.throwErrorMsg = function (msg) {
				$rootScope.hideAlert();
				$("html, body").animate({ scrollTop: 0 }, 200);
				$rootScope.errMsg = true;
				$rootScope.errorMsg = msg;
				jQuery("#errId").focus();
				jQuery("#errId").show();
			}
			$scope.throwSuccessMsg = function (msg) {
				$rootScope.hideAlert();
				$("html, body").animate({ scrollTop: 0 }, 200);
				$rootScope.sucMsg = true;
				$rootScope.successMsg = msg;
				jQuery("#successId").focus();
				jQuery("#successId").show();
			}

			// ADD VITALS

			$scope.getAllVitals = function () {
				url3 = '/ihspdc/rest/getallvitals'
				$http.get(url3).then(function (response, err) {
					if (!err) {
						$scope.AllVitals = [];
						$scope.vital_show = response.data;
						angular.forEach(response.data, function (vitl) {
							if (vitl.vitalName) {
								$scope.AllVitals.push(vitl.vitalName);
							}
						});
						$scope.boolean = true;
						makeDummyTable();
						console.log($scope.AllVitals);
					}
				});

			}
			$scope.getAllVitals();

			var makeDummyTable = function () {
				//TABLE VIEW - DUMMY DATA
				$scope.vitalDays = [];
				angular.forEach($scope.vital_show, function (vit) {
					var vitalObj = {};
					vitalObj['vitalName'] = vit.vitalName;
					vitalObj['referenceValue'] = vit.referenceValue;
					vitalObj['Unit'] = vit.Unit;
					vitalObj.vitalDayValue = [];
					for (j = 1; j <= 31; j++) {
						dayValue = {};
						dayValue.day = j;
						dayValue.vitalValue = '';
						vitalObj.vitalDayValue.push(dayValue);
					}
					$scope.vitalDays.push(vitalObj);
				});
				console.log("ALL VITAL RESULTS - DUMMY");
				console.log($scope.vitalDays);
			}

			var vitalspresentalready = false;
			$scope.presentAlready = function () {
				vitalspresentalready = false;
				$("#vitalsdate").datepicker('hide');
				angular.forEach($scope.Vitaldates, function (dateAlready) {
					if (dateAlready == $("#vitalsdate").val()) {
						$scope.throwErrorMsg("You have already entered vitals on this date");
						vitalspresentalready = true;
						$("#vitalsdate").datepicker('reset');
						$("#vitals_Date")[0].reset();
					}
				});
			}


			$scope.vitalsValidation = function (vitalInfoForValidation) {
				var validated = 0;
				angular.forEach(vitalInfoForValidation, function (toValid) {
					if (toValid.ObservedVitalValue) {
						validated = 1;
					};
				});
				$scope.presentAlready();
				if (vitalspresentalready != true) {
					if (!$("#vitalsdate").val()) {
						$scope.throwErrorMsg("Date is required");
					}
					else if (validated != 1) {
						$scope.throwErrorMsg("Enter atleast one observed value");
					}
					else {
						if (vitalspresentalready != true) {
							$('#saveModal').modal('show');
						} else {
							$scope.throwErrorMsg("You have already entered vitals on this date");
						}
					}
				}
			}


			// saving vitals
			$scope.saveVitals = function (vitalinfo) {
				//save if both Systolic and Diastolic has values - validation - yet to done
				var TotalVitals = [];
				vitalinfo.forEach(function (eachVital) {
					if (eachVital.ObservedVitalValue) {
						var individualVital = {};
						individualVital.patient = {};
						individualVital.vital = {};
						individualVital.vital.vitalCode = eachVital.vitalCode;
						individualVital.vital.referencceValue = eachVital.referenceValue;
						individualVital.vitalValue = eachVital.ObservedVitalValue;
						individualVital.vitalRecDate = $("#vitalsdate").val();
						individualVital.patient.opCode = $stateParams.id;
						TotalVitals.push(individualVital);
					}
				});
				console.log(TotalVitals);

				// send save request for vitals
				$http({
					url: '/ihspdc/rest/savevitals',
					data: TotalVitals,
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					}
				}).then(function (response) {
					$('#addvitals').hide();
					$('#addvitalstrigger').show();
					$scope.vital_show = {};
					$scope.throwSuccessMsg("Vitals has been successfully saved");
					$scope.Vitals();
					$rootScope.showBasicVitalsOnPatientProfile();
					document.getElementById("vitals_form").reset();
					$("#vitals_Date")[0].reset();
					validated = 0;
					$scope.getAllVitals();
				}, function (error) {
					console.log("error");
					$scope.Vitals();
				}
				);
			}


			$scope.nodeLimit = 5;// initially shows 5 Nodes.
			var noMore = 0;
			$scope.loadMore = function () {
				if ($scope.formattedVitalDate.length >= $scope.nodeLimit) {
					if (noMore != 1) {
						if ($scope.formattedVitalDate.length >= ($scope.nodeLimit + 5)) {
							$scope.nodeLimit += 5;
						}
						else {
							$scope.nodeLimit = $scope.formattedVitalDate.length;
							$scope.loadBtn = false;
							noMore = 1;
						}
					}
				}
				else {
					$scope.loadBtn = false;
				}
			}

			// On clicking Vitals tab
			$scope.Vitals = function () {
				getVitalDates = '/ihspdc/rest/displayVitalDates?patientId='
				getVitalDates += $stateParams.id;
				$scope.formattedVitalDate = [];
				$http
					.get(getVitalDates)
					.then(
						function (response, err) {
							if (!err) {
								console.log(response);
								$scope.Vitaldates = response.data;

								for (i = 0; i < $scope.Vitaldates.length; i++) {
									if ($scope.Vitaldates[i] != null) {            //If date is null

										$scope.formattedVitalDate[i] = moment(
											$scope.Vitaldates[i],
											'DD-MM-YYYY')
											.format(
												'MMM DD,YYYY');
									}

								}

								// Display Vitals by
								// Date for first block
								$scope.displayVital = {};
								getVitals = '/ihspdc/rest/displayVitalsByDate?patientId='
								getVitals += $stateParams.id;
								getVitals += '&date=';
								getVitals += $scope.Vitaldates[0];
								$http
									.get(getVitals)
									.then(
										function (
											response,
											err) {
											if (!err) {
												$scope.displayVital[0] = response.data;
											} else {
												//$scope.warn=1;
												console
													.log('Error');

											}
										});

							}
						});
			}

			$scope.displayVital = {};
			// Display Vitals on show details
			$scope.getVitalsByDate = function (index) {
				getVitals = '/ihspdc/rest/displayVitalsByDate?patientId='
				getVitals += $stateParams.id;
				getVitals += '&date=';
				getVitals += $scope.Vitaldates[index];
				$http
					.get(getVitals)
					.then(
						function (response, err) {
							if (!err) {
								console.log(response);
								$scope.displayVital[index] = response.data;
							} else {
								//$scope.warn=1;
								console.log('Error');
							}
						});
			}

			//TABLE VIEW PROCESS
			$scope.currentVitals = [];
			var patientVitalsData = [];
			patientService.getAllVitalResults($stateParams.id, function (err, response) {
				if (!err) {
					console.log("ALL VITAL RESULTS");
					patientVitalsData = _.sortBy(response.data, function (o) { return new moment(o.vitalRecDate, "DD-MM-YYYY"); }).reverse();
					$scope.vitalDateFormats = [];
					angular.forEach(patientVitalsData, function (vit) {
						$scope.vitalDateFormats.push(moment(vit.vitalRecDate, "DD-MM-YYYY").format("MMM DD YYYY"));
						if (moment(vit.vitalRecDate, "DD-MM-YYYY").isSame(new Date(), 'month') && moment(vit.vitalRecDate, "DD-MM-YYYY").isSame(new Date(), 'year')) {
							$scope.currentVitals.push(vit);
						}
					});
					$scope.vitalDateFormats = _.uniq($scope.vitalDateFormats);
					$scope.getLatestVitals();
					// if ($scope.vitalDays) {
					// 	angular.forEach($scope.currentVitals, function (resVital) {
					// 		angular.forEach($scope.vitalDays, function (vit) {
					// 			angular.forEach(vit.vitalDayValue, function (vitDay) {
					// 				if (vitDay.day == parseInt(moment(resVital.vitalRecDate, "DD-MM-YYYY").format('D')) && vit.vitalName == resVital.vital.vitalName) {
					// 					vitDay.vitalValue = resVital.vitalValue;
					// 				}
					// 			});
					// 		});
					// 	});
					// }
				}
			});

			$("#yearPick").datepicker({
				format: "yyyy",
				viewMode: "years",
				minViewMode: "years",
				autoclose: true,
				toggleActive: true,
				updateViewDate: true,
				startView: 2,
				changeYear: true,
				defaultViewDate: { year: '2018' },
			});
			$scope.currMonth = moment().format("MM");

			//FILTER VITALS BY MONTH
			$scope.allMonths = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
			$scope.getVitalsByMonth = function (selectedMonth) {
				$("#yearPick").datepicker('hide');
				if ($scope.selectedYear) {
					$scope.all = false;
					if (selectedMonth == "Jan") { selectedMonth = 01 };
					if (selectedMonth == "Feb") { selectedMonth = 02 };
					if (selectedMonth == "Mar") { selectedMonth = 03 };
					if (selectedMonth == "Apr") { selectedMonth = 04 };
					if (selectedMonth == "May") { selectedMonth = 05 };
					if (selectedMonth == "Jun") { selectedMonth = 06 };
					if (selectedMonth == "Jul") { selectedMonth = 07 };
					if (selectedMonth == "Aug") { selectedMonth = 08 };
					if (selectedMonth == "Sep") { selectedMonth = 09 };
					if (selectedMonth == "Oct") { selectedMonth = 10 };
					if (selectedMonth == "Nov") { selectedMonth = 11 };
					if (selectedMonth == "Dec") { selectedMonth = 12 };
					var startDate = moment([$scope.selectedYear, selectedMonth - 1]);
					var endDate = moment(startDate).endOf('month');
					$scope.currentVitals = [];
					makeDummyTable();
					angular.forEach(patientVitalsData, function (vit) {
						if (moment(vit.vitalRecDate, "DD-MM-YYYY").isSameOrAfter(startDate.toDate()) &&
							moment(vit.vitalRecDate, "DD-MM-YYYY").isSameOrBefore(endDate.toDate())) {
							$scope.currentVitals.push(vit);
						}
					});
					if ($scope.vitalDays) {
						angular.forEach($scope.currentVitals, function (resVital) {
							angular.forEach($scope.vitalDays, function (vit) {
								angular.forEach(vit.vitalDayValue, function (vitDay) {
									if (vitDay.day == parseInt(moment(resVital.vitalRecDate, "DD-MM-YYYY").format('D')) && vit.vitalName == resVital.vital.vitalName) {
										vitDay.vitalValue = resVital.vitalValue;
									}
								});
							});
						});
					}
				} else {
					$scope.throwErrorMsg("Year is required");
				}
			}

			$scope.getLatestVitals = function () {
				$scope.all = true;
				//TABLE VIEW LATEST VITALS - DUMMY DATA
				$scope.vitalDays = [];
				angular.forEach($scope.vital_show, function (vit) {
					var vitalObj = {};
					vitalObj['vitalName'] = vit.vitalName;
					vitalObj['referenceValue'] = vit.referenceValue;
					vitalObj['Unit'] = vit.Unit;
					vitalObj.vitalDayValue = [];
					angular.forEach($scope.vitalDateFormats, function (vit) {
						dayValue = {};
						dayValue.actualDate = vit;
						dayValue.day = moment(vit, "MMM DD").format("DD");
						dayValue.vitalValue = '';
						vitalObj.vitalDayValue.push(dayValue);
					});
					$scope.vitalDays.push(vitalObj);
				});
				console.log("ALL VITAL RESULTS - DUMMY");
				console.log($scope.vitalDays);
				if ($scope.vitalDays) {
					angular.forEach(patientVitalsData, function (resVital) {
						angular.forEach($scope.vitalDays, function (vit) {
							angular.forEach(vit.vitalDayValue, function (vitDay) {
								if (vitDay.day == parseInt(moment(resVital.vitalRecDate, "DD-MM-YYYY").format('D')) && vit.vitalName == resVital.vital.vitalName) {
									vitDay.vitalValue = resVital.vitalValue;
								}
							});
						});
					});
				}
			}

		}//End of vital directive

	]);
