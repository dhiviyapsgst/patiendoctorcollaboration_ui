(function () {
	'use strict';
	var App = angular.module('app');
	App.directive('dirAdvisedlabTest', dirAdvisedlabTest);
	dirAdvisedlabTest.$inject = ['$rootScope', '$compile'];

	function dirAdvisedlabTest($rootScope, $compile) {
		return {
			restrict: 'EA',
			templateUrl: 'assets/views/prescriptions/AdvisedlabTest.html',
			controller: 'AdvisedlabTestController',
			replace: true,
			scope: true,
			link: function (scope, element) {
			}

		};
	}

})();



App.controller(
	'AdvisedlabTestController',
	[
		'$scope',
		'$rootScope',
		'$http',
		'$localStorage',
		'$state',
		'$stateParams',
		'$window',
		function AdvisedlabTestController($scope, $rootScope,
			$http, $localStorage, $state, $stateParams,
			$window) {


			//rest call for getting all lab tests for advised lab tests
			getAllTests = '/ihspdc/rest/getalltests'
			$http.get(getAllTests).then(function (response, err) {
				if (!err) {
					console.log("getTests");
					console.log(response.data);
					$scope.labTests = response.data;
					angular.forEach($scope.labTests, function (tests) {
						tests.Selected = false;
					})

				}

			});



			getAllGroup = '/ihspdc/rest/getallgroups'
			$http.get(getAllGroup).then(function (response, err) {
				if (!err) {
					console.log("getgrps");
					console.log(response.data);
					$scope.labGrps = response.data;
					angular.forEach($scope.labGrps, function (testgrp) {
						testgrp.Selected = false;
					})

				}

			});



			//rest call for getting all saved lab templates
			getAllTemplates = '/ihspdc/rest/gettemplate';
			$rootScope.labTemplates = [];
			$http.get(getAllTemplates).then(function (response, err) {
				if (!err) {
					console.log("getTemplates");
					console.log(response.data);
					for (var i = 0; i < response.data.length; i++) {
						if (response.data[i].doctor.docId.toUpperCase() == $rootScope.Auth.tokenParsed.preferred_username.toUpperCase()) {
							$rootScope.labTemplates.push(response.data[i]);
							response.data[i].Selected = false;
						}
					}

				}
			});



			//to view tests belong to a template
			$scope.templateName = "";
			$scope.gettemptests = function (tempName) {
				$scope.templateName = tempName;
				angular.forEach($rootScope.labTemplates, function (eachTemplate) {
					if (eachTemplate.templateName == tempName) {
						$scope.showtemptests = eachTemplate.labTests;

					}
				});
			}

			//to view test belong to a group
			$scope.grpNameRecived = "";
			$scope.getgrptests = function (grpName) {
				$scope.showgrptests = [];
				$scope.grpNameRecived = grpName;
				console.log($scope.grpNameRecived);
				angular.forEach($scope.labTests, function (eachgrpTest) {
					if (eachgrpTest.group.groupName == grpName) {
						$scope.showgrptests.push(eachgrpTest);
					}
				});
			}


			$scope.finalSelectedTests = [];
			$scope.getSelectedTests = function () {
				$scope.finalSelectedTests = [];
				$scope.SelLabTest = [];
				for (var i = 0; i < $scope.labTests.length; i++) {
					var SelectedLabTest = {};
					SelectedLabTest.group = {};
					if ($scope.labTests[i].Selected) {
						console.log($scope.labTests[i].testName);
						SelectedLabTest.testName = ($scope.labTests[i].testName);
						SelectedLabTest.testCode = ($scope.labTests[i].testCode);
						SelectedLabTest.unit = ($scope.labTests[i].unit);
						SelectedLabTest.gender = ($scope.labTests[i].gender);
						SelectedLabTest.group.id = ($scope.labTests[i].group.id);
						SelectedLabTest.group.groupName = ($scope.labTests[i].group.groupName);
						SelectedLabTest.group.groupRank = ($scope.labTests[i].group.groupRank);
						$scope.SelLabTest.push(SelectedLabTest);
					}
				}
			}

			$scope.getSelectedGrps = function () {
				$scope.finalSelectedTests = [];
				$scope.SelLabGrp = [];
				for (var i = 0; i < $scope.labGrps.length; i++) {
					var SelectedLabGrp = {};
					SelectedLabGrp.group = {};
					if ($scope.labGrps[i].Selected) {
						angular.forEach($scope.labTests, function (eachgrpTest) {
							if (eachgrpTest.group.groupName == $scope.labGrps[i].groupName) {
								$scope.SelLabGrp.push(eachgrpTest);
							}
						});
					}
				}
			}


			$scope.getSelectedTemplate = function () {
				$scope.finalSelectedTests = [];
				$scope.SelLabTemp = [];
				for (var i = 0; i < $rootScope.labTemplates.length; i++) {
					if ($rootScope.labTemplates[i].Selected) {
						console.log($rootScope.labTemplates[i].groupName);
						console.log($rootScope.labTemplates[i].groupRank);
						angular.forEach($rootScope.labTemplates[i].labTests, function (test) {
							var SelectedLabTemp = {};
							SelectedLabTemp.group = {};
							SelectedLabTemp.testName = (test.testName);
							SelectedLabTemp.testCode = (test.testCode);
							SelectedLabTemp.unit = (test.unit);
							SelectedLabTemp.gender = (test.gender);
							SelectedLabTemp.group.id = (test.group.id);
							SelectedLabTemp.group.groupName = (test.group.groupName);
							SelectedLabTemp.group.groupRank = (test.group.groupRank);
							$scope.SelLabTemp.push(SelectedLabTemp);
						});

					}
				}
				angular.forEach($scope.SelLabTest, function (eachtest) {
					$scope.finalSelectedTests.push(eachtest);
				});
				angular.forEach($scope.SelLabGrp, function (eachtest) {
					$scope.finalSelectedTests.push(eachtest);
				});
				angular.forEach($scope.SelLabTemp, function (eachtemptest) {
					$scope.finalSelectedTests.push(eachtemptest);
				});

				getSelectedPrescription($scope.checkDuplicateInObject('testName', $scope.finalSelectedTests));
			}


			$scope.checkDuplicateInObject = function (propertyName, inputArray) {
				var seenDuplicate = false,
					testObject = {};
				inputArray.map(function (item) {
					var itemPropertyName = item[propertyName];
					if (itemPropertyName in testObject) {
						testObject[itemPropertyName].duplicate = true;
						item.duplicate = true;
						seenDuplicate = true;
					}
					else {
						testObject[itemPropertyName] = item;
						delete item.duplicate;
					}
				});

				return testObject;
			}

			// for check and uncheck all checkboxes of test tab
			$scope.checkAll = function () {
				$scope.selectedAll = true;
				angular.forEach($scope.labTests, function (tests) {
					tests.Selected = $scope.selectedAll;
				});
			};

			$rootScope.unCheckAll = function () {
				$scope.selectedAll = false;
				angular.forEach(
					$scope.labTests,
					function (tests) {
						tests.Selected = $scope.selectedAll;
					});
			};


			// for check and uncheck all checkboxes of testGroup tab
			$scope.CheckAllGroup = function () {
				$scope.selectedAllGrp = true;
				angular.forEach(
					$scope.labGrps,
					function (testgrp) {
						testgrp.Selected = $scope.selectedAllGrp;
					});
			};

			$rootScope.UnCheckAllGroup = function () {
				$scope.selectedAllGrp = false;
				angular.forEach($scope.labGrps, function (testgrp) {
					testgrp.Selected = $scope.selectedAllGrp;
				});
			};

	
			//final selected test generate funtion
			getSelectedPrescription = function (finalSelected) {
				$rootScope.finSelLabTestArr = finalSelected;
				var testCodes = [];
				if ($rootScope.finalUseArr && $rootScope.finalUseArr.length > 0) {
					angular.forEach($rootScope.finalUseArr, function (putSelTest) {
						if(putSelTest.labTest){
							testCodes.push(putSelTest.labTest.testCode);
						}
						else{
							testCodes.push(putSelTest.testCode);
						}
					});
					angular.forEach($rootScope.finSelLabTestArr,function(test){
						console.log(testCodes.indexOf(test.testCode));
						if(testCodes.indexOf(test.testCode) < 0){
							$rootScope.finalUseArr.push(test);
						}
					})
				}
				else {
					angular.forEach($rootScope.finSelLabTestArr,function(test){
						$rootScope.finalUseArr.push(test);
					});
				}
			}

		}

	]);
