//Patient Profile Controller
App.controller('PatientProfileCtrl',
	[
		'$scope',
		'$http',
		'$state',
		'$stateParams',
		'$window', '$rootScope', 'patientService',
		function ($scope, $http, $state,
			$stateParams, $window, $rootScope, patientService) {


			$rootScope.opforHist = $stateParams.id; // must need for historical entry
			$rootScope.finalUseArr = []; // must to have here

			//TO GET LAST VISIT
			getPatientLabTest = '/ihspdc/rest/getpatientlabtest?opCode=';
			getPatientLabTest += $stateParams.id;
			$http.get(getPatientLabTest).then(function (response, err) {
				if (!err) {
					$rootScope.labtestgrpdef = response.data;
				}
			});

			$scope.showToggleButton = true;
			$scope.showToggle = function () {
				$scope.showToggleButton = true;
				$rootScope.hideAlert();
			}
			$scope.hideToggle = function () {
				$scope.showToggleButton = false;
				$rootScope.hideAlert();
			}

			$rootScope.hideAlert = function () {
				$rootScope.sucMsg = false;
				$rootScope.errMsg = false;
			}

			//SWITCH TREE AND TABLE
			$scope.tree = true;
			$scope.table = false;
			$scope.changeViewMode = function () {
				$scope.table = false;
				$scope.tree = false;
				if ($('#myonoffswitch').is(':checked')) {
					$scope.table = true;
				}
				else {
					$scope.tree = true;
				}
			}

			// Patient profile
			var patientDetails = '/ihspdc/rest/getPatientDetails?opCode='
			patientDetails += $stateParams.id;
			$http.get(patientDetails).then(
				function (response, err) {
					if (!err) {
						console.log(response);
						if (response.data != null) {
							$scope.subscriptionInfo = response.data;
							$rootScope.patient = $scope.subscriptionInfo.patient;
							$scope.patient.BloodType = $rootScope.patient.BloodGroup.substr(0, 1);
							$scope.patient.BloodTypeVariant = $rootScope.patient.BloodGroup.substr(2, 4);
						}
					} else {
						console.log('Error');

					}
				});


			var callMedOnce = 0;
			$scope.callMedDates = function () {
				if (callMedOnce == 0) {
					$scope.$broadcast('medEvent');
					callMedOnce = 1;
				}
			}

			var callLabTestOnce = 0;
			$scope.loadLabTestTab = function () {
				if (callLabTestOnce == 0) {
					$scope.$broadcast('labTestTabEvent');
					callLabTestOnce = 1;
				}
			}

			// Basic Vitals to show in patient profile (height, weight etc)
			$rootScope.showBasicVitalsOnPatientProfile = function () {
				// Basic Vitals to show in patient profile (height, weight etc)
				$rootScope.basicVitals = {};
				getBasicVitals = '/ihspdc/rest/displayBasicVitals?patientId='
				getBasicVitals += $stateParams.id;
				$http.get(getBasicVitals).then(
					function (response, err) {
						if (!err) {
							console.log(response);
							if (response.data != null) {
								$scope.baseVitals = response.data;
								angular.forEach($scope.baseVitals, function (vitals, err) {

									if (vitals.vital.vitalName != "BloodGroup")
										$rootScope.basicVitals[vitals.vital.vitalName] = parseInt(vitals.vitalValue);

									else
										$rootScope.basicVitals[vitals.vital.vitalName] = vitals.vitalValue;

								});

								$scope.heightInMetres = ($rootScope.basicVitals["Height"]) / 100;

								$rootScope.basicVitals["BMI"] = ($rootScope.basicVitals["Weight"]) / (($scope.heightInMetres) * ($scope.heightInMetres));

								$rootScope.basicVitals["BMI"] = Math.floor($rootScope.basicVitals["BMI"]);

								$scope.BMIResult = "";
								if ($rootScope.basicVitals["BMI"] <= 19) { $scope.BMIResult = "UnderWeight"; }
								if ($rootScope.basicVitals["BMI"] > 19 && $rootScope.basicVitals["BMI"] <= 25) { $scope.BMIResult = "Normal"; }
								if ($rootScope.basicVitals["BMI"] > 25 && $rootScope.basicVitals["BMI"] <= 29) { $scope.BMIResult = "Overweight"; }
								if ($rootScope.basicVitals["BMI"] >= 30) { $scope.BMIResult = "Obese"; }


								$scope.realFeet = ((($rootScope.basicVitals["Height"]) * 0.393700) / 12);

								$scope.feet = Math.floor($scope.realFeet);
								$scope.inches = Math.round(($scope.realFeet - $scope.feet) * 12);

							}

							else {
								console.log('Error');
							}
						}

					});
			}
			$scope.showBasicVitalsOnPatientProfile();

		}



	]);
App.controller('singleOnBoardCtrl', [
	'$scope',
	'$http',
	'$state',
	'$stateParams',
	'$window', '$timeout', '$rootScope', 'patientService',
	function ($scope, $http, $state,
		$stateParams, $window, $timeout, $rootScope, patientService) {

		$scope.newPatient = {};
		$scope.newPatient.doctors = [];

		$.getJSON("cityStates.json", function (cityState) {
			$scope.country = cityState;
		});

		jQuery.validator.addMethod("phoneIndia", function (phone_number, element) {
			phone_number = phone_number.replace(/\s+/g, "");
			return this.optional(element) || phone_number.length > 9 &&
				phone_number.match(/^[0-9]\d{9}$/);
		}, "Enter a valid phone number");
		$scope.validateNewPatient = {
			debug: false,
			errorElement: "div",
			errorPlacement: function (error, element) {
				if (element.attr("name") == "patientName" || element.attr("name") == "patientName2" || element.attr("name") == "patientName3" || element.attr("name") == "caddress1" || element.attr("name") == "caddress2" || element.attr("name") == "radios") {
					error.insertAfter(element.parent('.input-group'));
				} else {
					error.insertAfter(element);
				}
			},
			rules: {
				OpCode: {
					required: true
				},
				patientName: {
					required: true
				},
				Department: {
					required: true
				},
				email: {
					required: true,
					email: true
				},
				mobile: {
					required: true,
					phoneIndia: true
				},
				patientDOB: {
					required: true
				},
				radios: {
					required: true
				},
				patientBloodGroup: {
					required: true
				},
				caddress1: {
					required: true
				},
				cstate: {
					required: true
				},
				ccity: {
					required: true
				},
				Aadhaar: {
					minlength: 12,
					maxlength: 12
				},
				czip: {
					required: true,
					minlength: 6,
					maxlength: 6
				}
			},
			messages: {
				OpCode: {
					required: "OP-Code is required"
				},
				patientName: {
					required: "Patient first name is required"
				},
				Department: {
					required: "Department is required"
				},
				email: {
					required: "email is required",
				},
				mobile: {
					required: "Mobile no is required",
				},
				patientDOB: {
					required: "DOB is required"
				},
				radios: {
					required: "Gender is required"
				},
				patientBloodGroup: {
					required: "Blood Group is required"
				},
				caddress1: {
					required: "Address Line 1 is required"
				},
				cstate: {
					required: "State is required"
				},
				ccity: {
					required: "City is required"
				},
				czip: {
					required: "Pin code is required",
					minlength: "6 Digit is required",
					maxlength: "6 Digit is required"
				}
			},
			highlight: function (element, errorClass) {
				$(element).removeClass(errorClass);
			}
		}

		$scope.registerPatient = function (patientForm, newPatientDetails) {
			if (!patientForm.validate()) {
				// event.preventDefault();
				// event.stopPropagation();
			}
			else {
				if (!newPatientDetails.department) {
					document.getElementById("departmentError").innerHTML = "Department is required";
				}
				else if (!newPatientDetails.doctors || newPatientDetails.doctors.length == 0) {
					document.getElementById("doctorError").innerHTML = "Select associated doctors";
				}
				else if (!newPatientDetails.BloodGroup) {
					document.getElementById("bloodGroupError").innerHTML = "Blood Group is required";
				}
				else if (!newPatientDetails.state) {
					document.getElementById("stateError").innerHTML = "State is required";
				}
				else if (!newPatientDetails.city) {
					document.getElementById("cityError").innerHTML = "City is required";
				}
				else {
					$('#addPatient').modal('hide');
					document.getElementById("departmentError").innerHTML = "";
					document.getElementById("doctorError").innerHTML = "";
					document.getElementById("stateError").innerHTML = "";
					document.getElementById("cityError").innerHTML = "";
					document.getElementById("bloodGroupError").innerHTML = "";

					angular.forEach($scope.AllDepartments, function (dep) {
						if (dep.name == newPatientDetails.department) {
							newPatientDetails.department = dep;
						}
					});

					//Making JSON structure respective to subscription class
					var subscriber_patient = {};
					subscriber_patient.patient = {};
					subscriber_patient.doctors = [];
					subscriber_patient.invitedBy = {};
					subscriber_patient.patient = newPatientDetails;
					subscriber_patient.doctors = newPatientDetails.doctors;
					subscriber_patient.invitedBy = $scope.associatedCoordinator; // Setting Associated Co-ordinator for patient
					subscriber_patient.status = "Active";
					subscriber_patient.inviteDate = moment(new Date()).format("DD-MM-YYYY");
					subscriber_patient.joinDate = moment(new Date()).format("DD-MM-YYYY");
					subscriber_patient.lastLogin = moment(new Date()).format("DD-MM-YYYY");
					console.log(subscriber_patient);
					$http({
						url: '/ihspdc/rest/addNewPatient',
						data: subscriber_patient,
						method: 'POST',
						headers: {
							'Content-Type': 'application/json'
						}
					}).then(function (response) {
						$("html, body").animate({ scrollTop: 0 }, 200);
						$scope.sucMsg = true;
						$scope.successMsg = "Patient has been successfully added";
						jQuery("#successId").focus();
						jQuery("#successId").show();
						$scope.getPatientsList(0, 1);
						subscriber_patient = {};
						$scope.newPatientDetails = {};
						$scope.newPatient = {};
						patientService.getAllPatientCount(function (err, response) {
							if (!err) {
								var page = 0;
								$scope.pagesNeeded = [];
								if (response % 10 == 0) {
									page = response / 10;
								}
								else if (response % 10 != 0 && response <= 10) {
									page = 1;
								}
								else {
									page = Math.floor(response / 10) + 1;
								}
								for (i = 0; i < page; i++) {
									$scope.pagesNeeded.push(i);
								}
							}
						});
					}, function (error) {
						console.log("Error in Adding Patient");
					});
				}
			}
		}

		$scope.cancelOfSavingPatient = function () {
			$scope.newPatient = {};
		}


		var doctorsDetails = '/ihspdc/rest/getDoctorsList';
		$http.get(doctorsDetails).then(
			function (response, err) {
				if (!err && response.data != "") {
					response.data.sort(function (a, b) {
						return a.docName.localeCompare(b.docName);
					});
					$scope.AllDoctors = response.data;
					console.log($scope.AllDoctors);
				} else {
					console.log('Error');
				}
			});

		$scope.getDepartmentList = function () {
			var department_Details = '/ihspdc/rest/getDepartmentsList';
			$http.get(department_Details).then(
				function (response, err) {
					if (!err && response.data != "") {
						response.data.sort(function (a, b) {
							return a.name.localeCompare(b.name);
						});
						$scope.AllDepartments = response.data;
					} else {
						console.log('Error');
					}
				});
		}
		$scope.getDepartmentList();

		var coordsDetails = '/ihspdc/rest/getCoordsList';
		$http.get(coordsDetails).then(
			function (response, err) {
				if (!err && response.data != "") {
					response.data.sort(function (a, b) {
						return a.name.localeCompare(b.name);
					});
					angular.forEach(response.data, function (coord) {
						if (coord.id.toUpperCase() == $rootScope.Auth.tokenParsed.preferred_username.toUpperCase()) {
							$scope.associatedCoordinator = coord;
						}
					});
					console.log($scope.associatedCoordinator);
				} else {
					console.log('Error');
				}
			});

		$scope.loadDepartmentDoctors = function (selectedDepartment) {
			$scope.editPatient.doctors = [];
			$scope.newPatient.doctors = [];
			$scope.departmentDoctors = [];
			angular.forEach($scope.AllDoctors, function (doc) {
				if (doc.department.name == selectedDepartment) {
					$scope.departmentDoctors.push(doc);
				}
			});
		}

		patientService.getAllPatientCount(function (err, response) {
			if (!err) {
				var page = 0;
				$scope.pagesNeeded = [];
				if (response % 10 == 0) {
					page = response / 10;
				}
				else if (response % 10 != 0 && response <= 10) {
					page = 1;
				}
				else {
					page = Math.floor(response / 10) + 1;
				}
				for (i = 0; i < page; i++) {
					$scope.pagesNeeded.push(i + 1);
				}
			}
			if ($scope.pagesNeeded.length) {
				$scope.getPatientsList(0, 1);
			}
		});

		$scope.getPatientsList = function (numberOfRecords, pageNo) {
			if (pageNo > 0 && pageNo <= $scope.pagesNeeded.length) {
				$scope.pageIn = pageNo;
				var getAllPatDetails = '/ihspdc/rest/getEveryPatients?getNext='; // Need to spend time for this
				getAllPatDetails += numberOfRecords;
				$http.get(getAllPatDetails)
					.then(function (response, err) {
						if (!err && response.data != "") {
							response.data.sort(function (a, b) {
								return a.patient.fName.localeCompare(b.patient.fName);
							});
							console.log(response);
							//$scope.loader = false;
							$scope.allPatientsList = response.data;
						} else {
							console.log('Error');
						}
					});
			}
		}

		$scope.hideAlert = function () {
			$scope.sucMsg = false;
		}

		$scope.checkOpExist = function (opCodeInp) {
			var found = true;
			patientService.getFullPatientsList(function (err, response) {
				if (!err) {
					angular.forEach(response.data, function (existPatient) {
						if (existPatient.patient.opCode.toUpperCase() == opCodeInp.toUpperCase() && found) {
							document.getElementById("opExistError").innerHTML = "OP-Code already exist";
							$scope.newPatient.opCode = "";
							found = false;
						}
					});
					if (found) {
						document.getElementById("opExistError").innerHTML = "";
					}
				}
			});
		}


		// //Search Patient by OP-Code
		// $scope.searchPatient = function (opCode) {
		// 	if (opCode.length >= 4) {
		// 		var getpat = '/ihspdc/rest/getPatientDetails?opCode='
		// 		getpat += opCode;
		// 		$http.get(getpat).then(function (response, err) {
		// 			if (!err && response.data != "") {
		// 				$scope.loader = false;
		// 				$scope.activePatientsList = response.data;
		// 				$scope.allPatientsList = [];
		// 				if ($rootScope.Auth.hasResourceRole('coord')) {
		// 					if ($rootScope.Auth.tokenParsed.preferred_username.toUpperCase() == $scope.activePatientsList.invitedBy.id.toUpperCase()) {
		// 						$scope.allPatientsList.push($scope.activePatientsList);
		// 					}
		// 				}
		// 				else {
		// 					angular.forEach($scope.activePatientsList.doctors, function (docs) {
		// 						if ($rootScope.Auth.tokenParsed.preferred_username.toUpperCase() == docs.docId.toUpperCase()) {
		// 							$scope.allPatientsList.push($scope.activePatientsList);
		// 						}
		// 					});
		// 				}
		// 				console.log("Patient" + $scope.allPatientsList);
		// 			} else {
		// 				console.log('Error in searching patient');
		// 			}
		// 		});
		// 	} else if (opCode.length == 0) {
		// 		$scope.getPatientsList(0, 1);
		// 	}
		// }

		$scope.searchPatient = function (searchTerm, byField) {
			if (searchTerm.length >= 4) {
				$scope.allPatientsList = [];
				if (byField == "1") {
					patientService.searchPatientByField(byField, searchTerm, function (err, response) {
						$scope.allPatientsList.push(response.data);
					});
				}
				else if (byField == "2") {
					patientService.searchPatientByField(byField, searchTerm, function (err, response) {
						$scope.allPatientsList = response;
					});
				}
				else if (byField == "3") {
					patientService.searchPatientByField(byField, searchTerm, function (err, response) {
						$scope.allPatientsList = response;
					});
				}
			}
			else if (searchTerm.length == 0) {
				$scope.getPatientsList(0, 1);
			}
		}

		//Age Calculation
		$scope.calculateAge = function (datePicked) {
			$("#patientDOB").datepicker('hide');
			var age = moment().diff(moment(datePicked, "DD-MM-YYYY"), 'years', true);
			$scope.newPatient.age = Math.floor(age);
			$scope.editPatient.age = Math.floor(age);

		};

		//Patient Status Change Process
		var patientRecordToChangeStatus = {};
		$scope.changePatientStatus = function (patientReference) {
			patientRecordToChangeStatus = JSON.parse(JSON.stringify(patientReference));
			//Just for showing purpose
			$scope.patietMajorData = {};
			$scope.patietMajorData.opCode = patientRecordToChangeStatus.patient.opCode;
			$scope.patietMajorData.patientName = patientRecordToChangeStatus.patient.fName;
			//Changimg status
			if (patientRecordToChangeStatus.status == "Active") {
				patientRecordToChangeStatus.status = "Inactive";
			}
			else if (patientRecordToChangeStatus.status == "Inactive") {
				patientRecordToChangeStatus.status = "Active";
			}
			patientRecordToChangeStatus = patientRecordToChangeStatus;
			$('#changeStatusModal').modal('show');
		}

		$scope.changeStatus = function () {
			$http({
				url: '/ihspdc/rest/addNewPatient',
				data: patientRecordToChangeStatus,
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				}
			}).then(function (response) {
				$("html, body").animate({ scrollTop: 0 }, 200);
				$scope.sucMsg = true;
				$scope.successMsg = "Status has been successfully changed";
				jQuery("#successId").focus();
				jQuery("#successId").show();
				$scope.getPatientsList(0, 1);
				subscriber_patient = {};
				patientRecordToChangeStatus = {};
			}, function (error) {
				console.log("Error in updating Patient");
			});
		}

		//Patient Editing Processes
		var subscriptionInfo = 0;
		$scope.editPatient = {};
		$scope.openEditPatient = function (patientDetails) {
			subscriptionInfo = patientDetails;
			$scope.editPatient = {};
			$scope.editPatient.department = {};
			$scope.editPatient.doctors = [];
			$scope.editPatient.opCode = patientDetails.patient.opCode;
			$scope.editPatient.department = patientDetails.patient.department.name;
			angular.forEach(patientDetails.doctors, function (doc) {
				$scope.editPatient.doctors.push(doc);
			});
			$scope.editPatient.fName = patientDetails.patient.fName;
			$scope.editPatient.mName = patientDetails.patient.mName;
			$scope.editPatient.lName = patientDetails.patient.lName;
			$scope.editPatient.dob = patientDetails.patient.dob;
			$scope.editPatient.BloodGroup = patientDetails.patient.BloodGroup;
			$scope.editPatient.height = patientDetails.patient.height;
			$scope.editPatient.aadharId = patientDetails.patient.aadharId;
			$scope.editPatient.gender = patientDetails.patient.gender;
			$scope.editPatient.marital = patientDetails.patient.marital;
			$scope.editPatient.mobileNo = patientDetails.patient.mobileNo;
			$scope.editPatient.addressLine1 = patientDetails.patient.addressLine1;
			$scope.editPatient.addressLine2 = patientDetails.patient.addressLine2;
			$scope.editPatient.state = patientDetails.patient.state;
			$scope.editPatient.city = patientDetails.patient.city;
			$scope.editPatient.email = patientDetails.patient.email;
			$scope.editPatient.zipCode = patientDetails.patient.zipCode;
			$scope.editPatient.occupation = patientDetails.patient.occupation;
			$('#editPatient').modal('show');
		}
		$scope.updatePatient = function (editpatientForm, patientWithEditedDetails) {
			if (!editpatientForm.validate()) {
				// event.preventDefault();
				// event.stopPropagation();
			}
			else {
				if (!patientWithEditedDetails.department) {
					document.getElementById("departmentError").innerHTML = "Department is required";
				}
				else if (!patientWithEditedDetails.doctors || patientWithEditedDetails.doctors.length == 0) {
					document.getElementById("doctorError").innerHTML = "Select associated doctors";
				}
				else if (!patientWithEditedDetails.BloodGroup) {
					document.getElementById("bloodGroupError").innerHTML = "Blood Group is required";
				}
				else if (!patientWithEditedDetails.state) {
					document.getElementById("stateError").innerHTML = "State is required";
				}
				else if (!patientWithEditedDetails.city) {
					document.getElementById("cityError").innerHTML = "City is required";
				}
				else {
					$('#editPatient').modal('hide');
					document.getElementById("departmentError").innerHTML = "";
					document.getElementById("doctorError").innerHTML = "";
					document.getElementById("stateError").innerHTML = "";
					document.getElementById("cityError").innerHTML = "";
					document.getElementById("bloodGroupError").innerHTML = "";
					angular.forEach($scope.AllDepartments, function (dep) {
						if (dep.name == patientWithEditedDetails.department) {
							patientWithEditedDetails.department = dep;
						}
					});
					//Making JSON structure respective to subscription class
					var subscriber_patient = {};
					subscriber_patient.subscriptionId = subscriptionInfo.subscriptionId;
					subscriber_patient.patient = {};
					subscriber_patient.doctors = [];
					subscriber_patient.invitedBy = {};
					subscriber_patient.inviteDate = subscriptionInfo.joinDate;
					subscriber_patient.lastLogin = subscriptionInfo.lastLogin;
					subscriber_patient.patient = patientWithEditedDetails;
					subscriber_patient.doctors = patientWithEditedDetails.doctors;
					subscriber_patient.invitedBy = $scope.associatedCoordinator; // Setting Associated Co-ordinator for patient
					subscriber_patient.status = subscriptionInfo.status;
					console.log(subscriber_patient);
					$http({
						url: '/ihspdc/rest/addNewPatient',
						data: subscriber_patient,
						method: 'POST',
						headers: {
							'Content-Type': 'application/json'
						}
					}).then(function (response) {
						$("html, body").animate({ scrollTop: 0 }, 200);
						$scope.sucMsg = true;
						$scope.successMsg = "Patient has been successfully updated";
						jQuery("#successId").focus();
						jQuery("#successId").show();
						$scope.getPatientsList(0, 1);
						subscriber_patient = {};
						$scope.patientWithEditedDetails = {};
					}, function (error) {
						console.log("Error in updating Patient" + error);
					});
				}
			}

		}


	}]);
// Historical LabResult Entry Controller
App.controller('HistoricalLabResultCtrl',
	['$scope', '$http', '$state', '$stateParams', '$window', '$rootScope', '$timeout', 'patientService',
		function ($scope, $http, $state, $stateParams, $window, $rootScope, $timeout, patientService) {

			$scope.opCodeObtained = $stateParams.id; // to get patient reference to add historical entry

			// Patient profile
			var patInfo = '/ihspdc/rest/displayPatientDetails?patientId='
			patInfo += $scope.opCodeObtained;
			$http.get(patInfo).then(
				function (response, err) {
					if (!err) {
						console.log(response);
						if (response.data != null) {

							$scope.patBioInfo = response.data;
						}
					} else {
						console.log('Error');
					}
				});



			var doctorDetails = '/ihspdc/rest/getDoctorsList';
			$http.get(doctorDetails).then(
				function (response, err) {
					if (!err) {
						$scope.allDoctors = response.data;
						console.log($scope.allDoctors);
					} else {
						console.log('Error');
					}
				});


			allTests = '/ihspdc/rest/getalltests'
			$http.get(allTests).then(
				function (response, err) {
					if (!err) {
						$scope.fullTests = response.data;
						console.log($scope.fullTests);
						var groups = {};
						for (var i = 0; i < $scope.fullTests.length; i++) {
							var groupName = $scope.fullTests[i].group.groupName;
							if (!groups[groupName]) {
								groups[groupName] = [];
							}
							groups[groupName].push($scope.fullTests[i]);
						}
						$scope.myArray = [];
						for (var groupName in groups) {
							$scope.myArray.push({ group: groupName, labTests: groups[groupName] });
						}
						console.log($scope.myArray);
					} else {
						console.log('Error');
					}
				});

			//datepicker global func
			$(document).ready(function () {
				$scope.toSetGlobalHistDate = function (date) {
					$scope.hideAlert();
					angular.forEach($scope.myArray, function (e1, index) {
						for (var i = 0; i < e1.labTests.length; i++) {
							console.log(index, i);
							$scope.eachResultDate = date;
							document.getElementById("dateOfTest" + index + i).value = date;
						}
					});
					$scope.checkAllResults(date);
				}
			});




			var historicalValidated = false;
			$scope.historicalValidation = function (hisResultsForValidate) {
				var hasValue = false;
				var hasDate = true;
				angular.forEach(hisResultsForValidate, function (eachLabTest, index) {
					angular.forEach(eachLabTest.labTests, function (singleTest, i) {
						if (singleTest.testValHis) {
							hasValue = true;
							if (document.getElementById("dateOfTest" + index + i).value == "") {
								hasDate = false;
							}
						}
					});
				});

				angular.forEach(hisResultsForValidate, function (eachLabTest, index) {
					angular.forEach(eachLabTest.labTests, function (singleTest, i) {
						if (singleTest.testCode && document.getElementById("dateOfTest" + index + i).value) {
							console.log(singleTest.testCode, document.getElementById("dateOfTest" + index + i).value);
							$scope.validateEachResultDate(singleTest.testCode, document.getElementById("dateOfTest" + index + i).value);
						}
					});
					if (index == (hisResultsForValidate.length - 1)) {
						duplicateResultFlag = false;
					}
				});

				if (duplicateResultFlag) {
					$scope.throwErrorMsg(dupResults + " results are already availlable on this date");
				}
				else if (!hasValue) {
					$scope.errorHistMsg = true;
					$("html, body").animate({ scrollTop: 0 }, 200);
					$scope.histErrMsg = "Observed value is required";
					jQuery("#errorMessage-div-histests").focus();
					jQuery("#errorMessage-div-histests").show();
				}
				else if (hasDate == false) {
					$scope.errorHistMsg = true;
					$("html, body").animate({ scrollTop: 0 }, 200);
					$scope.histErrMsg = "Date is required";
					jQuery("#errorMessage-div-histests").focus();
					jQuery("#errorMessage-div-histests").show();
				}
				else if ($scope.finalUploadedFilesHlrLength <= 0) {
					$scope.errorHistMsg = true;
					$("html, body").animate({ scrollTop: 0 }, 200);
					$scope.histErrMsg = "Please upload one or more corresponding lab results";
					jQuery("#errorMessage-div-histests").focus();
					jQuery("#errorMessage-div-histests").show();
				}
				else {
					$scope.errorHistMsg = false;
					historicalValidated = true;
					$('#submitModal').modal('show');
				}
			}

			$scope.doc = {};
			$scope.saveHisLabTestResults = function (hisLabRes) {
				if (historicalValidated && duplicateResultFlag != true) {
					var TotalHisLabResults = [];
					angular.forEach(hisLabRes, function (eachLabTest, index) {
						angular.forEach(eachLabTest.labTests, function (singleTest, i) {
							if (singleTest.testValHis) {
								var PatientLabResHis = {};
								PatientLabResHis.labTest = {};
								PatientLabResHis.patient = {};
								PatientLabResHis.doctorReference = "";
								PatientLabResHis.files = [];
								PatientLabResHis.patient.department = {};
								if (typeof $scope.doc.docReference !== 'undefined') { //Doctor name is not required
									PatientLabResHis.doctorReference = $scope.doc.docReference.docName;
								}
								angular.forEach($scope.finalUploadedFilesHlr, function (insertPatientReference) {
									insertPatientReference.patient = {};
									insertPatientReference.patient = $scope.patBioInfo;
								});
								PatientLabResHis.files = $scope.finalUploadedFilesHlr;
								PatientLabResHis.labTest.testCode = singleTest.testCode;
								PatientLabResHis.labTest.testName = singleTest.testName;
								PatientLabResHis.labTest.gender = singleTest.gender;
								PatientLabResHis.labTest.unit = singleTest.unit;
								PatientLabResHis.labTest.group = {};
								PatientLabResHis.labTest.group = {};
								PatientLabResHis.labTest.group.id = singleTest.group.id;
								PatientLabResHis.labTest.group.groupName = singleTest.group.groupName;
								PatientLabResHis.labTest.group.groupRank = singleTest.group.groupRank;
								PatientLabResHis.testValue = singleTest.testValHis;
								PatientLabResHis.testDateTime = document.getElementById("dateOfTest" + index + i).value;
								PatientLabResHis.status = "approved"; // always approved - Because activity done by Coordinator itself.
								console.log($scope.opCodeObtained);
								PatientLabResHis.patient.opCode = $scope.opCodeObtained;
								PatientLabResHis.patient.department = $scope.patBioInfo.department;
								TotalHisLabResults.push(PatientLabResHis);
							}
						});
					});

					angular.forEach($scope.finalUploadedFilesHlr, function (file) {
						file.description = "";
						file.description = $scope.descriptionHlr;
					})
					//console.log($scope.finalUploadedFiles);
					console.log(TotalHisLabResults);
					$http({
						url: '/ihspdc/rest/savehistoricallabresult',
						data: TotalHisLabResults,
						method: 'POST',
						headers: {
							'Content-Type': 'application/json'
						}

					}).then(function (response) {
						$scope.resetOfHistoricalResults();
						$scope.cancelUpload();

						$scope.successHistMsg = true;
						$("html, body").animate({ scrollTop: 0 }, 200);
						$scope.histSucMsg = "Historical lab results has been saved successfully";
						jQuery("#successMessage-div-histests").focus();
						jQuery("#successMessage-div-histests").show();

					}, function (error) {
						console.log(error);
						console.log("error of Saving LabResults Historical ");
					});
					TotalHisLabResults = [];
					$scope.opCodeObtained = "";
					$rootScope.opforHist = "";
					$scope.uploadFlagHlr = false;
				}
			}

			//ALERT MESSAGES
			$scope.hideAlert = function () {
				$scope.successHistMsg = false;
				$scope.errorHistMsg = false;
			}

			$scope.throwErrorMsg = function (msg) {
				$("html, body").animate({ scrollTop: 0 }, 200);
				$scope.errorHistMsg = true;
				$scope.histErrMsg = msg;
				jQuery("#errorMessage-div-histests").focus();
				jQuery("#errorMessage-div-histests").show();
			}
			$scope.throwSuccessMsg = function (msg) {
				$("html, body").animate({ scrollTop: 0 }, 200);
				$scope.successHistMsg = true;
				$scope.histSucMsg = msg;
				jQuery("#successMessage-div-histests").focus();
				jQuery("#successMessage-div-histests").show();
			}

			function bytesToSize(bytes) {
				var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
				if (bytes == 0) return '0 Byte';
				var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
				return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
			};

			$scope.resetOfHistoricalResults = function () {
				$scope.loader = true;
				$scope.doc = {};
				$timeout(function () {
					$scope.loader = false;
					document.getElementById("HisForm").reset();
					$("#hisDate")[0].reset();
					document.getElementById("descForUploads").value = "";
				}, 1000);
			}

			//UPLOADFILES 
			$(function () {
				$scope.finalUploadedFilesHlr = [];
				var uploadedFiles = [];
				var newFile = {};
				var removeFlag = false;
				var myDropzoneHlr = new Dropzone("form#myIdHlr", { paramName: "inputfile", maxFilesize: 10, addRemoveLinks: true, parallelUploads: 15, maxFiles: 15, method: 'POST', url: "/ihspdc/rest/storefile", acceptedFiles: 'image/png,image/jpeg,image/jpg,application/pdf', dictDefaultMessage: 'Drop Specifications file' });

				$scope.finalUploadedFilesHlr = [];
				$scope.finalUploadedFilesHlrLength = 0;

				myDropzoneHlr.on("error", function (file, errorMessage, xhr) {
					console.log(errorMessage);
					$(file.previewElement).find('.dz-error-message').text(errorMessage);
				});

				myDropzoneHlr.on("removedfile", function (file) {
					if (removeFlag != true) {
						console.log("Trying to remove me?", file.xhr.response);
						rmFile = '/ihspdc/rest/removeFile?id='
						rmFile += file.xhr.response;
						$http.delete(rmFile);
					}
					removeFlag = false;
				});

				//try to reduce angular forEach if any time
				myDropzoneHlr.on("complete", function (file, xhr, formData) {
					console.log(myDropzoneHlr);
					console.log("dropzone produced up");
					uploadedFiles = [];
					newFile = {};
					if (uploadedFiles.length > 0) {
						angular.forEach(myDropzoneHlr.files, function (file) {
							angular.forEach(uploadedFiles, function (dupFile) {
								newFile = {};
								if (dupFile.id != file.xhr.response) {
									newFile.fileName = file.name;
									newFile.id = file.xhr.response;
									newFile.fileSize = bytesToSize(file.size);
									uploadedFiles.push(newFile);
								}
							});
						});
					}
					else if (uploadedFiles.length == 0) {
						angular.forEach(myDropzoneHlr.files, function (file) {
							newFile = {};
							newFile.fileName = file.name;
							newFile.id = file.xhr.response;
							newFile.fileSize = bytesToSize(file.size);
							uploadedFiles.push(newFile);
						});
					}
					console.log(uploadedFiles);
					$scope.finalUploadedFilesHlr = uploadedFiles;
					$scope.finalUploadedFilesHlrLength = $scope.finalUploadedFilesHlr.length;
				});


				$scope.uploadFilesFun = function () {
					$scope.toShowFinalFilesHlr = myDropzoneHlr.files;
					$scope.toShowFinalFilesHlrLength = $scope.toShowFinalFilesHlr.length;
					if ($scope.toShowFinalFilesHlrLength > 0) {
						$scope.hideAlert();
						$scope.uploadFlagHlr = true;
					}
					else {
						$scope.finalUploadedFilesHlrLength = 0;
						$scope.uploadFlagHlr = false;
					}
				}

				$scope.cancelUpload = function () {
					removeFlag = true;
					myDropzoneHlr.removeAllFiles();
					$scope.uploadFlagHlr = false;
					$scope.finalUploadedFilesHlr = [];
					$scope.finalUploadedFilesHlrLength = $scope.finalUploadedFilesHlr.length;
					console.log(myDropzoneHlr.files.length + "see length");
				}

			});
			//ENDOFUPLOADFILES

			//FLATPICKR CONFIG
			$scope.dateOpts1 = {
				dateFormat: 'd-m-Y',
				allowInput: true
			};

			//GETTING ALL LAB RESULTS
			var patientLabResults = [];
			patientService.getAllLabResults($stateParams.id, function (err, response) {
				if (!err) {
					patientLabResults = response.data;
				}
			});

			//RESTRICTING OF ADDING LAB RESULT ON SAME DATE
			var duplicateResultFlag = false;
			var dupResults = "";
			$scope.checkAllResults = function (globalDate) {
				dupResults = "";
				duplicateResultFlag = false;
				angular.forEach($scope.myArray, function (grp) {
					angular.forEach(grp.labTests, function (test) {
						angular.forEach(patientLabResults, function (result) {
							if (test.testCode == result.labTest.testCode && result.testDateTime == globalDate) {
								test.dup = 1;
								duplicateResultFlag = true;
								dupResults += test.testName + ", ";
							}
						});
					});
				});
				if (duplicateResultFlag) {
					$scope.throwErrorMsg(dupResults + " results are already availlable on this date");
				}
				else {
					angular.forEach($scope.myArray, function (grp) {
						angular.forEach(grp.labTests, function (test) {
							test.dup = 0;
						});
					});
					$scope.hideAlert();
				}
			}

			$scope.validateEachResultDate = function (testCode, date) {
				dupResults = "";
				var dupTestCode = "";
				angular.forEach($scope.myArray, function (grp) {
					angular.forEach(grp.labTests, function (test) {
						if (test.testCode == testCode) {
							angular.forEach(patientLabResults, function (result) {
								if (test.testCode == result.labTest.testCode && result.testDateTime == date) {
									test.dup = 1;
									dupTestCode = test.testCode;
									duplicateResultFlag = true;
									dupResults += test.testName + ", ";
								}
							});
						}
					});
				});
				if (duplicateResultFlag && dupTestCode == testCode) {
					$scope.throwErrorMsg(dupResults + " result are already availlable on this date");
				}
				else {
					angular.forEach($scope.myArray, function (grp) {
						angular.forEach(grp.labTests, function (test) {
							if (testCode != dupTestCode && test.testCode == testCode) {
								test.dup = 0;
							}
						});
					});
					$scope.hideAlert();
				}
			}

		}]);
App.controller('doctorOnBoardCtrl', [
	'$scope',
	'$http',
	'$state',
	'$stateParams',
	'$window', '$timeout', '$rootScope', 'otherServices',
	function ($scope, $http, $state, $stateParams, $window, $timeout, $rootScope, otherServices) {

		$.getJSON("cityStates.json", function (cityState) {
			$scope.country = cityState;
		});

		otherServices.getDoctorCount(function (err, response) {
			if (!err) {
				var page = 0;
				$scope.pagesNeeded = [];
				if (response % 10 == 0) {
					page = response / 10;
				}
				else if (response % 10 != 0 && response <= 10) {
					page = 1;
				}
				else {
					page = Math.floor(response / 10) + 1;
				}
				for (i = 0; i < page; i++) {
					$scope.pagesNeeded.push(i);
				}
			}
		});

		$scope.doctor = {};

		//CHECK DUPLICATE DOCTOR ID
		var allDocList = [];
		otherServices.getAllDocList(function (err, response) {
			if (!err) {
				allDocList = response;
			}
		});

		var canDocSave = true;
		$scope.doctorExist = function (newDoctor) {
			var seenDuplicate = true;
			canDocSave = true;
			otherServices.getAllDocList(function (err, response) {
				if (!err) {
					allDocList = response;
					if (seenDuplicate) {
						angular.forEach(allDocList, function (oldDoc) {
							if (oldDoc.docId == newDoctor) {
								document.getElementById("docErrId").innerHTML = "id already exist";
								seenDuplicate = false;
								canDocSave = false;
							}
							else {
								document.getElementById("docErrId").innerHTML = "";
								canDocSave = true;
							}
						});
					}
				}
			});
		}

		$scope.getDoctorsList = function (numberOfRecords) {
			var doctorDetails = '/ihspdc/rest/getalldoctors?getNext=';
			doctorDetails += numberOfRecords;
			$http.get(doctorDetails).then(
				function (response, err) {
					if (!err && response.data != "") {
						response.data.sort(function (a, b) {
							return a.docName.localeCompare(b.docName);
						});
						$scope.alldoctors = response.data;
					} else {
						console.log('Error');
					}
				});
		}
		$scope.getDoctorsList(0);


		//DOCTOR STATUS CHANGE PROCESS
		var doctorRecordToChangeStatus = {};
		$scope.changeDoctorStatus = function (doctorReference) {
			doctorRecordToChangeStatus = JSON.parse(JSON.stringify(doctorReference));
			//Just for showing purpose
			$scope.doctorMajorData = {};
			$scope.doctorMajorData.id = doctorRecordToChangeStatus.docId;
			$scope.doctorMajorData.name = doctorRecordToChangeStatus.docName;
			//Changimg status
			if (doctorRecordToChangeStatus.status == "Active") {
				doctorRecordToChangeStatus.status = "Inactive";
			}
			else if (doctorRecordToChangeStatus.status == "Inactive") {
				doctorRecordToChangeStatus.status = "Active";
			}
			doctorRecordToChangeStatus = doctorRecordToChangeStatus;
			$('#changeStatusModal').modal('show');
		}

		$scope.changeStatus = function () {
			$http({
				url: '/ihspdc/rest/addNewDoctor',
				data: doctorRecordToChangeStatus,
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				}
			}).then(function (response) {
				$("html, body").animate({ scrollTop: 0 }, 200);
				$scope.sucMsg = true;
				$scope.successMsg = "Status has been successfully changed";
				jQuery("#successId").focus();
				jQuery("#successId").show();
				$scope.getDoctorsList(0);
				doctorRecordToChangeStatus = {};
			}, function (error) {
				console.log("Error in updating Patient");
			});
		}


		//SEARCH DOCTOR BY FIELD
		$scope.searchDoctor = function (searchTerm, byField) {
			var found = false;
			if (searchTerm.length >= 4) {
				if (byField == "1") {
					angular.forEach(allDocList, function (cod, index) {
						if (cod.id.toUpperCase() == searchTerm.toUpperCase()) {
							$scope.alldoctors = [];
							$scope.alldoctors.push(cod);
							found = true;
						}
						else {
							if ((index + 1) == allDocList.length && found != true) {
								$scope.alldoctors = [];
							}
						}
					});
				}
				else if (byField == "2") {
					angular.forEach(allDocList, function (cod, index) {
						if (cod.mobileNo == searchTerm) {
							$scope.alldoctors = [];
							$scope.alldoctors.push(cod);
						}
						else {
							if ((index + 1) == allDocList.length && found != true) {
								$scope.alldoctors = [];
							}
						}
					});
				}
				else if (byField == "3") {
					angular.forEach(allDocList, function (cod, index) {
						if (cod.name.toUpperCase() == searchTerm.toUpperCase()) {
							$scope.alldoctors = [];
							$scope.alldoctors.push(cod);
						}
						else {
							if ((index + 1) == allDocList.length && found != true) {
								$scope.alldoctors = [];
							}
						}
					});
				}
			}
			else if (searchTerm.length == 0) {
				$scope.getDoctorsList(0);
			}
		}


		$scope.getDepartmentList = function () {
			var department_Details = '/ihspdc/rest/getDepartmentsList';
			$http.get(department_Details).then(
				function (response, err) {
					if (!err && response.data != "") {
						response.data.sort(function (a, b) {
							return a.name.localeCompare(b.name);
						});
						$scope.allDepartments = response.data;
					} else {
						console.log('Error');
					}
				});
		}
		$scope.getDepartmentList();

		jQuery.validator.addMethod("phoneIndia", function (phone_number, element) {
			phone_number = phone_number.replace(/\s+/g, "");
			return this.optional(element) || phone_number.length > 8 &&
				phone_number.match(/^[0-9]\d{9}$/);
		}, "Enter a valid phone number");
		$scope.validateDoctorDetails = {
			debug: false,
			errorElement: "div",
			errorPlacement: function (error, element) {
				if (element.attr("name") == "doctorFname" || element.attr("name") == "doctorMname" || element.attr("name") == "doctorLname" || element.attr("name") == "caddress1" || element.attr("name") == "caddress2" || element.attr("name") == "radios") {
					error.insertAfter(element.parent('.input-group'));
				} else {
					error.insertAfter(element);
				}
			},
			rules: {
				empID: {
					required: true
				},
				doctorFname: {
					required: true
				},
				department: {
					required: true
				},
				email: {
					required: true,
					email: true
				},
				doctorDOB: {
					required: true
				},
				radios: {
					required: true
				},
				regno: {
					required: true
				},
				qualification: {
					required: true
				},
				mobile: {
					required: true,
					phoneIndia: true
				},
				caddress1: {
					required: true
				},
				cstate: {
					required: true
				},
				ccity: {
					required: true
				},
				czip: {
					required: true,
					minlength: 6,
					maxlength: 6
				},
				appType: {
					required: true
				}
			},
			messages: {
				empID: {
					required: "Employee ID is required"
				},
				doctorFname: {
					required: "Doctor Name is required"
				},
				department: {
					required: "Department is required"
				},
				mobile: {
					required: "Mobile number is required",
				},
				email: {
					required: "Email id is required",
				},
				doctorDOB: {
					required: "DOB is required"
				},
				radios: {
					required: "Gender is required"
				},
				regno: {
					required: "Registarion number is required"
				},
				qualification: {
					required: "Qualification is required"
				},
				caddress1: {
					required: "Address Line 1 is required"
				},
				cstate: {
					required: "State is required"
				},
				ccity: {
					required: "City is required"
				},
				czip: {
					required: "Pin code is required",
					minlength: "6 Digit is required",
					maxlength: "6 Digit is required"
				},
				appType: {
					required: "Appointment type is required"
				}
			},
			highlight: function (element, errorClass) {
				$(element).removeClass(errorClass);
			}
		}

		$scope.hideAlert = function () {
			$scope.sucMsg = false;
			$scope.successMsg = "";
		}

		$scope.cancelOfSavingDoctor = function () {
			$scope.doctor = {};
		}

		$scope.registerDoctor = function (doctorFormDetails, docInfo) { 	//think what if co-ord select other dept as doctor dept 
			if (!doctorFormDetails.validate()) {
				// event.preventDefault();
				// event.stopPropagation();
			}
			else if (canDocSave) {
				if (!docInfo.department) {
					document.getElementById("departmentError").innerHTML = "Department is required";
				}
				else if (!docInfo.state) {
					document.getElementById("stateError").innerHTML = "State is required";
				}
				else if (!docInfo.city) {
					document.getElementById("cityError").innerHTML = "City is required";
				}
				else {
					angular.forEach($scope.allDepartments, function (dep) {
						if (dep.name == docInfo.department) {
							docInfo.department = dep;
						}
					});
					docInfo.status = "Active";
					$('#addDoctor').modal('hide');
					$http({
						url: '/ihspdc/rest/addNewDoctor',
						data: docInfo,
						method: 'POST',
						headers: {
							'Content-Type': 'application/json'
						}
					}).then(function (response) {
						$("html, body").animate({ scrollTop: 0 }, 200);
						$scope.sucMsg = true;
						$scope.successMsg = "Doctor has been successfully added";
						jQuery("#successId").focus();
						jQuery("#successId").show();
						$scope.getDoctorsList(0);
						$scope.doctor = {};
					}, function (error) {
						console.log("Error in Adding Doctor");
					});
				}
			}
		}

		//Doctor Editing Processes
		$scope.openEditDoctor = function (doctorDetails) {
			$scope.editDoctor = {};
			$scope.editDoctor.department = {};
			$scope.editDoctor.docId = doctorDetails.docId;
			$scope.editDoctor.department = doctorDetails.department.name;
			$scope.editDoctor.docName = doctorDetails.docName;
			$scope.editDoctor.docMiddleName = doctorDetails.docMiddleName;
			$scope.editDoctor.docLastName = doctorDetails.docLastName;
			$scope.editDoctor.dateOfBirth = doctorDetails.dateOfBirth;
			$scope.editDoctor.gender = doctorDetails.gender;
			$scope.editDoctor.registrationNumber = doctorDetails.registrationNumber;
			$scope.editDoctor.qualification = doctorDetails.qualification;
			$scope.editDoctor.mobileNo = doctorDetails.mobileNo;
			$scope.editDoctor.addressLine1 = doctorDetails.addressLine1;
			$scope.editDoctor.addressLine2 = doctorDetails.addressLine2;
			$scope.editDoctor.state = doctorDetails.state;
			$scope.editDoctor.city = doctorDetails.city;
			$scope.editDoctor.email = doctorDetails.email;
			$scope.editDoctor.pincode = doctorDetails.pincode;
			$scope.editDoctor.appointmentType = doctorDetails.appointmentType;
			$scope.editDoctor.status = doctorDetails.status;
			$scope.editDoctor.fee = doctorDetails.fee;
			$('#editDoctor').modal('show');
		}
		$scope.updateDoctor = function (editDoctorForm, doctorWithEditedDetails) {
			if (!editDoctorForm.validate()) {
				// event.preventDefault();
				// event.stopPropagation();
			}
			else {
				if (!doctorWithEditedDetails.department) {
					document.getElementById("departmentError").innerHTML = "Department is required";
				}
				else if (!doctorWithEditedDetails.state) {
					document.getElementById("stateError").innerHTML = "State is required";
				}
				else if (!doctorWithEditedDetails.city) {
					document.getElementById("cityError").innerHTML = "City is required";
				}
				else {
					angular.forEach($scope.allDepartments, function (dep) {
						if (dep.name == doctorWithEditedDetails.department) {
							doctorWithEditedDetails.department = dep;
						}
					});
					$('#editDoctor').modal('hide');
					$http({
						url: '/ihspdc/rest/addNewDoctor',
						data: doctorWithEditedDetails,
						method: 'POST',
						headers: {
							'Content-Type': 'application/json'
						}
					}).then(function (response) {
						$("html, body").animate({ scrollTop: 0 }, 200);
						$scope.sucMsg = true;
						$scope.successMsg = "Doctor has been successfully updated";
						jQuery("#successId").focus();
						jQuery("#successId").show();
						$scope.getDoctorsList(0);
						$scope.editDoctor = {};
					}, function (error) {
						console.log("Error in updating Doctor");
					});
				}
			}
		}

		// APPOINTMENT BOOKING
		$scope.nonAppDates = {
			mode: "multiple",
			dateFormat: "d-m-Y",
			conjunction: " :: ",
			minDate: new Date()
		}
		$scope.appTypes = ["Session", "Slot"];
		$scope.appDateOpts = {
			noCalendar: true,
			enableTime: true,
			minuteIncrement: 15,
			time_24hr: true,
			allowInput: false
		}

		$scope.slotIntervalList = [5, 10, 15, 20, 30, 60];

		$scope.addNewSession = function () {
			$scope.docInfo.appSession.push({});
		}
		$scope.addNewSlot = function () {
			$scope.docInfo.appSession.push({});
		}

		$scope.sessionList = ["Morning", "Afternoon", "Evening", "Night"];
		$scope.checkAvaillSession = function (appSession) {
			angular.forEach(appSession, function (app) {
				$scope.sessionList.splice($scope.sessionList.indexOf(app.sessionName), 1);
			});
			console.log($scope.sessionList);
		}

		$scope.removeFromSessionList = function (sessionNamePicked) {
			$scope.sessionList.splice($scope.sessionList.indexOf(sessionNamePicked), 1);
		}

		$scope.clearAppointmentData = function () {
			$scope.docInfo = {};
			$scope.editOpen = true;
		}

		$scope.openDoctorApointmentSettings = function (doc) {
			$scope.docInfo = JSON.parse(JSON.stringify(doc));
			$scope.noOfAppSetting = $scope.docInfo.appSession.length;
			$scope.checkAvaillSession($scope.docInfo.appSession);
			// $scope.nonAppDates.maxDate = blockInput.setMonth(blockInput.getMonth()+$scope.docInfo.appOpenMonth);
			//DISABLING ALL INPUT FIELDS
			$scope.editOpen = true;
			$('#appointmentSettings').modal('show');
		}

		var canSave = true;
		$scope.registerAppointment = function (appSettingsWithDocInfo) {
			if (appSettingsWithDocInfo.appointmentType == "Session") {
				var count = 0;
				angular.forEach(appSettingsWithDocInfo.appSession, function (app, index) {
					app.sessionType = appSettingsWithDocInfo.appointmentType;
					app.slotInterval = 0;
					count++;
					if (!app.sessionName) {
						document.getElementById("sessionPeriodError" + index).innerHTML = "Session period is required";
						canSave = false;
					}
					else if (!app.fromTime && !app.toTime) {
						document.getElementById("sessionTimeError" + index).innerHTML = "Time range is required";
						canSave = false;
					}
					else if (!app.noOfAppointments) {
						document.getElementById("appointmentCountError" + index).innerHTML = "Number of appointments is required";
						canSave = false;
					}
					else {
						document.getElementById("sessionPeriodError" + index).innerHTML = "";
						document.getElementById("sessionTimeError" + index).innerHTML = "";
						document.getElementById("appointmentCountError" + index).innerHTML = "";
						if (count == appSettingsWithDocInfo.appSession.length && canSave) {
							canSave = true;
						}
					}
				});
			} else if (appSettingsWithDocInfo.appointmentType == "Slot") {
				var count = 0;
				angular.forEach(appSettingsWithDocInfo.appSession, function (app, index) {
					app.sessionType = appSettingsWithDocInfo.appointmentType;
					app.noOfAppointments = 0;
					count++;
					if (!app.sessionName) {
						document.getElementById("slotSessionPeriodError" + index).innerHTML = "Session period is required";
						canSave = false;
					}
					else if (!app.fromTime && !app.toTime) {
						document.getElementById("slotSessionTimeError" + index).innerHTML = "Time range is required";
						canSave = false;
					}
					else if (!app.slotInterval) {
						document.getElementById("slotIntervalError" + index).innerHTML = "Slot interval is required";
						canSave = false;
					}
					else {
						document.getElementById("slotSessionPeriodError" + index).innerHTML = "";
						document.getElementById("slotSessionTimeError" + index).innerHTML = "";
						document.getElementById("slotIntervalError" + index).innerHTML = "";
						if (count == appSettingsWithDocInfo.appSession.length && canSave) {
							canSave = true;
						}
					}
				});
			}
			if (canSave) {
				$http({
					url: '/ihspdc/rest/addNewDoctor',
					data: appSettingsWithDocInfo,
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					}
				}).then(function (response) {
					$('#appointmentSettings').modal('hide');
					$("html, body").animate({ scrollTop: 0 }, 200);
					$scope.sucMsg = true;
					$scope.successMsg = appSettingsWithDocInfo.appointmentType + " has been successfully saved";
					jQuery("#successId").focus();
					jQuery("#successId").show();
					$scope.getDoctorsList(0);
					$scope.docInfo = {};
				}, function (error) {
					console.log("Error in updating Doctor");
				});
			}
		}

		//REMOVE SESSION		
		$scope.remove = function (sessionName, index) {
			$scope.sessionList.push(sessionName);
			$scope.docInfo.appSession.splice(index, 1);
		};

		//APPOINTMENT TIME VALIDATION
		$scope.isToExist = function (toTime, fromTimeSelected, index) {
			if (toTime) {
				validateTimeRange(fromTimeSelected, toTime, index);
			}
		}
		$scope.isFromExist = function (fromTime, toTimeSelected, index) {
			if (fromTime) {
				validateTimeRange(fromTime, toTimeSelected, index);
			}
		}

		function validateTimeRange(from, to, index) {
			from = moment(from, "HH:mm");
			to = moment(to, "HH:mm");
			var id = "";
			if (to.isBefore(from) || to.isSame(from)) {
				if ($scope.docInfo.appointmentType == "Session") {
					id = "sessionTimeError" + index;
					document.getElementById(id).innerHTML = "Please select the correct time range";
					canSave = false;
				}
				else {
					id = "slotSessionTimeError" + index;
					document.getElementById(id).innerHTML = "Please select the correct time range";
					canSave = false;
				}
			}
			else {
				if ($scope.docInfo.appointmentType == "Session") {
					id = "sessionTimeError" + index;
					document.getElementById(id).innerHTML = "";
				}
				else {
					id = "slotSessionTimeError" + index;
					document.getElementById(id).innerHTML = "";
				}
				canSave = true;
			}
		}

		// EDIT APPOINTMENT
		$scope.editAppointment = function () {
			$scope.editOpen = false;
		}


	}]);
App.controller('coordinatorMaster', [
	'$scope',
	'$http',
	'$state',
	'$stateParams',
	'$window', '$timeout', '$rootScope', 'otherServices',
	function ($scope, $http, $state, $stateParams, $window, $timeout, $rootScope, otherServices) {

		$.getJSON("cityStates.json", function (cityState) {
			$scope.country = cityState;
		});

		otherServices.getCoordinatorCount(function (err, response) {
			if (!err) {
				var page = 0;
				$scope.pagesNeeded = [];
				if (response % 10 == 0) {
					page = response / 10;
				}
				else if (response % 10 != 0 && response <= 10) {
					page = 1;
				}
				else {
					page = Math.floor(response / 10) + 1;
				}
				for (i = 0; i < page; i++) {
					$scope.pagesNeeded.push(i);
				}
			}
		});

		$scope.allcoordsList = [];
		otherServices.getAllCoordsList(function (err, response) {
			if (!err) {
				$scope.allcoordsList = response;
			}
		});
		//CHECK DUPLICATE COORDINATOR ID
		var canSave = true;
		$scope.coordinatorExist = function (newCod) {
			var seenDuplicate = true;
			canSave = true;
			otherServices.getAllCoordsList(function (err, response) {
				if (!err) {
					$scope.allcoordsList = response;
					angular.forEach($scope.allcoordsList, function (oldCod) {
						if (seenDuplicate) {
							if (oldCod.id == newCod) {
								document.getElementById("codErrId").innerHTML = "Id already exist";
								seenDuplicate = false;
								canSave = false;
							}
							else {
								document.getElementById("codErrId").innerHTML = "";
								canSave = true;
							}
						}
					});
				}
			});
		}

		//SEARCH CO-ORDINATOR BY FIELD
		$scope.searchCoordinator = function (searchTerm, byField) {
			var found = false;
			if (searchTerm.length >= 4) {
				if (byField == "1") {
					angular.forEach($scope.allcoordsList, function (cod, index) {
						if (cod.id.toUpperCase() == searchTerm.toUpperCase()) {
							$scope.coordDet = [];
							$scope.coordDet.push(cod);
							found = true;
						}
						else {
							if ((index + 1) == $scope.allcoordsList.length && found != true) {
								$scope.coordDet = [];
							}
						}
					});
				}
				else if (byField == "2") {
					angular.forEach($scope.allcoordsList, function (cod, index) {
						if (cod.mobileNo == searchTerm) {
							$scope.coordDet = [];
							$scope.coordDet.push(cod);
						}
						else {
							if ((index + 1) == $scope.allcoordsList.length && found != true) {
								$scope.coordDet = [];
							}
						}
					});
				}
				else if (byField == "3") {
					angular.forEach($scope.allcoordsList, function (cod, index) {
						if (cod.name.toUpperCase() == searchTerm.toUpperCase()) {
							$scope.coordDet = [];
							$scope.coordDet.push(cod);
						}
						else {
							if ((index + 1) == $scope.allcoordsList.length && found != true) {
								$scope.coordDet = [];
							}
						}
					});
				}
			}
			else if (searchTerm.length == 0) {
				$scope.getCoordinatorList(0);
			}
		}

		$scope.getCoordinatorList = function (numberOfRecords) {
			var coord_Details = '/ihspdc/rest/getallcoords?getNext=';
			coord_Details += numberOfRecords;
			$http.get(coord_Details).then(
				function (response, err) {
					if (!err && response.data != "") {
						response.data.sort(function (a, b) {
							return a.name.localeCompare(b.name);
						});
						$scope.coordDet = response.data;
					} else {
						console.log('Error');
					}
				});
		}
		$scope.getCoordinatorList(0);

		$scope.coordinator = {};
		$scope.getDepartmentList = function () {
			var department_Details = '/ihspdc/rest/getDepartmentsList';
			$http.get(department_Details).then(
				function (response, err) {
					if (!err && response.data != "") {
						response.data.sort(function (a, b) {
							return a.name.localeCompare(b.name);
						});
						$scope.allDepartments = response.data;
					} else {
						console.log('Error');
					}
				});
		}
		$scope.getDepartmentList();

		//COORDINATOR STATUS CHANGE PROCESS
		var coordRecordToChangeStatus = {};
		$scope.changeCoordStatus = function (coordReference) {
			coordRecordToChangeStatus = JSON.parse(JSON.stringify(coordReference));
			//Just for showing purpose
			$scope.coordMajorData = {};
			$scope.coordMajorData.id = coordRecordToChangeStatus.id;
			$scope.coordMajorData.name = coordRecordToChangeStatus.name;
			//Changimg status
			if (coordRecordToChangeStatus.status == "Active") {
				coordRecordToChangeStatus.status = "Inactive";
			}
			else if (coordRecordToChangeStatus.status == "Inactive") {
				coordRecordToChangeStatus.status = "Active";
			}
			coordRecordToChangeStatus = coordRecordToChangeStatus;
			$('#changeStatusModal').modal('show');
		}

		$scope.changeStatus = function () {
			$http({
				url: '/ihspdc/rest/addNewCoordinator',
				data: coordRecordToChangeStatus,
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				}
			}).then(function (response) {
				$("html, body").animate({ scrollTop: 0 }, 200);
				$scope.sucMsg = true;
				$scope.successMsg = "Status has been successfully changed";
				jQuery("#successId").focus();
				jQuery("#successId").show();
				$scope.getCoordinatorList(0);
				coordRecordToChangeStatus = {};
			}, function (error) {
				console.log("Error in updating Patient");
			});
		}


		jQuery.validator.addMethod("phoneIndia", function (phone_number, element) {
			phone_number = phone_number.replace(/\s+/g, "");
			return this.optional(element) || phone_number.length > 9 &&
				phone_number.match(/^[0-9]\d{9}$/);
		}, "Enter a valid phone number");
		$scope.validateCoordinatorDetails = {
			debug: false,
			errorElement: "div",
			errorPlacement: function (error, element) {
				if (element.attr("name") == "CoordinatorFname" || element.attr("name") == "CoordinatorFname" || element.attr("name") == "CoordinatorFname" || element.attr("name") == "caddress1" || element.attr("name") == "caddress2" || element.attr("name") == "radios") {
					error.insertAfter(element.parent('.input-group'));
				} else {
					error.insertAfter(element);
				}
			},
			rules: {
				empID: {
					required: true
				},
				doctorFname: {
					required: true
				},
				department: {
					required: true
				},
				email: {
					required: true,
					email: true
				},
				doctorDOB: {
					required: true
				},
				radios: {
					required: true
				},
				regno: {
					required: true
				},
				qualification: {
					required: true
				},
				mobile: {
					required: true,
					phoneIndia: true
				},
				caddress1: {
					required: true
				},
				czip: {
					required: true,
					minlength: 6,
					maxlength: 6
				},
				cstate: {
					required: true
				},
				ccity: {
					required: true
				}
			},
			messages: {
				empID: {
					required: "Employee ID is required"
				},
				doctorFname: {
					required: "Doctor Name is required"
				},
				department: {
					required: "Department is required"
				},
				mobile: {
					required: "Mobile no is required",
				},
				email: {
					required: "Email id is required",
				},
				doctorDOB: {
					required: "DOB is required"
				},
				radios: {
					required: "Gender is required"
				},
				regno: {
					required: "Registarion number is required"
				},
				qualification: {
					required: "Qualification is required"
				},
				caddress1: {
					required: "Address Line 1 is required"
				},
				cstate: {
					required: "State is required"
				},
				ccity: {
					required: "City is required"
				},
				czip: {
					required: "Pin code is required",
					minlength: "6 Digit is required",
					maxlength: "6 Digit is required"
				}
			},
			highlight: function (element, errorClass) {
				$(element).removeClass(errorClass);
			}
		}

		$scope.hideAlert = function () {
			$scope.sucMsg = false;
			$scope.successMsg = "";
		}

		$scope.cancelOfSavingCoordinator = function () {
			$scope.coordinator = {};
		}

		$scope.registerCoordinator = function (CoordinatorForm, coordinator) { 	//think what if co-ord select other dept as doctor dept 
			if (!CoordinatorForm.validate()) {
				// event.preventDefault();
				// event.stopPropagation();
			}
			else if (canSave) {
				if (!coordinator.department) {
					document.getElementById("departmentError").innerHTML = "Department is required";
				}
				else if (!coordinator.state) {
					document.getElementById("stateError").innerHTML = "State is required";
				}
				else if (!coordinator.city) {
					document.getElementById("cityError").innerHTML = "City is required";
				}
				else {
					angular.forEach($scope.allDepartments, function (dep) {
						if (dep.name == coordinator.department) {
							coordinator.department = dep;
						}
					});
					coordinator.status = "Active";
					$('#addCoordinator').modal('hide');
					$http({
						url: '/ihspdc/rest/addNewCoordinator',
						data: coordinator,
						method: 'POST',
						headers: {
							'Content-Type': 'application/json'
						}
					}).then(function (response) {
						$("html, body").animate({ scrollTop: 0 }, 200);
						$scope.sucMsg = true;
						$scope.successMsg = "Coordinator has been successfully added";
						jQuery("#successId").focus();
						jQuery("#successId").show();
						$scope.getCoordinatorList(0);
						$scope.coordinator = {};
					}, function (error) {
						console.log("Error in Adding Coordinator");
					});
				}
			}
		}

		//Coordinator Editing Processes
		$scope.openEditCoordinator = function (coordinatorDetails) {
			$scope.editCoordinator = {};
			$scope.editCoordinator.department = {};
			$scope.editCoordinator.id = coordinatorDetails.id;
			$scope.editCoordinator.department = coordinatorDetails.department.name;
			$scope.editCoordinator.name = coordinatorDetails.name;
			$scope.editCoordinator.codMiddleName = coordinatorDetails.codMiddleName;
			$scope.editCoordinator.codLastName = coordinatorDetails.codLastName;
			$scope.editCoordinator.dateOfBirth = coordinatorDetails.dateOfBirth;
			$scope.editCoordinator.gender = coordinatorDetails.gender;
			$scope.editCoordinator.qualification = coordinatorDetails.qualification;
			$scope.editCoordinator.mobileNo = coordinatorDetails.mobileNo;
			$scope.editCoordinator.addressLine1 = coordinatorDetails.addressLine1;
			$scope.editCoordinator.addressLine2 = coordinatorDetails.addressLine2;
			$scope.editCoordinator.state = coordinatorDetails.state;
			$scope.editCoordinator.city = coordinatorDetails.city;
			$scope.editCoordinator.email = coordinatorDetails.email;
			$scope.editCoordinator.pincode = coordinatorDetails.pincode;
			$('#editCoordinator').modal('show');
		}

		$scope.updateCoordinator = function (editCoordinatorForm, coordinatorWithEditedDetails) {
			if (!editCoordinatorForm.validate()) {
				// event.preventDefault();
				// event.stopPropagation();
			}
			else if (canSave) {
				if (!coordinatorWithEditedDetails.department) {
					document.getElementById("departmentError").innerHTML = "Department is required";
				}
				else if (!coordinatorWithEditedDetails.state) {
					document.getElementById("stateError").innerHTML = "State is required";
				}
				else if (!coordinatorWithEditedDetails.city) {
					document.getElementById("cityError").innerHTML = "City is required";
				}
				else {
					angular.forEach($scope.allDepartments, function (dep) {
						if (dep.name == coordinatorWithEditedDetails.department) {
							coordinatorWithEditedDetails.department = dep;
						}
					});
					$('#editCoordinator').modal('hide');
					$http({
						url: '/ihspdc/rest/addNewCoordinator',
						data: coordinatorWithEditedDetails,
						method: 'POST',
						headers: {
							'Content-Type': 'application/json'
						}
					}).then(function (response) {
						$("html, body").animate({ scrollTop: 0 }, 200);
						$scope.sucMsg = true;
						$scope.successMsg = "Coordinator has been successfully updated";
						jQuery("#successId").focus();
						jQuery("#successId").show();
						$scope.getCoordinatorList(0);
						$scope.editCoordinator = {};
					}, function (error) {
						console.log("Error in updating Coordinator");
					});
				}
			}

		}
	}]);
App.controller('viewAllNotificationCtrl', [
	'$scope',
	'$http',

	'$state',
	'$stateParams',
	'$window', '$timeout', '$rootScope',
	function ($scope, $http, $state,
		$stateParams, $window, $timeout, $rootScope) {
		console.log("viewAllNotificationCtrl");
		var notify = '/ihspdc/rest/getnotification?logInId='; //logInId - userName which is used to LogIn
		notify += $rootScope.Auth.tokenParsed.preferred_username;
		$http.get(notify).then(function (response, err) {
			if (!err) {
				$scope.noti_length = 0;
				$scope.notifications = response.data; // Need to write rest call to fetch all notification - currently only top 10 fetching
			}
		});
	}]);
App.controller('departmentMaster', [
	'$scope',
	'$http',
	'$state',
	'$stateParams',
	'$rootScope', 'otherServices',
	function ($scope, $http, $state, $stateParams, $rootScope, otherServices) {

		$scope.department = {};

		otherServices.getDepartmentCount(function (err, response) {
			if (!err) {
				var page = 0;
				$scope.pagesNeeded = [];
				if (response % 10 == 0) {
					page = response / 10;
				}
				else if (response % 10 != 0 && response <= 10) {
					page = 1;
				}
				else {
					page = Math.floor(response / 10) + 1;
				}
				for (i = 0; i < page; i++) {
					$scope.pagesNeeded.push(i);
				}
			}
		});


		$scope.getDepartmentList = function (numberOfRecords) {
			var department_Details = '/ihspdc/rest/getalldepartments?getNext=';
			department_Details += numberOfRecords;
			$http.get(department_Details).then(
				function (response, err) {
					if (!err && response.data != "") {
						response.data.sort(function (a, b) {
							return a.name.localeCompare(b.name);
						});
						$scope.allDepartments = response.data;
					} else {
						console.log('Error');
					}
				});
		}
		$scope.getDepartmentList(0);

		//CHECL DEPARTMENT EXIST ALREADY
		var canSaveId = true;
		var canSaveName = true;
		$scope.departmentIdExist = function (newDept) {
			seenDuplicate = true;
			canSaveId = true;
			angular.forEach($scope.allDepartments, function (dep) {
				if (seenDuplicate) {
					if (dep.id == newDept.id) {
						document.getElementById("depErrId").innerHTML = "Department id already exist";
						seenDuplicate = false;
						canSaveId = false;
					}
					else {
						document.getElementById("depErrId").innerHTML = "";
						canSaveId = true;
					}
				}
			});
		}
		$scope.departmentNameExist = function (newDept) {
			seenDuplicate = true;
			canSaveName = true;
			angular.forEach($scope.allDepartments, function (dep) {
				if (seenDuplicate) {
					if (dep.name == newDept.name) {
						document.getElementById("depErrName").innerHTML = "Department name already exist";
						seenDuplicate = false;
						canSaveName = false;
					}
					else {
						document.getElementById("depErrName").innerHTML = "";
						canSaveName = true;
					}
				}
			});
		}


		jQuery.validator.addMethod("phoneIndia", function (phone_number, element) {
			phone_number = phone_number.replace(/\s+/g, "");
			return this.optional(element) || phone_number.length > 9 &&
				phone_number.match(/^[0-9]\d{9}$/);
		}, "Enter a valid phone number");
		$scope.validateDepartment = {
			debug: false,
			errorElement: "div",
			rules: {
				depID: {
					required: true
				},
				depName: {
					required: true
				},
				mobile: {
					required: true,
					phoneIndia: true
				},
				email: {
					required: true,
					email: true
				}
			},
			messages: {
				depID: {
					required: "Department ID is required",
				},
				depName: {
					required: "Department name is required"
				},
				mobile: {
					required: "Mobile number is required",
				},
				email: {
					required: "Email id is required"
				}
			},
			highlight: function (element, errorClass) {
				$(element).removeClass(errorClass);
			}
		}

		$scope.hideAlert = function () {
			$scope.sucMsg = false;
			$scope.successMsg = "";
		}

		$scope.cancelOfSavingDepartment = function () {
			$scope.department = {};
		}

		$scope.registerDepartment = function (newDepartmentForm, depDetails) {
			if (!newDepartmentForm.validate()) {
				// event.preventDefault();
				// event.stopPropagation();
			}
			else if (canSaveId && canSaveName) {
				$('#addDepartment').modal('hide');
				$http({
					url: '/ihspdc/rest/addNewDepartment',
					data: depDetails,
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					}
				}).then(function (response) {
					$("html, body").animate({ scrollTop: 0 }, 200);
					$scope.sucMsg = true;
					$scope.successMsg = "Department has been successfully saved";
					jQuery("#successId").focus();
					jQuery("#successId").show();
					$scope.getDepartmentList(0);
					$scope.department = {};
				}, function (error) {
					console.log("Error in Adding Department");
				});
			}
		}

		//Department Delete Process
		var departmentToDelete = {};
		$scope.deleteDepartment = function (dep) {
			departmentToDelete = JSON.parse(JSON.stringify(dep));
			//Just for showing purpose
			$scope.depMajorData = {};
			$scope.depMajorData.id = departmentToDelete.id;
			$scope.depMajorData.name = departmentToDelete.name;
			$('#changeStatusModal').modal('show');
		}
		$scope.deletedDep = function () {
			$http({
				url: '/ihspdc/rest/deleteDepartment',
				data: departmentToDelete,
				method: 'DELETE',
				headers: {
					'Content-Type': 'application/json'
				}
			}).then(function () { }, function () {
				$("html, body").animate({ scrollTop: 0 }, 200);
				$scope.sucMsg = true;
				$scope.successMsg = "Department has been successfully deleted";
				jQuery("#successId").focus();
				jQuery("#successId").show();
				$scope.getDepartmentList(0);
				departmentToDelete = {};
			});
		}
		//Department Editing Processes
		$scope.openEditDepartment = function (departmentDetails) {
			$scope.editDepartment = {};
			$scope.editDepartment.id = departmentDetails.id;
			$scope.editDepartment.name = departmentDetails.name;
			$scope.editDepartment.contact = departmentDetails.contact;
			$scope.editDepartment.email = departmentDetails.email;
			$('#editDepartment').modal('show');
		}
		$scope.UpdateDepartment = function (editDepartmentForm, departmentWithEditedDetails) {
			if (!editDepartmentForm.validate()) {
				// event.preventDefault();
				// event.stopPropagation();
			}
			else if (canSaveId && canSaveName) {
				$('#editDepartment').modal('hide');
				$http({
					url: '/ihspdc/rest/addNewDepartment',
					data: departmentWithEditedDetails,
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					}
				}).then(function (response) {
					$("html, body").animate({ scrollTop: 0 }, 200);
					$scope.sucMsg = true;
					$scope.successMsg = "Department has been successfully updated";
					jQuery("#successId").focus();
					jQuery("#successId").show();
					$scope.getDepartmentList(0);
					$scope.department = {};
				}, function (error) {
					console.log("Error in Adding Department");
				});
			}

		}
	}]);
//Lab Crud - Add Test grp & Tests
App.controller('labCrud_GroupsCtrl', ['$scope', '$http', '$window', '$timeout',
	function ($scope, $http, $window, $timeout) {

		$scope.closetestgrpmodal = function () {
			$('#testGroupModal').modal('hide');
			$scope.labTestGrp = {};
			$scope.groupCodeExistError = "";
			$scope.groupNameExistError = "";
		};

		$scope.show = function () {
			$('#testGroupModal').modal('show');
		};

		$scope.validateTestGroupDetails = {
			debug: false,
			errorElement: "div",
			rules: {
				GroupCode: {
					required: true
				},
				GroupName: {
					required: true
				}
			},
			messages: {
				GroupCode: {
					required: "Group code is required"
				},
				GroupName: {
					required: "Group name is required"
				}
			},
			highlight: function (element, errorClass) {
				$(element).removeClass(errorClass);
			}
		}


		$scope.labTestGrp = {};
		//FUNCTION CALL FOR SAVE LAB TEST GROUP
		$scope.saveTestgrp = function (groupForm, groupDetails) {
			if (!groupForm.validate()) {
				// event.preventDefault();
				// event.stopPropagation();
			}
			else {
				if (codeExist != true && nameExist != true) {
					$http({
						url: '/ihspdc/rest/saveTestGrp',
						data: groupDetails,
						method: 'POST',
						headers: {
							'Content-Type': 'application/json'
						}
					}).then(function () {
						$scope.labTestGrp = {};
						$('#testGroupModal').modal('hide');
						$("html, body").animate({ scrollTop: 0 }, 200);
						$scope.sucMsg = true;
						$scope.successMsg = "Lab group has been successfully added";
						jQuery("#successId").focus();
						jQuery("#successId").show();
						$scope.getAllLabGroups();
					}, function (error) {
						console.log("Error in adding Groups");
					});
				}
			}
		}//END OF SAVING LAB TEST GROUP

		//dynamically checks with existing group id and names - throws error if matches
		$scope.getAllLabGroups = function () {
			var toGetAllGroups = '/ihspdc/rest/getallgroups'
			$http.get(toGetAllGroups).then(function (response, err) {
				if (!err && response.data != "") {
					response.data.sort(function (a, b) {
						return a.groupName.localeCompare(b.groupName);
					});
					$scope.allLabTestGroups = response.data;
				}
			});
		}
		$scope.getAllLabGroups();

		var codeExist = false;
		$scope.dynamic_change = function (grpIdEntered) {
			$scope.groupCodeExistError = "";
			var j;
			for (j = 0; j < $scope.allLabTestGroups.length; j++) {
				if ($scope.allLabTestGroups[j].id == grpIdEntered) {
					$scope.groupCodeExistError = "Group ID already exist";
					codeExist = true;
				}
			}
			if ($scope.groupCodeExistError == "") {
				codeExist = false;
			}
		}

		//dynamically checks with existing group  and give error if it matches
		var nameExist = false;
		$scope.dynamic_change1 = function (grpNameEntered) {
			$scope.groupNameExistError = "";
			var j;
			var flag1 = 1;
			console.log($scope.allLabTestGroups.length);
			for (j = 0; j < $scope.allLabTestGroups.length; j++) {
				if ($scope.allLabTestGroups[j].groupName == grpNameEntered) {
					flag1 = 0;
					nameExist = true;
				}
			}
			if (flag1 == 0) {
				$scope.groupNameExistError = "Group name already exist";
			}
			else {
				nameExist = false;
			}
		};

	}]);
App.controller('labCrudCtrl', ['$scope', '$http', '$window', '$timeout',
	function ($scope, $http, $window, $timeout) {
		console.log("ladcrudctrl entered");
		//To close the add test modal and remove validation
		$scope.closetestmodal = function () {
			$scope.disabledAllCheckboxesTestModal();
			$('#testModal').modal('hide');
			$scope.addtest = {};
			$scope.novalue = "";
			$('.form-group').removeClass('has-error');
			$('.help-block').remove();
		};
		//To close the add test group modal and remove validation
		$scope.closetestgrpmodal = function () {
			$('#testGroupModal').modal('hide');
			$scope.labTestGrp = {};
			$('.form-group').removeClass('has-error');

			$('.help-block').remove();

		};
		//to open testgroupmodal
		$scope.show = function () {
			$('#testGroupModal').modal('show');
			$('.form-group').removeClass('has-error');
			$('.help-block').remove();
		};
		//to get all tests to view
		// $scope.normalvalue = [];

		$scope.toGetAllTests = function () {
			var getEveryTests = '/ihspdc/rest/getalltests'
			$http.get(getEveryTests).then(function (response, err) {
				if (!err && response.data != "") {
					console.log(response.data);
					response.data.sort(function (a, b) {
						return a.testName.localeCompare(b.testName);
					});
					$scope.allAvailTests = response.data;
				}
			});
		}
		console.log("all test");
		$scope.toGetAllTests();


		//jquery form validation function
		var validationOfProfileSection = function (onLoad, formId, callback) {
			var contextScope = this;
			contextScope.onLoad = onLoad;
			var validator = $(formId).validate({
				ignore: [],
				errorClass: 'help-block animated fadeInDown',
				errorElement: 'div',
				errorPlacement: function (error, e) {
					if (!contextScope.onLoad) {
						jQuery(e).parents('.form-group > div').append(error);
					}
				},
				highlight: function (e) {
					var elem = jQuery(e);
					if (!contextScope.onLoad) {
						elem.closest('.form-group').removeClass('has-error').addClass('has-error');
						elem.closest('.help-block').remove();
					}
				},
				success: function (e) {
					var elem = jQuery(e);
					elem.closest('.form-group').removeClass('has-error');
					elem.closest('.help-block').remove();
				},
				rules: {
					'testCode': {
						required: true,
					},
					'testName': {
						required: true,

					},
					'testUnit': {
						required: true
					}
				},
				messages: {
					'testCode': {
						required: 'Test code is required',

					},
					'testName': 'Test name is required',
					'testUnit': 'Test unit is required'
				}
			});
			if (contextScope.onLoad) {
				$(formId).valid();
			}
			else if ($(formId).valid()) {
				callback(null, validator);
			}
		};
		//Start 

		$scope.TestGrp = function () {
			//onload call for validation
			var onLoad = true;
			var formId = "#testgrpform";
			var callback = null;
			var success = 0;
			validation_Of_Profile_Section(onLoad, formId, function (err, response) {
				if (!err) {
					console.log("success");
				}
			});
		}


		$scope.disabledAllCheckboxesTestModal = function () {
			document.getElementById('value1').disabled = true;
			document.getElementById('from1').disabled = true;
			document.getElementById('to1').disabled = true;
			document.getElementById('value2').disabled = true;
			document.getElementById('from2').disabled = true;
			document.getElementById('to2').disabled = true;
			document.getElementById('value3').disabled = true;
			document.getElementById('from3').disabled = true;
			document.getElementById('to3').disabled = true;
		}
		$scope.disabledAllCheckboxesTestModal();

		//FUNCTION CALL FOR SAVE LAB TEST GROUP
		$scope.saveTestgrp = function () {
			$scope.masterAlerts = true;
			// //on clicking save button call jquery validation function
			// var onLoad = false;
			// var formId = "#testgrpform";
			// var callback = null;

			validation_Of_Profile_Section(onLoad, formId, function (err, response) {
				if (!err) {
					console.log("success grp");
					$scope.success_grp = 1;
				}
			});
			console.log($scope.labTestGrp);
			if ($scope.success_grp == 1) {
				$('#testGroupModal').modal('hide');
				var request = {
					url: '/ihspdc/rest/saveTestGrp',
					data: $scope.labTestGrp,
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					}
				};
				$http(request);
				document.getElementById("testgrpform").reset();
				//to show success level message on saving the test group
				jQuery(window).scrollTop(jQuery('#page_level').offset().top);
				jQuery("#page_level").focus();
				jQuery("#page_level").show();
				jQuery("#page_level").addClass("alert alert-success alert-white rounded");
				jQuery("#page_level").html('<button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button><div class="icon"><i class="fa fa-check"></i></div><strong>Success!</strong> Test Group has been saved successfully!');
				$timeout(function () { $scope.masterAlerts = false; }, 9000);
				$scope.getAllLabGroups();
			}

		}//END OF SAVING LAB TEST GROUP

		//dynamically checks with existing group id and names - throws error if matches
		$scope.getAllLabGroups = function () {
			var toGetAllGroups = '/ihspdc/rest/getallgroups'
			$http.get(toGetAllGroups).then(function (response, err) {
				if (!err && response.data != "") {
					response.data.sort(function (a, b) {
						return a.groupName.localeCompare(b.groupName);
					});
					$scope.allLabTestGroups = response.data;
				}
			});
		}
		$scope.getAllLabGroups();

		$scope.dynamic_change = function (grpIdEntered) {
			$scope.disp = " ";
			var j;
			if ($scope.allLabTestGroups.length) {
				for (j = 0; j < $scope.allLabTestGroups.length; j++) {
					if ($scope.allLabTestGroups[j].id == grpIdEntered) {
						$scope.disp = "Group ID already exist";
					}
				}
			}
		}


		//dynamically checks with existing group  and give error if it matches
		$scope.dynamic_change1 = function (grpNameEntered) {
			$scope.display = " ";
			var j;
			$scope.flag1 = 1;
			if ($scope.allLabTestGroups.length) {
				for (j = 0; j < $scope.allLabTestGroups.length; j++) {
					if ($scope.allLabTestGroups[j].groupName == grpNameEntered) {
						$scope.flag1 = 0;
					}
				}
			}
			if ($scope.flag1 == 0) {
				$scope.display = "Group name already exist";
			}
		};


		//jquery validation function for addgroup modal
		var validation_Of_Profile_Section = function (onLoad, formId, callback) {
			var contextScope = this;
			contextScope.onLoad = onLoad;
			var validator = $(formId).validate({
				ignore: [],
				errorClass: 'help-block animated fadeInDown',
				errorElement: 'div',
				errorPlacement: function (error, e) {
					if (!contextScope.onLoad) {
						jQuery(e).parents('.form-group > div').append(error);
					}
				},
				highlight: function (e) {
					var elem = jQuery(e);
					if (!contextScope.onLoad) {
						elem.closest('.form-group').removeClass('has-error').addClass('has-error');
						elem.closest('.help-block').remove();
					}
				},
				success: function (e) {
					var elem = jQuery(e);
					elem.closest('.form-group').removeClass('has-error');
					elem.closest('.help-block').remove();
				},
				rules: {
					'GroupName': {
						required: true,
					},
					'GroupCode': {
						required: true,

					}
				},
				messages: {
					'GroupCode': {
						required: 'Group code is required',

					},
					'GroupName': {
						required: 'Group name is required'
					}

				}
			});
			if (contextScope.onLoad) {
				$(formId).valid();
			}
			else if ($(formId).valid()) {
				callback(null, validator);
			}
		};




		//dynamically checks with the test id and gives error if it matches
		$scope.dynamicchange = function (addtest) {
			$scope.disp = " ";
			console.log("entered");
			var gettestsToCheckId = '/ihspdc/rest/getalltests'
			$http.get(gettestsToCheckId).then(function (response, err) {
				if (!err) {

					console.log(response.data);
					$scope.idToBeValid = response.data;
					console.log("labtest");
					console.log($scope.idToBeValid);
					var j;
					$scope.flag = 1;
					console.log($scope.idToBeValid.length);
					for (j = 0; j < $scope.idToBeValid.length; j++) {
						if ($scope.idToBeValid[j].testCode == addtest.testCode) {
							$scope.flag = 0;
						}
					}
					if ($scope.flag == 0) {
						console.log("already exists");
						$scope.disp = "Duplicate Test Code Found";
					}
				}
			})
		};
		//dynamically checks with the test name and gives error if it matches
		$scope.dynamicchange1 = function (addtest) {
			$scope.disp1 = " ";
			console.log("entered dynamichange1");
			var gettestsToCheckName = '/ihspdc/rest/getalltests'
			$http.get(gettestsToCheckName).then(function (response, err) {
				if (!err) {

					console.log(response.data);
					$scope.nameToBeValid = response.data;
					console.log("labtest1");
					console.log($scope.nameToBeValid);
					var j;
					$scope.flag1 = 1;
					console.log($scope.nameToBeValid.length);
					for (j = 0; j < $scope.nameToBeValid.length; j++) {
						if ($scope.nameToBeValid[j].testName == addtest.testName) {
							$scope.flag1 = 0;
						}
					}
					if ($scope.flag1 == 0) {
						console.log("already exists");
						$scope.disp1 = "Duplicate Test Name Found";
					}
				}
			})
		};


		$scope.showInputIfChecked = function () {
			maleValue = document.getElementById('male1');
			maleRange = document.getElementById('male2');
			femaleValue = document.getElementById('female1');
			femaleRange = document.getElementById('female1');;
			commonValue = document.getElementById('common1');
			commonRange = document.getElementById('common2');
			if (maleValue.checked == true) {
				document.getElementById('value1').disabled = false;
			}
			if (maleRange.checked == true) {
				document.getElementById('from1').disabled = false;
				document.getElementById('to1').disabled = false;
			}
			if (femaleValue.checked == true) {
				document.getElementById('value2').disabled = false;
			}
			if (femaleRange.checked == true) {
				document.getElementById('from2').disabled = false;
				document.getElementById('to2').disabled = false;
			}
			if (commonValue.checked == true) {
				document.getElementById('value3').disabled = false;
			}
			if (commonRange.checked == true) {
				document.getElementById('from3').disabled = false;
				document.getElementById('to3').disabled = false;
			}
		}

		$scope.validateTestDetails = {
			debug: false,
			errorElement: "div",
			rules: {
				testCode: {
					required: true
				},
				testName: {
					required: true
				},
				testGroup: {
					required: true
				},
				testUnit: {
					required: true,
				}
			},
			messages: {
				testCode: {
					required: "Test code is required"
				},
				testName: {
					required: "Test name is required"
				},
				testGroup: {
					required: "Test group is required"
				},
				testUnit: {
					required: "Test unit is required",
				}
			},
			highlight: function (element, errorClass) {
				$(element).removeClass(errorClass);
			}
		}

		$scope.registerNewTest = function (testForm, testDetails) {
			if (!testForm.validate()) {
				// event.preventDefault();
				// event.stopPropagation();
			}
			else {
				$scope.addNewTest(testDetails);
			}
		}

		//TO ADD NEW LAB TEST
		$scope.addNewTest = function (addtest) {
			console.log("$scope.addlabtest entered");
			console.log(addtest);
			var onLoad = false;
			var formId = "#testForm";
			var callback = null;
			var validateform;
			$scope.masterTestAlerts = true;
			validationOfProfileSection(onLoad, formId, function (err, response) {
				if (!err) {
					$scope.success = 1;
					console.log("success");
				}
			});
			var validdata = 1;
			$scope.valueerror = " ";
			$scope.valueerror1 = " ";
			$scope.valueerror2 = " ";
			$scope.valueerror3 = " ";
			$scope.valueerror4 = " ";
			$scope.valueerror5 = " ";
			$scope.novalue = " ";
			//each validation rule is handled using jquery explicity indicating the specified id
			if ($('#value1').val() == "" && $('#value2').val() == "" && $('#value3').val() == "" && $('#from1').val() == "" && $('#from2').val() == "" && $('#from3').val() == "" && $('#to1').val() == "" && $('#to2').val() == "" && $('#to3').val() == "") {
				validdata = 0;
				$scope.novalue = "Value or Range is required";
			}
			if ($('#common1').is(':checked')) {

				if ($('#value3').val() == "") {
					validdata = 0;
					$scope.valueerror = "common value not entered";
				}
			}
			if ($('#common2').is(':checked')) {

				if ($('#from3').val() == "") {
					validdata = 0;
					$scope.valueerror1 = "common value not entered";
				}
			}
			if ($('#male1').is(':checked') && $('#value1').val() == "") {
				validdata = 0;
				$scope.valueerror2 = "male value not entered";
				// if ($('#value2').val() == "" && $('#value2').val() == "") {
				// 	validdata = 0;
				// 	$scope.valueerror4 = "female value not entered";
				// 	$scope.valueerror5 = "female range not entered";
				// }
			}
			if ($('#male2').is(':checked') && $('#from1').val() == "" && $('#to1').val() == "") {
				validdata = 0;
				$scope.valueerror3 = "male range not entered";
				// if ($('#value2').val() == "" && $('#value2').val() == "") {
				// 	validdata = 0;
				// 	$scope.valueerror4 = "female value not entered";
				// 	$scope.valueerror5 = "female range not entered";
				// }
			}

			if ($('#female1').is(':checked') && $('#value2').val() == "") {
				validdata = 0;
				$scope.valueerror4 = "female value not entered";
				// if ($('#value1').val() == "" && $('#from1').val() == "") {
				// 	validdata = 0;
				// 	$scope.valueerror2 = "male value not entered";
				// 	$scope.valueerror3 = "male range not entered";
				// }
			}

			if ($('#female2').is(':checked') && $('#from2').val() == "" && $('#to2').val() == "") {
				validdata = 0;
				$scope.valueerror4 = "female Range not entered";

			}

			// if ($('#male2').is(':checked') && $('#from1').val() == "") {
			// 	validdata = 0;
			// 	$scope.valueerror3 = "male range not entered";
			// 	if ($('#value2').val() == "" && $('#value2').val() == "") {
			// 		validdata = 0;
			// 		$scope.valueerror4 = "female value not entered";
			// 		$scope.valueerror5 = "female range not entered";
			// 	}

			// }

			if (validdata == 1) {
				$scope.addlabtest = {};

				values = {};
				$scope.addlabtest.labValidations = [];
				$scope.addlabtest.group = {};
				$scope.addlabtest.testCode = addtest.testCode;
				$scope.addlabtest.testName = addtest.testName;
				$scope.addlabtest.unit = addtest.testUnit;
				$scope.addlabtest.group.id = addtest.selectedName.id;
				$scope.addlabtest.group.groupName = addtest.selectedName.groupName;
				console.log(addtest); console.log("Making Of Json"); console.log(addtest.genderCommonValue + addtest.genderCommonRange + addtest.maleValue + addtest.maleRange + addtest.femaleValue + addtest.femaleRange);
				//making json object appropriately for the required json structure

				if (addtest.genderCommonValue && $('#common1').is(':checked')) {
					$scope.addlabtest.labValidations.push({
						"normalValue": addtest.value_Common,
						"gender": "Common"
					})
				}
				if (addtest.genderCommonRange && $('#common2').is(':checked')) {
					$scope.addlabtest.labValidations.push({
						"lowValue": addtest.from3,
						"highValue": addtest.to3,
						"gender": "Common"
					})
				}
				if (addtest.maleValue && $('#male1').is(':checked')) {
					$scope.addlabtest.labValidations.push({
						"normalValue": addtest.value_Male,
						"gender": "Male"
					})
				}
				if (addtest.maleRange && $('#male2').is(':checked')) {
					$scope.addlabtest.labValidations.push({
						"lowValue": addtest.from1,
						"highValue": addtest.to1,
						"gender": "Male"
					})
				}
				if (addtest.femaleValue && $('#female1').is(':checked')) {
					$scope.addlabtest.labValidations.push({
						"normalValue": addtest.value_Female,
						"gender": "Female"
					})
				}
				if (addtest.femaleRange && $('#female2').is(':checked')) {
					$scope.addlabtest.labValidations.push({
						"lowValue": addtest.from2,
						"highValue": addtest.to2,
						"gender": "Female"
					})
				}

				console.log("$scope.addlabtest");
				console.log($scope.addlabtest);

				if (1) {
					if ($scope.success == 1 && validdata == 1) {
						$('#testModal').modal('hide');
					}

					$http({
						url: '/ihspdc/rest/savelabtest',
						data: $scope.addlabtest,
						method: 'POST',
						headers: {
							'Content-Type': 'application/json'
						}
					}).then(function (response) {
						addtest = {}; addtest = "";
						$scope.addlabtest = {}; $scope.addlabtest = "";
						console.log("success of saving labTest");
						document.getElementById("testForm").reset();
						$scope.toGetAllTests();
						console.log(response);
					}, function (error) {
						addtest = {}; addtest = "";
						$scope.addlabtest = {}; $scope.addlabtest = "";
						console.log("error");
						document.getElementById("testForm").reset();
						$scope.toGetAllTests();
					}
					);
					//success level page
					jQuery(window).scrollTop(jQuery('#message-div').offset().top);
					jQuery("#message-div").focus();
					jQuery("#message-div").show();
					jQuery("#message-div").addClass("alert alert-success alert-white rounded");
					jQuery("#message-div").html('<button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button><div class="icon"><i class="fa fa-check"></i></div><strong>Success!</strong> Test has been successfully saved');
					$timeout(function () {
						$scope.masterTestAlerts = false;
					}, 9000);
				}


			}

		}

		//to disable the check box
		$scope.disablecheckbox = function () {
			$scope.disabledAllCheckboxesTestModal();
			document.getElementById('from1').value = "";
			document.getElementById('to1').value = "";
			document.getElementById('from2').value = "";
			document.getElementById('to2').value = "";
			document.getElementById('value3').value = "";
			document.getElementById('from3').value = "";
			document.getElementById('to3').value = "";

			document.getElementById('value1').disabled = false;

			document.getElementById('from1').disabled = true;
			document.getElementById('to1').disabled = true;
			document.getElementById('from2').disabled = true;
			document.getElementById('to2').disabled = true;
			document.getElementById('value3').disabled = true;
			document.getElementById('from3').disabled = true;
			document.getElementById('to3').disabled = true;

			document.getElementById('female2').checked = false;
			document.getElementById('male2').checked = false;
			document.getElementById('common1').checked = false;
			document.getElementById('common2').checked = false;
		}

		//to disable the check box
		$scope.disablecheckbox1 = function () {
			$scope.disabledAllCheckboxesTestModal();
			document.getElementById('value1').value = "";
			document.getElementById('value2').value = "";
			document.getElementById('value3').value = "";
			document.getElementById('from3').value = "";
			document.getElementById('to3').value = "";

			document.getElementById('from1').disabled = false;
			document.getElementById('to1').disabled = false;

			document.getElementById('value1').disabled = true;
			document.getElementById('value2').disabled = true;
			document.getElementById('value3').disabled = true;
			document.getElementById('from3').disabled = true;
			document.getElementById('to3').disabled = true;

			document.getElementById('female1').checked = false;
			document.getElementById('male1').checked = false;
			document.getElementById('common1').checked = false;
			document.getElementById('common2').checked = false;


		}

		//to disable the check box
		$scope.disablecheckbox2 = function () {
			$scope.disabledAllCheckboxesTestModal();
			document.getElementById('from1').value = "";
			document.getElementById('to1').value = "";
			document.getElementById('from2').value = "";
			document.getElementById('to2').value = "";
			document.getElementById('value3').value = "";
			document.getElementById('from3').value = "";
			document.getElementById('to3').value = "";

			document.getElementById('value2').disabled = false;

			document.getElementById('from1').disabled = true;
			document.getElementById('to1').disabled = true;
			document.getElementById('from2').disabled = true;
			document.getElementById('to2').disabled = true;
			document.getElementById('value3').disabled = true;
			document.getElementById('from3').disabled = true;
			document.getElementById('to3').disabled = true;

			document.getElementById('male2').checked = false;
			document.getElementById('female2').checked = false;
			document.getElementById('common1').checked = false;
			document.getElementById('common2').checked = false;


		}
		//to disable the check box
		$scope.disablecheckbox3 = function () {
			$scope.disabledAllCheckboxesTestModal();
			document.getElementById('value1').value = "";
			document.getElementById('value2').value = "";
			document.getElementById('value3').value = "";
			document.getElementById('from3').value = "";
			document.getElementById('to3').value = "";

			document.getElementById('from2').disabled = false;
			document.getElementById('to2').disabled = false;

			document.getElementById('value1').disabled = true;
			document.getElementById('value2').disabled = true;
			document.getElementById('value3').disabled = true;
			document.getElementById('from3').disabled = true;
			document.getElementById('to3').disabled = true;

			document.getElementById('male1').checked = false;
			document.getElementById('female1').checked = false;
			document.getElementById('common1').checked = false;
			document.getElementById('common2').checked = false;


		}

		//to disable the check box
		$scope.disablecheckbox4 = function () {
			$scope.disabledAllCheckboxesTestModal();
			document.getElementById('from1').value = "";
			document.getElementById('to1').value = "";
			document.getElementById('value1').value = "";
			document.getElementById('from2').value = "";
			document.getElementById('to2').value = "";
			document.getElementById('value2').value = "";
			document.getElementById('from3').value = "";
			document.getElementById('to3').value = "";

			document.getElementById('value3').disabled = false;

			document.getElementById('from3').disabled = true;
			document.getElementById('to3').disabled = true;
			document.getElementById('value2').disabled = true;
			document.getElementById('from2').disabled = true;
			document.getElementById('to2').disabled = true;
			document.getElementById('value1').disabled = true;
			document.getElementById('from1').disabled = true;
			document.getElementById('to1').disabled = true;

			document.getElementById('male2').checked = false;
			document.getElementById('male1').checked = false;
			document.getElementById('female2').checked = false;
			document.getElementById('female1').checked = false;
			document.getElementById('common2').checked = false;
		}
		//to disable the check box
		$scope.disablecheckbox5 = function () {
			$scope.disabledAllCheckboxesTestModal();
			document.getElementById('from1').value = "";
			document.getElementById('to1').value = "";
			document.getElementById('value1').value = "";
			document.getElementById('from2').value = "";
			document.getElementById('to2').value = "";
			document.getElementById('value2').value = "";
			document.getElementById('value3').value = "";

			document.getElementById('from3').disabled = false;
			document.getElementById('to3').disabled = false;

			document.getElementById('value3').disabled = true;
			document.getElementById('value2').disabled = true;
			document.getElementById('from2').disabled = true;
			document.getElementById('to2').disabled = true;
			document.getElementById('value1').disabled = true;
			document.getElementById('from1').disabled = true;
			document.getElementById('to1').disabled = true;

			document.getElementById('male2').checked = false;
			document.getElementById('male1').checked = false;
			document.getElementById('female2').checked = false;
			document.getElementById('female1').checked = false;
			document.getElementById('common1').checked = false;

		}

		//to remove the validation done
		$scope.showtestgrp = function () {
			$('.form-group').removeClass('has-error');

			$('.help-block').remove();
		}
		//to call validation function on load
		$scope.addallgroup = function () {
			$('.form-group').removeClass('has-error');
			$scope.addtest = {};
			$scope.addlabtest = {};
			$('.help-block').remove();
			var onLoad = true;
			var formId = "#testForm";
			var callback = null;
			validationOfProfileSection(onLoad, formId, function () {
				if (!err) {
					console.log("success");
				}
			});
			//to get all groups
			console.log("add groups");
			var getgroups = '/ihspdc/rest/getallgroups';
			$http.get(getgroups).then(function (response, err) {
				if (!err) {
					console.log(response.data);
					$scope.groupsAvaillable = response.data;
					console.log("get groupsAvaillable");
					console.log($scope.groupsAvaillable);

				}
			});

		}
	}]);
App.controller('manageAppointmentCtrl', ['$scope', '$http', '$rootScope', '$state', 'patientService',
	function ($scope, $http, $rootScope, $state, patientService) {

		function getAllAppointments() {
			patientService.getAllAppointments(function (err, response) {
				var today = moment().format("DD-MM-YYYY");
				if ($rootScope.Auth && ($rootScope.Auth.hasResourceRole('patient'))) {
					$scope.bookedAppointments = [];
					angular.forEach(response.data, function (app) {
						if (app.patient.opCode.toUpperCase() == $rootScope.Auth.tokenParsed.preferred_username.toUpperCase()) {
							if (moment(app.appDate, "DD-MM-YYYY").isBefore(moment()) &&
								app.appDate != today) {
								app.pastAppointment = true;
							}
							else {
								app.pastAppointment = false;
							}
							$scope.bookedAppointments.push(app);
						}
					});
				} else {
					$scope.bookedAppointments = response.data;
					angular.forEach($scope.bookedAppointments, function (app) {
						if (moment(app.appDate, "DD-MM-YYYY").isBefore(moment()) &&
							app.appDate != today) {
							app.pastAppointment = true;
						}
						else {
							app.pastAppointment = false;
						}
					});
				}
			});
		}
		getAllAppointments();

		$scope.hideAlert = function () {
			$scope.sucMsg = false;
			$scope.errMsg = false;
		}

		$scope.reschduleAppointment = function (appInfo) {
			$rootScope.appInfo = {};
			$rootScope.appInfo = appInfo;
			$state.go("reschduleAppointment");
		}

		//ALERT MESSAGES
		$scope.throwError = function (msg) {
			$scope.hideAlert();
			$("html, body").animate({ scrollTop: 0 }, 200);
			$scope.errMsg = true;
			$scope.errorMsg = msg;
			jQuery("#errId").focus();
			jQuery("#errId").show();
		}
		$scope.throwSuccess = function (msg) {
			$scope.hideAlert();
			$("html, body").animate({ scrollTop: 0 }, 200);
			$scope.sucMsg = true;
			$scope.successMsg = msg;
			jQuery("#successId").focus();
			jQuery("#successId").show();
		}

		//CANCEL APPOINTMENT PROCESS
		var appToCancel = {};
		$scope.cancelAppointment = function (app) {
			appToCancel = app;
			$('#cancelModal').modal('show');
		}
		$scope.cancelAppointmentSure = function () {
			appToCancel.appStatus = "Cancelled";
			$http({
				url: '/ihspdc/rest/cancelAppointment',
				data: appToCancel,
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				}
			}).then(function (response) {
				$('#cancelModal').modal('hide');
				$scope.throwSuccess("Appointment has been cancelled successfully");
				appToCancel = {};
				getAllAppointments();
			}, function (error) {
				console.log("Error in cancelling appointment");
			});
		}
	}
]);

App.controller('reschduleAppointmentCtrl', ['$scope', '$http', '$rootScope', '$compile', 'patientService', '$stateParams',
	function ($scope, $http, $rootScope, $compile, patientService, $stateParams) {

		//UI CONTROLS
		$(document).on("click", '.date-numer', function () {
			var id = $(this).attr("id");
			$(".list-available").each(function () {
				if ($(this).attr("id") == "list-book-" + id) {
					$(this).fadeIn();
				} else {
					$(this).hide();
				}
			});
		});

		$('.close, .confirm-appointment').on("click", function () {
			$(".list-available").fadeOut('slow');
		});
		//END UI CONTROLS

		function getAllAppointments() {
			patientService.getAllAppointments(function (err, response) {
				$scope.allAppointments = response.data;
			});
		}
		getAllAppointments();

		//ALERT MESSAGES
		$scope.throwError = function (msg) {
			$scope.hideAlert();
			$("html, body").animate({ scrollTop: 0 }, 200);
			$scope.errMsg = true;
			$scope.errorMsg = msg;
			jQuery("#errId").focus();
			jQuery("#errId").show();
		}
		$scope.throwSuccess = function (msg) {
			$scope.hideAlert();
			$("html, body").animate({ scrollTop: 0 }, 200);
			$scope.sucMsg = true;
			$scope.successMsg = msg;
			jQuery("#successId").focus();
			jQuery("#successId").show();
		}


		$scope.getInput = function (slot) {
			$scope.slotChosen = slot;
			var d = $scope.datePicked + "-" + $scope.currMonth + "-" + $scope.year;
			$scope.dateChosen = moment(d, "D-MMMM-YYYY").format("MMMM DD, YYYY");
		}

		$scope.validateAppointment = function () {
			if (!$scope.app.patient && $rootScope.Auth && ($rootScope.Auth.hasResourceRole('coord'))) {
				$scope.throwError("Patient is required");
			}
			else if ($scope.app.doctor.appointmentType == "Slot" && !$scope.slotChosen && !$scope.dateChosen) {
				$scope.throwError("Slot is required");
			}
			else if ($scope.app.doctor.appointmentType == "Session" && !$scope.slotChosen && !$scope.dateChosen) {
				$scope.throwError("Session is required");
			}
			else {
				if ($scope.app.doctor.appointmentType == "Session" && $scope.slotChosen && $scope.dateChosen) {
					var date = moment($scope.dateChosen, "MMMM DD, YYYY").format("DD-MM-YYYY");
					var availableCount = 0;
					angular.forEach($scope.allAppointments, function (app) {
						if (app.appType == "Session") {
							if (app.appTime == $scope.slotChosen && date == app.appDate) {
								availableCount++;
							}
						}
					});
					angular.forEach($scope.app.doctor.appSession, function (appSetting) {
						var sessionTime = appSetting.fromTime + " - " + appSetting.toTime; //SPACE MATTERS
						if (sessionTime == $scope.slotChosen) {
							if (availableCount < appSetting.noOfAppointments) {
								$('#confirmAppointment').modal('show');
							}
							else {
								$scope.throwError("This slot have exceeded the maximum number of appointments!");
							}
						}
					});
				}
				else {
					$('#confirmAppointment').modal('show');
				}
			}
		}

		//RE-SCHDULE BOOKED APPOINTMENT PROCESS
		$scope.bookAppointmentOn = function () {
			var appointment = {};
			if ($rootScope.Auth && ($rootScope.Auth.hasResourceRole('coord'))) {
				appointment.patient = $scope.app.patient;
			}
			else if ($rootScope.Auth && ($rootScope.Auth.hasResourceRole('patient'))) {
				patientService.searchPatientByField(1, $stateParams.id, function (err, response) {
					appointment.patient = response.data;
				});
			}
			appointment.appDate = moment($scope.dateChosen, "MMMM DD, YYYY").format("DD-MM-YYYY");
			appointment.appTime = $scope.slotChosen;
			appointment.department = $scope.app.department;
			appointment.doctor = $scope.app.doctor;
			appointment.visitStatus = "Not Yet";
			appointment.appStatus = "Booked";
			appointment.appType = $scope.app.doctor.appointmentType;
			$http({
				url: '/ihspdc/rest/reScheduleAppointment',
				data: appointment,
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				}
			}).then(function (response) {
				$('#confirmAppointment').modal('hide');
				$scope.throwSuccess("Appointment has been re-schduled successfully");
				$scope.app = {};
				getAllAppointments();
			}, function (error) {
				console.log("Error in updating Doctor");
			});
			//CANCELLING THE PREVIOUS APPOINTMENT
			$rootScope.appInfo.appStatus = "Reschduled";
			$rootScope.appInfo.reschduleInfo = "Re-scheduled on " + appointment.appDate + " at " + appointment.appTime;
			$http({
				url: '/ihspdc/rest/cancelAppointment',
				data: $rootScope.appInfo,
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				}
			}).then(function (response) {
				$('#confirmAppointment').modal('hide');
				$scope.throwSuccess("Appointment has been re-schduled successfully");
				appointment = {};
				$scope.app = {};
				getAllAppointments();
			}, function (error) {
				console.log("Error in updating Doctor");
			});
		}

		var blockedDates = [];
		$scope.getCalender = function (doctor) {
			blockedDates = [];
			date = new Date();
			var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
			var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
			var dateStr = doctor.nonAvaillDates;
			var dates = dateStr.split(";");
			angular.forEach(dates, function (date) {
				var d = moment(date, "DD-MM-YYYY");
				if (d.isSameOrAfter(firstDay) && d.isSameOrBefore(lastDay)) {
					blockedDates.push(parseInt(d.format("DD")));
				}
			});
			calendar(date.getFullYear(), date.getMonth());
		}

		function getBlockedDates(year, month) {
			blockedDates = [];
			date = new Date(year + $scope.monthNames[month] + "01");
			var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
			var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
			var dateStr = $scope.app.doctor.nonAvaillDates;
			var dates = dateStr.split(";");
			angular.forEach(dates, function (date) {
				var d = moment(date, "DD-MM-YYYY");
				if (d.isSameOrAfter(firstDay) && d.isSameOrBefore(lastDay)) {
					blockedDates.push(parseInt(d.format("DD")));
				}
			});
		}

		$scope.nextMonthCount = 1;
		$scope.getNextMonth = function (month, year) {
			if ($scope.app.doctor.appOpenMonth > 0) {
				if (month == "December") {
					$scope.year = year + 1;
					$scope.monthNow = 0;
					getBlockedDates($scope.year, $scope.monthNow);
					calendar($scope.year, $scope.monthNow)
				} else {
					var nxtMonth = $scope.monthNames.indexOf(month) + 1;
					$scope.year = year;
					getBlockedDates($scope.year, nxtMonth);
					calendar($scope.year, nxtMonth);
				}
				$scope.nextMonthCount++;
			}
		}

		$scope.getPrevMonth = function (month, year) {
			date = new Date();
			var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
			var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
			var cmonth = "01-" + month + "-" + year;
			if ($scope.app.doctor.appOpenMonth > 0 && moment(cmonth, "DD-MMMM-YYYY").isSameOrAfter(firstDay) == true && moment(cmonth, "DD-MMMM-YYYY").isSameOrBefore(lastDay) != true) {
				if (month == "January") {
					$scope.year = year - 1;
					$scope.monthNow = 11;
					getBlockedDates($scope.year, $scope.monthNow);
					calendar($scope.year, $scope.monthNow)
				} else {
					var prevMonth = $scope.monthNames.indexOf(month) - 1;
					$scope.year = year;
					getBlockedDates($scope.year, prevMonth);
					calendar($scope.year, prevMonth);
				}
				$scope.nextMonthCount--;
			}
		}

		//CALENDER MAKING
		function calendar(year, month) {

			//Variables to be used later.  Place holders right now.
			$scope.padding = "";
			var totalFeb = "";
			var i = 1;
			var testing = "";

			var current = new Date();
			var cmonth = current.getMonth();
			var day = current.getDate();
			$scope.year = year;
			var tempMonth = month + 1; //+1; //Used to match up the current month with the correct start date.
			var prevMonth = month - 1;

			//Determing if Feb has 28 or 29 days in it.  
			if (month == 1) {
				if (($scope.year % 100 !== 0) && ($scope.year % 4 === 0) || ($scope.year % 400 === 0)) {
					totalFeb = 29;
				} else {
					totalFeb = 28;
				}
			}

			// Setting up arrays for the name of    //
			// the	months, days, and the number of	//
			// days in the month.                   //

			$scope.monthNames = ["January", "February", "March", "April", "May", "June", "July", "Auguest", "September", "October", "November", "December"];
			$scope.dayNames = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
			var totalDays = ["31", "" + totalFeb + "", "31", "30", "31", "30", "31", "31", "30", "31", "30", "31"];
			$scope.currMonth = $scope.monthNames[month];

			// Temp values to get the number of days//
			// in current month, and previous month.//
			// Also getting the day of the week.	//

			var tempDate = new Date(tempMonth + ' 1 ,' + $scope.year);
			var tempweekday = tempDate.getDay();
			var tempweekday2 = tempweekday;
			var dayAmount = totalDays[month];
			// var preAmount = totalDays[prevMonth] - tempweekday + 1;	

			// After getting the first day of the week for	//
			// the month, padding the other days for that	//
			// week with the previous months days.  I.E.,, if	//
			// the first day of the week is on a Thursday,	//
			// then this fills in Sun - Wed with the last	//
			// months dates, counting down from the last	//
			// day on Wed, until Sunday.                    //

			while (tempweekday > 0) {
				$scope.padding += "<td class='nextmonth'></td>";
				//preAmount++;
				tempweekday--;
			}
			// Filling in the calendar with the current     //
			// month days in the correct location along.    //

			while (i <= dayAmount) {

				// Determining when to start a new row	//

				if (tempweekday2 > 6) {
					tempweekday2 = 0;
					$scope.padding += "<tr></tr>";
				}

				// checking to see if i is equal to the current day, if so then we are making the color of //
				//that cell a different color using CSS. Also adding a rollover effect to highlight the  //
				//day the user rolls over. This loop creates the acutal calendar that is displayed.		//

				// if (i == day && month == cmonth) { TO GET CURRENT DATE
				var date = i + "-" + $scope.currMonth + "-" + $scope.year;
				console.log(moment(date, "DD-MMMM-YYYY").isSame(current), moment(date, "DD-MMMM-YYYY").isSameOrAfter(current), moment(date, "DD-MMMM-YYYY").isBefore(current));
				if (blockedDates.includes(i) != true && moment(date, "DD-MMMM-YYYY").isSameOrAfter(current)) {
					$scope.padding += "<td title='Available!' ng-click='callTimes(" + i + ", $event)'><span class='date-numer' id='" + i + "'title='Available!'>" + i + "</span></td>";
				} else {
					if (moment(date, "DD-MMMM-YYYY").isBefore(current)) {
						$scope.padding += "<td title='Not Available' class='passdate'>" + i + "</td>";
					}
					// else if (i == day) {
					// 	$scope.padding += "<td title='Not Available!' class='currentmonth passdate'>" + i + "</td>";
					// }
					else {
						$scope.padding += "<td title='Not Available!'>" + i + "</td>";
					}
				}
				tempweekday2++;
				i++;
			}

			// Ouptputing the calendar onto the	//
			// site.  Also, putting in the month	//
			// name and days of the week.		//
			$scope.calendarTable = "<table class='table' id='" + month + "'><thead><tr><td colspan='7'><a class='arrows' ng-hide='nextMonthCount <= 1' ng-click='getPrevMonth(currMonth,year)'><i class='fa fa-angle-double-left'></i></a><h2>" + $scope.currMonth + "&nbsp" + $scope.year + "<a class='arrows' ng-hide='nextMonthCount == app.doctor.appOpenMonth' ng-click='getNextMonth(currMonth,year)'><i class='fa fa-angle-double-right'></i></a></h2></td></tr><tr class='days'><th class='text-uppercase'>Sun</th><th class='text-uppercase'>Mon</th><th class='text-uppercase'>Tue</th><th class='text-uppercase'>Wed</th><th class='text-uppercase'>Thu</th><th class='text-uppercase'>Fri</th><th class='text-uppercase'>Sat</th></tr></thead>";
			$scope.calendarTable += "<tbody>";
			$scope.calendarTable += $scope.padding;
			$scope.calendarTable += "</tbody></table>";
			$compile($($scope.calendarTable).appendTo("#calendar"))($scope);
		}

		function isSlotAvaillbalefunction(date, timeSlot) {
			var availlable = true;
			for (i = 0; i < $scope.allAppointments.length; i++) {
				if ($scope.allAppointments[i].appType != "Session") {
					if ($scope.allAppointments[i].appDate == date && $scope.allAppointments[i].appTime == timeSlot) {
						availlable = false;
						break;
					}
				}
			}
			return availlable;
		}

		$scope.callTimes = function (id, $event) {
			$scope.datePicked = id;
			$scope.slotChosen = "";
			$scope.dateChosen = "";
			var timeBooks = "";
			if ($scope.app.doctor.appointmentType == "Session") {
				angular.element('.list-available').remove();
				for (i = 0; i < $scope.app.doctor.appSession.length; i++) {
					timeBooks += "<div class='time-book'><span class='text-white text-uppercase' style='font-size:15px'>" + $scope.app.doctor.appSession[i].sessionName + "</span><br><div class='radio'><label><input type='radio'  name='optionRadios' value='" + $scope.app.doctor.appSession[i].fromTime + " - " + $scope.app.doctor.appSession[i].toTime + "' ng-click='getInput($event.target.value)' id='option" + id + "-" + i + "'><span>" + $scope.app.doctor.appSession[i].fromTime + " - " + $scope.app.doctor.appSession[i].toTime + "</span></label></div></div>";
				}
				var timeInfos = "<tr class='list-available' id='list-book-" + id + "'><td colspan='7' class='booked-list'><span class='close'><i class='fa fa-times-circle'></i></span><h4>Available Appointments on " + $scope.currMonth + "&nbsp" + id + ",&nbsp" + $scope.year + "</h4><div class='row'>" + timeBooks + "<span data-toggle='modal' ng-click='validateAppointment()' class='btn'>Book Appointment</span></div></td></tr>";
				$(angular.element($compile(timeInfos)($scope)).insertAfter(angular.element($event.currentTarget).parent('tr')));
			}
			else if ($scope.app.doctor.appointmentType == "Slot") {
				angular.element('.list-available').remove();
				angular.forEach($scope.app.doctor.appSession, function (slot, index) {
					var from = "";
					var i = 0;
					var timeNodes = "";
					var count = 0;
					do {
						if (from == "") {
							from = moment(slot.fromTime, "HH:mm").format("hh:mm A");
							var date = $scope.datePicked + "-" + $scope.currMonth + "-" + $scope.year;
							date = moment(date, "D-MMMM-YYYY").format("DD-MM-YYYY");
							if (isSlotAvaillbalefunction(date, from)) {
								timeNodes += "<div class='radio'><label><input type='radio' name='optionRadios' ng-click='getInput($event.target.value)' value='" + from + "' id='option" + id + "-" + index + "-" + i + "'><span>" + from + "</span><label></div>"
							}
						}
						else {
							from = moment(from, "HH:mm A").add(slot.slotInterval, 'minutes').format("hh:mm A");
							var date = $scope.datePicked + "-" + $scope.currMonth + "-" + $scope.year;
							date = moment(date, "D-MMMM-YYYY").format("DD-MM-YYYY");
							if (isSlotAvaillbalefunction(date, from)) {
								timeNodes += "<div class='radio'><label><input type='radio' name='optionRadios' ng-click='getInput($event.target.value)' value='" + from + "' id='option" + id + "-" + index + "-" + i + "'><span>" + from + "</span><label></div>"
							}
						}
						var toTime = moment(slot.toTime, "HH:mm").format("hh:mm A");
						i += 1;
						count += 1;
					}
					while (from != toTime);
					timeBooks += "<div class='time-book time-align'><h5 class='text-white text-uppercase' style='font-size:15px'>" + slot.sessionName + "</h5><br>" + timeNodes + "</div>";
				});
				var timeInfos = "<tr class='list-available' id='list-book-" + id + "'><td colspan='7' class='booked-list'><span class='close'><i class='fa fa-times-circle'></i></span><h4>Available Appointments on " + $scope.currMonth + "&nbsp" + id + ",&nbsp" + $scope.year + "</h4><div class='row'>" + timeBooks + "<span data-toggle='modal' ng-click='validateAppointment()' class='btn'>Book Appointment</span></div></td></tr>";
				angular.element($compile(timeInfos)($scope)).insertAfter(angular.element($event.currentTarget).parent('tr'));
			}
		}

		$scope.hideAlert = function () {
			$scope.sucMsg = false;
			$scope.errMsg = false;
		}

		//ASSIGNING PROCESS TO SPECIFIC PATIENT
		$scope.app = {};
		$scope.app = $rootScope.appInfo;
		$scope.getCalender($scope.app.doctor);

	}
]);

App.controller('bookAppointmentCtrl', ['$scope', '$http', 'otherServices', 'patientService', '$compile', '$rootScope', '$stateParams',
	function ($scope, $http, otherServices, patientService, $compile, $rootScope, $stateParams) {

		//UI CONTROLS
		$(document).on("click", '.date-numer', function () {
			var id = $(this).attr("id");
			$(".list-available").each(function () {
				if ($(this).attr("id") == "list-book-" + id) {
					$(this).fadeIn();
				} else {
					$(this).hide();
				}
			});
		});

		$('.close, .confirm-appointment').on("click", function () {
			$(".list-available").fadeOut('slow');
		});
		//END UI CONTROLS

		$scope.hideAlert = function () {
			$scope.sucMsg = false;
			$scope.errMsg = false;
		}

		//SELECTION OF DOCTOR OPERATION
		$scope.app = {};
		otherServices.getDeptList(function (err, response) {
			$scope.departmentList = response.data;
		});

		$scope.loadDeptDoctors = function (selectedDepartment) {
			$scope.doctorList = [];
			$scope.app.doctor = "";
			otherServices.getDeptDoctors(selectedDepartment.department.id, function (err, response) {
				angular.forEach(response.data, function (doc) {
					$scope.doctorList.push(doc);
				});
			});
		}
		$scope.patientList = [];
		$scope.loadPatient = function (searchTerm) {
			$scope.patientList = [];
			patientService.searchPatientByField(1, searchTerm, function (err, response) {
				$scope.patientList.push(response.data);
			});
		}
		//END OF SELECTION OF DOCTOR OPERATIONS

		function getAllAppointments() {
			patientService.getAllAppointments(function (err, response) {
				$scope.allAppointments = response.data;
			});
		}
		getAllAppointments();

		//ALERT MESSAGES
		$scope.throwError = function (msg) {
			$scope.hideAlert();
			$("html, body").animate({ scrollTop: 0 }, 200);
			$scope.errMsg = true;
			$scope.errorMsg = msg;
			jQuery("#errId").focus();
			jQuery("#errId").show();
		}
		$scope.throwSuccess = function (msg) {
			$scope.hideAlert();
			$("html, body").animate({ scrollTop: 0 }, 200);
			$scope.sucMsg = true;
			$scope.successMsg = msg;
			jQuery("#successId").focus();
			jQuery("#successId").show();
		}


		$scope.getInput = function (slot) {
			$scope.slotChosen = slot;
			var d = $scope.datePicked + "-" + $scope.currMonth + "-" + $scope.year;
			$scope.dateChosen = moment(d, "D-MMMM-YYYY").format("MMMM DD, YYYY");
		}

		$scope.validateAppointment = function () {
			if (!$scope.app.subscription && $rootScope.Auth && ($rootScope.Auth.hasResourceRole('coord'))) {
				$scope.throwError("Patient is required");
			}
			else if ($scope.app.doctor.appointmentType == "Slot" && !$scope.slotChosen && !$scope.dateChosen) {
				$scope.throwError("Slot is required");
			}
			else if ($scope.app.doctor.appointmentType == "Session" && !$scope.slotChosen && !$scope.dateChosen) {
				$scope.throwError("Session is required");
			}
			else {
				if ($scope.app.doctor.appointmentType == "Session" && $scope.slotChosen && $scope.dateChosen) {
					var date = moment($scope.dateChosen, "MMMM DD, YYYY").format("DD-MM-YYYY");
					var availableCount = 0;
					angular.forEach($scope.allAppointments, function (app) {
						if (app.appType == "Session") {
							if (app.appTime == $scope.slotChosen && date == app.appDate) {
								availableCount++;
							}
						}
					});
					angular.forEach($scope.app.doctor.appSession, function (appSetting) {
						var sessionTime = appSetting.fromTime + " - " + appSetting.toTime; //SPACE MATTERS
						if (sessionTime == $scope.slotChosen) {
							if (availableCount < appSetting.noOfAppointments) {
								$('#confirmAppointment').modal('show');
							}
							else {
								$scope.throwError("This slot have exceeded the maximum number of appointments!");
							}
						}
					});
				}
				else {
					$('#confirmAppointment').modal('show');
				}
			}
		}

		//BOOKING APPOINT PROCESS
		$scope.bookAppointmentOn = function () {
			var appointment = {};
			if ($rootScope.Auth && ($rootScope.Auth.hasResourceRole('coord'))) {
				appointment.patient = $scope.app.subscription.patient;
			}
			else if ($rootScope.Auth && ($rootScope.Auth.hasResourceRole('patient'))) {
				patientService.searchPatientByField(1, $stateParams.id, function (err, response) {
					appointment.patient = response.data;
				});
			}
			appointment.appDate = moment($scope.dateChosen, "MMMM DD, YYYY").format("DD-MM-YYYY");
			appointment.appTime = $scope.slotChosen;
			appointment.department = $scope.app.department;
			appointment.doctor = $scope.app.doctor;
			appointment.visitStatus = "Not Yet";
			appointment.appStatus = "Booked";
			appointment.appType = $scope.app.doctor.appointmentType;
			$http({
				url: '/ihspdc/rest/bookAppointment',
				data: appointment,
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				}
			}).then(function (response) {
				$('#confirmAppointment').modal('hide');
				$scope.throwSuccess("Appointment has been booked successfully");
				appointment = {};
				$scope.app = {};
				getAllAppointments();
			}, function (error) {
				console.log("Error in updating Doctor");
			});


		}

		var blockedDates = [];
		$scope.getCalender = function (doctor) {
			blockedDates = [];
			date = new Date();
			var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
			var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
			var dateStr = doctor.nonAvaillDates;
			var dates = dateStr.split(";");
			angular.forEach(dates, function (date) {
				var d = moment(date, "DD-MM-YYYY");
				if (d.isSameOrAfter(firstDay) && d.isSameOrBefore(lastDay)) {
					blockedDates.push(parseInt(d.format("DD")));
				}
			});
			calendar(date.getFullYear(), date.getMonth());
		}

		function getBlockedDates(year, month) {
			blockedDates = [];
			date = new Date(year + $scope.monthNames[month] + "01");
			var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
			var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
			var dateStr = $scope.app.doctor.nonAvaillDates;
			var dates = dateStr.split(";");
			angular.forEach(dates, function (date) {
				var d = moment(date, "DD-MM-YYYY");
				if (d.isSameOrAfter(firstDay) && d.isSameOrBefore(lastDay)) {
					blockedDates.push(parseInt(d.format("DD")));
				}
			});
		}

		$scope.nextMonthCount = 1;
		$scope.getNextMonth = function (month, year) {
			if ($scope.app.doctor.appOpenMonth > 0) {
				angular.element('.table-month-list').hide();
				angular.element('#' + (month + 1)).show();
				if (month == "December") {
					$scope.year = year + 1;
					$scope.monthNow = 0;
					getBlockedDates($scope.year, $scope.monthNow);
					calendar($scope.year, $scope.monthNow)
				} else {
					var nxtMonth = $scope.monthNames.indexOf(month) + 1;
					$scope.year = year;
					getBlockedDates($scope.year, nxtMonth);
					calendar($scope.year, nxtMonth);
				}
				$scope.nextMonthCount++;
			}
		}

		$scope.getPrevMonth = function (month, year) {
			date = new Date();
			var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
			var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
			var cmonth = "01-" + month + "-" + year;
			angular.element('.table-month-list').hide();
			angular.element('#' + (month + 1)).show();
			if ($scope.app.doctor.appOpenMonth > 0 && moment(cmonth, "DD-MMMM-YYYY").isSameOrAfter(firstDay) == true && moment(cmonth, "DD-MMMM-YYYY").isSameOrBefore(lastDay) != true) {
				if (month == "January") {
					$scope.year = year - 1;
					$scope.monthNow = 11;
					getBlockedDates($scope.year, $scope.monthNow);
					calendar($scope.year, $scope.monthNow)
				} else {
					var prevMonth = $scope.monthNames.indexOf(month) - 1;
					$scope.year = year;
					getBlockedDates($scope.year, prevMonth);
					calendar($scope.year, prevMonth);
				}
				$scope.nextMonthCount--;
			}
		}

		//CALENDER MAKING
		function calendar(year, month) {

			//Variables to be used later.  Place holders right now.
			$scope.padding = "";
			var totalFeb = "";
			var i = 1;
			var testing = "";

			var current = new Date();
			var cmonth = current.getMonth();
			var day = current.getDate() + 1;
			$scope.year = year;
			var tempMonth = month + 1; //+1; //Used to match up the current month with the correct start date.
			var prevMonth = month - 1;

			//Determing if Feb has 28 or 29 days in it.  
			if (month == 1) {
				if (($scope.year % 100 !== 0) && ($scope.year % 4 === 0) || ($scope.year % 400 === 0)) {
					totalFeb = 29;
				} else {
					totalFeb = 28;
				}
			}

			// Setting up arrays for the name of    //
			// the	months, days, and the number of	//
			// days in the month.                   //

			$scope.monthNames = ["January", "February", "March", "April", "May", "June", "July", "Auguest", "September", "October", "November", "December"];
			$scope.dayNames = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
			var totalDays = ["31", "" + totalFeb + "", "31", "30", "31", "30", "31", "31", "30", "31", "30", "31"];
			$scope.currMonth = $scope.monthNames[month];

			// Temp values to get the number of days//
			// in current month, and previous month.//
			// Also getting the day of the week.	//

			var tempDate = new Date(tempMonth + ' 1 ,' + $scope.year);
			var tempweekday = tempDate.getDay();
			var tempweekday2 = tempweekday;
			var dayAmount = totalDays[month];
			// var preAmount = totalDays[prevMonth] - tempweekday + 1;	

			// After getting the first day of the week for	//
			// the month, padding the other days for that	//
			// week with the previous months days.  I.E.,, if	//
			// the first day of the week is on a Thursday,	//
			// then this fills in Sun - Wed with the last	//
			// months dates, counting down from the last	//
			// day on Wed, until Sunday.                    //

			while (tempweekday > 0) {
				$scope.padding += "<td class='nextmonth'></td>";
				//preAmount++;
				tempweekday--;
			}
			// Filling in the calendar with the current     //
			// month days in the correct location along.    //

			while (i <= dayAmount) {

				// Determining when to start a new row	//

				if (tempweekday2 > 6) {
					tempweekday2 = 0;
					$scope.padding += "<tr></tr>";
				}

				// checking to see if i is equal to the current day, if so then we are making the color of //
				//that cell a different color using CSS. Also adding a rollover effect to highlight the  //
				//day the user rolls over. This loop creates the acutal calendar that is displayed.		//

				// if (i == day && month == cmonth) { TO GET CURRENT DATE
				var date = i + "-" + $scope.currMonth + "-" + $scope.year;
				console.log(moment(date, "DD-MMMM-YYYY").isSame(current), moment(date, "DD-MMMM-YYYY").isSameOrAfter(current), moment(date, "DD-MMMM-YYYY").isBefore(current));
				if (blockedDates.includes(i) != true && moment(date, "DD-MMMM-YYYY").isSameOrAfter(current)) {
					
						$scope.padding += "<td title='Available!' ng-click='callTimes(" + i + ", $event)'><span class='date-numer' id='" + i + "'title='Available!'>" + i + "</span></td>";
					
				} else {
					if (moment(date, "DD-MMMM-YYYY").isBefore(current)) {
						$scope.padding += "<td title='Not Available' class='passdate'>" + i + "</td>";
					}
					// else if (i == day) {
					// 	$scope.padding += "<td title='Not Available!' class='currentmonth passdate'>" + i + "</td>";
					// }
					else {
						$scope.padding += "<td title='Not Available!'>" + i + "</td>";
					}
				}
				tempweekday2++;
				i++;
			}

			// Ouptputing the calendar onto the	//
			// site.  Also, putting in the month	//
			// name and days of the week.		//
			$scope.calendarTable = "<table class='table table-month-list' id='" + (month + 1) + "'><thead><tr><td colspan='7'><h2><a class='arrows' ng-hide='nextMonthCount <= 1' ng-click='getPrevMonth(currMonth,year)'><i class='fa fa-angle-double-left'></i></a>" + $scope.currMonth + "&nbsp" + $scope.year + "<a class='arrows' ng-hide='nextMonthCount == app.doctor.appOpenMonth' ng-click='getNextMonth(currMonth,year)'><i class='fa fa-angle-double-right'></i></a></h2></td></tr><tr class='days'><th class='text-uppercase'>Sun</th><th class='text-uppercase'>Mon</th><th class='text-uppercase'>Tue</th><th class='text-uppercase'>Wed</th><th class='text-uppercase'>Thu</th><th class='text-uppercase'>Fri</th><th class='text-uppercase'>Sat</th></tr></thead>";
			$scope.calendarTable += "<tbody>";
			$scope.calendarTable += $scope.padding;
			$scope.calendarTable += "</tbody></table>";
			$compile($($scope.calendarTable).appendTo("#calendar"))($scope);
		}

		function isSlotAvaillbalefunction(date, timeSlot) {
			var availlable = true;
			for (i = 0; i < $scope.allAppointments.length; i++) {
				if ($scope.allAppointments[i].appType != "Session") {
					if ($scope.allAppointments[i].appDate == date && $scope.allAppointments[i].appTime == timeSlot) {
						availlable = false;
						break;
					}
				}
			}
			return availlable;
		}

		$scope.callTimes = function (id, $event) {
			$scope.datePicked = id;
			$scope.slotChosen = "";
			$scope.dateChosen = "";
			var timeBooks = "";
			if ($scope.app.doctor.appointmentType == "Session") {
				angular.element('.list-available').remove();
				for (i = 0; i < $scope.app.doctor.appSession.length; i++) {
					timeBooks += "<div class='time-book'><span class='text-white text-uppercase' style='font-size:15px'>" + $scope.app.doctor.appSession[i].sessionName + "</span><br><div class='radio'><label><input type='radio'  name='optionRadios' value='" + $scope.app.doctor.appSession[i].fromTime + " - " + $scope.app.doctor.appSession[i].toTime + "' ng-click='getInput($event.target.value)' id='option" + id + "-" + i + "'><span>" + $scope.app.doctor.appSession[i].fromTime + " - " + $scope.app.doctor.appSession[i].toTime + "</span></label></div></div>";
				}
				var timeInfos = "<tr class='list-available' id='list-book-" + id + "'><td colspan='7' class='booked-list'><span class='close'><i class='fa fa-times-circle'></i></span><h4>Available Appointments on " + $scope.currMonth + "&nbsp" + id + ",&nbsp" + $scope.year + "</h4><div class='row'>" + timeBooks + "<span data-toggle='modal' ng-click='validateAppointment()' class='btn'>Book Appointment</span></div></td></tr>";
				$(angular.element($compile(timeInfos)($scope)).insertAfter(angular.element($event.currentTarget).parent('tr')));
			}
			else if ($scope.app.doctor.appointmentType == "Slot") {
				angular.element('.list-available').remove();
				angular.forEach($scope.app.doctor.appSession, function (slot, index) {
					var from = "";
					var i = 0;
					var timeNodes = "";
					var count = 0;
					do {
						if (from == "") {
							from = moment(slot.fromTime, "HH:mm").format("hh:mm A");
							var date = $scope.datePicked + "-" + $scope.currMonth + "-" + $scope.year;
							date = moment(date, "D-MMMM-YYYY").format("DD-MM-YYYY");
							if (isSlotAvaillbalefunction(date, from)) {
								timeNodes += "<div class='radio'><label><input type='radio' name='optionRadios' ng-click='getInput($event.target.value)' value='" + from + "' id='option" + id + "-" + index + "-" + i + "'><span>" + from + "</span><label></div>"
							}
						}
						else {
							from = moment(from, "HH:mm A").add(slot.slotInterval, 'minutes').format("hh:mm A");
							var date = $scope.datePicked + "-" + $scope.currMonth + "-" + $scope.year;
							date = moment(date, "D-MMMM-YYYY").format("DD-MM-YYYY");
							if (isSlotAvaillbalefunction(date, from)) {
								timeNodes += "<div class='radio'><label><input type='radio' name='optionRadios' ng-click='getInput($event.target.value)' value='" + from + "' id='option" + id + "-" + index + "-" + i + "'><span>" + from + "</span><label></div>"
							}
						}
						var toTime = moment(slot.toTime, "HH:mm").format("hh:mm A");
						i += 1;
						count += 1;
					}
					while (from != toTime);
					timeBooks += "<div class='time-book time-align'><h5 class='text-white text-uppercase' style='font-size:15px'>" + slot.sessionName + "</h5><br>" + timeNodes + "</div>";
				});
				var timeInfos = "<tr class='list-available' id='list-book-" + id + "'><td colspan='7' class='booked-list'><span class='close'><i class='fa fa-times-circle'></i></span><h4>Available Appointments on " + $scope.currMonth + "&nbsp" + id + ",&nbsp" + $scope.year + "</h4><div class='row'>" + timeBooks + "<span data-toggle='modal' ng-click='validateAppointment()' class='btn'>Book Appointment</span></div></td></tr>";
				angular.element($compile(timeInfos)($scope)).insertAfter(angular.element($event.currentTarget).parent('tr'));
			}
		}

	}]);

//ONBOARDING
App.controller('onBoardCtrl', ['$scope', '$http', '$state',
	'$stateParams', '$window',
	function ($scope, $http, $state, $stateParams, $window) {
		console.log("onBoardCtrl");
		//		console.log("$state.current  : "+$state.current)

		var ext_subList = {};
		var pUrl = '/ihspdc/rest/patientsToSubscribe';
		// TODO - set depId from co-od dep logged in
		var depId = 1001;
		var dUrl = '/ihspdc/rest/doctorsDep?depId=' + depId;
		if ($state.current.name == 'patientOnboard') {
			$state.subscribers = {}
			$http.get(pUrl).then(function (response, err) {
				if (!err) {
					console.log("subs RESPONSE:  ", response.data);
					var subList = response.data;
					if (subList != null && subList.length > 0) {
						angular.forEach(
							subList,
							function (sub) {
								if (sub.doctors == null || sub.doctors == undefined) {
									sub.doctors = [];
								} else {
									var items = sub.doctors;
									var docnames = items.map(function (item) {
										var nam = item.docId + " - " + item.docName;
										return nam;
									});
									sub.doctors = docnames;
									console.log("doctors sub");
									console.log(sub.doctors);
								}
							});

					}
					$scope.subList = subList;
					$state.subscribers = subList;
					ext_subList = angular.copy(subList); //COPY WITHOUT POINTING TO SAME OBJECT
				}
			});

			$http.get(dUrl).then(function (response, err) {
				if (!err) {
					console.log("docList RESPONSE:  ", response.data);
					$scope.docList = response.data;
					var items = $scope.docList;
					var docnames = items.map(function (item) {
						var nam = item.docId + " - " + item.docName;
						return nam;
					});
					console.log("docnames : ", docnames);
					$scope.docList = docnames;
					console.log("doclist");
					console.log($scope.docList);
				}
			});
		}

		$scope.changeDoc = function (doctors) {
			console.log("ngchange");
		}

		$scope.onNext = function () {
			console.log("onNext : ", $scope.subList);
			var subscribers = [];
			var subList = $scope.subList;
			for (subPos in subList) {
				var docs = subList[subPos].doctors; // array of docnames
				var subDocs = [];

				var unchanged = angular.equals(ext_subList[subPos].doctors, docs);
				if (docs != null && docs.length > 0 && !unchanged) {
					console.log("docs : ", docs)
					for (dp in docs) {
						if (!docs[dp] == "") {
							var nam = docs[dp].split(" - ");
							var doc = {
								"docId": nam[0],
								"docName": nam[1]
							}
							subDocs.push(doc);
						}
					}
					subList[subPos].doctors = subDocs;
					//TODO - add invitedBy coordinator obj
					subList[subPos].invitedBy = {
						"id": 1,
						"department": {
							"id": 1001,
							"name": "Liver",
							"details": "Liver"
						},
						"name": "hhgf"
					}
					subscribers.push(subList[subPos])
				}
			}
			$state.subscribers = subscribers;
			if (subscribers != null && subscribers.length > 0) {
				$state.go("patientOnboard2")
			} else {
				alert("Add doctors to proceed");
			}
		}

		$scope.subList = $state.subscribers;
		//		 console.log("subList from state : " , $scope.subList);

		// $scope.hasElem = function(elem){
		// if(elem!=null && elem.doctors!=null && elem.doctors.length>0){
		// return true;
		// }
		// return false;
		// }

		$scope.getDocs = function (items) {
			if (items != null) {
				var names = items.map(function (item) {
					return item.docName;
				});
				var names = names.join(" , ");
				return names;
			} else
				return null;
		}

		$scope.getPatients = function (items) {
			if (items != null) {
				var names = items.map(function (item) {
					return item.patient.patientName;
				});
				var names = names.join(" , ");
				return names;
			} else
				return null;
		}

		$scope.selectAll = function () {
			angular.forEach(
				$scope.subList,
				function (sub) {
					sub.selected = $scope.checkAllSub;
				});
		};

		$scope.checkOpt = function (val) {
			if (val == false) {
				$scope.checkAllSub = false;
			}
		}

		$scope.onThird = function () {
			var subList = $scope.subList;
			var subscribers = [];
			for (s in subList) {
				if (subList[s].selected == true) {
					subscribers.push(subList[s]);
				}
			}
			$state.subscribers = subscribers;
			console.log(" onThird subscribers : ", $state.subscribers);
			if (subscribers != null && subscribers.length > 0) {
				$state.go("patientOnboard3")
			} else {
				alert("Select records to proceed");
			}
			$scope.subList = $state.subscribers;
			console.log("sublist");
			console.log($scope.subList);
		}

		$scope.sendmail = function () {

			$http({
				url: '/ihspdc/rest/invitePatients',
				data: $scope.subList,
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				}
			}).then(function (response) {
				console.log("sendmail success");
			}, function (error) {
				console.log("sendmail error");
			}
			);

		}

	}]);
App.controller('HeaderCtrl', ['$scope',
	'$http',
	'$state',
	'$stateParams',
	'$window',
	'$timeout', '$rootScope', 'patientService', function ($scope, $http, $state,
		$stateParams, $window, $timeout, $rootScope, patientService) {

		// Init HighlightJS


		//to get the notifications for particular opcode and determine its length onLoad
		console.log("Role : " + $rootScope.Auth.tokenParsed.preferred_username);
		console.log($rootScope.Auth);
		console.log("her you want");
		notify = '/ihspdc/rest/getnotification?logInId='; //logInId - userName which is used to LogIn
		notify += $rootScope.Auth.tokenParsed.preferred_username;
		console.log(notify);
		$http.get(notify).then(function (response, err) {
			if (!err) {
				$scope.noti_length = 0;
				console.log("Notifications..!");
				$scope.notifications = response.data;
				console.log($scope.notifications);
				angular.forEach($scope.notifications, function (not) {
					$scope.noti_date = moment(not.time, 'DD-MM-YYYY').format('MMM DD');
					if (not.status == false) {
						$scope.noti_length += 1;
					}
				});
			}
		});

		$scope.goBack = function () {
			window.history.back();
		}

		patientService.getSenderInfo($rootScope.Auth.tokenParsed.preferred_username, function (err, response) {
			if (!err) {
				$scope.loggedInDetails = response.data;
			}
		});


		var deleteAllCookies = function () {
			var cookies = document.cookie.split(";");
			for (var i = 0; i < cookies.length; i++) {
				var cookie = cookies[i];
				var eqPos = cookie.indexOf("=");
				var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
				document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
			}
		}

		$scope.sessionLogOut = function () {
			deleteAllCookies();
			//To save patient's last login ( to find if he is active or inactive)
			if ($rootScope.Auth && ($rootScope.Auth.hasResourceRole('patient'))) {
				$scope.lastlogin = moment(new Date()).format("DD-MM-YYYY, h:mm:ss a");
				console.log("Curr login:" + $scope.lastlogin);
				$scope.list = {};
				console.log('patient-profile ctrl entered!');

				$scope.op = $stateParams.id;

				//Store last login - save or update
				var LastLogin = '/ihspdc/rest/saveLastLogin?patientId='
				LastLogin += $stateParams.id;
				LastLogin += "&lastLogin="
				LastLogin += $scope.lastlogin;

				$http({
					url: LastLogin,
					data: $scope.lastlogin,
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					}

				}).then(function (response) {
					console.log("success of save last login ");
					console.log(response);

				}, function (error) {
					console.log("error");

				}
				);
				$state.go('patientProfile', null, { notify: false }).then(function () {
					$rootScope.Auth.tokenParsed = {};
					$window.location.reload();
					$rootScope.Auth.logout();
				});
			} else {
				$state.go('patient', null, { notify: false }).then(function () {
					$rootScope.Auth.tokenParsed = {};
					$window.location.reload();
					$rootScope.Auth.logout();
				});
			}
		}

		//function called upon clicking the notification message
		$scope.redirectNotification = function (noti) {
			console.log(noti);
			$state.go('patientProfile', { id: noti.notifyFrom });
		}//END OF PATIENT

		//for LOG OUT - COMMON
		$scope.logout = function () {
			HttpServletRequest.logout();
		}

	}]); // end of HeaderCtrl

App.controller('sendMessageCtrl', ['$scope', '$http', '$timeout', 'patientService', '$rootScope', '$state', 'otherServices',
	function ($scope, $http, $timeout, patientService, $rootScope, $state, otherServices) {
		console.log("Send Message Control");
		if ($rootScope.Auth.hasResourceRole('coord') || $rootScope.Auth.hasResourceRole('doctor')) {
			//To search patient - DB level
			$scope.searchPatient = function (opCode) {
				if (opCode.length >= 4) {
					var getpat = '/ihspdc/rest/getPatientDetails?opCode='
					getpat += opCode;
					$http.get(getpat).then(function (response, err) {
						if (!err && response.data != "") {
							$scope.loader = false;
							$scope.activePatientsList = response.data;
							$scope.patientList = [];
							if ($rootScope.Auth.hasResourceRole('coord')) {
								if ($rootScope.Auth.tokenParsed.preferred_username.toUpperCase() == $scope.activePatientsList.invitedBy.id.toUpperCase()) {
									$scope.patientList.push($scope.activePatientsList);
								}
							}
							else {
								angular.forEach($scope.activePatientsList.doctors, function (docs) {
									if ($rootScope.Auth.tokenParsed.preferred_username.toUpperCase() == docs.docId.toUpperCase()) {
										$scope.patientList.push($scope.activePatientsList);
									}
								});
							}
							console.log("Patient" + $scope.patientList);
						} else {
							console.log('Error in searching patient');
						}
					});
				} else if (opCode.length == 0) {
					$scope.getActivePatientsList(0);
				}
			}
			//Patient selection to send message
			$scope.putInSendList = function (patRef, index) {
				if ($("#checkboxlocal" + index).is(':checked')) {
					$rootScope.selectedPatientsList.push(patRef.patient);
				}
				else {
					$rootScope.selectedPatientsList = $rootScope.selectedPatientsList.filter(function (pat) {
						return pat.opCode !== patRef.patient.opCode;
					});
				}
			}

			//Patients list
			$scope.getActivePatientsList = function (getNext, pageNo) {
				if (pageNo > 0 && pageNo <= $scope.pagesNeeded.length) {
					$scope.pageIn = pageNo;
					var getpat = '/ihspdc/rest/activePatientsDoctor?getNext=';
					getpat += getNext;
					getNext = "&id=";
					getNext += $rootScope.Auth.tokenParsed.preferred_username;
					$http.get(getpat).then(function (response, err) {
						if (!err && response.data != "") {
							response.data.sort(function (a, b) {
								return a.patient.fName.localeCompare(b.patient.fName);
							});
							$scope.activePatientsList = response.data;
							$scope.patientList = [];
							if ($rootScope.Auth.hasResourceRole('coord')) {
								angular.forEach($scope.activePatientsList, function (pat) {
									if ($rootScope.Auth.tokenParsed.preferred_username.toUpperCase() == pat.invitedBy.id.toUpperCase()) {
										$scope.patientList.push(pat);
									}
								});
							}
							else {
								angular.forEach($scope.activePatientsList, function (pat) {
									angular.forEach(pat.doctors, function (docs) {
										if ($rootScope.Auth.tokenParsed.preferred_username.toUpperCase() == docs.docId.toUpperCase()) {
											$scope.patientList.push(pat);
										}
									});
								});
							}
							console.log("List of Active Patients" + $scope.values);
						} else {
							console.log('Error in showing active patient list');
						}
					});
				}
			}
			//Get patient count for pagination
			patientService.getPatientCount(function (err, response) {
				if (!err) {
					var page = 0;
					$scope.pagesNeeded = [];
					if (response % 10 == 0) {
						page = response / 10;
					}
					else if (response % 10 != 0 && response <= 10) {
						page = 1;
					}
					else {
						page = Math.floor(response / 10) + 1;
					}
					for (i = 0; i < page; i++) {
						$scope.pagesNeeded.push(i + 1);
					}
				}
				if ($scope.pagesNeeded.length) {
					$scope.getActivePatientsList(0, 1);
				}
			});

			//Get Back
			$scope.goBack = function () {
				window.history.back();
			}

			//Check_all function
			$(document).ready(function () {
				$("#checkboxMaster").click(function () {
					$(".checkBoxClass").prop('checked', $(this).prop('checked'));
				});
			});

			//Hide alerts
			$scope.hideAlert = function () {
				$scope.sucMsg = false;
				$scope.errMsg = false;
			}

			$scope.setAllAsRecipients = function () {
				patientService.getFullPatientsList(function (err, response) {
					$scope.patientList = response.data;
				});
			}
			//Getting selected patients only
			$rootScope.selectedPatientsList = [];
			$scope.getSelectedToSend = function () {
				console.log($rootScope.selectedPatientsList);
				angular.forEach($scope.patientList, function (pat, index) {
					console.log($("#checkboxlocal" + index).is(':checked'));
					if ($("#checkboxlocal" + index).is(':checked')) {
						$rootScope.selectedPatientsList.push(pat.patient);
					}
				});
				$rootScope.selectedPatientsList = _.uniq($rootScope.selectedPatientsList, 'opCode');
				if ($rootScope.selectedPatientsList.length > 0) {
					$state.go("sendMessageStep2");
				}
				else {
					$("html, body").animate({ scrollTop: 0 }, 200);
					$scope.errMsg = true;
					$scope.errorMsg = "Select atleast one patient";
					jQuery("#errId").focus();
					jQuery("#errId").show();
				}
			}
		}
		//MARKETING DEPARTMENT
		else if ($rootScope.Auth.hasResourceRole('marketing')) {

			otherServices.getDepartmentCount(function (err, response) {
				if (!err) {
					var page = 0;
					$scope.pagesNeeded = [];
					if (response % 10 == 0) {
						page = response / 10;
					}
					else if (response % 10 != 0 && response <= 10) {
						page = 1;
					}
					else {
						page = Math.floor(response / 10) + 1;
					}
					for (i = 0; i < page; i++) {
						$scope.pagesNeeded.push(i);
					}
				}
			});

			$scope.getDepartmentList = function (numberOfRecords) {
				var department_Details = '/ihspdc/rest/getalldepartments?getNext=';
				department_Details += numberOfRecords;
				$http.get(department_Details).then(
					function (response, err) {
						if (!err && response.data != "") {
							response.data.sort(function (a, b) {
								return a.name.localeCompare(b.name);
							});
							$scope.allDepartments = response.data;
						} else {
							console.log('Error');
						}
					});
			}
			$scope.getDepartmentList(0);

			$rootScope.selectedDepartmentList = [];
			$scope.getSelectedDepartmentsToSend = function () {
				angular.forEach($scope.allDepartments, function (dep, index) {
					console.log($("#checkboxlocal" + index).is(':checked'));
					if ($("#checkboxlocal" + index).is(':checked')) {
						$rootScope.selectedDepartmentList.push(dep);
					}
				});
				$rootScope.selectedDepartmentList = _.uniq($rootScope.selectedDepartmentList, 'id');
				if ($rootScope.selectedDepartmentList.length > 0) {
					$state.go("sendMessageStep2");
				}
				else {
					$("html, body").animate({ scrollTop: 0 }, 200);
					$scope.errMsg = true;
					$scope.errorMsg = "Select atleast one department";
					jQuery("#errId").focus();
					jQuery("#errId").show();
				}
			}

			//Department selection to send message
			$scope.putInSendList = function (deptRef, index) {
				if ($("#checkboxlocal" + index).is(':checked')) {
					$rootScope.selectedDepartmentList.push(deptRef);
				}
				else {
					$rootScope.selectedDepartmentList = $rootScope.selectedDepartmentList.filter(function (dept) {
						return dept.id !== deptRef.id;
					});
				}
			}
		}
	}]);

App.controller('sendMessageStep2Ctrl', ['$scope', '$http', 'patientService', '$rootScope', '$timeout', '$state', 'otherServices',
	function ($scope, $http, patientService, $rootScope, $timeout, $state, otherServices) {

		//Uploading_Files_Associated_With_Message
		var messageDZ = new Dropzone("form#messageDropzone", { paramName: "inputfile", maxFilesize: 10, addRemoveLinks: true, parallelUploads: 15, maxFiles: 15, method: 'POST', url: "/ihspdc/rest/storefile", acceptedFiles: 'image/png,image/jpeg,image/jpg,application/pdf', dictDefaultMessage: 'Drop Specifications file' });
		var messageAssociatedFiles = [];
		messageDZ.on("queuecomplete", function (file, xhr, formData) {
			angular.forEach(messageDZ.files, function (file) {
				messageAssociatedFiles.push(file.xhr.response);
			});
		});
		messageDZ.on("removedfile", function (file) {
			if (removeFlag != true) {
				console.log("Trying to remove me?", file.xhr.response);
				rmFile = '/ihspdc/rest/removeFile?id='
				rmFile += file.xhr.response;
				$http.delete(rmFile);
			}
			removeFlag = false;
		});
		//End_Of_Uploading_Files_Associated_With_Message

		//Getting sender information
		var sender = [];
		if ($rootScope.Auth.hasResourceRole('coord') || $rootScope.Auth.hasResourceRole('doctor')) {
			patientService.getSenderInfo($rootScope.Auth.tokenParsed.preferred_username, function (err, response) {
				if (!err) {
					sender = response.data;
				}
			});
		}
		else if ($rootScope.Auth.hasResourceRole('marketing')) {
			sender = "Marketing Department";
		}

		//Hide Alerts
		$scope.hideAlert = function () {
			$scope.sucMsg = false;
			$scope.errMsg = false;
		}

		//Message Sending Process
		$scope.message = {};
		var removeFlag = false;
		var finalMessageContent = {};
		$scope.sendMessage = function () {
			if ($rootScope.Auth.hasResourceRole('coord') || $rootScope.Auth.hasResourceRole('doctor')) {
				if ($("#summerNotes").summernote('code').length >= 10 && $rootScope.selectedPatientsList && $rootScope.selectedPatientsList.length > 0 && typeof $scope.message.title !== 'undefined' && $scope.message.title != "") {
					console.log($("#summerNotes").summernote('code').length);
					var uniqueFileIds = [];
					var opCodes = [];
					finalMessageContent.links = [];
					angular.forEach($rootScope.selectedPatientsList, function (pat) {
						opCodes.push(pat.opCode.toUpperCase());
					});
					opCodes.push($rootScope.Auth.tokenParsed.preferred_username.toUpperCase()); // To have sender info
					finalMessageContent.receivedPatientsList = opCodes;
					finalMessageContent.messageTitle = $scope.message.title;
					finalMessageContent.timeOfSent = moment().format("MMMM Do YYYY, h:mm:ss a");
					angular.forEach($scope.message.linkTags, function (link) {
						finalMessageContent.links.push(link.text);
					});
					finalMessageContent.summerNotes = $("#summerNotes").summernote('code');
					if (messageAssociatedFiles && messageAssociatedFiles.length >= 1) {
						$.each(messageAssociatedFiles, function (i, el) {
							if ($.inArray(el, uniqueFileIds) === -1) uniqueFileIds.push(el);
						});
						finalMessageContent.files = uniqueFileIds;
					};

					finalMessageContent.senderOfMessage = JSON.stringify(sender);
					finalMessageContent.peopleRecived = opCodes.length;
					console.log(finalMessageContent);

					//MESSAGE SENDING - IT SHOULD SEND MAIL TO LOT OF PATIENTS
					$scope.hideAlert();
					$("html, body").animate({ scrollTop: 0 }, 200);
					$scope.sucMsg = true;
					if ((opCodes.length - 1) > 1) {
						$scope.successMsg = "Messages have been successfully sent to  " + (opCodes.length - 1) + " patients";
					}
					else {
						$scope.successMsg = "Message has been successfully sent";
					}
					jQuery("#successId").focus();
					jQuery("#successId").show();
					//MESSAGE SAVED ALERT INFO - ENDS 

					$http({
						url: '/ihspdc/rest/saveMessage',
						data: finalMessageContent,
						method: 'POST',
						headers: {
							'Content-Type': 'application/json'
						}
					}).then(function (response) {
						//After saving of message
						$rootScope.selectedPatientsList = [];
						$scope.message = {};
						removeFlag = true;
						messageDZ.removeAllFiles();
						messageAssociatedFiles = [];
						$timeout(function () { $state.go("viewMessage"); }, 6000); // remove it after testing
					}, function (error) {
						console.log("Error in Adding Patient");
					});

				} else {
					if ($rootScope.selectedPatientsList.length == 0) {
						$("html, body").animate({ scrollTop: 0 }, 200);
						$scope.errMsg = true;
						$scope.errorMsg = "Please select patient(s) to send.";
						jQuery("#errId").focus();
						jQuery("#errId").show();
						$timeout(function () { $state.go("sendMessageStep1"); }, 6000); // remove it after testing
					}
					else if ($("#summerNotes").summernote('code').length < 10) {
						$("html, body").animate({ scrollTop: 0 }, 200);
						$scope.errMsg = true;
						$scope.errorMsg = "Please type minimum of 10 charecters";
						jQuery("#errId").focus();
						jQuery("#errId").show();
					}
					else {
						$("html, body").animate({ scrollTop: 0 }, 200);
						$scope.errMsg = true;
						$scope.errorMsg = "Title is required";
						jQuery("#errId").focus();
						jQuery("#errId").show();
					}
				}
			}
			else if ($rootScope.Auth.hasResourceRole('marketing')) {
				if ($("#summerNotes").summernote('code').length >= 10 && $rootScope.selectedDepartmentList && $rootScope.selectedDepartmentList.length > 0 && typeof $scope.message.title !== 'undefined' && $scope.message.title != "") {
					console.log($("#summerNotes").summernote('code').length);
					var uniqueFileIds = [];
					var opCodes = [];
					finalMessageContent.links = [];
					finalMessageContent.receivedPatientsList = [];
					otherServices
					var doctorsDetails = '/ihspdc/rest/getDoctorsList'; // write rest call for getting all doctors without pagination method
					$http.get(doctorsDetails).then(function (response, err) {
						if (!err && response.data != "") {
							var allDoctors = response.data;
							angular.forEach($rootScope.selectedDepartmentList, function (dep) {
								angular.forEach(allDoctors, function (doc) {
									if (dep.name == doc.department.name) {
										finalMessageContent.receivedPatientsList.push(doc.docId.toUpperCase());
									}
								});
							});
							var coord_Details = '/ihspdc/rest/getCoordsList';
							$http.get(coord_Details).then(function (response, err) {
								if (!err) {
									var allCoords = response.data;
									angular.forEach($rootScope.selectedDepartmentList, function (dep) {
										angular.forEach(allCoords, function (cod) {
											if (dep.name == cod.department.name) {
												finalMessageContent.receivedPatientsList.push(cod.id.toUpperCase());
											}
										});
									});
									patientService.getDepartmentAssociatedPatients($rootScope.selectedDepartmentList, function (err, response) {
										if (!err) {
											opCodes = response;
											angular.forEach(response, function (op) {
												finalMessageContent.receivedPatientsList.push(op.toUpperCase());
											});
											finalMessageContent.receivedPatientsList.push($rootScope.Auth.tokenParsed.preferred_username.toUpperCase());
											if (finalMessageContent.receivedPatientsList.length > 0) {
												finalMessageContent.messageTitle = $scope.message.title;

												angular.forEach($scope.message.linkTags, function (link) {
													finalMessageContent.links.push(link.text);
												});

												finalMessageContent.summerNotes = $("#summerNotes").summernote('code');

												if (messageAssociatedFiles && messageAssociatedFiles.length >= 1) {
													$.each(messageAssociatedFiles, function (i, el) {
														if ($.inArray(el, uniqueFileIds) === -1) uniqueFileIds.push(el);
													});
													finalMessageContent.files = uniqueFileIds;
												};
												finalMessageContent.timeOfSent = moment().format("MMMM Do YYYY, h:mm:ss a");
												finalMessageContent.senderOfMessage = JSON.stringify(sender);
												finalMessageContent.peopleRecived = opCodes.length;

												//MESSAGE SENDING - IT SHOULD SEND MAIL TO LOT OF PATIENTS
												$scope.hideAlert();
												$("html, body").animate({ scrollTop: 0 }, 200);
												$scope.sucMsg = true;
												if ((opCodes.length - 1) > 1) {
													$scope.successMsg = "Message has been successfully sent to " + opCodes.length + " patients from " + $rootScope.selectedDepartmentList.length + " departments";
												}
												else {
													$scope.successMsg = "Message has been successfully sent";
												}
												jQuery("#successId").focus();
												jQuery("#successId").show();
												//MESSAGE SAVED ALERT INFO - ENDS 

												$http({
													url: '/ihspdc/rest/saveMessage',
													data: finalMessageContent,
													method: 'POST',
													headers: {
														'Content-Type': 'application/json'
													}
												}).then(function (response) {
													//After saving of message
													$scope.message = {};
													removeFlag = true;
													messageDZ.removeAllFiles();
													messageAssociatedFiles = [];
													$rootScope.selectedDepartmentList = [];
													$timeout(function () { $state.go("viewMessage"); }, 6000); // remove it after testing
												}, function (error) {
													console.log("Error in Sending Message!!");
												});
											}
										}
									});
								} else {
									console.log('Error');
								}
							});
						} else {
							console.log('Error');
						}
					});
				} else {
					if ($rootScope.selectedDepartmentList.length == 0) {
						$("html, body").animate({ scrollTop: 0 }, 200);
						$scope.errMsg = true;
						$scope.errorMsg = "Please select department(s) to send.";
						jQuery("#errId").focus();
						jQuery("#errId").show();
						$timeout(function () { $state.go("sendMessageStep1"); }, 6000); // remove it after testing
					}
					else if ($("#summerNotes").summernote('code').length < 10) {
						$("html, body").animate({ scrollTop: 0 }, 200);
						$scope.errMsg = true;
						$scope.errorMsg = "Please type minimum of 10 charecters";
						jQuery("#errId").focus();
						jQuery("#errId").show();
					}
					else {
						$("html, body").animate({ scrollTop: 0 }, 200);
						$scope.errMsg = true;
						$scope.errorMsg = "Title is required";
						jQuery("#errId").focus();
						jQuery("#errId").show();
					}
				}
			}
		}//End of Message sending process
	}]);
App.controller('viewMessageCtrl', ['$scope', '$http', 'messageService', '$rootScope', '$stateParams', '$window',
	function ($scope, $http, messageService, $rootScope, $stateParams, $window) {
		console.log("View Message Control");

		//RETRIVING FILE FUNCTION	
		$scope.getFile = function (id) {
			var gfile = '/ihspdc/rest/getFile?id=' + id;
			$('#modalForImage').modal('show');
			$('.imagepreview').attr('src', gfile);
		}

		$scope.viewMessages = [];
		var maxMessageLength = 0;

		if ($stateParams.from != 1) {
			$scope.fromDashBoard = $stateParams.from;
			messageService.viewAllMessages(maxMessageLength, 10, function (err, response) {
				if (!err) {
					$scope.showLoadMoreMessages = true;
					if (response.data.length == 0) {
						$scope.showLoadMoreMessages = false;
					}
					else {
						$scope.showLoadMoreMessages = true;
					}
					angular.forEach(response.data, function (message) {
						message.senderOfMessage = JSON.parse(message.senderOfMessage);
						$scope.viewMessages.push(message);
					});
					console.log("Message_Response", response.data);
				}
			});
		}
		else {
			console.log($stateParams.id, $stateParams.from);
			$scope.fromDashBoard = $stateParams.from;
			$scope.showLoadMoreMessages = false;
			messageService.viewAllMessages(0, 0, function (err, response) {
				if (!err) {
					angular.forEach(response.data, function (message) {
						message.senderOfMessage = JSON.parse(message.senderOfMessage);
						if (message._id.$oid == $stateParams.id) {
							$scope.viewMessages.push(message);
						}
					});
					console.log("Message_Response", $scope.viewMessages);
				}
			});
		}


		//READ-MORE_READ-LESS IMPLEMENTATION
		$scope.readMoreFunction = function ($event) {
			angular.element($event.currentTarget).children('span.readmore').toggle();
			angular.element($event.currentTarget).children('span.readless').toggle();
			angular.element($event.currentTarget).parent().parent().find('.msg-content-list').toggleClass('msg-content-short');
		}

		//LoadMore Function
		$scope.loadMoreMessages = function () {
			maxMessageLength += 10;
			messageService.viewAllMessages(maxMessageLength, 10, function (err, response) {
				if (!err) {
					$scope.showLoadMoreMessages = true;
					if (response.data.length == 0) {
						$scope.showLoadMoreMessages = false;
					}
					else {
						$scope.showLoadMoreMessages = true;
					}
					angular.forEach(response.data, function (message) {
						message.senderOfMessage = JSON.parse(message.senderOfMessage);
					});
					angular.forEach(response.data, function (msg) {
						$scope.viewMessages.push(msg);
					});
					console.log("Message_Response", response.data);
				}
			});
		}

		//ALERTS
		$scope.hideAlert = function () {
			$scope.sucMsg = false;
			$rootScope.errMsg = false;
		}
		$scope.throwErrorMsg = function (msg) {
			$scope.hideAlert();
			$("html, body").animate({ scrollTop: 0 }, 200);
			$rootScope.errMsg = true;
			$rootScope.errorMsg = msg;
			jQuery("#errId").focus();
			jQuery("#errId").show();
		}
		$scope.throwSuccessMsg = function (msg) {
			$scope.hideAlert();
			$("html, body").animate({ scrollTop: 0 }, 200);
			$rootScope.sucMsg = true;
			$rootScope.successMsg = msg;
			jQuery("#successId").focus();
			jQuery("#successId").show();
		}


		//DELETE MESSAGE PROCESS
		$scope.deleteMessage = function (msg) {
			if ($rootScope.Auth && ($rootScope.Auth.hasResourceRole('coord'))) {
				if (msg.senderOfMessage && msg.senderOfMessage.id != $rootScope.Auth.tokenParsed.preferred_username) {
					$scope.throwErrorMsg("You don't have permission to delete this message");
				}
				else {
					$scope.toBeDeleteMessage = msg;
					$("#alertModal").modal('show');
				}
			}
			else if ($rootScope.Auth && ($rootScope.Auth.hasResourceRole('doctor'))) {
				if (msg.senderOfMessage && msg.senderOfMessage.docId != $rootScope.Auth.tokenParsed.preferred_username) {
					$scope.throwErrorMsg("You don't have permission to delete this message");
				}
				else {
					$scope.toBeDeleteMessage = msg;
					$("#alertModal").modal('show');
				}
			}
			else if ($rootScope.Auth && ($rootScope.Auth.hasResourceRole('marketing'))) {
				if (msg.senderOfMessage != "Marketing Department") {
					$scope.throwErrorMsg("You don't have permission to delete this message");
				}
				else {
					$scope.toBeDeleteMessage = msg;
					$("#alertModal").modal('show');
				}
			}
		}
		$scope.deleteMessageSure = function () {
			messageService.deleteByMessageId($scope.toBeDeleteMessage._id.$oid, function (err, response) {
				if (!err) {
					$scope.throwSuccessMsg("Message has been successfully deleted");
					messageService.viewAllMessages(0, 10, function (err, response) {
						if (!err) {
							$scope.showLoadMoreMessages = true;
							$scope.viewMessages = [];
							if (response.data.length == 0) {
								$scope.showLoadMoreMessages = false;
							}
							if (response.data.length <= 10) {
								$scope.showLoadMoreMessages = false;
							}
							angular.forEach(response.data, function (message) {
								message.senderOfMessage = JSON.parse(message.senderOfMessage);
							});
							angular.forEach(response.data, function (msg) {
								$scope.viewMessages.push(msg);
							});
							console.log("Message_Response", response.data);
						}
					});
				} else {
					console.log(err);
				}
			});
			$scope.toBeDeleteMessage = {};
		}


	}]);

// UI Elements Activity Controller
App.controller(
	'UiActivityCtrl',
	[
		'$scope',

		'$window',
		function ($scope, $window) {
			// Preview page loader
			$scope.previewPageLoader = function () {
				$scope.helpers.uiLoader('show');

				setTimeout(function () {
					$scope.helpers.uiLoader('hide');
				}, 3000);
			};

			// Randomize progress bars values
			var barsRandomize = function () {
				jQuery('.js-bar-randomize')
					.on(
						'click',
						function () {
							jQuery(this)
								.parents('.block')
								.find(
									'.progress-bar')
								.each(
									function () {
										var el = jQuery(this);
										var random = Math
											.floor((Math
												.random() * 91) + 10)
											+ '%';

										el
											.css(
												'width',
												random);

										if (!el
											.parent()
											.hasClass(
												'progress-mini')) {
											el
												.html(random);
										}
									});
						});
			};

			// SweetAlert, for more examples you can check out
			// https://github.com/t4t5/sweetalert
			var sweetAlert = function () {
				// Init a simple alert on button click
				jQuery('.js-swal-alert')
					.on(
						'click',
						function () {
							swal('Hi, this is a simple alert!');
						});

				// Init an success alert on button click
				jQuery('.js-swal-success')
					.on('click', function () {
						swal(
							'Success',
							'Everything updated perfectly!',
							'success');
					});

				// Init an error alert on button click
				jQuery('.js-swal-error').on(
					'click',
					function () {
						swal('Oops...',
							'Something went wrong!',
							'error');
					});

				// Init an example confirm alert on button click
				jQuery('.js-swal-confirm')
					.on(
						'click',
						function () {
							swal(
								{
									title: 'Are you sure?',
									text: 'You will not be able to recover this imaginary file!',
									type: 'warning',
									showCancelButton: true,
									confirmButtonColor: '#d26a5c',
									confirmButtonText: 'Yes, delete it!',
									html: false,
									preConfirm: function () {
										return new Promise(
											function (
												resolve) {
												setTimeout(
													function () {
														resolve();
													},
													50);
											});
									}
								})
								.then(
									function (
										result) {
										swal(
											'Deleted!',
											'Your imaginary file has been deleted.',
											'success');
									},
									function (
										dismiss) {
										// dismiss
										// can
										// be
										// 'cancel',
										// 'overlay',
										// 'esc'
										// or
										// 'timer'
									});
						});
			};

			// Init randomize bar values
			barsRandomize();

			// Init SweetAlert
			sweetAlert();
		}]);

// UI Elements Chat Controller
App.controller(
	'UiChatCtrl',
	[
		'$scope',

		'$window',
		function ($scope, $window) {
			// Helper variables - set in initChat()
			var lWindow, lHeader, lFooter, cContainer, cHead, cTalk, cPeople, cform, cTimeout;

			// Init chat
			var initChat = function () {
				// Set variables
				lWindow = jQuery(window);
				lHeader = jQuery('#header-navbar');
				lFooter = jQuery('#page-footer');
				cContainer = jQuery('.js-chat-container');
				cHead = jQuery('.js-chat-head');
				cTalk = jQuery('.js-chat-talk');
				cPeople = jQuery('.js-chat-people');
				cform = jQuery('.js-chat-form');

				// Add word wraping to chat content
				cTalk.css('word-wrap', 'break-word');

				// Chat layout mode
				switch (cContainer.data('chat-mode')) {
					case 'full':
						// Init chat windows' height
						initChatWindows();

						// ..also on browser resize or orientation
						// change
						jQuery(window).on(
							'resize orientationchange',
							function () {
								clearTimeout(cTimeout);

								cTimeout = setTimeout(
									function () {
										initChatWindows();
									}, 150);
							});
						break;
					case 'fixed':
						// Init chat windows' height with a specific
						// height
						initChatWindows(cContainer
							.data('chat-height'));
						break;
					case 'popup':
						// Init chat windows' height with a specific
						// height
						initChatWindows(cContainer
							.data('chat-height'));

						// Adjust chat container
						cContainer.css({
							'position': 'fixed',
							'right': '10px',
							'bottom': 0,
							'display': 'inline-block',
							'padding': 0,
							'width': '70%',
							'max-width': '420px',
							'min-width': '300px',
							'z-index': '1031'
						});
						break;
					default:
						return false;
				}

				// Enable scroll lock to chat talk window
				cTalk.scrollLock('enable');

				// Init form submission
				cform.on('submit', function (e) {
					// Stop form submission
					e.preventDefault();

					// Get chat input
					var chatInput = jQuery('.js-chat-input',
						jQuery(this));

					// Add message
					chatAddMessage(chatInput
						.data('target-chat-id'), chatInput
							.val(), 'self', chatInput);
				});
			};

			// Init chat windows' height
			var initChatWindows = function (customHeight) {
				if (customHeight) {
					cHeight = customHeight;
				} else {
					// Calculate height
					var cHeight = lWindow.height()
						- lHeader.outerHeight()
						- lFooter.outerHeight()
						- cHead.outerHeight()
						- (parseInt(cContainer
							.css('padding-top')) + parseInt(cContainer
								.css('padding-bottom')));

					// Add a minimum height
					if (cHeight < 200) {
						cHeight = 200;
					}
				}

				// Set height to chat windows (+ people window
				// if exists)
				if (cPeople) {
					cPeople.css('height', cHeight);
				}

				cTalk.css('height', cHeight
					- cform.outerHeight());
			};

			// Add a message to a chat window
			var chatAddMessage = function (chatId, chatMsg,
				chatMsgLevel, chatInput) {
				// Get chat window
				var chatWindow = jQuery('.js-chat-talk[data-chat-id="'
					+ chatId + '"]');

				// If message and chat window exists
				if (chatMsg && chatWindow.length) {
					var chatBlockClasses = 'animated fadeIn push-50-l';
					var chatMsgClasses = 'bg-gray-lighter';

					// Post it to its related window (if message
					// level is 'self', make it stand out)
					if (chatMsgLevel === 'self') {
						chatBlockClasses = 'animated fadeInUp push-50-r';
						chatMsgClasses = 'bg-gray-light';
					}

					chatWindow
						.append('<div class="block block-rounded block-transparent push-15 '
							+ chatBlockClasses
							+ '">'
							+ '<div class="block-content block-content-full block-content-mini '
							+ chatMsgClasses
							+ '">'
							+ jQuery('<div />').text(
								chatMsg).html()
							+ '</div>' + '</div>');

					// Scroll the message list to the bottom
					chatWindow.animate({
						scrollTop: chatWindow[0].scrollHeight
					}, 150);

					// If input is set, reset it
					if (chatInput) {
						chatInput.val("");
					}
				}
			};

			// Init chat
			initChat();

			// Add Message
			$scope.addMessage = function (chatId, chatMsg,
				chatMsgLevel) {
				chatAddMessage(chatId, chatMsg, chatMsgLevel,
					false);
			};
		}]);

// Tables DataTables Controller
App.controller(
	'TablesDatatablesCtrl',
	[
		'$scope',

		'$window',
		function ($scope, $window) {
			// Init full DataTable, for more examples you can
			// check out https://www.datatables.net/
			var initDataTableFull = function () {
				jQuery('.js-dataTable-full').dataTable(
					{
						columnDefs: [{
							orderable: false,
							targets: [4]
						}],
						pageLength: 10,
						lengthMenu: [[5, 10, 15, 20],
						[5, 10, 15, 20]]
					});
			};

			// Init simple DataTable, for more examples you can
			// check out https://www.datatables.net/
			var initDataTableSimple = function () {
				jQuery('.js-dataTable-simple')
					.dataTable(
						{
							columnDefs: [{
								orderable: false,
								targets: [4]
							}],
							pageLength: 10,
							lengthMenu: [
								[5, 10, 15, 20],
								[5, 10, 15, 20]],
							searching: false,
							oLanguage: {
								sLengthMenu: ""
							},
							dom: "<'row'<'col-sm-12'tr>>"
								+ "<'row'<'col-sm-6'i><'col-sm-6'p>>"
						});
			};

			// DataTables Bootstrap integration
			var bsDataTables = function () {
				var DataTable = jQuery.fn.dataTable;

				// Set the defaults for DataTables init
				jQuery
					.extend(
						true,
						DataTable.defaults,
						{
							dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>"
								+ "<'row'<'col-sm-12'tr>>"
								+ "<'row'<'col-sm-6'i><'col-sm-6'p>>",
							renderer: 'bootstrap',
							oLanguage: {
								sLengthMenu: "_MENU_",
								sInfo: "Showing <strong>_START_</strong>-<strong>_END_</strong> of <strong>_TOTAL_</strong>",
								oPaginate: {
									sPrevious: '<i class="fa fa-angle-left"></i>',
									sNext: '<i class="fa fa-angle-right"></i>'
								}
							}
						});

				// Default class modification
				jQuery
					.extend(
						DataTable.ext.classes,
						{
							sWrapper: "dataTables_wrapper form-inline dt-bootstrap",
							sFilterInput: "form-control",
							sLengthSelect: "form-control"
						});

				// Bootstrap paging button renderer
				DataTable.ext.renderer.pageButton.bootstrap = function (
					settings, host, idx, buttons, page,
					pages) {
					var api = new DataTable.Api(settings);
					var classes = settings.oClasses;
					var lang = settings.oLanguage.oPaginate;
					var btnDisplay, btnClass;

					var attach = function (container, buttons) {
						var i, ien, node, button;
						var clickHandler = function (e) {
							e.preventDefault();
							if (!jQuery(e.currentTarget)
								.hasClass('disabled')) {
								api.page(e.data.action).draw(
									false);
							}
						};

						for (i = 0, ien = buttons.length; i < ien; i++) {
							button = buttons[i];

							if (jQuery.isArray(button)) {
								attach(container, button);
							} else {
								btnDisplay = "";
								btnClass = "";

								switch (button) {
									case 'ellipsis':
										btnDisplay = '&hellip;';
										btnClass = 'disabled';
										break;

									case 'first':
										btnDisplay = lang.sFirst;
										btnClass = button
											+ (page > 0 ? ""
												: ' disabled');
										break;

									case 'previous':
										btnDisplay = lang.sPrevious;
										btnClass = button
											+ (page > 0 ? ""
												: ' disabled');
										break;

									case 'next':
										btnDisplay = lang.sNext;
										btnClass = button
											+ (page < pages - 1 ? ""
												: ' disabled');
										break;

									case 'last':
										btnDisplay = lang.sLast;
										btnClass = button
											+ (page < pages - 1 ? ""
												: ' disabled');
										break;

									default:
										btnDisplay = button + 1;
										btnClass = page === button ? 'active'
											: "";
										break;
								}

								if (btnDisplay) {
									node = jQuery(
										'<li>',
										{
											'class': classes.sPageButton
												+ ' '
												+ btnClass,
											'aria-controls': settings.sTableId,
											'tabindex': settings.iTabIndex,
											'id': idx === 0
												&& typeof button === 'string' ? settings.sTableId
												+ '_'
												+ button
												: null
										})
										.append(
											jQuery(
												'<a>',
												{
													'href': '#'
												})
												.html(
													btnDisplay))
										.appendTo(container);

									settings.oApi
										._fnBindAction(
											node,
											{
												action: button
											},
											clickHandler);
								}
							}
						}
					};

					attach(jQuery(host).empty().html(
						'<ul class="pagination"/>')
						.children('ul'), buttons);
				};

				// TableTools Bootstrap compatibility - Required
				// TableTools 2.1+
				if (DataTable.TableTools) {
					// Set the classes that TableTools uses to
					// something suitable for Bootstrap
					jQuery
						.extend(
							true,
							DataTable.TableTools.classes,
							{
								"container": "DTTT btn-group",
								"buttons": {
									"normal": "btn btn-default",
									"disabled": "disabled"
								},
								"collection": {
									"container": "DTTT_dropdown dropdown-menu",
									"buttons": {
										"normal": "",
										"disabled": "disabled"
									}
								},
								"print": {
									"info": "DTTT_print_info"
								},
								"select": {
									"row": "active"
								}
							});

					// Have the collection use a bootstrap
					// compatible drop down
					jQuery
						.extend(
							true,
							DataTable.TableTools.DEFAULTS.oTags,
							{
								"collection": {
									"container": "ul",
									"button": "li",
									"liner": "a"
								}
							});
				}
			};

			// Init Datatables
			bsDataTables();
			initDataTableSimple();
			initDataTableFull();
		}]);

// Forms Validation Controller
App.controller(
	'FormsValidationCtrl',
	[
		'$scope',

		'$window',
		function ($scope, $window) {
			// Init Bootstrap Forms Validation, for more
			// examples you can check out
			// https://github.com/jzaefferer/jquery-validation
			var initValidationBootstrap = function () {
				jQuery('.js-validation-bootstrap')
					.validate(
						{
							ignore: [],
							errorClass: 'help-block animated fadeInDown',
							errorElement: 'div',
							errorPlacement: function (
								error, e) {
								jQuery(e)
									.parents(
										'.form-group > div')
									.append(error);
							},
							highlight: function (e) {
								var elem = jQuery(e);

								elem
									.closest(
										'.form-group')
									.removeClass(
										'has-error')
									.addClass(
										'has-error');
								elem.closest(
									'.help-block')
									.remove();
							},
							success: function (e) {
								var elem = jQuery(e);

								elem
									.closest(
										'.form-group')
									.removeClass(
										'has-error');
								elem.closest(
									'.help-block')
									.remove();
							},
							rules: {
								'val-username': {
									required: true,
									minlength: 3
								},
								'val-email': {
									required: true,
									email: true
								},
								'val-password': {
									required: true,
									minlength: 5
								},
								'val-confirm-password': {
									required: true,
									equalTo: '#val-password'
								},
								'val-select2': {
									required: true
								},
								'val-select2-multiple': {
									required: true,
									minlength: 2
								},
								'val-suggestions': {
									required: true,
									minlength: 5
								},
								'val-skill': {
									required: true
								},
								'val-currency': {
									required: true,
									currency: ['$',
										true]
								},
								'val-website': {
									required: true,
									url: true
								},
								'val-phoneus': {
									required: true,
									phoneUS: true
								},
								'val-digits': {
									required: true,
									digits: true
								},
								'val-number': {
									required: true,
									number: true
								},
								'val-range': {
									required: true,
									range: [1, 5]
								},
								'val-terms': {
									required: true
								}
							},
							messages: {
								'val-username': {
									required: 'Please enter a username',
									minlength: 'Your username must consist of at least 3 characters'
								},
								'val-email': 'Please enter a valid email address',
								'val-password': {
									required: 'Please provide a password',
									minlength: 'Your password must be at least 5 characters long'
								},
								'val-confirm-password': {
									required: 'Please provide a password',
									minlength: 'Your password must be at least 5 characters long',
									equalTo: 'Please enter the same password as above'
								},
								'val-select2': 'Please select a value!',
								'val-select2-multiple': 'Please select at least 2 values!',
								'val-suggestions': 'What can we do to become better?',
								'val-skill': 'Please select a skill!',
								'val-currency': 'Please enter a price!',
								'val-website': 'Please enter your website!',
								'val-phoneus': 'Please enter a US phone!',
								'val-digits': 'Please enter only digits!',
								'val-number': 'Please enter a number!',
								'val-range': 'Please enter a number between 1 and 5!',
								'val-terms': 'You must agree to the service terms!'
							}
						});
			};

			// Init Material Forms Validation, for more examples
			// you can check out
			// https://github.com/jzaefferer/jquery-validation
			var initValidationMaterial = function () {
				jQuery('.js-validation-material')
					.validate(
						{
							ignore: [],
							errorClass: 'help-block text-right animated fadeInDown',
							errorElement: 'div',
							errorPlacement: function (
								error, e) {
								jQuery(e)
									.parents(
										'.form-group > div')
									.append(error);
							},
							highlight: function (e) {
								var elem = jQuery(e);

								elem
									.closest(
										'.form-group')
									.removeClass(
										'has-error')
									.addClass(
										'has-error');
								elem.closest(
									'.help-block')
									.remove();
							},
							success: function (e) {
								var elem = jQuery(e);

								elem
									.closest(
										'.form-group')
									.removeClass(
										'has-error');
								elem.closest(
									'.help-block')
									.remove();
							},
							rules: {
								'val-username2': {
									required: true,
									minlength: 3
								},
								'val-email2': {
									required: true,
									email: true
								},
								'val-password2': {
									required: true,
									minlength: 5
								},
								'val-confirm-password2': {
									required: true,
									equalTo: '#val-password2'
								},
								'val-select22': {
									required: true
								},
								'val-select2-multiple2': {
									required: true,
									minlength: 2
								},
								'val-suggestions2': {
									required: true,
									minlength: 5
								},
								'val-skill2': {
									required: true
								},
								'val-currency2': {
									required: true,
									currency: ['$',
										true]
								},
								'val-website2': {
									required: true,
									url: true
								},
								'val-phoneus2': {
									required: true,
									phoneUS: true
								},
								'val-digits2': {
									required: true,
									digits: true
								},
								'val-number2': {
									required: true,
									number: true
								},
								'val-range2': {
									required: true,
									range: [1, 5]
								},
								'val-terms2': {
									required: true
								}
							},
							messages: {
								'val-username2': {
									required: 'Please enter a username',
									minlength: 'Your username must consist of at least 3 characters'
								},
								'val-email2': 'Please enter a valid email address',
								'val-password2': {
									required: 'Please provide a password',
									minlength: 'Your password must be at least 5 characters long'
								},
								'val-confirm-password2': {
									required: 'Please provide a password',
									minlength: 'Your password must be at least 5 characters long',
									equalTo: 'Please enter the same password as above'
								},
								'val-select22': 'Please select a value!',
								'val-select2-multiple2': 'Please select at least 2 values!',
								'val-suggestions2': 'What can we do to become better?',
								'val-skill2': 'Please select a skill!',
								'val-currency2': 'Please enter a price!',
								'val-website2': 'Please enter your website!',
								'val-phoneus2': 'Please enter a US phone!',
								'val-digits2': 'Please enter only digits!',
								'val-number2': 'Please enter a number!',
								'val-range2': 'Please enter a number between 1 and 5!',
								'val-terms2': 'You must agree to the service terms!'
							}
						});
			};

			// Init Bootstrap Forms Validation
			initValidationBootstrap();

			// Init Material Forms Validation
			initValidationMaterial();

			// Init Validation on Select2 change
			jQuery('[data-js-select2]').on('change',
				function () {
					jQuery(this).valid();
				});
		}]);

App.controller('superAdminCtrl', [
	'$scope',
	'$http',

	'$state',
	'$stateParams',
	'$window', '$rootScope',
	function ($scope, $http, $state,
		$stateParams, $window, $rootScope) {

		var coord_Details = '/ihspdc/rest/getCoordsList';
		$http.get(coord_Details).then(
			function (response, err) {
				if (!err) {
					$scope.coordDet = response.data;
					angular.forEach($scope.coordDet, function (cDet) {
						if (cDet.id == $rootScope.Auth.tokenParsed.preferred_username) {
							$scope.coordDepartment = cDet.department;
						}
					})
				} else {
					console.log('Error');
				}
			});

	}

]);

// Forms Wizard Controller
App.controller(
	'FormsWizardCtrl',
	[
		'$scope',

		'$window',
		function ($scope, $window) {
			// Init simple wizard, for more examples you can
			// check out
			// http://vadimg.com/twitter-bootstrap-wizard-example/
			var initWizardSimple = function () {
				jQuery('.js-wizard-simple')
					.bootstrapWizard(
						{
							'tabClass': "",
							'firstSelector': '.wizard-first',
							'previousSelector': '.wizard-prev',
							'nextSelector': '.wizard-next',
							'lastSelector': '.wizard-last',
							'onTabShow': function (tab,
								navigation, index) {
								var total = navigation
									.find('li').length;
								var current = index + 1;
								var percent = (current / total) * 100;

								// Get vital wizard
								// elements
								var wizard = navigation
									.parents('.block');
								var progress = wizard
									.find('.wizard-progress > .progress-bar');
								var btnPrev = wizard
									.find('.wizard-prev');
								var btnNext = wizard
									.find('.wizard-next');
								var btnFinish = wizard
									.find('.wizard-finish');

								// Update progress bar
								// if there is one
								if (progress) {
									progress.css({
										width: percent
											+ '%'
									});
								}

								// If it's the last tab
								// then hide the last
								// button and show the
								// finish instead
								if (current >= total) {
									btnNext.hide();
									btnFinish.show();
								} else {
									btnNext.show();
									btnFinish.hide();
								}
							}
						});
			};

			// Init wizards with validation, for more examples
			// you can check out
			// http://vadimg.com/twitter-bootstrap-wizard-example/
			var initWizardValidation = function () {
				// Get forms
				var form1 = jQuery('.js-form1');
				var form2 = jQuery('.js-form2');

				// Prevent forms from submitting on enter key
				// press
				form1.add(form2).on('keyup keypress',
					function (e) {
						var code = e.keyCode || e.which;

						if (code === 13) {
							e.preventDefault();
							return false;
						}
					});

				// Init form validation on classic wizard form
				var validator1 = form1
					.validate({
						errorClass: 'help-block animated fadeInDown',
						errorElement: 'div',
						errorPlacement: function (error, e) {
							jQuery(e).parents(
								'.form-group > div')
								.append(error);
						},
						highlight: function (e) {
							jQuery(e)
								.closest('.form-group')
								.removeClass(
									'has-error')
								.addClass('has-error');
							jQuery(e)
								.closest('.help-block')
								.remove();
						},
						success: function (e) {
							jQuery(e)
								.closest('.form-group')
								.removeClass(
									'has-error');
							jQuery(e)
								.closest('.help-block')
								.remove();
						},
						rules: {
							'validation-classic-firstname': {
								required: true,
								minlength: 2
							},
							'validation-classic-lastname': {
								required: true,
								minlength: 2
							},
							'validation-classic-email': {
								required: true,
								email: true
							},
							'validation-classic-details': {
								required: true,
								minlength: 5
							},
							'validation-classic-city': {
								required: true
							},
							'validation-classic-skills': {
								required: true
							},
							'validation-classic-terms': {
								required: true
							}
						},
						messages: {
							'validation-classic-firstname': {
								required: 'Please enter a firstname',
								minlength: 'Your firtname must consist of at least 2 characters'
							},
							'validation-classic-lastname': {
								required: 'Please enter a lastname',
								minlength: 'Your lastname must consist of at least 2 characters'
							},
							'validation-classic-email': 'Please enter a valid email address',
							'validation-classic-details': 'Let us know a few thing about yourself',
							'validation-classic-skills': 'Please select a skill!',
							'validation-classic-terms': 'You must agree to the service terms!'
						}
					});

				// Init form validation on the other wizard form
				var validator2 = form2
					.validate({
						errorClass: 'help-block text-right animated fadeInDown',
						errorElement: 'div',
						errorPlacement: function (error, e) {
							jQuery(e).parents(
								'.form-group > div')
								.append(error);
						},
						highlight: function (e) {
							jQuery(e)
								.closest('.form-group')
								.removeClass(
									'has-error')
								.addClass('has-error');
							jQuery(e)
								.closest('.help-block')
								.remove();
						},
						success: function (e) {
							jQuery(e)
								.closest('.form-group')
								.removeClass(
									'has-error');
							jQuery(e)
								.closest('.help-block')
								.remove();
						},
						rules: {
							'validation-firstname': {
								required: true,
								minlength: 2
							},
							'validation-lastname': {
								required: true,
								minlength: 2
							},
							'validation-email': {
								required: true,
								email: true
							},
							'validation-details': {
								required: true,
								minlength: 5
							},
							'validation-city': {
								required: true
							},
							'validation-skills': {
								required: true
							},
							'validation-terms': {
								required: true
							}
						},
						messages: {
							'validation-firstname': {
								required: 'Please enter a firstname',
								minlength: 'Your firtname must consist of at least 2 characters'
							},
							'validation-lastname': {
								required: 'Please enter a lastname',
								minlength: 'Your lastname must consist of at least 2 characters'
							},
							'validation-email': 'Please enter a valid email address',
							'validation-details': 'Let us know a few thing about yourself',
							'validation-skills': 'Please select a skill!',
							'validation-terms': 'You must agree to the service terms!'
						}
					});

				// Init classic wizard with validation
				jQuery('.js-wizard-classic-validation')
					.bootstrapWizard(
						{
							'tabClass': "",
							'previousSelector': '.wizard-prev',
							'nextSelector': '.wizard-next',
							'onTabShow': function (tab,
								nav, index) {
								var total = nav
									.find('li').length;
								var current = index + 1;

								// Get vital wizard
								// elements
								var wizard = nav
									.parents('.block');
								var btnNext = wizard
									.find('.wizard-next');
								var btnFinish = wizard
									.find('.wizard-finish');

								// If it's the last tab
								// then hide the last
								// button and show the
								// finish instead
								if (current >= total) {
									btnNext.hide();
									btnFinish.show();
								} else {
									btnNext.show();
									btnFinish.hide();
								}
							},
							'onNext': function (tab,
								navigation, index) {
								var valid = form1
									.valid();

								if (!valid) {
									validator1
										.focusInvalid();

									return false;
								}
							},
							onTabClick: function (tab,
								navigation, index) {
								return false;
							}
						});

				// Init wizard with validation
				jQuery('.js-wizard-validation')
					.bootstrapWizard(
						{
							'tabClass': "",
							'previousSelector': '.wizard-prev',
							'nextSelector': '.wizard-next',
							'onTabShow': function (tab,
								nav, index) {
								var total = nav
									.find('li').length;
								var current = index + 1;

								// Get vital wizard
								// elements
								var wizard = nav
									.parents('.block');
								var btnNext = wizard
									.find('.wizard-next');
								var btnFinish = wizard
									.find('.wizard-finish');

								// If it's the last tab
								// then hide the last
								// button and show the
								// finish instead
								if (current >= total) {
									btnNext.hide();
									btnFinish.show();
								} else {
									btnNext.show();
									btnFinish.hide();
								}
							},
							'onNext': function (tab,
								navigation, index) {
								var valid = form2
									.valid();

								if (!valid) {
									validator2
										.focusInvalid();

									return false;
								}
							},
							onTabClick: function (tab,
								navigation, index) {
								return false;
							}
						});
			};

			// Init simple wizard
			initWizardSimple();

			// Init wizards with validation
			initWizardValidation();
		}]);

// Components Charts Controller
App.controller(
	'CompChartsCtrl',
	[
		'$scope',

		'$window',
		function ($scope, $window) {
			// Chart.js Charts, for more examples you can check
			// out http://www.chartjs.org/docs
			var initChartsChartJS = function () {
				// Get Chart Containers
				var chartLinesCon = jQuery('.js-chartjs-lines')[0]
					.getContext('2d');
				var chartBarsCon = jQuery('.js-chartjs-bars')[0]
					.getContext('2d');
				var chartRadarCon = jQuery('.js-chartjs-radar')[0]
					.getContext('2d');

				// Set Chart and Chart Data variables
				var chartLines, chartBars, chartRadar;
				var chartLinesBarsRadarData;

				// Set global chart options
				var globalOptions = {
					scaleFontFamily: "'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif",
					scaleFontColor: '#999',
					scaleFontStyle: '600',
					tooltipTitleFontFamily: "'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif",
					tooltipCornerRadius: 3,
					maintainAspectRatio: false,
					responsive: true
				};

				// Lines/Bar/Radar Chart Data
				var chartLinesBarsRadarData = {
					labels: ['MON', 'TUE', 'WED', 'THU',
						'FRI', 'SAT', 'SUN'],
					datasets: [
						{
							label: 'Last Week',
							fillColor: 'rgba(220,220,220,.3)',
							strokeColor: 'rgba(220,220,220,1)',
							pointColor: 'rgba(220,220,220,1)',
							pointStrokeColor: '#fff',
							pointHighlightFill: '#fff',
							pointHighlightStroke: 'rgba(220,220,220,1)',
							data: [30, 32, 40, 45, 43,
								38, 55]
						},
						{
							label: 'This Week',
							fillColor: 'rgba(171, 227, 125, .3)',
							strokeColor: 'rgba(171, 227, 125, 1)',
							pointColor: 'rgba(171, 227, 125, 1)',
							pointStrokeColor: '#fff',
							pointHighlightFill: '#fff',
							pointHighlightStroke: 'rgba(171, 227, 125, 1)',
							data: [15, 16, 20, 25, 23,
								25, 32]
						}]
				};

				// Init Charts
				chartLines = new Chart(chartLinesCon).Line(
					chartLinesBarsRadarData, globalOptions);
				chartBars = new Chart(chartBarsCon).Bar(
					chartLinesBarsRadarData, globalOptions);
				chartRadar = new Chart(chartRadarCon).Radar(
					chartLinesBarsRadarData, globalOptions);
			};

			/*
			 * // jQuery Sparkline Charts, for more examples you can check
			 * out http://omnipotent.net/jquery.sparkline/#s-docs var
			 * initChartsSparkline = function(){ // Bar Charts var
			 * barOptions = { type: 'bar', barWidth: 8, barSpacing: 6,
			 * height: '70px', barColor: '#fadb7d', tooltipPrefix: "",
			 * tooltipSuffix: ' Tickets', tooltipFormat:
			 * '{{prefix}}{{value}}{{suffix}}' };
			 * jQuery('.js-slc-bar1').sparkline('html', barOptions);
			 * 
			 * barOptions['barColor'] = '#abe37d';
			 * barOptions['tooltipPrefix'] = '$ ';
			 * barOptions['tooltipSuffix'] = "";
			 * jQuery('.js-slc-bar2').sparkline('html', barOptions);
			 * 
			 * barOptions['barColor'] = '#faad7d';
			 * barOptions['tooltipPrefix'] = ""; barOptions['tooltipSuffix'] = '
			 * Sales'; jQuery('.js-slc-bar3').sparkline('html', barOptions); //
			 * Line Charts var lineOptions = { type: 'line', width: '120px',
			 * height: '70px', tooltipOffsetX: -25, tooltipOffsetY: 20,
			 * lineColor: '#fadb7d', fillColor: '#fadb7d', spotColor:
			 * '#777777', minSpotColor: '#777777', maxSpotColor: '#777777',
			 * highlightSpotColor: '#777777', highlightLineColor: '#777777',
			 * spotRadius: 2, tooltipPrefix: "", tooltipSuffix: ' Tickets',
			 * tooltipFormat: '{{prefix}}{{y}}{{suffix}}' };
			 * jQuery('.js-slc-line1').sparkline('html', lineOptions);
			 * 
			 * lineOptions['lineColor'] = '#abe37d';
			 * lineOptions['fillColor'] = '#abe37d';
			 * lineOptions['tooltipPrefix'] = '$ ';
			 * lineOptions['tooltipSuffix'] = "";
			 * jQuery('.js-slc-line2').sparkline('html', lineOptions);
			 * 
			 * lineOptions['lineColor'] = '#faad7d';
			 * lineOptions['fillColor'] = '#faad7d';
			 * lineOptions['tooltipPrefix'] = "";
			 * lineOptions['tooltipSuffix'] = ' Sales';
			 * jQuery('.js-slc-line3').sparkline('html', lineOptions); //
			 * Pie Charts var pieCharts = { type: 'pie', width: '50px',
			 * height: '50px', sliceColors: ['#fadb7d','#faad7d',
			 * '#75b0eb','#abe37d'], tooltipPrefix: "", tooltipSuffix: '
			 * Tickets', tooltipFormat: '{{prefix}}{{value}}{{suffix}}' };
			 * jQuery('.js-slc-pie1').sparkline('html', pieCharts);
			 * 
			 * pieCharts['tooltipPrefix'] = '$ '; pieCharts['tooltipSuffix'] =
			 * ""; jQuery('.js-slc-pie2').sparkline('html', pieCharts);
			 * 
			 * pieCharts['tooltipPrefix'] = ""; pieCharts['tooltipSuffix'] = '
			 * Sales'; jQuery('.js-slc-pie3').sparkline('html', pieCharts); //
			 * Tristate Charts var tristateOptions = { type: 'tristate',
			 * barWidth: 8, barSpacing: 6, height: '80px', posBarColor:
			 * '#abe37d', negBarColor: '#faad7d' };
			 * jQuery('.js-slc-tristate1').sparkline('html',
			 * tristateOptions);
			 * jQuery('.js-slc-tristate2').sparkline('html',
			 * tristateOptions);
			 * jQuery('.js-slc-tristate3').sparkline('html',
			 * tristateOptions); }; // Randomize Easy Pie Chart values var
			 * initRandomEasyPieChart = function(){
			 * jQuery('.js-pie-randomize').on('click', function(){
			 * jQuery(this) .parents('.block') .find('.pie-chart')
			 * .each(function() { var random = Math.floor((Math.random() *
			 * 100) + 1);
			 * 
			 * jQuery(this) .data('easyPieChart') .update(random); }); }); };
			 */

			// Flot charts, for more examples you can check out
			// http://www.flotcharts.org/flot/examples/
			var initChartsFlot = function () {
				// Get the elements where we will attach the
				// charts
				var flotLines = jQuery('.js-flot-lines');
				var flotStacked = jQuery('.js-flot-stacked');
				var flotLive = jQuery('.js-flot-live');
				var flotPie = jQuery('.js-flot-pie');
				var flotBars = jQuery('.js-flot-bars');

				// Demo Data
				var dataEarnings = [[1, 2500], [2, 2300],
				[3, 3200], [4, 2500], [5, 4500],
				[6, 2800], [7, 3900], [8, 3100],
				[9, 4600], [10, 3200],
				[11, 4200], [12, 5700]];
				var dataSales = [[1, 1100], [2, 700],
				[3, 1300], [4, 900], [5, 1900],
				[6, 950], [7, 1700], [8, 1250],
				[9, 1800], [10, 1300],
				[11, 1750], [12, 2900]];

				var dataSalesBefore = [[1, 500], [4, 390],
				[7, 1000], [10, 600], [13, 800],
				[16, 1050], [19, 1200],
				[22, 750], [25, 980], [28, 900],
				[31, 1350], [34, 1200]];
				var dataSalesAfter = [[2, 650], [5, 600],
				[8, 1400], [11, 900], [14, 1300],
				[17, 1200], [20, 1420],
				[23, 1650], [26, 1300],
				[29, 1120], [32, 1550],
				[35, 1650]];

				var dataMonths = [[1, 'Jan'], [2, 'Feb'],
				[3, 'Mar'], [4, 'Apr'],
				[5, 'May'], [6, 'Jun'],
				[7, 'Jul'], [8, 'Aug'],
				[9, 'Sep'], [10, 'Oct'],
				[11, 'Nov'], [12, 'Dec']];
				var dataMonthsBars = [[2, 'Jan'],
				[5, 'Feb'], [8, 'Mar'],
				[11, 'Apr'], [14, 'May'],
				[17, 'Jun'], [20, 'Jul'],
				[23, 'Aug'], [26, 'Sep'],
				[29, 'Oct'], [32, 'Nov'],
				[35, 'Dec']];

				// Init lines chart
				jQuery.plot(flotLines, [{
					label: 'Earnings',
					data: dataEarnings,
					lines: {
						show: true,
						fill: true,
						fillColor: {
							colors: [{
								opacity: .7
							}, {
								opacity: .7
							}]
						}
					},
					points: {
						show: true,
						radius: 6
					}
				}, {
					label: 'Sales',
					data: dataSales,
					lines: {
						show: true,
						fill: true,
						fillColor: {
							colors: [{
								opacity: .5
							}, {
								opacity: .5
							}]
						}
					},
					points: {
						show: true,
						radius: 6
					}
				}], {
						colors: ['#abe37d', '#333333'],
						legend: {
							show: true,
							position: 'nw',
							backgroundOpacity: 0
						},
						grid: {
							borderWidth: 0,
							hoverable: true,
							clickable: true
						},
						yaxis: {
							tickColor: '#ffffff',
							ticks: 3
						},
						xaxis: {
							ticks: dataMonths,
							tickColor: '#f5f5f5'
						}
					});

				// Creating and attaching a tooltip to the
				// classic chart
				var previousPoint = null, ttlabel = null;
				flotLines
					.bind(
						'plothover',
						function (event, pos, item) {
							if (item) {
								if (previousPoint !== item.dataIndex) {
									previousPoint = item.dataIndex;

									jQuery(
										'.js-flot-tooltip')
										.remove();
									var x = item.datapoint[0], y = item.datapoint[1];

									if (item.seriesIndex === 0) {
										ttlabel = '$ <strong>'
											+ y
											+ '</strong>';
									} else if (item.seriesIndex === 1) {
										ttlabel = '<strong>'
											+ y
											+ '</strong> sales';
									} else {
										ttlabel = '<strong>'
											+ y
											+ '</strong> tickets';
									}

									jQuery(
										'<div class="js-flot-tooltip flot-tooltip">'
										+ ttlabel
										+ '</div>')
										.css(
											{
												top: item.pageY - 45,
												left: item.pageX + 5
											})
										.appendTo(
											"body")
										.show();
								}
							} else {
								jQuery(
									'.js-flot-tooltip')
									.remove();
								previousPoint = null;
							}
						});

				// Stacked Chart
				jQuery.plot(flotStacked, [{
					label: 'Sales',
					data: dataSales
				}, {
					label: 'Earnings',
					data: dataEarnings
				}], {
						colors: ['#faad7d', '#fadb7d'],
						series: {
							stack: true,
							lines: {
								show: true,
								fill: true
							}
						},
						lines: {
							show: true,
							lineWidth: 0,
							fill: true,
							fillColor: {
								colors: [{
									opacity: 1
								}, {
									opacity: 1
								}]
							}
						},
						legend: {
							show: true,
							position: 'nw',
							sorted: true,
							backgroundOpacity: 0
						},
						grid: {
							borderWidth: 0
						},
						yaxis: {
							tickColor: '#ffffff',
							ticks: 3
						},
						xaxis: {
							ticks: dataMonths,
							tickColor: '#f5f5f5'
						}
					});

				// Live Chart
				var dataLive = [];

				function getRandomData() { // Random data
					// generator

					if (dataLive.length > 0)
						dataLive = dataLive.slice(1);

					while (dataLive.length < 300) {
						var prev = dataLive.length > 0 ? dataLive[dataLive.length - 1]
							: 50;
						var y = prev + Math.random() * 10 - 5;
						if (y < 0)
							y = 0;
						if (y > 100)
							y = 100;
						dataLive.push(y);
					}

					var res = [];
					for (var i = 0; i < dataLive.length; ++i)
						res.push([i, dataLive[i]]);

					// Show live chart info
					jQuery('.js-flot-live-info').html(
						y.toFixed(0) + '%');

					return res;
				}

				function updateChartLive() { // Update live
					// chart
					chartLive.setData([getRandomData()]);
					chartLive.draw();
					setTimeout(updateChartLive, 70);
				}

				var chartLive = jQuery.plot(flotLive, // Init
					// live
					// chart
					[{
						data: getRandomData()
					}], {
						series: {
							shadowSize: 0
						},
						lines: {
							show: true,
							lineWidth: 2,
							fill: true,
							fillColor: {
								colors: [{
									opacity: .2
								}, {
									opacity: .2
								}]
							}
						},
						colors: ['#75b0eb'],
						grid: {
							borderWidth: 0,
							color: '#aaaaaa'
						},
						yaxis: {
							show: true,
							min: 0,
							max: 110
						},
						xaxis: {
							show: false
						}
					});

				updateChartLive(); // Start getting new data

				// Bars Chart
				jQuery.plot(flotBars, [{
					label: 'Sales Before',
					data: dataSalesBefore,
					bars: {
						show: true,
						lineWidth: 0,
						fillColor: {
							colors: [{
								opacity: 1
							}, {
								opacity: 1
							}]
						}
					}
				}, {
					label: 'Sales After',
					data: dataSalesAfter,
					bars: {
						show: true,
						lineWidth: 0,
						fillColor: {
							colors: [{
								opacity: 1
							}, {
								opacity: 1
							}]
						}
					}
				}], {
						colors: ['#faad7d', '#fadb7d'],
						legend: {
							show: true,
							position: 'nw',
							backgroundOpacity: 0
						},
						grid: {
							borderWidth: 0
						},
						yaxis: {
							ticks: 3,
							tickColor: '#f5f5f5'
						},
						xaxis: {
							ticks: dataMonthsBars,
							tickColor: '#f5f5f5'
						}
					});

				// Pie Chart
				jQuery
					.plot(
						flotPie,
						[{
							label: 'Sales',
							data: 22
						}, {
							label: 'Tickets',
							data: 22
						}, {
							label: 'Earnings',
							data: 56
						}],
						{
							colors: ['#fadb7d',
								'#75b0eb',
								'#abe37d'],
							legend: {
								show: false
							},
							series: {
								pie: {
									show: true,
									radius: 1,
									label: {
										show: true,
										radius: 2 / 3,
										formatter: function (
											label,
											pieSeries) {
											return '<div class="flot-pie-label">'
												+ label
												+ '<br>'
												+ Math
													.round(pieSeries.percent)
												+ '%</div>';
										},
										background: {
											opacity: .75,
											color: '#000000'
										}
									}
								}
							}
						});
			};

			// Init all charts
			initChartsChartJS();
			initChartsSparkline();
			initChartsFlot();

			// Randomize Easy Pie values functionality
			initRandomEasyPieChart();
		}]);

// Components Syntax Highlighting Controller


// Components Calendar Controller
App.controller(
	'CompCalendarCtrl',
	[
		'$scope',

		'$window',
		function ($scope, $window) {
			// Add new event in the event list
			var addEvent = function () {
				var eventInput = jQuery('.js-add-event');
				var eventInputVal = "";

				// When the add event form is submitted
				jQuery('.js-form-add-event')
					.on(
						'submit',
						function () {
							eventInputVal = eventInput
								.prop('value'); // Get
							// input
							// value

							// Check if the user entered
							// something
							if (eventInputVal) {
								// Add it to the events
								// list
								jQuery('.js-events')
									.prepend(
										'<li class="animated fadeInDown">'
										+ jQuery(
											'<div />')
											.text(
												eventInputVal)
											.html()
										+ '</li>');

								// Clear input field
								eventInput.prop(
									'value', "");

								// Re-Init Events
								initEvents();
							}

							return false;
						});
			};

			// Init drag and drop event functionality
			var initEvents = function () {
				jQuery('.js-events')
					.find('li')
					.each(
						function () {
							var event = jQuery(this);

							// create an Event Object
							var eventObject = {
								title: jQuery
									.trim(event
										.text()),
								color: event
									.css('background-color')
							};

							// store the Event Object in
							// the DOM element so we can
							// get to it later
							jQuery(this).data(
								'eventObject',
								eventObject);

							// make the event draggable
							// using jQuery UI
							jQuery(this).draggable({
								zIndex: 999,
								revert: true,
								revertDuration: 0
							});
						});
			};

			// Init FullCalendar
			var initCalendar = function () {
				var date = new Date();
				var d = date.getDate();
				var m = date.getMonth();
				var y = date.getFullYear();

				jQuery('.js-calendar')
					.fullCalendar(
						{
							firstDay: 1,
							editable: true,
							droppable: true,
							defaultView: 'month',
							editable: true,
							minTime: "09:00:00",
							maxTime: "19:00:00",
							header: {
								left: "prev,next today",
								center: "title",
								right: "month,agendaWeek,agendaDay"
							},
							drop: function (date,
								allDay) { // this
								// function
								// is
								// called
								// when
								// something
								// is
								// dropped
								// retrieve the dropped
								// element's stored
								// Event Object
								var originalEventObject = jQuery(
									this).data(
										'eventObject');

								// we need to copy it,
								// so that multiple
								// events don't have a
								// reference to the same
								// object
								var copiedEventObject = jQuery
									.extend({},
										originalEventObject);

								// assign it the date
								// that was reported
								copiedEventObject.start = date;

								// render the event on
								// the calendar
								// the last `true`
								// argument determines
								// if the event "sticks"
								// (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
								jQuery('.js-calendar')
									.fullCalendar(
										'renderEvent',
										copiedEventObject,
										true);

								// remove the element
								// from the "Draggable
								// Events" list
								jQuery(this).remove();
							},

							events: [
								{
									title: 'Free day',
									start: new Date(
										y, m, 1),
									allDay: true,
									color: '#faeab9'
								},

								{
									title: 'Skype Meeting',
									start: new Date(
										y, m, 2)
								},
								{
									title: 'Secret Project',
									start: new Date(
										y, m, 5),
									end: new Date(
										y, m, 8),
									allDay: true,
									color: '#fac5a5'
								},
								{
									title: 'Work',
									start: new Date(
										y, m, 9),
									end: new Date(
										y, m,
										11),
									allDay: true,
									color: '#fac5a5'
								},
								{
									id: 999,
									title: 'Biking (repeated)',
									start: new Date(
										y, m,
										d - 3,
										15, 0)
								},
								{
									id: 999,
									title: 'Biking (repeated)',
									start: new Date(
										y, m,
										d + 2,
										15, 0)
								},
								{
									title: 'Badminton',
									start: new Date(
										y, m,
										d - 1),
									end: new Date(
										y, m,
										d - 1),
									allDay: true,
									color: '#faeab9'
								},
								{
									title: 'Lunch Meeting',
									start: new Date(
										y, m,
										d + 5,
										14, 00),
									color: '#fac5a5'
								},
								{
									title: 'Weight Training',
									start: new Date(
										y, m,
										d, 9, 0),
									end: new Date(
										y, m,
										d, 12,
										0),
									allDay: true,
									color: '#faeab9'
								},
								{
									title: 'Party',
									start: new Date(
										y, m,
										15),
									end: new Date(
										y, m,
										16),
									allDay: true,
									color: '#faeab9'
								},
								{
									title: 'Reading',
									start: new Date(
										y, m,
										d + 8,
										21, 0),
									end: new Date(
										y, m,
										d + 8,
										23, 30),
									allDay: true
								},
								{
									title: 'Follow me on Twitter',
									start: new Date(
										y, m,
										23),
									end: new Date(
										y, m,
										25),
									url: '#',
									color: '#32ccfe'
								}],

							eventClick: function (
								event, jsEvent,
								view) {
								// set the values and
								// open the modal
								$('#modalTitle').html(
									event.title);
								$('#modalBody')
									.html(
										event.description);
								$('#fullCalModal')
									.modal();
							}
						});
			};

			// Add Event functionality
			addEvent();

			// FullCalendar, for more examples you can check out
			// http://fullcalendar.io/
			initEvents();
			initCalendar();
		}]);

// Components Syntax Highlighting Controller
App.controller('CompSyntaxHighlightingCtrl', ['$scope',
	'$window', function ($scope, $window) {
		// Init HighlightJS
		hljs.initHighlighting();
	}]);

// Components Rating Controller
App.controller(
	'CompRatingCtrl',
	[
		'$scope',

		'$window',
		function ($scope, $window) {
			// jQuery Raty, for more examples you can check out
			// https://github.com/wbotelhos/raty

			// Init Rating
			var initRating = function () {
				// Set Default options
				jQuery.fn.raty.defaults.starType = 'i';
				jQuery.fn.raty.defaults.hints = ['Bad',
					'Poor', 'Regular', 'Good', 'Gorgeous'];

				// Init Raty on .js-rating class
				jQuery('.js-rating')
					.each(
						function () {
							var ratingEl = jQuery(this);

							ratingEl
								.raty({
									score: ratingEl
										.data('score') ? ratingEl
											.data('score')
										: 0,
									number: ratingEl
										.data('number') ? ratingEl
											.data('number')
										: 5,
									cancel: ratingEl
										.data('cancel') ? ratingEl
											.data('cancel')
										: false,
									target: ratingEl
										.data('target') ? ratingEl
											.data('target')
										: false,
									targetScore: ratingEl
										.data('target-score') ? ratingEl
											.data('target-score')
										: false,
									precision: ratingEl
										.data('precision') ? ratingEl
											.data('precision')
										: false,
									cancelOff: ratingEl
										.data('cancel-off') ? ratingEl
											.data('cancel-off')
										: 'fa fa-fw fa-times text-danger',
									cancelOn: ratingEl
										.data('cancel-on') ? ratingEl
											.data('cancel-on')
										: 'fa fa-fw fa-times',
									starHalf: ratingEl
										.data('star-half') ? ratingEl
											.data('star-half')
										: 'fa fa-fw fa-star-half-o text-warning',
									starOff: ratingEl
										.data('star-off') ? ratingEl
											.data('star-off')
										: 'fa fa-fw fa-star text-gray',
									starOn: ratingEl
										.data('star-on') ? ratingEl
											.data('star-on')
										: 'fa fa-fw fa-star text-warning',
									click: function (
										score,
										evt) {
										// Here you
										// could add
										// your
										// logic on
										// rating
										// click
										// console.log('ID:
										// ' +
										// this.id +
										// "\nscore:
										// " + score
										// +
										// "\nevent:
										// " + evt);
									}
								});
						});
			};

			// Init all Ratings
			initRating();
		}]);

// Components Treeview Controller
App.controller('CompTreeviewCtrl', [
	'$scope',

	'$window',
	function ($scope, $window) {
		// Bootstrap Tree View, for more examples you can check out
		// https://github.com/jonmiles/bootstrap-treeview

		// Init Tree Views
		var initTreeViews = function () {
			// Set default example tree data for all Tree Views
			var treeData = [{
				text: 'Bootstrap',
				href: '#parent1',
				tags: ['4'],
				nodes: [{
					text: 'eLearning',
					href: '#child1',
					tags: ['2'],
					nodes: [{
						text: 'Code',
						href: '#grandchild1'
					}, {
						text: 'Tutorials',
						href: '#grandchild2'
					}]
				}, {
					text: 'Templates',
					href: '#child2'
				}, {
					text: 'CSS',
					href: '#child3',
					tags: ['2'],
					nodes: [{
						text: 'Less',
						href: '#grandchild3'
					}, {
						text: 'SaSS',
						href: '#grandchild4'
					}]
				}]
			}, {
				text: 'Design',
				href: '#parent3'
			}, {
				text: 'Coding',
				href: '#parent4'
			}, {
				text: 'Marketing',
				href: '#parent5'
			}];

			// Init Simple Tree
			jQuery('.js-tree-simple').treeview({
				data: treeData,
				color: '#555',
				expandIcon: 'fa fa-plus',
				collapseIcon: 'fa fa-minus',
				onhoverColor: '#f9f9f9',
				selectedColor: '#555',
				selectedBackColor: '#f1f1f1',
				showBorder: false,
				levels: 3
			});

			// Init Icons Tree
			jQuery('.js-tree-icons').treeview({
				data: treeData,
				color: '#555',
				expandIcon: 'fa fa-plus',
				collapseIcon: 'fa fa-minus',
				nodeIcon: 'fa fa-folder text-primary',
				onhoverColor: '#f9f9f9',
				selectedColor: '#555',
				selectedBackColor: '#f1f1f1',
				showBorder: false,
				levels: 3
			});

			// Init Alternative Icons Tree
			jQuery('.js-tree-icons-alt').treeview({
				data: treeData,
				color: '#555',
				expandIcon: 'fa fa-angle-down',
				collapseIcon: 'fa fa-angle-up',
				nodeIcon: 'fa fa-file-o text-city',
				onhoverColor: '#f9f9f9',
				selectedColor: '#555',
				selectedBackColor: '#f1f1f1',
				showBorder: false,
				levels: 3
			});

			// Init Badges Tree
			jQuery('.js-tree-badges').treeview({
				data: treeData,
				color: '#555',
				expandIcon: 'fa fa-plus',
				collapseIcon: 'fa fa-minus',
				nodeIcon: 'fa fa-folder text-primary',
				onhoverColor: '#f9f9f9',
				selectedColor: '#555',
				selectedBackColor: '#f1f1f1',
				showTags: true,
				levels: 3
			});

			// Init Collapsed Tree
			jQuery('.js-tree-collapsed').treeview({
				data: treeData,
				color: '#555',
				expandIcon: 'fa fa-plus',
				collapseIcon: 'fa fa-minus',
				nodeIcon: 'fa fa-folder text-primary-light',
				onhoverColor: '#f9f9f9',
				selectedColor: '#555',
				selectedBackColor: '#f1f1f1',
				showTags: true,
				levels: 1
			});

			// Set example JSON data for JSON Tree View
			var treeDataJson = '[' + '{' + '"text": "Bootstrap",'
				+ '"nodes": [' + '{' + '"text": "eLearning",'
				+ '"nodes": [' + '{' + '"text": "Code"' + '},' + '{'
				+ '"text": "Tutorials"' + '}' + ']' + '},' + '{'
				+ '"text": "Templates"' + '},' + '{' + '"text": "CSS",'
				+ '"nodes": [' + '{' + '"text": "Less"' + '},' + '{'
				+ '"text": "SaSS"' + '}' + ']' + '}' + ']' + '},' + '{'
				+ '"text": "Design"' + '},' + '{' + '"text": "Coding"'
				+ '},' + '{' + '"text": "Marketing"' + '}' + ']';

			// Init Json Tree
			jQuery('.js-tree-json').treeview({
				data: treeDataJson,
				color: '#555',
				expandIcon: 'fa fa-arrow-down',
				collapseIcon: 'fa fa-arrow-up',
				nodeIcon: 'fa fa-file-code-o text-flat',
				onhoverColor: '#f9f9f9',
				selectedColor: '#555',
				selectedBackColor: '#f1f1f1',
				showTags: true,
				levels: 3
			});
		};

		// Init all Tree Views
		initTreeViews();
	}]);

// Components Maps Google Controller
App.controller(
	'CompMapsGoogleCtrl',
	[
		'$scope',

		'$window',
		function ($scope, $window) {
			// Gmaps.js, for more examples you can check out
			// https://hpneo.github.io/gmaps/

			// Init Search Map
			var initMapSearch = function () {
				// Init Map
				var mapSearch = new GMaps({
					div: '#js-map-search',
					lat: 20,
					lng: 0,
					zoom: 2,
					scrollwheel: false
				});

				// When the search form is submitted
				jQuery('.js-form-search')
					.on(
						'submit',
						function () {
							GMaps
								.geocode({
									address: jQuery(
										'.js-search-address')
										.val()
										.trim(),
									callback: function (
										results,
										status) {
										if ((status === 'OK')
											&& results) {
											var latlng = results[0].geometry.location;

											mapSearch
												.removeMarkers();
											mapSearch
												.addMarker({
													lat: latlng
														.lat(),
													lng: latlng
														.lng()
												});
											mapSearch
												.fitBounds(results[0].geometry.viewport);
										} else {
											alert('Address not found!');
										}
									}
								});

							return false;
						});
			};

			// Init Satellite Map
			var initMapSat = function () {
				new GMaps({
					div: '#js-map-sat',
					lat: 0,
					lng: 0,
					zoom: 1,
					scrollwheel: false
				})
					.setMapTypeId(google.maps.MapTypeId.SATELLITE);
			};

			// Init Terrain Map
			var initMapTer = function () {
				new GMaps({
					div: '#js-map-ter',
					lat: 0,
					lng: 0,
					zoom: 1,
					scrollwheel: false
				}).setMapTypeId(google.maps.MapTypeId.TERRAIN);
			};

			// Init Overlay Map
			var initMapOverlay = function () {
				new GMaps({
					div: '#js-map-overlay',
					lat: 37.7577,
					lng: -122.4376,
					zoom: 11,
					scrollwheel: false
				})
					.drawOverlay({
						lat: 37.7577,
						lng: -122.4376,
						content: '<div class="alert alert-danger alert-dismissable"><h4 class="push-15">Overlay Message</h4><p class="push-10">You can overlay messages on your maps!</p></div>'
					});
			};

			// Init Markers Map
			var initMapMarkers = function () {
				new GMaps({
					div: '#js-map-markers',
					lat: 37.7577,
					lng: -122.4376,
					zoom: 11,
					scrollwheel: false
				}).addMarkers([{
					lat: 37.70,
					lng: -122.49,
					title: 'Marker #1',
					animation: google.maps.Animation.DROP,
					infoWindow: {
						content: '<strong>Marker #1</strong>'
					}
				}, {
					lat: 37.76,
					lng: -122.46,
					title: 'Marker #2',
					animation: google.maps.Animation.DROP,
					infoWindow: {
						content: '<strong>Marker #2</strong>'
					}
				}, {
					lat: 37.72,
					lng: -122.41,
					title: 'Marker #3',
					animation: google.maps.Animation.DROP,
					infoWindow: {
						content: '<strong>Marker #3</strong>'
					}
				}, {
					lat: 37.78,
					lng: -122.39,
					title: 'Marker #4',
					animation: google.maps.Animation.DROP,
					infoWindow: {
						content: '<strong>Marker #4</strong>'
					}
				}, {
					lat: 37.74,
					lng: -122.46,
					title: 'Marker #5',
					animation: google.maps.Animation.DROP,
					infoWindow: {
						content: '<strong>Marker #5</strong>'
					}
				}]);
			};

			// Init Street Map
			var initMapStreet = function () {
				new GMaps.createPanorama({
					el: '#js-map-street',
					lat: 37.809345,
					lng: -122.475825,
					pov: {
						heading: 340.91,
						pitch: 4
					},
					scrollwheel: false
				});
			};

			// Init Geolocation Map
			var initMapGeo = function () {
				var gmapGeolocation = new GMaps({
					div: '#js-map-geo',
					lat: 0,
					lng: 0,
					scrollwheel: false
				});

				GMaps
					.geolocate({
						success: function (position) {
							gmapGeolocation
								.setCenter(
									position.coords.latitude,
									position.coords.longitude);
							gmapGeolocation
								.addMarker({
									lat: position.coords.latitude,
									lng: position.coords.longitude,
									animation: google.maps.Animation.DROP,
									title: 'GeoLocation',
									infoWindow: {
										content: '<div class="text-success"><i class="fa fa-map-marker"></i> <strong>Your location!</strong></div>'
									}
								});
						},
						error: function (error) {
							alert('Geolocation failed: '
								+ error.message);
						},
						not_supported: function () {
							alert("Your browser does not support geolocation");
						},
						always: function () {
							// Message when geolocation
							// succeed
						}
					});
			};

			// Init Map with Search functionality
			initMapSearch();

			// Init Example Maps
			initMapSat();
			initMapTer();
			initMapOverlay();
			initMapMarkers();
			initMapStreet();
			initMapGeo();
		}]);

// Components Maps Google Full Controller
App.controller('CompMapsGoogleFullCtrl', ['$scope',
	'$window', function ($scope, $window) {
		// Gmaps.js, for more examples you can check out
		// https://hpneo.github.io/gmaps/

		// Init Full Map
		var initMapFull = function () {
			var mainCon = jQuery('#main-container');
			var mlat = 37.7577;
			var mlong = -122.4376;
			var rTimeout;

			// Set #main-container position to be relative
			mainCon.css('position', 'relative');

			// Adjust map container position
			jQuery('#js-map-full').css({
				'position': 'absolute',
				'top': mainCon.css('padding-top'),
				'right': '0',
				'bottom': '0',
				'left': '0',
				'height': '100%'
			});

			// Init map itself
			var mapFull = new GMaps({
				div: '#js-map-full',
				lat: mlat,
				lng: mlong,
				zoom: 11
			});

			// Set map type
			mapFull.setMapTypeId(google.maps.MapTypeId.TERRAIN);

			// Resize and center the map on browser window resize
			jQuery(window).on('resize orientationchange', function () {
				clearTimeout(rTimeout);

				rTimeout = setTimeout(function () {
					mapFull.refresh();
					mapFull.setCenter(mlat, mlong);
				}, 150);
			});

			// Trigger a resize to refresh the map (helps for proper
			// rendering because we dynamically change the height of map's
			// container)
			jQuery(window).resize();
		};

		// Init Full Map
		initMapFull();
	}]);

// Components Maps Vector Controller
App.controller(
	'CompMapsVectorCtrl',
	[
		'$scope',

		'$window',
		function ($scope, $window) {
			// jVectorMap, for more examples you can check out
			// http://jvectormap.com/documentation/

			// Set default options for all maps
			var mapOptions = {
				container: "",
				map: "",
				backgroundColor: '#ffffff',
				regionStyle: {
					initial: {
						fill: '#5490d2',
						'fill-opacity': 1,
						stroke: 'none',
						'stroke-width': 0,
						'stroke-opacity': 1
					},
					hover: {
						'fill-opacity': .8,
						cursor: 'pointer'
					}
				}
			};

			// Maps variables
			var mapWorld, mapEurope, mapUsa, mapIndia, mapChina, mapAustralia, mapSouthAfrica, mapFrance, mapGermany;

			// Init World Map
			var initMapWorld = function () {
				// Set Active Map and Container
				mapOptions['map'] = 'world_mill_en';
				mapOptions['container'] = jQuery('.js-vector-map-world');

				// Init Map
				mapWorld = new jvm.Map(mapOptions);
			};

			// Init Europe Map
			var initMapEurope = function () {
				// Set Active Map and Container
				mapOptions['map'] = 'europe_mill_en';
				mapOptions['container'] = jQuery('.js-vector-map-europe');

				// Init Map
				mapEurope = new jvm.Map(mapOptions);
			};

			// Init USA Map
			var initMapUsa = function () {
				// Set Active Map and Container
				mapOptions['map'] = 'us_aea_en';
				mapOptions['container'] = jQuery('.js-vector-map-usa');

				// Init Map
				mapUsa = new jvm.Map(mapOptions);
			};

			// Init India Map
			var initMapIndia = function () {
				// Set Active Map and Container
				mapOptions['map'] = 'in_mill_en';
				mapOptions['container'] = jQuery('.js-vector-map-india');

				// Init Map
				mapIndia = new jvm.Map(mapOptions);
			};

			// Init China Map
			var initMapChina = function () {
				// Set Active Map and Container
				mapOptions['map'] = 'cn_mill_en';
				mapOptions['container'] = jQuery('.js-vector-map-china');

				// Init Map
				mapChina = new jvm.Map(mapOptions);
			};

			// Init Australia Map
			var initMapAustralia = function () {
				// Set Active Map and Container
				mapOptions['map'] = 'au_mill_en';
				mapOptions['container'] = jQuery('.js-vector-map-australia');

				// Init Map
				mapAustralia = new jvm.Map(mapOptions);
			};

			// Init South Africa Map
			var initMapSouthAfrica = function () {
				// Set Active Map and Container
				mapOptions['map'] = 'za_mill_en';
				mapOptions['container'] = jQuery('.js-vector-map-south-africa');

				// Init Map
				mapSouthAfrica = new jvm.Map(mapOptions);
			};

			// Init France Map
			var initMapFrance = function () {
				// Set Active Map and Container
				mapOptions['map'] = 'fr_mill_en';
				mapOptions['container'] = jQuery('.js-vector-map-france');

				// Init Map
				mapFrance = new jvm.Map(mapOptions);
			};

			// Init Germany Map
			var initMapGermany = function () {
				// Set Active Map and Container
				mapOptions['map'] = 'de_mill_en';
				mapOptions['container'] = jQuery('.js-vector-map-germany');

				// Init Map
				mapGermany = new jvm.Map(mapOptions);
			};

			// Init Example Maps
			initMapWorld();
			initMapEurope();
			initMapUsa();
			initMapIndia();
			initMapChina();
			initMapAustralia();
			initMapSouthAfrica();
			initMapFrance();
			initMapGermany();

			// When leaving the page remove maps resize event
			// (causes JS errors in other pages)
			$scope.$on('$stateChangeStart', function (event) {
				jQuery(window).unbind('resize',
					mapWorld.onResize);
				jQuery(window).unbind('resize',
					mapEurope.onResize);
				jQuery(window)
					.unbind('resize', mapUsa.onResize);
				jQuery(window).unbind('resize',
					mapIndia.onResize);
				jQuery(window).unbind('resize',
					mapChina.onResize);
				jQuery(window).unbind('resize',
					mapAustralia.onResize);
				jQuery(window).unbind('resize',
					mapSouthAfrica.onResize);
				jQuery(window).unbind('resize',
					mapFrance.onResize);
				jQuery(window).unbind('resize',
					mapGermany.onResize);
			});

			// When returning to the page re-enable maps resize
			// functionality
			$scope.$on('$stateChangeSuccess', function (event) {
				jQuery(window).resize(mapWorld.onResize);
				jQuery(window).resize(mapEurope.onResize);
				jQuery(window).resize(mapUsa.onResize);
				jQuery(window).resize(mapIndia.onResize);
				jQuery(window).resize(mapChina.onResize);
				jQuery(window).resize(mapAustralia.onResize);
				jQuery(window).resize(mapSouthAfrica.onResize);
				jQuery(window).resize(mapFrance.onResize);
				jQuery(window).resize(mapGermany.onResize);
			});
		}]);