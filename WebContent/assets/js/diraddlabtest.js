(function() {
    'use strict';
    var App = angular.module('app');
    App.directive('diraddlabtest', diraddlabtest);
    diraddlabtest.$inject = ['$rootScope', '$compile'];

    function diraddlabtest($rootScope, $compile) {
        return {
            restrict: 'EA',
            templateUrl: 'assets/views/historicalLabResults/diraddlabtest.html',
            controller: 'diraddlabtestCtrl',
            replace: true,
            scope: true,
            link: function(scope, element) {}
        };
    }
})();