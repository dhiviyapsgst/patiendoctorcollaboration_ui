(function () {
    'use strict';
    var App = angular.module('app');
    App.directive('dirViewVitalTrends', dirViewVitalTrends);
    dirViewVitalTrends.$inject = ['$rootScope', '$compile'];

    function dirViewVitalTrends($rootScope, $compile) {
        return {
            restrict: 'EA',
            templateUrl: 'assets/views/vitals/ViewVitalTrends.html',
            controller: 'ViewVitalTrendsController',
            replace: true,
            scope: true,
            link: function (scope, element) {
            }

        };
    }

})();


App
    .controller(
        'ViewVitalTrendsController',
        [
            '$scope',
            '$rootScope',
            '$http',
            '$localStorage',
            '$state',
            '$stateParams',
            '$window',
            function ViewVitalTrendsController($scope, $rootScope,
                $http, $localStorage, $state, $stateParams,
                $window) {


                $scope.date = [];
                var index = 0;
                var index1 = 0;
                var index2 = 0;
                var ind = 0;
                var ind1 = 0;
                var ind2 = 0;
                var dateTrends = [];

                //Find start date of current year

                var thisYear = (new Date()).getFullYear();
                var start = new Date("1/1/" + thisYear);
                var defaultStart = moment(start.valueOf()).format('YYYY-MM-DD');


                //To populate drop down 
                getAllVitals = '/ihspdc/rest/getallvitals'
                $scope.flag = [];
                $http.get(getAllVitals).then(function (response, err) {
                    if (!err) {
                        $scope.AllVital = [];
                        var count = 0;
                        $scope.AllVitals = [];
                        console.log(response.data);
                        $scope.vitals = response.data;
                        angular.forEach($scope.vitals, function (vitl) {
                            //Dont insert blood group for trends
                            if (vitl.vitalName != "BloodGroup")
                                $scope.AllVital.push(vitl.vitalName);
                        }
                        );

                        //To store BP(systolic and diastolic)
                        for (i = 0; i < $scope.AllVital.length; i++) {
                            if ($scope.AllVital[i].match("Blood Pressure")) {
                                $scope.flag[i] = 1;
                            }
                            else {
                                $scope.flag[i] = 0;
                            }
                        }

                        //Store all vitals
                        for (i = 0; i < $scope.AllVital.length; i++) {

                            if ($scope.flag[i] == 1 && count == 0) {
                                $scope.AllVitals.push("Blood Pressure");
                                count = 1;
                            }
                            else if ($scope.flag[i] == 0) {
                                $scope.AllVitals.push($scope.AllVital[i]);
                            }

                        }

                        $scope.vitalName = $scope.AllVitals[0];
                        $scope.boolean = true;

                    }
                    else {
                        console.log("error");
                    }

                    // if drop down appears (ON LOAD)
                    if ($scope.boolean == true) {

                        var AllDates = [];

                        //To get the results for a particular vital (1st vital)

                        vitaltrends = '/ihspdc/rest/displayVitalTrends?patientId='
                        vitaltrends += $stateParams.id;
                        vitaltrends += "&vitalName=";
                        vitaltrends += $scope.vitalName;

                        $http.get(vitaltrends)
                            .then(function (response, err) {
                                if (!err) {
                                    console.log(response);
                                    $scope.trends = response.data;
                                } else {
                                    console.log('Error');

                                }

                                //To store all dates
                                angular.forEach($scope.trends, function (trend) {

                                    if (trend.vitalRecDate) {
                                        AllDates.push(trend.vitalRecDate);

                                    }

                                }
                                );

                                var uniqueDates = [];

                                //To find unique dates

                                let uniqueArray = AllDates
                                    .map(function (date) { return date })
                                    .filter(function (date, i, array) {
                                        return array.indexOf(date) === i;
                                    });


                                for (i = 0; i < uniqueArray.length; i++) {
                                    uniqueDates[i] = moment(uniqueArray[i], "DD-MM-YYYY").format("YYYY-MM-DD");
                                }

                                console.log("Unique dates" + uniqueDates);

                                //Find latest date of all results

                                var maxDate = moment(uniqueDates[uniqueDates.length - 1], "YYYY-MM-DD").format("YYYY-MM-DD");


                                //Find last date of the current month

                                var date = new Date(maxDate);
                                y = date.getFullYear();
                                m = date.getMonth();
                                var lastDay = new Date(y, m + 1, 0)
                                lastDay = moment(lastDay, "YYYY-MM-DD").format('YYYY-MM-DD');

                                console.log("Last" + lastDay);

                                var flags = 0;

                                //Loop from start of the year till latest date
                                for (var i = moment(defaultStart, "YYYY-MM-DD"); moment(i).diff(lastDay, 'days') <= 0; i = moment(i, "YYYY-MM-DD").add(1, 'd').format("YYYY-MM-DD")) {

                                    $scope.date.push(moment(i,
                                        'YYYY-MM-DD')
                                        .format(
                                            'MMM DD'));


                                }

                                //To get the vital codes for Systolic and Diastolic (BP)

                                $.getJSON("VitalCode.json", function (vital_code) {

                                    $scope.sys_vital_code = vital_code.Systolic;
                                    $scope.dias_vital_code = vital_code.Diastolic;

                                });



                                //To store dates to plot values

                                angular.forEach($scope.trends, function (trend) {

                                    if (!($scope.vitalName.match("Blood Pressure"))) {

                                        dateTrends.push(moment(trend.vitalRecDate,
                                            'DD-MM-YYYY')
                                            .format(
                                                'MMM DD'));
                                    }

                                    //VitalCode - take from VitalCode JSON file
                                    else if (trend.vital.vitalCode == $scope.sys_vital_code) {


                                        $scope.systrends.push(moment(trend.vitalRecDate,
                                            'DD-MM-YYYY')
                                            .format(
                                                'MMM DD'));

                                    }
                                    //VitalCode - take from VitalCode JSON file

                                    else if (trend.vital.vitalCode == $scope.dias_vital_code) {


                                        $scope.diastrends.push(moment(trend.vitalRecDate,
                                            'DD-MM-YYYY')
                                            .format(
                                                'MMM DD'));

                                    }




                                });

                                var tempArray = [];
                                var BPsysArray = [];
                                var BPdiasArray = [];


                                // Names: constant - DO NOT CHANGE 
                                var bloodPressure = 0;
                                angular
                                    .forEach(
                                        $scope.trends,
                                        function (
                                            vitltrend,
                                            i) {

                                            //To store the values(x,y)

                                            //Separate array for other vitals

                                            if (!(vitltrend.vital.vitalName.match("Blood Pressure"))) {

                                                bloodPressure = 0;
                                                if (!(tempArray[index])) {
                                                    tempArray[index] = [];

                                                }



                                                if ((vitltrend.vitalRecDate)) {

                                                    tempArray[index].push(dateTrends[ind++]);
                                                    tempArray[index].push(parseFloat(vitltrend.vitalValue));


                                                }
                                                index += 1;

                                                console.log(tempArray);
                                                console.log("temparray277");


                                            }
                                            //Separate array for BP(x,y)- Systolic
                                            else {
                                                bloodPressure = 1;
                                                if (vitltrend.vital.vitalCode == "4") {


                                                    if (!(BPsysArray[index1])) {
                                                        BPsysArray[index1] = [];

                                                    }



                                                    if ((vitltrend.vitalRecDate)) {
                                                        BPsysArray[index1].push($scope.systrends[ind1++]);
                                                        BPsysArray[index1].push(parseFloat(vitltrend.vitalValue));
                                                    }
                                                    index1 += 1;

                                                    console.log(BPsysArray);


                                                }
                                                //Separate array for BP(x,y)- Diastolic

                                                if (vitltrend.vital.vitalCode == "5") {


                                                    if (!(BPdiasArray[index2])) {
                                                        BPdiasArray[index2] = [];

                                                    }

                                                    if ((vitltrend.vitalRecDate)) {
                                                        BPdiasArray[index2].push($scope.diastrends[ind2++]);
                                                        BPdiasArray[index2].push(parseFloat(vitltrend.vitalValue));
                                                    }
                                                    index2 += 1;

                                                    console.log(BPdiasArray);

                                                }
                                            }
                                        });

                                //Chart for BP

                                if (bloodPressure == 1) {
                                    $scope.dias = {
                                        "values": BPdiasArray,
                                        "text": "Diastolic",
                                        "line-width": "2px",
                                        "line-color": "#25a6f7",
                                        "shadow": 0,
                                        "marker": {
                                            "background-color": "#fff",
                                            "size": 4,
                                            "border-width": 2,
                                            "border-color": "#25a6f7",
                                            "shadow": 0
                                        },
                                        "palette": 1,
                                        "visible": 1
                                    }

                                    $scope.VitalJson = {
                                        "type": "line",
                                        "plot": {
                                            "animation": {
                                                "effect": "1",
                                                "method": "3",
                                                "sequence": "ANIMATION_BY_PLOT_AND_NODE",
                                                "speed": 10
                                            }
                                        },
                                        "plotarea": {
                                            "margin": "25 25 125 25"
                                        },
                                        "scroll-x": {

                                        },
                                        "scale-y": {
                                            "line-color": "none",
                                            "guide": {
                                                "line-style": "solid",
                                                "line-color": "#d2dae2",
                                                "line-width": "1px",
                                                "alpha": 0.5
                                            },
                                            "tick": {
                                                "visible": true
                                            },
                                            "item": {
                                                "font-color": "#8391a5",
                                                "font-size": "10px",
                                                "padding-right": "5px"
                                            }
                                        },
                                        "preview": {},

                                        "scale-x": {
                                            "mirrored": false,
                                            "zooming": true,
                                            "zoom-to": [0, 50],
                                            "decimals":3,
                                            "item": {
                                                "font-size": 10
                                            },
                                            "values":
                                                $scope.date.reverse(),
                                            "line-color": "#d2dae2",
                                            "line-width": "2px",
                                            "tick": {
                                                "line-color": "#d2dae2",
                                                "line-width": "1px"
                                            },
                                            "guide": {
                                                "visible": true
                                            },
                                            "item": {
                                                "font-color": "#8391a5",
                                                "font-size": "10px",
                                                "padding-top": "5px",
                                            }
                                        },
                                        // "legend": {
                                        //     "layout": "x4",
                                        //     "background-color": "none",
                                        //     "shadow": 0,
                                        //     "margin": "15 auto auto 15",
                                        //     "border-width": 0,
                                        //     "item": {
                                        //         "font-color": "#707d94",
                                        //         "padding": "0px",
                                        //         "margin": "0px",
                                        //         "font-size": "11px"
                                        //     },
                                        //     "marker": {
                                        //         "show-line": "true",
                                        //         "type": "match",
                                        //         "font-family": "Arial",
                                        //         "font-size": "10px",
                                        //         "size": 4,
                                        //         "line-width": 2,
                                        //         "padding": "3px"
                                        //     }
                                        // },
                                        "crosshair-x": {
                                            "lineWidth": 1,
                                            "line-color": "#707d94",
                                            "plotLabel": {
                                                "shadow": false,
                                                "font-color": "#000",
                                                "font-family": "Arial",
                                                "font-size": "10px",
                                                "padding": "5px 10px",
                                                "border-radius": "5px",
                                                "alpha": 1
                                            },
                                            "scale-label": {
                                                "font-color": "#ffffff",
                                                "background-color": "#707d94",
                                                "font-family": "Arial",
                                                "font-size": "10px",
                                                "padding": "5px 10px",
                                                "border-radius": "5px"
                                            }
                                        },
                                        "tooltip": {
                                            "visible": false
                                        },
                                        "series": [
                                            {
                                                "values": BPsysArray,
                                                "text": "Systolic",
                                                "line-color": "#D37E04",
                                                "line-width": "2px",
                                                "shadow": 0,
                                                "marker": {
                                                    "background-color": "#fff",
                                                    "size": 4,
                                                    "border-width": 2,
                                                    "border-color": "#D37E04",
                                                    "shadow": 0
                                                },
                                                "palette": 0

                                                // "visible": 1
                                            }

                                        ]
                                    };

                                    $scope.VitalJson.series.push($scope.dias);



                                }

                                //Chart for Other Vitals
                                else {
                                    $scope.VitalJson = {
                                        "type": "line",
                                        "plot": {
                                            "animation": {
                                                "effect": "1",
                                                "method": "3",
                                                "sequence": "ANIMATION_BY_PLOT_AND_NODE",
                                                "speed": 10
                                            }
                                        },
                                        "scroll-x": {

                                        },
                                        "plotarea": {
                                            "margin": "25 25 125 25"
                                        },
                                        "scale-y": {
                                            "line-color": "none",
                                            "guide": {
                                                "line-style": "solid",
                                                "line-color": "#d2dae2",
                                                "line-width": "1px",
                                                "alpha": 0.5
                                            },
                                            "tick": {
                                                "visible": true
                                            },
                                            "item": {
                                                "font-color": "#8391a5",
                                                "font-size": "10px",
                                                "padding-right": "5px"
                                            }
                                        },
                                        "preview": {},

                                        "scale-x": {
                                            "mirrored": false,
                                            "zooming": true,
                                            "zoom-to": [0, 50],
                                            "decimals":3,
                                            "item": {
                                                "font-size": 10
                                            },
                                            "values": $scope.date.reverse(),
                                            "line-color": "#d2dae2",
                                            "line-width": "2px",
                                            "tick": {
                                                "line-color": "#d2dae2",
                                                "line-width": "1px"
                                            },
                                            "guide": {
                                                "visible": true
                                            },
                                            "item": {
                                                "font-color": "#8391a5",
                                                "font-size": "10px",
                                                "padding-top": "5px"
                                            }
                                        },
                                        // "legend": {
                                        //     "layout": "x4",
                                        //     "background-color": "none",
                                        //     "shadow": 0,
                                        //     "margin": "15 auto auto 15",
                                        //     "border-width": 0,
                                        //     "item": {
                                        //         "font-color": "#707d94",
                                        //         "padding": "0px",
                                        //         "margin": "0px",
                                        //         "font-size": "11px"
                                        //     },
                                        //     "marker": {
                                        //         "show-line": "true",
                                        //         "type": "match",
                                        //         "font-family": "Arial",
                                        //         "font-size": "10px",
                                        //         "size": 4,
                                        //         "line-width": 2,
                                        //         "padding": "3px"
                                        //     }
                                        // },
                                        "crosshair-x": {
                                            "lineWidth": 1,
                                            "line-color": "#707d94",
                                            "plotLabel": {
                                                "shadow": false,
                                                "font-color": "#000",
                                                "font-family": "Arial",
                                                "font-size": "10px",
                                                "padding": "5px 10px",
                                                "border-radius": "5px",
                                                "alpha": 1
                                            },
                                            "scale-label": {
                                                "font-color": "#ffffff",
                                                "background-color": "#707d94",
                                                "font-family": "Arial",
                                                "font-size": "10px",
                                                "padding": "5px 10px",
                                                "border-radius": "5px"
                                            }
                                        },
                                        "tooltip": {
                                            "visible": false
                                        },
                                        "series": [
                                            {
                                                "values": tempArray,
                                                "text": $scope.vitalName,
                                                "line-color": "#D37E04",
                                                "line-width": "2px",
                                                "shadow": 0,
                                                "marker": {
                                                    "background-color": "#fff",
                                                    "size": 4,
                                                    "border-width": 2,
                                                    "border-color": "#D37E04",
                                                    "shadow": 0
                                                },
                                                "palette": 0

                                            }

                                        ]
                                    };
                                }

                            });
                    }
                });


                //On drop down selection (same as ON LOAD)

                $scope.VitalTrend = function (vitalName) {

                    if (vitalName) {

                        $scope.date = [];
                        var index = 0;
                        var index1 = 0;
                        var index2 = 0;
                        var ind = 0;
                        var ind1 = 0;
                        var ind2 = 0;
                        var dateTrends = [];
                        $scope.systrends = [];
                        $scope.diastrends = [];

                        //Find start date of current year

                        var thisYear = (new Date()).getFullYear();
                        var start = new Date("1/1/" + thisYear);
                        var defaultStart = moment(start.valueOf()).format('YYYY-MM-DD');

                        var AllDates = [];

                        //To get the results for a particular vital (1st vital)

                        vitaltrends = '/ihspdc/rest/displayVitalTrends?patientId='
                        vitaltrends += $stateParams.id;
                        vitaltrends += "&vitalName=";
                        vitaltrends += vitalName;

                        $http.get(vitaltrends)
                            .then(function (response, err) {
                                if (!err) {
                                    console.log(response);
                                    $scope.trends = response.data;
                                } else {
                                    console.log('Error');

                                }


                                angular.forEach($scope.trends, function (trend) {

                                    if (trend.vitalRecDate) {
                                        AllDates.push(trend.vitalRecDate);

                                    }

                                }
                                );


                                var uniqueDates = [];

                                let uniqueArray = AllDates
                                    .map(function (date) { return date })
                                    .filter(function (date, i, array) {
                                        return array.indexOf(date) === i;
                                    });

                                for (i = 0; i < uniqueArray.length; i++) {
                                    uniqueDates[i] = moment(uniqueArray[i], "DD-MM-YYYY").format("YYYY-MM-DD");
                                }
                                console.log("Unique dates" + uniqueDates);
                                console.log("uniqueArray" + uniqueArray);



                                //Find latest date of all results


                                var maxDate = moment(uniqueDates[uniqueDates.length - 1], "YYYY-MM-DD").format("YYYY-MM-DD");


                                //Find last date of the current month

                                var date = new Date(maxDate);
                                y = date.getFullYear();
                                m = date.getMonth();
                                var lastDay = new Date(y, m + 1, 0)
                                lastDay = moment(lastDay, "YYYY-MM-DD").format('YYYY-MM-DD');

                                console.log("Last" + lastDay);

                                var flags = 0;

                                //Loop till latest date
                                for (var i = moment(defaultStart, "YYYY-MM-DD"); moment(i).diff(lastDay, 'days') <= 0; i = moment(i, "YYYY-MM-DD").add(1, 'd').format("YYYY-MM-DD")) {

                                    $scope.date.push(moment(i,
                                        'YYYY-MM-DD')
                                        .format(
                                            'MMM DD'));


                                }

                                angular.forEach($scope.trends, function (trend) {

                                    if (!(vitalName.match("Blood Pressure"))) {

                                        dateTrends.push(moment(trend.vitalRecDate,
                                            'DD-MM-YYYY')
                                            .format(
                                                'MMM DD'));
                                    }
                                    else if (trend.vital.vitalCode == $scope.sys_vital_code) {


                                        $scope.systrends.push(moment(trend.vitalRecDate,
                                            'DD-MM-YYYY')
                                            .format(
                                                'MMM DD'));

                                    }

                                    else if (trend.vital.vitalCode == $scope.dias_vital_code) {


                                        $scope.diastrends.push(moment(trend.vitalRecDate,
                                            'DD-MM-YYYY')
                                            .format(
                                                'MMM DD'));

                                    }




                                });


                                var tempArray = [];
                                var BPsysArray = [];
                                var BPdiasArray = [];

                                // Names: constant - DO NOT CHANGE 
                                var bloodPressure = 0;
                                angular
                                    .forEach(
                                        $scope.trends,
                                        function (
                                            vitltrend,
                                            i) {

                                            //To store the values(x,y)

                                            //Separate array for other vitals

                                            if (!(vitltrend.vital.vitalName.match("Blood Pressure"))) {

                                                bloodPressure = 0;
                                                if (!(tempArray[index])) {
                                                    tempArray[index] = [];

                                                }



                                                if ((vitltrend.vitalRecDate)) {
                                                    console.log("VITAL TRENDS CHART");
                                                    console.log(vitltrend.vitalValue,parseFloat(vitltrend.vitalValue));
                                                    tempArray[index].push(dateTrends[ind++]);
                                                    tempArray[index].push(parseFloat(vitltrend.vitalValue));
                                                }
                                                index += 1;

                                                console.log(tempArray);
                                                console.log("temparray791");


                                            }
                                            //Separate array for BP(x,y)- Systolic
                                            else {
                                                bloodPressure = 1;
                                                if (vitltrend.vital.vitalCode == "4") {


                                                    if (!(BPsysArray[index1])) {
                                                        BPsysArray[index1] = [];

                                                    }



                                                    if ((vitltrend.vitalRecDate)) {

                                                        BPsysArray[index1].push($scope.systrends[ind1++]);
                                                        BPsysArray[index1].push(parseFloat(vitltrend.vitalValue));


                                                    }
                                                    index1 += 1;

                                                    console.log(BPsysArray);


                                                }
                                                //Separate array for BP(x,y)- Diastolic

                                                if (vitltrend.vital.vitalCode == "5") {


                                                    if (!(BPdiasArray[index2])) {
                                                        BPdiasArray[index2] = [];

                                                    }



                                                    if ((vitltrend.vitalRecDate)) {

                                                        BPdiasArray[index2].push($scope.diastrends[ind2++]);
                                                        BPdiasArray[index2].push(parseFloat(vitltrend.vitalValue));


                                                    }
                                                    index2 += 1;

                                                    console.log(BPdiasArray);

                                                }
                                            }
                                        });

                                //Chart for BP

                                if (bloodPressure == 1) {
                                    $scope.dias = {
                                        "values": BPdiasArray,
                                        "text": "Diastolic",
                                        "line-width": "2px",
                                        "line-color": "#25a6f7",
                                        "shadow": 0,
                                        "marker": {
                                            "background-color": "#fff",
                                            "size": 4,
                                            "border-width": 2,
                                            "border-color": "#25a6f7",
                                            "shadow": 0
                                        },
                                        "palette": 1,
                                        "visible": 1
                                    }

                                    $scope.VitalJson = {
                                        "type": "line",
                                        "plot": {
                                            "animation": {
                                                "effect": "1",
                                                "method": "3",
                                                "sequence": "ANIMATION_BY_PLOT_AND_NODE",
                                                "speed": 10
                                            }
                                        },
                                        "plotarea": {
                                            "margin": "25 25 125 25"
                                        },
                                        "scroll-x": {

                                        },
                                        "scale-y": {
                                            "line-color": "none",
                                            "guide": {
                                                "line-style": "solid",
                                                "line-color": "#d2dae2",
                                                "line-width": "1px",
                                                "alpha": 0.5
                                            },
                                            "tick": {
                                                "visible": true
                                            },
                                            "item": {
                                                "font-color": "#8391a5",
                                                "font-size": "10px",
                                                "padding-right": "2px"
                                            }
                                        },
                                        "preview": {},

                                        "scale-x": {
                                            "mirrored": false,
                                            "zooming": true,
                                            "zoom-to": [0, 50],
                                            "decimals":3,
                                            "item": {
                                                "font-size": 10
                                            },
                                            "values":
                                                $scope.date.reverse(),
                                            "line-color": "#d2dae2",
                                            "line-width": "2px",
                                            "tick": {
                                                "line-color": "#d2dae2",
                                                "line-width": "1px"
                                            },
                                            "guide": {
                                                "visible": true
                                            },
                                            "item": {
                                                "font-color": "#8391a5",
                                                "font-size": "10px",
                                                "padding-top": "5px",
                                            }
                                        },
                                        // "legend": {
                                        //     "layout": "x4",
                                        //     "background-color": "none",
                                        //     "shadow": 0,
                                        //     "margin": "15 auto auto 15",
                                        //     "border-width": 0,
                                        //     "item": {
                                        //         "font-color": "#707d94",
                                        //         "padding": "0px",
                                        //         "margin": "0px",
                                        //         "font-size": "11px"
                                        //     },
                                        //     "marker": {
                                        //         "show-line": "true",
                                        //         "type": "match",
                                        //         "font-family": "Arial",
                                        //         "font-size": "10px",
                                        //         "size": 4,
                                        //         "line-width": 2,
                                        //         "padding": "3px"
                                        //     }
                                        // },
                                        "crosshair-x": {
                                            "lineWidth": 1,
                                            "line-color": "#707d94",
                                            "plotLabel": {
                                                "shadow": false,
                                                "font-color": "#000",
                                                "font-family": "Arial",
                                                "font-size": "10px",
                                                "padding": "5px 10px",
                                                "border-radius": "5px",
                                                "alpha": 1
                                            },
                                            "scale-label": {
                                                "font-color": "#ffffff",
                                                "background-color": "#707d94",
                                                "font-family": "Arial",
                                                "font-size": "10px",
                                                "padding": "5px 10px",
                                                "border-radius": "5px"
                                            }
                                        },
                                        "tooltip": {
                                            "visible": false
                                        },
                                        "series": [
                                            {
                                                "values": BPsysArray,
                                                "text": "Systolic",
                                                "line-color": "#D37E04",
                                                "line-width": "2px",
                                                "shadow": 0,
                                                "marker": {
                                                    "background-color": "#fff",
                                                    "size": 4,
                                                    "border-width": 2,
                                                    "border-color": "#D37E04",
                                                    "shadow": 0
                                                },
                                                "palette": 0

                                                // "visible": 1
                                            }

                                        ]
                                    };

                                    $scope.VitalJson.series.push($scope.dias);



                                }

                                //Chart for Other Vitals
                                else {
                                    $scope.VitalJson = {
                                        "type": "line",
                                        "plot": {
                                            "animation": {
                                                "effect": "1",
                                                "method": "3",
                                                "sequence": "ANIMATION_BY_PLOT_AND_NODE",
                                                "speed": 10
                                            }
                                        },
                                        "scroll-x": {

                                        },
                                        "plotarea": {
                                            "margin": "25 25 125 25"
                                        },
                                        "scale-y": {
                                            "line-color": "none",
                                            "guide": {
                                                "line-style": "solid",
                                                "line-color": "#d2dae2",
                                                "line-width": "1px",
                                                "alpha": 0.5
                                            },
                                            "tick": {
                                                "visible": true
                                            },
                                            "item": {
                                                "font-color": "#8391a5",
                                                "font-size": "10px",
                                                "padding-right": "2px"
                                            }
                                        },
                                        "preview": {},

                                        "scale-x": {
                                            "mirrored": false,
                                            "zooming": true,
                                            "zoom-to": [0, 50],
                                            "decimals":3,
                                            "item": {
                                                "font-size": 10
                                            },
                                            "values": $scope.date.reverse(),
                                            "line-color": "#d2dae2",
                                            "line-width": "2px",
                                            "tick": {
                                                "line-color": "#d2dae2",
                                                "line-width": "1px"
                                            },
                                            "guide": {
                                                "visible": true
                                            },
                                            "item": {
                                                "font-color": "#8391a5",
                                                "font-size": "10px",
                                                "padding-top": "5px"
                                            }
                                        },
                                        // "legend": {
                                        //     "layout": "x4",
                                        //     "background-color": "none",
                                        //     "shadow": 0,
                                        //     "margin": "15 auto auto 15",
                                        //     "border-width": 0,
                                        //     "item": {
                                        //         "font-color": "#707d94",
                                        //         "padding": "0px",
                                        //         "margin": "0px",
                                        //         "font-size": "11px"
                                        //     },
                                        //     "marker": {
                                        //         "show-line": "true",
                                        //         "type": "match",
                                        //         "font-family": "Arial",
                                        //         "font-size": "10px",
                                        //         "size": 4,
                                        //         "line-width": 2,
                                        //         "padding": "3px"
                                        //     }
                                        // },
                                        "crosshair-x": {
                                            "lineWidth": 1,
                                            "line-color": "#707d94",
                                            "plotLabel": {
                                                "shadow": false,
                                                "font-color": "#000",
                                                "font-family": "Arial",
                                                "font-size": "10px",
                                                "padding": "5px 10px",
                                                "border-radius": "5px",
                                                "alpha": 1
                                            },
                                            "scale-label": {
                                                "font-color": "#ffffff",
                                                "background-color": "#707d94",
                                                "font-family": "Arial",
                                                "font-size": "10px",
                                                "padding": "5px 10px",
                                                "border-radius": "5px"
                                            }
                                        },
                                        "tooltip": {
                                            "visible": false
                                        },
                                        "series": [
                                            {
                                                "values": tempArray,
                                                "text": vitalName,
                                                "line-color": "#D37E04",
                                                "line-width": "2px",
                                                "shadow": 0,
                                                "marker": {
                                                    "background-color": "#fff",
                                                    "size": 4,
                                                    "border-width": 2,
                                                    "border-color": "#D37E04",
                                                    "shadow": 0
                                                },
                                                "palette": 0

                                            }

                                        ]
                                    };



                                }


                            });
                    }

                }
            }

        ]);
