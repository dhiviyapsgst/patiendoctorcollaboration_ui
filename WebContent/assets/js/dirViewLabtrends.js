(function () {
    'use strict';
    var App = angular.module('app');
    App.directive('dirViewLabtrends', dirViewLabtrends);
    dirViewLabtrends.$inject = ['$rootScope', '$compile'];

    function dirViewLabtrends($rootScope, $compile) {
        return {
            restrict: 'EA',
            templateUrl: 'assets/views/labResults/ViewLabtrends.html',
            controller: 'ViewLabtrendsController',
            replace: true,
            scope: true,
            link: function (scope, element) {
            }

        };
    }

})();

App.controller(
    'ViewLabtrendsController',
    [
        '$scope',
        '$rootScope',
        '$http',
        '$localStorage',
        '$state',
        '$stateParams',
        '$window',
        function ViewLabtrendsController($scope, $rootScope,
            $http, $localStorage, $state, $stateParams,
            $window) {

            $scope.grp_set = false;

            $scope.date_lab = [];
            $scope.AllTests = [];
            var index_lab = 0;
            var index_grp = 0;
            var ind_lab = 0;
            var dateTrends_lab = [];
            var AllDates_lab = [];

            var thisYear_lab = (new Date()).getFullYear();
            var start_lab = new Date("1/1/" + thisYear_lab);
            var defaultStart_lab = moment(start_lab.valueOf()).format('YYYY-MM-DD');


            /*
            *Change during display grp charts
            */








            getAllGroups = '/ihspdc/rest/getallgroups'
            $http.get(getAllGroups).then(function (response, err) {
                if (!err) {
                    console.log(response.data);
                    // $scope.grps = response.data;
                    // angular.forEach($scope.grps, function (testgrp) {
                    //     if (testgrp.groupName)
                    //         $scope.AllTests.push(testgrp.groupName);
                    // });

                    //To populate drop down 
                    getAllTests = '/ihspdc/rest/getalltests'
                    $http.get(getAllTests).then(function (response, err) {
                        if (!err) {
                            console.log(response.data);
                            $scope.labTests = response.data;
                            angular.forEach($scope.labTests, function (tests) {
                                if (tests.testName)
                                    $scope.AllTests.push(tests.testName);
                                $scope.testName = $scope.AllTests[0];
                            });
                            //*** 
                            $scope.booleanvar = true;
                            labtrends = '/ihspdc/rest/displayLabTrends?patientId='
                            labtrends += $stateParams.id;
                            labtrends += '&testName=';
                            labtrends += $scope.testName;
                            $http.get(labtrends)
                                .then(function (response, err) {
                                    if (!err) {
                                        console.log(response);
                                        $scope.Labtrends = response.data;
                                        console.log("Lab trendssss.. ");
                                        console.log($scope.Labtrends);
                                    } else {
                                        console.log('Error');
        
                                    }
        
                                    //     for(i=0;i<$scope.Labtrends.length;i++)
                                    //     {
                                    //         for(j=0;j<$scope.grps;j++){
                                    //         if($scope.Labtrends.labTest.testName == $scope.grps[j])
                                    //         {
                                    //             $scope.grp_set=true;
                                    //         }
                                    //     }
                                    // }
        
        
                                    if ($scope.booleanvar == true) {
        
                                        //To store all dates
                                        angular.forEach($scope.Labtrends, function (trend) {
        
                                            if (trend.testDateTime) {
                                                AllDates_lab.push(trend.testDateTime);
        
                                            }
        
                                        }
                                        );
                                        console.log(AllDates_lab);
        
                                        var uniqueDates = [];
        
        
                                        //Find latest date of all results
        
        
                                        var maxDate_lab = moment(AllDates_lab[AllDates_lab.length - 1], "DD-MM-YYYY").format("YYYY-MM-DD");
        
        
                                        //Find last date of the current month
        
        
                                        var date = new Date(maxDate_lab);
                                        y = date.getFullYear();
                                        m = date.getMonth();
                                        var lastDay_lab = new Date(y, m + 1, 0)
                                        lastDay_lab = moment(lastDay_lab, "YYYY-MM-DD").format('YYYY-MM-DD');
                                        console.log("Last" + lastDay_lab);
        
                                        var flags = 0;
        
                                        //Loop till latest date
                                        for (var i = moment(defaultStart_lab, "YYYY-MM-DD"); moment(i).diff(lastDay_lab, 'days') <= 0; i = moment(i, "YYYY-MM-DD").add(1, 'd').format("YYYY-MM-DD")) {
                                            $scope.date_lab.push(moment(i, 'YYYY-MM-DD').format('MMM DD'));
                                        }
        
                                        //To store dates to plot values
        
                                        angular.forEach($scope.Labtrends, function (trend) {
                                            if (trend.testDateTime) {
                                                dateTrends_lab.push(moment(trend.testDateTime,
                                                    'DD-MM-YYYY')
                                                    .format(
                                                        'MMM DD'));
                                            }
        
        
        
                                        });
        
                                        if ($scope.grp_set == true) {
                                            $scope.grp = [];
        
        
                                            angular
                                                .forEach(
                                                    $scope.Labtrends,
                                                    function (
                                                        labtrend,
                                                        i) {
        
                                                        //To store the values(x,y)
        
        
                                                        if ((labtrend.labTest)) {
        
        
                                                            if (!($scope.grp[index_grp])) {
                                                                $scope.grp[index_grp] = {};
                                                            }
        
        
        
                                                            $scope.grp.push({
                                                                "testName": labtrend.labTest.testName,
                                                                "testDate": dateTrends_lab[ind_lab++],
                                                                "testValue": parseFloat(labtrend.testValue)
                                                            });
        
        
                                                            console.log("Group: ");
                                                            console.log($scope.grp);
        
        
                                                        }
                                                        index_grp += 1;
        
        
                                                    });
        
                                        }// close this in the enddd
        
                                        else {
        
        
        
                                            var tempArray_lab = []; //To store values for a single lab test
        
                                            angular
                                                .forEach(
                                                    $scope.Labtrends,
                                                    function (
                                                        labtrend,
                                                        i) {
        
                                                        //To store the values(x,y)
        
        
                                                        if ((labtrend.labTest.testName)) {
        
        
                                                            if (!(tempArray_lab[index_lab])) {
                                                                tempArray_lab[index_lab] = [];
                                                            }
        
                                                            if (labtrend.testDateTime) {
        
                                                                tempArray_lab[index_lab].push(dateTrends_lab[ind_lab++]);
                                                                tempArray_lab[index_lab].push(parseFloat(labtrend.testValue));
        
        
                                                            }
                                                            index_lab += 1;
        
                                                            console.log(tempArray_lab);
        
        
                                                        }
        
        
                                                    });
        
        
                                            $scope.labJson = {
                                                "type": "line",
                                                "plot": {
                                                    "animation": {
                                                        "effect": "11",
                                                        "method": "3",
                                                        "sequence": "ANIMATION_BY_PLOT_AND_NODE",
                                                        "speed": 10
                                                    }
                                                },
                                                "scroll-x": {
        
                                                },
                                                "plotarea": {
                                                    "margin": "25 25 125 25"
                                                },
                                                "scale-y": {
                                                    "line-color": "none",
                                                    "guide": {
                                                        "line-style": "solid",
                                                        "line-color": "#d2dae2",
                                                        "line-width": "1px",
                                                        "alpha": 0.5
                                                    },
                                                    "tick": {
                                                        "visible": true
                                                    },
                                                    "item": {
                                                        "font-color": "#8391a5",
                                                        "font-size": "10px",
                                                        "padding-right": "5px"
                                                    }
                                                },
                                                "preview": {},
                                                "scale-x": {
                                                    "mirrored": false,
                                                    "zooming": true,
                                                    "zoom-to": [0, 50],
                                                    "decimals": 3,
                                                    "item": {
                                                        "font-size": 10
                                                    },
                                                    "line-color": "#d2dae2",
                                                    "line-width": "2px",
                                                    "values": $scope.date_lab.reverse(),
                                                    "tick": {
                                                        "line-color": "#d2dae2",
                                                        "line-width": "1px"
                                                    },
                                                    "guide": {
                                                        "visible": true
                                                    },
                                                    "item": {
                                                        "font-color": "#8391a5",
                                                        "font-size": "10px",
                                                        "padding-top": "5px"
                                                    }
                                                },
                                                // "legend": {
                                                //     "layout": "x4",
                                                //     "background-color": "none",
                                                //     "shadow": 0,
                                                //     "margin": "15 auto auto 15",
                                                //     "border-width": 0,
                                                //     "item": {
                                                //         "font-color": "#707d94",
                                                //         "padding": "0px",
                                                //         "margin": "0px",
                                                //         "font-size": "11px"
                                                //     },
                                                //     "marker": {
                                                //         "show-line": "true",
                                                //         "type": "match",
                                                //         "font-family": "Arial",
                                                //         "font-size": "10px",
                                                //         "size": 4,
                                                //         "line-width": 2,
                                                //         "padding": "3px"
                                                //     }
                                                // },
                                                "crosshair-x": {
                                                    "lineWidth": 1,
                                                    "line-color": "#707d94",
                                                    "plotLabel": {
                                                        "shadow": false,
                                                        "font-color": "#000",
                                                        "font-family": "Arial",
                                                        "font-size": "10px",
                                                        "padding": "5px 10px",
                                                        "border-radius": "5px",
                                                        "alpha": 1
                                                    },
                                                    "scale-label": {
                                                        "font-color": "#ffffff",
                                                        "background-color": "#707d94",
                                                        "font-family": "Arial",
                                                        "font-size": "10px",
                                                        "padding": "5px 10px",
                                                        "border-radius": "5px"
                                                    }
                                                },
                                                "tooltip": {
                                                    "visible": false
                                                },
                                                "series": [
                                                    {
                                                        "values": tempArray_lab,
                                                        "text": $scope.testName,
                                                        "line-color": "#D37E04",
                                                        "line-width": "3px",
                                                        "shadow": 0,
                                                        "marker": {
                                                            "background-color": "#fff",
                                                            "size": 4,
                                                            "border-width": 2,
                                                            "border-color": "#D37E04",
                                                            "shadow": 0
                                                        },
                                                        "palette": 0
        
                                                    }
        
                                                ]
                                            };
        
        
                                        }
                                    }
                                });
                            // ****
                        }
                    });

                }
            });









            //To call the chart for first test

            //On drop down selection (same as ON LOAD)


            $scope.LabTrend = function (testName) {


                if (testName) {


                    var thisYear_lab = (new Date()).getFullYear();
                    var start_lab = new Date("1/1/" + thisYear_lab);
                    var defaultStart_lab = moment(start_lab.valueOf()).format('YYYY-MM-DD');

                    labtrends = '/ihspdc/rest/displayLabTrends?patientId='
                    labtrends += $stateParams.id;
                    labtrends += '&testName=';
                    labtrends += testName
                    $http.get(labtrends)
                        .then(function (response, err) {
                            if (!err) {
                                console.log(response);
                                $scope.Labtrends = response.data;
                            } else {
                                console.log('Error');

                            }

                            //To store all dates
                            angular.forEach($scope.Labtrends, function (trend) {

                                if (trend.testDateTime) {
                                    AllDates_lab.push(trend.testDateTime);

                                }

                            }
                            );
                            console.log(AllDates_lab);

                            var uniqueDates = [];


                            //Find latest date of all results


                            var maxDate_lab = moment(AllDates_lab[AllDates_lab.length - 1], "DD-MM-YYYY").format("YYYY-MM-DD");


                            //Find last date of the current month

                            var date_lab = new Date(maxDate_lab);
                            y1 = date_lab.getFullYear();
                            m1 = date_lab.getMonth();
                            var lastDay_lab = new Date(y1, m1 + 1, 0)
                            lastDay_lab = moment(lastDay_lab, "YYYY-MM-DD").format('YYYY-MM-DD');

                            console.log("Last" + lastDay_lab);

                            var flags = 0;


                            //Loop till latest date

                            for (var i = moment(defaultStart_lab, "YYYY-MM-DD"); moment(i).diff(lastDay_lab, 'days') <= 0; i = moment(i, "YYYY-MM-DD").add(1, 'd').format("YYYY-MM-DD")) {

                                $scope.date_lab.push(moment(i,
                                    'YYYY-MM-DD')
                                    .format(
                                        'MMM DD'));
                            }

                            angular.forEach($scope.Labtrends, function (trend) {



                                if (trend.testDateTime) {

                                    dateTrends_lab.push(moment(trend.testDateTime,
                                        'DD-MM-YYYY')
                                        .format(
                                            'MMM DD'));
                                }



                            });


                            var tempArray_lab = [];

                            angular
                                .forEach(
                                    $scope.Labtrends,
                                    function (
                                        labtrend,
                                        i) {

                                        //To store the values(x,y)


                                        if ((labtrend.labTest.testName)) {


                                            if (!(tempArray_lab[index_lab])) {
                                                tempArray_lab[index_lab] = [];

                                            }

                                            if (labtrend.testDateTime) {

                                                tempArray_lab[index_lab].push(dateTrends_lab[ind_lab++]);
                                                tempArray_lab[index_lab].push(parseFloat(labtrend.testValue));


                                            }
                                            index_lab += 1;

                                            console.log(tempArray_lab);


                                        }


                                    });


                            $scope.labJson = {
                                "type": "line",
                                "plot": {
                                    "animation": {
                                        "effect": "11",
                                        "method": "3",
                                        "sequence": "ANIMATION_BY_PLOT_AND_NODE",
                                        "speed": 10
                                    }
                                },
                                "scroll-x": {

                                },
                                "plotarea": {
                                    "margin": "25 25 125 25"
                                },
                                "scale-y": {
                                    "line-color": "none",
                                    "guide": {
                                        "line-style": "solid",
                                        "line-color": "#d2dae2",
                                        "line-width": "1px",
                                        "alpha": 0.5
                                    },
                                    "tick": {
                                        "visible": true
                                    },
                                    "item": {
                                        "font-color": "#8391a5",
                                        "font-size": "10px",
                                        "padding-right": "5px"
                                    }
                                },
                                "preview": {},
                                "scale-x": {
                                    "mirrored": false,
                                    "zooming": true,
                                    "zoom-to": [0, 50],
                                    "decimals": 3,
                                    "item": {
                                        "font-size": 10
                                    },
                                    "line-color": "#d2dae2",
                                    "line-width": "2px",
                                    "values": $scope.date_lab.reverse(),
                                    "tick": {
                                        "line-color": "#d2dae2",
                                        "line-width": "1px"
                                    },
                                    "guide": {
                                        "visible": true
                                    },
                                    "item": {
                                        "font-color": "#8391a5",
                                        "font-size": "10px",
                                        "padding-top": "5px"
                                    }
                                },
                                // "legend": {
                                //     "layout": "x4",
                                //     "background-color": "none",
                                //     "shadow": 0,
                                //     "margin": "15 auto auto 15",
                                //     "border-width": 0,
                                //     "item": {
                                //         "font-color": "#707d94",
                                //         "padding": "0px",
                                //         "margin": "0px",
                                //         "font-size": "11px"
                                //     },
                                //     "marker": {
                                //         "show-line": "true",
                                //         "type": "match",
                                //         "font-family": "Arial",
                                //         "font-size": "10px",
                                //         "size": 4,
                                //         "line-width": 2,
                                //         "padding": "3px"
                                //     }
                                // },
                                "crosshair-x": {
                                    "lineWidth": 1,
                                    "line-color": "#707d94",
                                    "plotLabel": {
                                        "shadow": false,
                                        "font-color": "#000",
                                        "font-family": "Arial",
                                        "font-size": "10px",
                                        "padding": "5px 10px",
                                        "border-radius": "5px",
                                        "alpha": 1
                                    },
                                    "scale-label": {
                                        "font-color": "#ffffff",
                                        "background-color": "#707d94",
                                        "font-family": "Arial",
                                        "font-size": "10px",
                                        "padding": "5px 10px",
                                        "border-radius": "5px"
                                    }
                                },
                                "tooltip": {
                                    "visible": false
                                },
                                "series": [
                                    {
                                        "values": tempArray_lab,
                                        "text": testName,
                                        "line-color": "#D37E04",
                                        "line-width": "2px",
                                        "shadow": 0,
                                        "marker": {
                                            "background-color": "#fff",
                                            "size": 4,
                                            "border-width": 2,
                                            "border-color": "#D37E04",
                                            "shadow": 0
                                        },
                                        "palette": 0

                                    }

                                ]
                            };


                        });

                }
            }

        }

    ]);
