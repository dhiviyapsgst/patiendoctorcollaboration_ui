(function () {
    'use strict';
    var App = angular.module('app');
    App.directive('dirViewConsultancy', dirViewConsultancy);
    dirViewConsultancy.$inject = ['$rootScope', '$compile'];

    function dirViewConsultancy($rootScope, $compile) {
        return {
            restrict: 'EA',
            templateUrl: 'assets/views/consultancy/ViewConsultancy.html',
            controller: 'ViewConsultancyController',
            replace: true,
            scope: true,
            link: function (scope, element) {
            }
        };
    }
    App.controller(
        'ViewConsultancyController',
        [
            '$scope',
            '$rootScope',
            '$http',
            '$localStorage',
            '$state',
            '$stateParams',
            '$window',
            'messageService', 'patientService', '$timeout',
            function ViewConsultancyController($scope, $rootScope,
                $http, $localStorage, $state, $stateParams,
                $window, messageService, patientService, $timeout) {

                console.log("ViewConsul");
                $scope.con = {};
                //DATETIME PICKER
                $(function () {
                    $('#datetimepicker4').datetimepicker({ useCurrent: false });
                });

                //SAVING OF CONSULATATION NOTES
                $scope.registerConsultancyNotes = function (consultationForm, notesAdded) {
                    console.log(notesAdded);
                    if (!consultationForm.validate()) {
                        // event.preventDefault();
                        // event.stopPropagation();
                    }
                    else {
                        if (!notesAdded.doctor) {
                            document.getElementById("doctorError").innerHTML = "Doctor is required";
                        }
                        else if (!notesAdded.modeOfVisit) {
                            document.getElementById("visitType").innerHTML = "Mode of visit is required";
                        }
                        else if (notesAdded.modeOfVisit == "In-person" && $scope.finalUploadedFilesConsolts.length == 0) {
                            $rootScope.hideAlert();
                            $("html, body").animate({ scrollTop: 0 }, 200);
                            $rootScope.errMsg = true;
                            $rootScope.errorMsg = "Please upload one or more consultation documents";
                            jQuery("#errId").focus();
                            jQuery("#errId").show();
                        }
                        else {
                            var dateInp = $("#datetimepicker4").val().substr(0, 10);
                            notesAdded.date = dateInp;
                            notesAdded.time = $("#datetimepicker4").val().substr(12, 10);
                            notesAdded.coordinator = $scope.coordinator;
                            notesAdded.patient = $scope.patientDetail;
                            notesAdded.files = $scope.finalUploadedFilesConsolts;
                            $http({
                                url: '/ihspdc/rest/addConsultancyNotes',
                                data: notesAdded,
                                method: 'POST',
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(function (response) {
                                $rootScope.hideAlert();
                                $("html, body").animate({ scrollTop: 0 }, 200);
                                $rootScope.sucMsg = true;
                                $rootScope.successMsg = "Consultation notes has been successfully saved";
                                jQuery("#successId").focus();
                                jQuery("#successId").show();
                                $scope.clearNotes();
                                notesAdded = {};
                                $scope.retriveConsoltations();
                                $scope.cancelUpload();
                            }, function (error) {
                                console.log("Error in Adding Consultation notes");
                            });
                        }
                    }
                }

                //VALIDATION PART OF CONSULATATION NOTES
                $scope.validateConsultationForm = {
                    debug: false,
                    errorElement: "div",
                    rules: {
                        doctor: {
                            required: true
                        },
                        notesDate: {
                            required: true
                        },
                        patientQuery: {
                            required: true
                        },
                        doctorResponse: {
                            required: true,
                        },
                        visitType: {
                            required: true
                        }
                    },
                    messages: {
                        doctor: {
                            required: "Doctor is required"
                        },
                        visitType: {
                            required: "Mode of visit is required"
                        },
                        notesDate: {
                            required: "Consulatation Date and time is required"
                        },
                        patientQuery: {
                            required: "Patient query is required"
                        },
                        doctorResponse: {
                            required: "Doctor response is required",
                        }
                    },
                    highlight: function (element, errorClass) {
                        $(element).removeClass(errorClass);
                    }
                }

                //MODE OF VISITS
                $scope.visitMode = ['Remote', 'In-person'];

                //RETRIVING PATIENT INFO
                var patInfo = '/ihspdc/rest/displayPatientDetails?patientId='
                patInfo += $stateParams.id;
                $http.get(patInfo).then(
                    function (response, err) {
                        if (!err) {
                            $scope.patientDetail = response.data;
                        } else {
                            console.log('Error');
                        }
                    });

                //RETRIVIING DOCTOR DETAILS ACCORDING TO COORDINATOR DEPARTMENT
                if ($rootScope.Auth && ($rootScope.Auth.hasResourceRole('coord'))) {
                    patientService.getSenderInfo($rootScope.Auth.tokenParsed.preferred_username, function (err, response) {
                        $scope.coordinator = response.data;
                        var doctorsDetails = '/ihspdc/rest/getDoctorsList';
                        $scope.departmentDoctors = [];
                        $http.get(doctorsDetails).then(function (response, err) {
                            if (!err && response.data != "") {
                                response.data.sort(function (a, b) {
                                    return a.docName.localeCompare(b.docName);
                                });
                                $scope.AllDoctors = response.data;
                                angular.forEach($scope.AllDoctors, function (doc) {
                                    if (doc.department.name == $scope.coordinator.department.name) {
                                        $scope.departmentDoctors.push(doc);
                                    }
                                });
                            } else {
                                console.log('Error');
                            }
                        });
                    });
                }

                //CLEAR NOTES
                $scope.clearNotes = function () {
                    $scope.con = {};
                    document.getElementById("datetimepicker4").value = "";
                }

                //CHECKING DUPLICATE FUNCTION
                $scope.checkDuplicateInObject = function (propertyName, inputArray) {
                    var seenDuplicate = false,
                        testObject = {};
                    inputArray.map(function (item) {
                        var itemPropertyName = item[propertyName];
                        if (itemPropertyName in testObject) {
                            testObject[itemPropertyName].duplicate = true;
                            item.duplicate = true;
                            seenDuplicate = true;
                        }
                        else {
                            testObject[itemPropertyName] = item;
                            delete item.duplicate;
                        }
                    });
                    return testObject;
                }


                //LOAD MORE
                $scope.nodeConsultancyLimit = 5;// initially shows 5 Nodes.
                var noMore = 0;
                $scope.loadMoreConsultancy = function () {
                    if ($scope.consultDates.length >= $scope.nodeConsultancyLimit) {
                        if (noMore != 1) {
                            if ($scope.consultDates.length >= ($scope.nodeConsultancyLimit + 5)) {
                                $scope.nodeConsultancyLimit += 5;
                            }
                            else {
                                $scope.nodeConsultancyLimit = $scope.consultDates.length;
                                $scope.loadConsultancyBtn = false;
                                noMore = 1;
                            }
                        }
                    }
                    else {
                        $scope.loadConsultancyBtn = false;
                    }
                }

                //RETRIVING PART - CONSILTATION NOTES
                var consultationDates = [];
                $scope.retriveConsoltations = function () {
                    $scope.consultDates = [];
                    patientService.getConsultationDates($stateParams.id, function (err, response) {
                        consultationDates = response.data;
                        angular.forEach(response.data, function (date) {
                            $scope.consultDates.push(moment(date, "DD-MM-YYYY, h:mm a").format("MMM DD, YYYY"));
                        });
                        if ($scope.consultDates.length > 5) {
                            $scope.loadConsultancyBtn = true;
                        }
                        if (consultationDates.length > 0) {
                            $scope.getConsultationNotes(0);
                        }
                    });
                    $scope.displayConsultNotes = {};
                }
                $scope.retriveConsoltations();
                $scope.getConsultationNotes = function (index) {
                    var getConsults = '/ihspdc/rest/getConsultByDate?date=';
                    getConsults += consultationDates[index];
                    getConsults += '&opCode=';
                    getConsults += $stateParams.id;
                    $http.get(getConsults)
                        .then(function (response, err) {
                            if (!err) {
                                if (response.data.length > 0) {
                                    $scope.displayConsultNotes[index] = response.data;
                                    //to Show Files
                                    var AllFiles = [];
                                    $scope.displayConsultNotes[index].filesAssociated = {};
                                    var fCount = 0;
                                    angular.forEach($scope.displayConsultNotes[index], function (note) {
                                        if (fCount == 0) {
                                            angular.forEach(note.files, function (f) {
                                                $scope.displayConsultNotes[index].filesAssociated.description = f.description;
                                                AllFiles.push(f);
                                            });
                                            fCount += 1;
                                        }
                                    });

                                    var AllFilesRetrived = $scope.checkDuplicateInObject('id', AllFiles);
                                    $scope.displayConsultNotes[index].filesAssociated.files = AllFilesRetrived;
                                    $scope.showFiles = "";
                                    if (AllFiles.length > 0) {
                                        $scope.showFiles = true;
                                    }
                                    console.log($scope.displayConsultNotes[index]);
                                } else {
                                    console.log('Error');
                                }
                            }
                        });
                }

                //UPLOADFILES 
                $(function () {
                    $scope.finalUploadedFilesConsolts = [];
                    var uploadedFiles = [];
                    var newFile = {};
                    var removeFlag = false;
                    var myDropzoneConsolts = new Dropzone("form#myIdConsolts", { paramName: "inputfile", maxFilesize: 10, addRemoveLinks: true, parallelUploads: 15, maxFiles: 15, method: 'POST', url: "/ihspdc/rest/storefile", acceptedFiles: 'image/png,image/jpeg,image/jpg,application/pdf', dictDefaultMessage: 'Drop Specifications file' });
                    $scope.finalUploadedFilesConsolts = [];
                    $scope.finalUploadedFilesConsoltsLength = 0;

                    myDropzoneConsolts.on("error", function (file, errorMessage, xhr) {
                        $(file.previewElement).find('.dz-error-message').text(response);
                    });

                    myDropzoneConsolts.on("removedfile", function (file) {
                        if (removeFlag != true) {
                            console.log("Trying to remove me?", file.xhr.response);
                            rmFile = '/ihspdc/rest/removeFile?id='
                            rmFile += file.xhr.response;
                            $http.delete(rmFile);
                        }
                        removeFlag = false;
                    });

                    //try to reduce angular forEach if any time
                    myDropzoneConsolts.on("complete", function (file, xhr, formData) {
                        console.log(myDropzoneConsolts);
                        console.log("dropzone produced up");
                        uploadedFiles = [];
                        newFile = {};
                        if (uploadedFiles.length > 0) {
                            angular.forEach(myDropzoneConsolts.files, function (file) {
                                angular.forEach(uploadedFiles, function (dupFile) {
                                    newFile = {};
                                    if (dupFile.id != file.xhr.response) {
                                        newFile.fileName = file.name;
                                        newFile.id = file.xhr.response;
                                        uploadedFiles.push(newFile);
                                    }
                                });
                            });
                        }
                        else if (uploadedFiles.length == 0) {
                            angular.forEach(myDropzoneConsolts.files, function (file) {
                                newFile = {};
                                newFile.fileName = file.name;
                                newFile.id = file.xhr.response;
                                uploadedFiles.push(newFile);
                            });
                        }
                        console.log(uploadedFiles);
                        $scope.finalUploadedFilesConsolts = uploadedFiles;
                        $scope.finalUploadedFilesConsoltsLength = $scope.finalUploadedFilesConsolts.length;
                    });


                    $scope.uploadFilesFun = function () {
                        $scope.toShowFinalFilesConsolts = myDropzoneConsolts.files;
                        $scope.toShowFinalFilesConsoltsLength = $scope.toShowFinalFilesConsolts.length;
                        if ($scope.toShowFinalFilesConsoltsLength > 0) {
                            $scope.hideAlert();
                            $scope.uploadFlagConsolts = true;
                        }
                        else {
                            $scope.finalUploadedFilesConsoltsLength = 0;
                            $scope.uploadFlagConsolts = false;
                        }
                    }

                    $scope.cancelUpload = function () {
                        removeFlag = true;
                        myDropzoneConsolts.removeAllFiles();
                        $scope.uploadFlagConsolts = false;
                        $scope.finalUploadedFilesConsolts = [];
                        $scope.finalUploadedFilesConsoltsLength = $scope.finalUploadedFilesConsolts.length;
                        console.log(myDropzoneConsolts.files.length + "see length");
                    }
                });
                //ENDOFUPLOADFILES

                //RETRIVING FILE FUNCTION	
                $scope.getFile = function (id) {
                    var gfile = '/ihspdc/rest/getFile?id=' + id;
                    $('#modalForImage').modal('show');
                    $('.imagepreview').attr('src', gfile);
                }
            }
        ]);
})();