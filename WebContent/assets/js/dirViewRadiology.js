(function () {
    'use strict';
    var App = angular.module('app');
    App.directive('dirViewRadiology', dirViewRadiology);
    dirViewRadiology.$inject = ['$rootScope', '$compile'];

    function dirViewRadiology($rootScope, $compile) {
        return {
            restrict: 'EA',    
            templateUrl: 'assets/views/radiology/ViewRadiology.html',
            controller: 'ViewRadiologyController',
            replace: true,
            scope: true,
            link: function (scope, element) {
            }
        };
    }
    App.controller(
        'ViewRadiologyController',
        [
            '$scope',
            '$rootScope',
            '$http',
            '$localStorage',
            '$state',
            '$stateParams',
            '$window',
            'messageService', 'patientService', '$timeout',
            function ViewRadiologyController($scope, $rootScope,
                $http, $localStorage, $state, $stateParams,
                $window, messageService, patientService, $timeout) {
                console.log("View Radiology");
                $scope.rad = {};
                //RETRIVIING DOCTOR DETAILS ACCORDING TO COORDINATOR DEPARTMENT
                if ($rootScope.Auth && ($rootScope.Auth.hasResourceRole('coord'))) {
                    patientService.getSenderInfo($rootScope.Auth.tokenParsed.preferred_username, function (err, response) {
                        $scope.coordinator = response.data;
                        var doctorsDetails = '/ihspdc/rest/getDoctorsList';
                        $scope.departmentDoctors = [];
                        $http.get(doctorsDetails).then(function (response, err) {
                            if (!err && response.data != "") {
                                response.data.sort(function (a, b) {
                                    return a.docName.localeCompare(b.docName);
                                });
                                $scope.AllDoctors = response.data;
                                angular.forEach($scope.AllDoctors, function (doc) {
                                    if (doc.department.name == $scope.coordinator.department.name) {
                                        $scope.departmentDoctors.push(doc);
                                    }
                                });
                            } else {
                                console.log('Error');
                            }
                        });
                    });
                }


                //UPLOADFILES 
                $(function () {
                    $scope.finalUploadedFilesRadiology = [];
                    var uploadedFiles = [];
                    var newFile = {};
                    var removeFlag = false;
                    var myDropzoneRadiology = new Dropzone("form#myIdRadiology", { paramName: "inputfile", maxFilesize: 10, addRemoveLinks: true, parallelUploads: 15, maxFiles: 15, method: 'POST', url: "/ihspdc/rest/storefile", acceptedFiles: 'image/png,image/jpeg,image/jpg,application/pdf', dictDefaultMessage: 'Drop Specifications file' });
                    $scope.finalUploadedFilesRadiology = [];
                    $scope.finalUploadedFilesRadiologyLength = 0;

                    myDropzoneRadiology.on("error", function (file, errorMessage, xhr) {
                        $(file.previewElement).find('.dz-error-message').text(response);
                    });

                    myDropzoneRadiology.on("removedfile", function (file) {
                        if (removeFlag != true) {
                            console.log("Trying to remove me?", file.xhr.response);
                            rmFile = '/ihspdc/rest/removeFile?id='
                            rmFile += file.xhr.response;
                            $http.delete(rmFile);
                        }
                        removeFlag = false;
                    });

                    //try to reduce angular forEach if any time
                    myDropzoneRadiology.on("complete", function (file, xhr, formData) {
                        console.log(myDropzoneRadiology);
                        console.log("dropzone produced up");
                        uploadedFiles = [];
                        newFile = {};
                        if (uploadedFiles.length > 0) {
                            angular.forEach(myDropzoneRadiology.files, function (file) {
                                angular.forEach(uploadedFiles, function (dupFile) {
                                    newFile = {};
                                    if (dupFile.id != file.xhr.response) {
                                        newFile.fileName = file.name;
                                        newFile.id = file.xhr.response;
                                        uploadedFiles.push(newFile);
                                    }
                                });
                            });
                        }
                        else if (uploadedFiles.length == 0) {
                            angular.forEach(myDropzoneRadiology.files, function (file) {
                                newFile = {};
                                newFile.fileName = file.name;
                                newFile.id = file.xhr.response;
                                uploadedFiles.push(newFile);
                            });
                        }
                        console.log(uploadedFiles);
                        $scope.finalUploadedFilesRadiology = uploadedFiles;
                        $scope.finalUploadedFilesRadiologyLength = $scope.finalUploadedFilesRadiology.length;
                    });


                    $scope.uploadFilesFun = function () {
                        $scope.toShowFinalFilesRadiology = myDropzoneRadiology.files;
                        $scope.toShowFinalFilesRadiologyLength = $scope.toShowFinalFilesRadiology.length;
                        if ($scope.toShowFinalFilesRadiologyLength > 0) {
                            $scope.hideAlert();
                            $scope.uploadFlagRadiology = true;
                        }
                        else {
                            $scope.finalUploadedFilesRadiologyLength = 0;
                            $scope.uploadFlagRadiology = false;
                        }
                    }

                    $scope.cancelUpload = function () {
                        removeFlag = true;
                        myDropzoneRadiology.removeAllFiles();
                        $scope.uploadFlagRadiology = false;
                        $scope.finalUploadedFilesRadiology = [];
                        $scope.finalUploadedFilesRadiologyLength = $scope.finalUploadedFilesRadiology.length;
                        console.log(myDropzoneRadiology.files.length + "see length");
                    }

                });
                //ENDOFUPLOADFILES

                //RETRIVING FILE FUNCTION	
                $scope.getFile = function (id) {
                    var gfile = '/ihspdc/rest/getFile?id=' + id;
                    $('#modalForRadiologyImage').modal('show');
                    $('.imagepreview').attr('src', gfile);
                }


                //SAVING RADIOLOGY PART
                $scope.registerRadiology = function (radiologyForm, radDetails) {
                    console.log(radDetails);
                    if (!radiologyForm.validate()) {
                        // event.preventDefault();
                        // event.stopPropagation();
                    }
                    else {
                        if (!radDetails.doctor) {
                            document.getElementById("doctorError").innerHTML = "Doctor is required";
                        }
                        else if ($scope.finalUploadedFilesRadiology.length == 0) {
                            $rootScope.hideAlert();
                            $("html, body").animate({ scrollTop: 0 }, 200);
                            $rootScope.errMsg = true;
                            $rootScope.errorMsg = "Please upload one or more radiology documents";
                            jQuery("#errId").focus();
                            jQuery("#errId").show();
                        }
                        else {
                            radDetails.date = $("#datetimepicker3").val();
                            radDetails.coordinator = $scope.coordinator;
                            radDetails.patient = $scope.patientDetail;
                            radDetails.files = $scope.finalUploadedFilesRadiology;
                            $http({
                                url: '/ihspdc/rest/addRadiology',
                                data: radDetails,
                                method: 'POST',
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(function (response) {
                                $rootScope.hideAlert();
                                $("html, body").animate({ scrollTop: 0 }, 200);
                                $rootScope.sucMsg = true;
                                $rootScope.successMsg = "Radiology has been successfully saved";
                                jQuery("#successId").focus();
                                jQuery("#successId").show();
                                $scope.clearRadiology();
                                radDetails = {};
                                $scope.retriveRadiology();
                            }, function (error) {
                                console.log("Error in Adding Radiology");
                            });
                        }
                    }
                }


                //VALIDATION PART OF RADIOLOGY NOTES
                $scope.validateRadiologyForm = {
                    debug: false,
                    errorElement: "div",
                    rules: {
                        notesDate: {
                            required: true
                        },
                        radDescription: {
                            required: true
                        }
                    },
                    messages: {
                        notesDate: {
                            required: "Date is required"
                        },
                        radDescription: {
                            required: "Description is required"
                        }
                    },
                    highlight: function (element, errorClass) {
                        $(element).removeClass(errorClass);
                    }
                }

                $scope.clearRadiology = function () {
                    $scope.rad = {};
                    document.getElementById("datetimepicker3").value = "";
                    $scope.cancelUpload();
                }

                // Patient profile
                var patientDetails = '/ihspdc/rest/displayPatientDetails?patientId='
                patientDetails += $stateParams.id;
                $http.get(patientDetails).then(
                    function (response, err) {
                        if (!err) {
                            console.log(response);
                            if (response.data != null) {
                                $scope.patientDetail = response.data;
                            }
                        } else {
                            console.log('Error');

                        }
                    });

                //LOAD MORE
                $scope.nodeRadiologyLimit = 5;// initially shows 5 Nodes.
                var noMore = 0;
                $scope.loadMoreRadiology = function () {
                    if ($scope.radialDates.length >= $scope.nodeRadiologyLimit) {
                        if (noMore != 1) {
                            if ($scope.radialDates.length >= ($scope.nodeRadiologyLimit + 5)) {
                                $scope.nodeRadiologyLimit += 5;
                            }
                            else {
                                $scope.nodeRadiologyLimit = $scope.radialDates.length;
                                $scope.loadRadiologyBtn = false;
                                noMore = 1;
                            }
                        }
                    }
                    else {
                        $scope.loadRadiologyBtn = false;
                    }
                }

                //RETRIVING PART - RADIOLOGY NOTES
                var RadiologyDates = [];
                $scope.displayRadiology = {};
                $scope.retriveRadiology = function () {
                    $scope.radialDates = [];
                    patientService.getRadiologyDates($stateParams.id, function (err, response) {
                        RadiologyDates = response.data;
                        angular.forEach(response.data, function (date) {
                            $scope.radialDates.push(moment(date, "DD-MM-YYYY").format("MMM DD,YYYY"));
                        });
                        if($scope.radialDates.length > 5){
                            $scope.loadRadiologyBtn = true;
                        }
                        if (RadiologyDates.length > 0) {
                            $scope.getRadiology(0);
                        }
                    });
                }
                $scope.retriveRadiology();

                $scope.getRadiology = function (index) {
                    var getConsults = '/ihspdc/rest/getRadialByDate?date=';
                    getConsults += RadiologyDates[index];
                    getConsults += '&opCode=';
                    getConsults += $stateParams.id;
                    $http.get(getConsults)
                        .then(function (response, err) {
                            if (!err) {
                                console.log(response.data);
                                if (response.data.length > 0) {
                                    $scope.displayRadiology[index] = response.data;
                                } else {
                                    console.log('Error');
                                }
                            }
                        });
                }

            }//END OF RADIOLOGY - SELF INVOKE FUNCTION
        ]);
})();