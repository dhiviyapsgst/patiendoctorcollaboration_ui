(function () {
	'use strict';
	var App = angular.module('app');
	App.directive('dirViewResults', dirViewResults);
	dirViewResults.$inject = ['$rootScope', '$compile'];

	function dirViewResults($rootScope, $compile) {
		return {
			restrict: 'EA',
			templateUrl: 'assets/views/labResults/ViewResults.html',
			controller: 'ViewLabResultsController',
			replace: true,
			scope: true,
			link: function (scope, element) {
			}

		};
	}

})();

App.controller(
	'ViewLabResultsController',
	[
		'$scope',
		'$rootScope',
		'$http',
		'$localStorage',
		'$state',
		'$stateParams',
		'$window', '$timeout', 'patientService',
		function ViewLabResultsController($scope, $rootScope,
			$http, $localStorage, $state, $stateParams,
			$window, $timeout, patientService) {

			//Add lab results

			//below commented code works fine..and below API fetch medication table fully eager load
			// getPatientLabTest = '/ihspdc/rest/getpatientlabtest?opCode=';
			// getPatientLabTest += $stateParams.id;
			// $http.get(getPatientLabTest).then(function (response, err) {
			// 	if (!err) {

			// 	}
			// });

			var getPatientLabTest = '/ihspdc/rest/getpatientlabtest?opCode=';
			getPatientLabTest += $stateParams.id;
			$http.get(getPatientLabTest).then(function (response, err) {
				if (!err) {
					$scope.lastVisitInfo = response.data;
				}
			});

			var patientBioInfo = '/ihspdc/rest/displayPatientDetails?patientId='
			patientBioInfo += $stateParams.id;
			$http.get(patientBioInfo).then(function (response, err) {
				if (!err) {
					console.log(response);
					if (response.data != null) {
						$scope.patBio = response.data;
					}
				} else {
					console.log('Error');
				}
			});

			var url = '/ihspdc/rest/getPatientDetails?opCode='
			url += $stateParams.id;
			$http.get(url).then(function (response, err) {
				if (!err) {
					console.log(response);
					if (response.data != null) {
						$scope.subPatientInfo = response.data;
					}
				} else {
					console.log('Error');
				}
			});

			var getResultsDates = function () {
				var getDates = '/ihspdc/rest/displayDates?patientId='
				getDates += $stateParams.id;
				$scope.formattedDate = [];
				$http.get(getDates).then(function (response, err) {
					if (!err) {
						console.log(response);
						$scope.dates = response.data;
						for (i = 0; i < $scope.dates.length; i++) {
							$scope.formattedDate.push(moment($scope.dates[i], 'DD-MM-YYYY').format('MMM DD,YYYY'));
						}
						// Display Labresults by
						// date for first block
						console.log($scope.formattedDate);
						if (response.data.length > 0) {
							$scope.getLabTestsByDate(0);
							if (response.data.length > 5) {
								$scope.showLoadMoreRes = true;
							}
						} else {
							$scope.ifNoRes = "No lab test available";
						}
					}
				});
			}

			//View lab results - On-Clicking LabTests Tab - Initial Load Function
			var ft = true;
			$scope.$on('labTestTabEvent', function (e) {
				$scope.labResNotEntered = true;
				$scope.showLoadMoreRes = false;
				$scope.groups = {};
				console.log("Entered");
				getResultsDates();
			});

			$(document).ready(function () {
				$scope.toSetGlobalDate = function (date) {
					$rootScope.hideAlert();
					angular.forEach($rootScope.labtestgrpdef.labTests, function (test, index) {
						$scope.eachResultDate = date;
						document.getElementById("dateOfTest" + index).value = date;
					});
					$scope.checkAllResults(date);
				}
			});

			//LoadMore Function
			$scope.nodeLimitLR = 5;// initially shows 5 Nodes.
			var noMoreRes = 0;
			$scope.loadMoreResults = function () {
				if ($scope.formattedDate.length >= $scope.nodeLimitLR) {
					if (noMoreRes != 1) {
						if ($scope.formattedDate.length >= ($scope.nodeLimitLR + 5)) {
							$scope.nodeLimitLR += 5;
						}
						else {
							$scope.nodeLimitLR = $scope.formattedDate.length;
							noMoreRes = 1;
							$scope.showLoadMoreRes = 0;
						}
					}
				}
				else {
					$scope.showLoadMoreRes = 0;
				}
			}


			$scope.resetOfEnterResults = function () {
				document.getElementById("labResultEntryTable").reset();
				document.getElementById("description").value = "";
				document.getElementById("dateForAllTest").value = "";
				$scope.cancelUpload();
			}


			//UPLOADFILES 
			$scope.openUploadModal = function () {
				//ELSE-IF NEEDED BECAUSE dateForAllTest IS MANDATORY FOR PATIENT BUT NOT FOR CO-ORD
				if ($rootScope.Auth.hasResourceRole('patient')) {
					if (!document.getElementById("dateForAllTest").value) {
						$scope.throwErrorMsg("Date is required");
					}
					else {
						$('#myModal').modal('show');
					}
				}
				else {
					$('#myModal').modal('show');
				}
			}

			$scope.finalUploadedFiles = [];
			var uploadedFiles = [];
			var newFile = {};
			var removeFlag = false;
			var myDropzone = new Dropzone("form#myId", { paramName: "inputfile", maxFilesize: 10, addRemoveLinks: true, parallelUploads: 15, maxFiles: 15, method: 'POST', url: "/ihspdc/rest/storefile", acceptedFiles: 'image/png,image/jpeg,image/jpg,application/pdf', dictDefaultMessage: 'Drop Specifications file' });
			$scope.finalUploadedFiles = [];
			$scope.finalUploadedFilesLength = 0;

			myDropzone.on("error", function (file, errorMessage, xhr) {
				$(file.previewElement).find('.dz-error-message').text(response);
			});

			myDropzone.on("removedfile", function (file) {
				if (removeFlag != true) {
					console.log("Trying to remove me?", file.xhr.response);
					rmFile = '/ihspdc/rest/removeFile?id='
					rmFile += file.xhr.response.toUpperCase();
					$http.delete(rmFile);
				}
				removeFlag = false;
			});
			//try to reduce angular forEach if any time
			myDropzone.on("complete", function (file, xhr, formData) {
				uploadedFiles = [];
				newFile = {};
				if (uploadedFiles.length > 0) {
					angular.forEach(myDropzone.files, function (file) {
						angular.forEach(uploadedFiles, function (dupFile) {
							newFile = {};
							if (dupFile.id != file.xhr.response) {
								newFile.fileName = file.name;
								newFile.id = file.xhr.response;
								newFile.uploadedDate = document.getElementById("dateForAllTest").value;
								uploadedFiles.push(newFile);
							}
						});
					});
				}
				else if (uploadedFiles.length == 0) {
					angular.forEach(myDropzone.files, function (file) {
						newFile = {};
						newFile.fileName = file.name;
						newFile.id = file.xhr.response;
						newFile.uploadedDate = document.getElementById("dateForAllTest").value;
						uploadedFiles.push(newFile);
					});
				}
				console.log("completemultiple");
				$scope.finalUploadedFiles = uploadedFiles;
				$scope.finalUploadedFilesLength = $scope.finalUploadedFiles.length;
				console.log($scope.description + " description entered");
			});


			$scope.cancelUpload = function () {
				removeFlag = true;
				myDropzone.removeAllFiles();
				$('#myModal').modal('hide');
				$scope.description = "";
				$scope.uploadFlag = false;
				$scope.finalUploadedFiles = [];
				$scope.finalUploadedFilesLength = $scope.finalUploadedFiles.length;
				console.log(myDropzone.files.length + "see length");
				$scope.toShowFinalFiles = myDropzone.files;
				$scope.toShowFinalFilesLength = $scope.toShowFinalFiles.length;
			}

			$scope.uploadFilesFun = function () {
				$('#myModal').modal('hide');
				$scope.toShowFinalFiles = myDropzone.files;
				$scope.toShowFinalFilesLength = $scope.toShowFinalFiles.length;
				$scope.uploadFlag = true;
				if ($scope.toShowFinalFilesLength > 0) {
					$rootScope.hideAlert();
					$scope.uploadFlag = true;
				}
				else {
					$scope.finalUploadedFilesLength = 0;
					$scope.uploadFlag = false;
				}
			}

			//ENDOFUPLOADFILES


			var labResultsValidated = false;
			$scope.validateLabResults = function (resultsForValidate) {
				if ($rootScope.Auth && ($rootScope.Auth.hasResourceRole('coord'))) {
					var hasValue = false;
					var hasDate = true;
					var resultsCount = 0;
					angular.forEach(resultsForValidate.labTests, function (eachLabTest, index) {
						if (eachLabTest.testVal) {
							resultsCount = resultsCount + 1;
							if (resultsCount == resultsForValidate.labTests.length) {
								hasValue = true;
							}
						}
					});
					var noDate = 0;
					angular.forEach(resultsForValidate.labTests, function (eachLabTest, index) {
						if (eachLabTest.testVal) {
							if (document.getElementById("dateOfTest" + index).value == "" && noDate != 1) {
								hasDate = false;
								noDate = 1;
							}
						}
					});

					if (duplicateResultFlag) {
						$scope.throwErrorMsg(dupResults + " results are already availlable on this date");
					}
					else if (!hasValue) {
						$scope.throwErrorMsg("Observed value is required");
					}
					else if (hasDate == false) {
						$scope.throwErrorMsg("Date is required");
					}
					else if ($scope.finalUploadedFilesLength <= 0) {
						$scope.throwErrorMsg("Please upload one or more corresponding lab results");
					}
					else {
						$rootScope.hideAlert();
						labResultsValidated = true;
						resultsForValidate = {};
						$('#submitModaloflabresult').modal('show');
					}
				}
				else if ($rootScope.Auth && ($rootScope.Auth.hasResourceRole('patient'))) {
					if (!$scope.date.dateForAllTest) {
						$scope.throwErrorMsg("Date is required");
					}
					else if ($scope.finalUploadedFilesLength <= 0) {
						$scope.throwErrorMsg("Please upload one or more corresponding lab results");
					}
					else {
						$scope.savePatientLabReports();
					}
				}
			}




			//LabResult Saving Function
			$scope.savefunc = function (labgrpres) {
				$scope.closecollapseoflabresult();
				angular.forEach($scope.finalUploadedFiles, function (file) {
					file.description = "";
					file.description = $scope.description;
				});
				if (labResultsValidated && duplicateResultFlag != true) {
					if ($rootScope.Auth && ($rootScope.Auth.hasResourceRole('coord'))) {
						console.log("Saving LabResults!");
						console.log(labgrpres);
						var TotalLabResults = [];
						labgrpres.labTests.forEach(function (test, index) {
							var PatientLabRes = {};
							PatientLabRes.labTest = {};
							PatientLabRes.labTest.group = {};
							PatientLabRes.labTest.group.labGroup = {};

							PatientLabRes.visit = {};
							PatientLabRes.visit.patient = {};
							PatientLabRes.visit.doctor = {};
							PatientLabRes.visit.doctor.department = {};
							PatientLabRes.doctor = {};
							PatientLabRes.patient = {};
							PatientLabRes.patient.doctor = {};
							PatientLabRes.patient.coordinator = {};
							PatientLabRes.patient.department = {};

							PatientLabRes.files = [];
							PatientLabRes.files = $scope.finalUploadedFiles;

							PatientLabRes.labTest.testCode = test.labTest.testCode;
							PatientLabRes.labTest.testName = test.labTest.testName;
							PatientLabRes.labTest.gender = test.labTest.gender;
							PatientLabRes.labTest.unit = test.labTest.unit;

							PatientLabRes.labTest.group.labGroup.id = test.labTest.group.id;
							PatientLabRes.labTest.group.labGroup.groupName = test.labTest.group.groupName;
							PatientLabRes.labTest.group.labGroup.groupRank = test.labTest.group.groupRank;

							PatientLabRes.testValue = test.testVal;
							console.log(document.getElementById("dateOfTest" + index).value);
							PatientLabRes.testDateTime = document.getElementById("dateOfTest" + index).value;

							PatientLabRes.status = "approved";

							PatientLabRes.patient.opCode = $stateParams.id;
							PatientLabRes.patient.department = $scope.patBio.department;
							PatientLabRes.doctor = $rootScope.labtestgrpdef.doctor.docName;
							PatientLabRes.visit.patient = $scope.patBio;
							PatientLabRes.visit.doctor = $rootScope.labtestgrpdef.doctor;
							PatientLabRes.visit.visitId = labgrpres.visitId;
							PatientLabRes.visit.patient.opCode = $stateParams.id;
							TotalLabResults.push(PatientLabRes);
						});
						//CO-ORDINATOR SEND LABDOCUMENTS WITH ENTERED RESULTS VALUE
						$http({
							url: '/ihspdc/rest/savelabresult',
							data: TotalLabResults,
							method: 'POST',
							headers: {
								'Content-Type': 'application/json'
							}
						}).then(function (response) {
							$scope.resetOfEnterResults();
							$scope.description = "";
							$scope.throwSuccessMsg("Patient's Lab Result has been saved successfully");
							labgrpres = {};
							$rootScope.labtestgrpdef.labTests = [];
							PatientLabRes = {};
							TotalLabResults = [];
							$('#addtests').hide();
							$('#addteststrigger').show();
							getResultsDates();
						}, function (error) {
							console.log("error");
						});
					}
				}

			}

			//PATIENT LAB REPORTS UPLOADING
			$scope.date = {};
			$scope.savePatientLabReports = function () {
				var reportsDetail = {};
				reportsDetail.patient = $scope.patBio;
				reportsDetail.uploadDate = $scope.date.dateForAllTest;
				angular.forEach($scope.finalUploadedFiles, function (file) {
					file.description = "";
					file.description = $scope.description;
				});
				reportsDetail.status = "Pending";
				reportsDetail.files = $scope.finalUploadedFiles;
				reportsDetail.visit = $rootScope.labtestgrpdef;
				$http({
					url: '/ihspdc/rest/savelabdocuments',
					data: reportsDetail,
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					}
				}).then(function (response) {
					$scope.description = "";
					$scope.date = {};
					$scope.resetOfEnterResults();
					$scope.throwSuccessMsg("Lab reports has been submitted successfully. Waiting for coordinator approval");
					$('#addtests').hide();
					$('#addteststrigger').show();
					$http({ //ALERT FOR CO-ORDINATOR FOR PATIENT UPLOADED RESULTS
						url: '/ihspdc/rest/notifyCoordinator',
						data: $scope.subPatientInfo,
						method: 'POST',
						headers: {
							'Content-Type': 'application/json'
						}
					}).then(function (response) {
						console.log("Notified to Coordinator");
					}, function (error) {
						console.log("error in sending lab result notification to coordinator");
					});
				}, function (error) {
					console.log("error");
				});
			}

			//makeUnEditable Function
			$scope.makeUnEditable = function () {
				var formId = document.getElementById("labResultEntryTable");
				var elements = formId.elements;
				for (var i = 0, len = elements.length; i < len; ++i) {
					elements[i].disabled = true;
				}
			}

			// Checking Duplicate Function
			$scope.checkDuplicateInObject = function (propertyName, inputArray) {
				var seenDuplicate = false,
					testObject = {};

				inputArray.map(function (item) {
					var itemPropertyName = item[propertyName];
					if (itemPropertyName in testObject) {
						testObject[itemPropertyName].duplicate = true;
						item.duplicate = true;
						seenDuplicate = true;
					}
					else {
						testObject[itemPropertyName] = item;
						delete item.duplicate;
					}
				});
				return testObject;
			}

			// Get recent Dates for LabTests Function
			$scope.LabTestsNodes = function () {
				$scope.groups = {};
				console.log("Entered");
				getDates = '/ihspdc/rest/displayDates?patientId='
				getDates += $stateParams.id;
				$scope.formattedDate = [];
				$http.get(getDates)
					.then(
						function (response, err) {
							if (!err) {
								console.log(response);
								$scope.dates = response.data;
								for (i = 0; i < $scope.dates.length; i++) {
									$scope.formattedDate[i] = moment($scope.dates[i], 'DD-MM-YYYY').format('MMM DD,YYYY');
								}
								// Display Labresults by
								// date for first block
								console.log($scope.dates[0]);
								getLabTests = '/ihspdc/rest/displayLabResultsByDate?patientId='
								getLabTests += $stateParams.id;
								getLabTests += '&date=';
								getLabTests += $scope.dates[0];
								getLabTests += '&gender=';
								getLabTests += $scope.patient.gender;
								$http.get(getLabTests)
									.then(function (response, err) {
										if (!err) {
											console.log(response);
											var labTestsByDate = response.data;
											$scope.groups[0] = {};
											angular.forEach(labTestsByDate, function (labresult, i) {
												if ((labresult.labTest.group && labresult.labTest.group.groupName)) {
													if (!$scope.groups[0][labresult.labTest.group.groupName]) {
														$scope.groups[0][labresult.labTest.group.groupName] = [];
														$scope.groups[0][labresult.labTest.group.groupName].push(labresult);
													} else {
														$scope.groups[0][labresult.labTest.group.groupName].push(labresult);
													}
												} else {
													if (!$scope.groups[0]["Others"]) {
														$scope.groups[0]["Others"] = [];
														$scope.groups[0]["Others"].push(labresult);
													} else {
														$scope.groups[0]["Others"].push(labresult);
													}
												}
											});
											console.log("LabRes");
											console.log($scope.groups);
										} else {
											console.log('Error');
										}
									});
							}
						});
			}

			//Validation Function
			var validationOfProfileSection = function (onLoad, formId, callback) {

				var contextScope = this;
				contextScope.onLoad = onLoad;
				var validator = $(formId).validate({
					ignore: [],
					errorClass: 'help-block animated fadeInDown',
					errorElement: 'div',
					errorPlacement: function (error, e) {
						if (!contextScope.onLoad) {
							jQuery(e).parents('.form-group > div').append(error);
						}
					},
					highlight: function (e) {
						var elem = jQuery(e);
						if (!contextScope.onLoad) {
							elem.closest('.form-group').removeClass('has-error').addClass('has-error');
							elem.closest('.help-block').remove();
						}
					},
					success: function (e) {
						var elem = jQuery(e);
						elem.closest('.form-group').removeClass('has-error');
						elem.closest('.help-block').remove();
					}
					// rules: {
					// 	'dateForAllTest': {
					// 		required: false
					// 	}

					// },
					// messages: {
					// 	'dateForAllTest': {
					// 		required: 'Date cannot be blank'

					// 	},

					// }
				});
				if (contextScope.onLoad) {
					$(formId).valid();
				}
				else if ($(formId).valid()) {
					callback(null, validator);
				}
			};

			//ALERT MESSAGES
			$scope.throwErrorMsg = function (msg) {
				$("html, body").animate({ scrollTop: 0 }, 200);
				$rootScope.errMsg = true;
				$rootScope.errorMsg = msg;
				jQuery("#errId").focus();
				jQuery("#errId").show();
			}
			$scope.throwSuccessMsg = function (msg) {
				$("html, body").animate({ scrollTop: 0 }, 200);
				$rootScope.sucMsg = true;
				$rootScope.successMsg = msg;
				jQuery("#successId").focus();
				jQuery("#successId").show();
			}


			//RETRIVING FILE FUNCTION	
			$scope.getFile = function (id) {
				var gfile = '/ihspdc/rest/getFile?id=' + id;
				$('#modalForImage').modal('show');
				$('.imagepreview').attr('src', gfile);
			}


			// Getting LabResults By Date - OnClick Function
			$scope.groups = {};
			$scope.labTestsByDate = {};
			$scope.getLabTestsByDate = function (index) {
				getLabTests = '/ihspdc/rest/displayLabResultsByDate?patientId='
				getLabTests += $stateParams.id;
				getLabTests += '&date=';
				getLabTests += $scope.dates[index];
				getLabTests += '&gender=';
				getLabTests += $scope.patient.gender;
				$scope.labTestsByDate[index] = [];
				$http.get(getLabTests)
					.then(
						function (response, err) {
							if (!err) {
								$scope.labTestsByDate[index] = response.data;
								console.log($scope.labTestsByDate[index]);
								if (index == 0 && ft) {
									ft = false;
								}
								//to Show Files
								var AllFiles = [];
								var fCount = 0;
								angular.forEach($scope.labTestsByDate[index], function (labTes) {
									if (fCount == 0) {
										angular.forEach(labTes.files, function (f) {
											AllFiles.push(f);
										});
										fCount += 1;
									}
								});

								var AllFilesRetrived = $scope.checkDuplicateInObject('id', AllFiles);
								$scope.labTestsByDate[index].filesAssociated = {};
								$scope.labTestsByDate[index].filesAssociated = AllFilesRetrived;
								$scope.showFiles = "";
								if (AllFiles.length > 0) {
									$scope.showFiles = true;
								}
								console.log($scope.labTestsByDate[index]);
							} else {
								console.log('Error');

							}
						});

			}


			//Collapsing Function
			$scope.closecollapseoflabresult = function () {

				$('#addtests').hide();
				$('#addteststrigger').show();
				$('#closeModaloflabresult').hide();
				$('.form-group').removeClass('has-error');
				$('.help-block').remove();
			}

			$scope.opencollapse = function () {
				$('#addtests').show();
				$('#addteststrigger').hide();
			}


			//TABLE VIEW PROCESS
			$scope.currentLabResults = [];
			var patientLabResults = [];
			patientService.getAllLabResults($stateParams.id, function (err, response) {
				if (!err) {
					console.log("ALL LAB RESULTS");
					patientLabResults = response.data;
					$scope.all = true;
					$scope.getLatestResults();
					//BELOW CODE WILL SHOW CURRENT MONTH DATA BY DEFAULT
					// angular.forEach(response.data, function (lab) {
					// 	if (moment(lab.testDateTime, "DD-MM-YYYY").isSame(new Date(), 'month') && moment(lab.testDateTime, "DD-MM-YYYY").isSame(new Date(), 'year')) {
					// 		$scope.currentLabResults.push(lab);
					// 	}
					// });
					// if ($scope.currentLabResults.length) {
					// 	//TABLE VIEW - DUMMY DATA
					// 	$scope.resultDays = [];
					// 	angular.forEach($scope.currentLabResults, function (lab) {
					// 		var resultObj = {};
					// 		resultObj['testCode'] = lab.labTest.testCode;
					// 		resultObj['testName'] = lab.labTest.testName;
					// 		resultObj['referenceValue'] = "10 - 15";
					// 		resultObj['unit'] = lab.labTest.unit;
					// 		resultObj.resultDayValue = [];
					// 		for (j = 1; j <= 31; j++) {
					// 			dayValue = {};
					// 			dayValue.day = j;
					// 			dayValue.resultValue = '';
					// 			resultObj.resultDayValue.push(dayValue);
					// 		}
					// 		$scope.resultDays.push(resultObj);
					// 	});
					// 	console.log("ALL LAB RESULTS - DUMMY");
					// 	console.log($scope.resultDays);
					// 	$scope.resultDays = _.uniq($scope.resultDays, 'testCode');
					// 	console.log($scope.resultDays);
					// }
					// if ($scope.resultDays) {
					// 	angular.forEach($scope.currentLabResults, function (labResult) {
					// 		angular.forEach($scope.resultDays, function (lab) {
					// 			angular.forEach(lab.resultDayValue, function (labDay) {
					// 				if (labDay.day == parseInt(moment(labResult.testDateTime, "DD-MM-YYYY").format('D')) && lab.testCode == labResult.labTest.testCode) {
					// 					labDay.resultValue = labResult.testValue;
					// 				}
					// 			});
					// 		});
					// 	});
					// }
				}
			});

			$("#yearPickResults").datepicker({
				format: "yyyy",
				viewMode: "years",
				minViewMode: "years",
				setStartYear: new Date()
			});
			$scope.currMonth = moment().format("MM");
			$scope.selectedYear = (new Date()).getFullYear();
			//FILTER LAB RESULTS BY MONTH
			$scope.allMonths = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
			$scope.getResultsByMonth = function (selectedMonth) {
				$("#yearPickResults").datepicker('hide');
				$rootScope.hideAlert();
				if ($scope.selectedYear) {
					if (selectedMonth == "Jan") { selectedMonth = 01 };
					if (selectedMonth == "Feb") { selectedMonth = 02 };
					if (selectedMonth == "Mar") { selectedMonth = 03 };
					if (selectedMonth == "Apr") { selectedMonth = 04 };
					if (selectedMonth == "May") { selectedMonth = 05 };
					if (selectedMonth == "Jun") { selectedMonth = 06 };
					if (selectedMonth == "Jul") { selectedMonth = 07 };
					if (selectedMonth == "Aug") { selectedMonth = 08 };
					if (selectedMonth == "Sep") { selectedMonth = 09 };
					if (selectedMonth == "Oct") { selectedMonth = 10 };
					if (selectedMonth == "Nov") { selectedMonth = 11 };
					if (selectedMonth == "Dec") { selectedMonth = 12 };
					$scope.all = false;
					$rootScope.hideAlert();
					var startDate = moment([$scope.selectedYear, selectedMonth - 1]);
					var endDate = moment(startDate).endOf('month');
					console.log(startDate.toDate());
					console.log(endDate.toDate());
					$scope.currentLabResults = [];
					angular.forEach(patientLabResults, function (lab) {
						if (moment(lab.testDateTime, "DD-MM-YYYY").isSameOrAfter(startDate.toDate()) && moment(lab.testDateTime, "DD-MM-YYYY").isSameOrBefore(endDate.toDate())) {
							$scope.currentLabResults.push(lab);
						}
					});
					$scope.resultDays = [];
					if ($scope.currentLabResults.length) {
						//TABLE VIEW - DUMMY DATA
						$scope.resultDays = [];
						angular.forEach($scope.currentLabResults, function (lab) {
							var resultObj = {};
							resultObj['testCode'] = lab.labTest.testCode;
							resultObj['testName'] = lab.labTest.testName;
							resultObj['referenceValue'] = "10 - 15";
							resultObj['unit'] = lab.labTest.unit;
							resultObj.resultDayValue = [];
							for (j = 1; j <= 31; j++) {
								dayValue = {};
								dayValue.day = j;
								dayValue.resultValue = '';
								resultObj.resultDayValue.push(dayValue);
							}
							$scope.resultDays.push(resultObj);
						});
						console.log("ALL LAB RESULTS - DUMMY");
						console.log($scope.resultDays);
						$scope.resultDays = _.uniq($scope.resultDays, 'testCode');
						console.log($scope.resultDays);
					}
					if ($scope.resultDays) {
						angular.forEach($scope.currentLabResults, function (labResult) {
							angular.forEach($scope.resultDays, function (lab) {
								angular.forEach(lab.resultDayValue, function (labDay) {
									if (labDay.day == parseInt(moment(labResult.testDateTime, "DD-MM-YYYY").format('D')) && lab.testCode == labResult.labTest.testCode) {
										labDay.resultValue = labResult.testValue;
									}
								});
							});
						});
					}
				} else {
					$scope.throwErrorMsg("Year is required");
				}
			}

			//GET LATEST RESULTS - TABLE VIEW
			$scope.getLatestResults = function () {
				$rootScope.hideAlert();
				var latestLabResults = _.sortBy(patientLabResults, function (o) { return new moment(o.testDateTime, "DD-MM-YYYY"); }).reverse();
				$scope.dateFormats = [];
				angular.forEach(latestLabResults, function (res) {
					$scope.dateFormats.push(moment(res.testDateTime, "DD-MM-YYYY").format("MMM DD YYYY"));
				});
				$scope.dateFormats = _.uniq($scope.dateFormats);
				$scope.all = true;
				latestLabResults = _.uniq(latestLabResults, function (elem) { return [elem.testDateTime, elem.labTest.testCode].join() });
				console.log(latestLabResults);
				$scope.resultDays = [];
				if (latestLabResults.length) {
					//TABLE VIEW - DUMMY DATA
					$scope.resultDays = [];
					angular.forEach(latestLabResults, function (lab) {
						var resultObj = {};
						resultObj['testCode'] = lab.labTest.testCode;
						resultObj['testName'] = lab.labTest.testName;
						resultObj['referenceValue'] = "10 - 15";
						resultObj['unit'] = lab.labTest.unit;
						resultObj.resultDayValue = [];
						var lastDay = 0;
						angular.forEach(latestLabResults, function (res) {
							if (moment(res.testDateTime, "DD-MM-YYYY").format("DD") != lastDay) {
								dayValue = {};
								dayValue.actualDate = moment(res.testDateTime, "DD-MM-YYYY").format("MMM DD YYYY");
								dayValue.day = moment(res.testDateTime, "DD-MM-YYYY").format("DD");
								dayValue.resultValue = '';
								resultObj.resultDayValue.push(dayValue);
								lastDay = dayValue.day;
							}
						});
						$scope.resultDays.push(resultObj);
					});
					console.log("ALL LAB RESULTS - DUMMY");
					console.log($scope.resultDays);
				}
				if ($scope.resultDays) {
					$scope.resultDays = _.uniq($scope.resultDays, 'testCode');
					var found = 0;
					angular.forEach(latestLabResults, function (labResult) {
						found = 0;
						angular.forEach($scope.resultDays, function (lab) {
							angular.forEach(lab.resultDayValue, function (labDay) {
								if (found != 1) {
									if (labDay.day == parseInt(moment(labResult.testDateTime, "DD-MM-YYYY").format('DD')) && lab.testCode == labResult.labTest.testCode) {
										labDay.resultValue = labResult.testValue;
										found = 1;
									}
								}
							});
						});
					});
				}
			}

			//FLATPICKR CONFIG
			$scope.dateOpts1 = {
				dateFormat: 'd-m-Y',
				allowInput: true
			};

			//RESTRICTING OF ADDING LAB RESULT ON SAME DATE
			var duplicateResultFlag = false;
			var dupResults = "";
			$scope.checkAllResults = function (globalDate) {
				dupResults = "";
				duplicateResultFlag = false;
				angular.forEach($rootScope.labtestgrpdef.labTests, function (test) {
					angular.forEach(patientLabResults, function (result) {
						if (test.labTest.testCode == result.labTest.testCode && result.testDateTime == globalDate) {
							test.dup = 1;
							duplicateResultFlag = true;
							dupResults += test.labTest.testName + ", ";
						}
					});
				});
				if (duplicateResultFlag) {
					$scope.throwErrorMsg(dupResults + " results are already availlable on this date");
				}
				else {
					angular.forEach($rootScope.labtestgrpdef.labTests, function (test) {
						test.dup = 0;
					});
					$rootScope.hideAlert();
				}
			}

			$scope.validateEachResultDate = function (testCode, date) {
				dupResults = "";
				var dupTestCode = "";
				angular.forEach($rootScope.labtestgrpdef.labTests, function (test) {
					if (test.labTest.testCode == testCode) {
						angular.forEach(patientLabResults, function (result) {
							if (test.labTest.testCode == result.labTest.testCode && result.testDateTime == date) {
								test.dup = 1;
								dupTestCode = test.labTest.testCode;
								duplicateResultFlag = true;
								dupResults += test.labTest.testName + ", ";
							}
						});
					}
				});
				if (duplicateResultFlag && dupTestCode == testCode) {
					$scope.throwErrorMsg(dupResults + " result are already availlable on this date");
				}
				else {
					angular.forEach($rootScope.labtestgrpdef.labTests, function (test) {
						if (testCode != dupTestCode && test.labTest.testCode == testCode) {
							test.dup = 0;
						}
					});
					$rootScope.hideAlert();
				}
			}

		} //End Of dirViewResults Function

	]);

