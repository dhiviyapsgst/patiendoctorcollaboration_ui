(function () {
	'use strict';
	var App = angular.module('app');
	App.directive('dirPatientsList', dirPatientsList);
	dirPatientsList.$inject = ['$rootScope', '$compile'];

	function dirPatientsList($rootScope, $compile) {
		return {
			restrict: 'EA',
			templateUrl: 'assets/views/dashboards/patientsList.html',
			controller: 'PatientsListCtrl',
			replace: true,
			scope: true,
			link: function (scope, element) {
			}

		};
	}

})();

App.controller(
	'PatientsListCtrl',
	[
		'$scope',
		'$rootScope',
		'$http',
		'$localStorage',
		'$state',
		'$stateParams',
		'$window',
		'messageService', 'patientService', '$timeout',
		function PatientsListCtrl($scope, $rootScope,
			$http, $localStorage, $state, $stateParams,
			$window, messageService, patientService, $timeout) {

			$rootScope.patient = ""; // to reduce more rest calls to obtain patient reference
			console.log($rootScope.Auth.tokenParsed.preferred_username);
			if ($rootScope.Auth && ($rootScope.Auth.hasResourceRole('patient'))) {
				$state.go('patientProfile', {
					id: $rootScope.Auth.tokenParsed.preferred_username //selectedItem and id is defined
				});
			} else if ($rootScope.Auth && ($rootScope.Auth.hasResourceRole('coord')) || $rootScope.Auth && ($rootScope.Auth.hasResourceRole('doctor'))) {

				//Dashboard role of screen purpose
				if ($rootScope.Auth.hasResourceRole('coord')) {
					$rootScope.loggedInRole = "Coordinator";
				}
				else {
					$rootScope.loggedInRole = "Doctor";
				}

				messageService.viewAllMessages(0, 20, function (err, response) {
					if (!err) {
						console.log("Message_Response", response.data);
						angular.forEach(response.data, function (message) {
							message.senderOfMessage = JSON.parse(message.senderOfMessage);
							message.timeOfSent = moment(message.timeOfSent, 'MMMM Do YYYY, h:mm:ss a').format('LL');
						});
						$scope.viewMessages = response.data;
					}
				});


				//GET TOTAL REGISTERED PATIENTS INFO - TRENDS CHART
				var trendsPatientsTable = [];
				patientService.getFullPatientsList(function (err, response) {
					if (!err && response.data != "") {
						console.log(response.data);
						trendsPatientsTable = _.sortBy(response.data, function (o) { return new moment(o.lastLogin, "DD-MM-YYYY"); }).reverse();
						console.log(trendsPatientsTable);
					}
				});
				$scope.getTotalPatientsInfo = function () {
					$scope.totalRegisteredPatients = [];
					angular.forEach(trendsPatientsTable, function (sub) {
						if (sub.status == "Active") {
							$scope.totalRegisteredPatients.push(sub);
						}
					});
					$('#showTotalPatientTrendsInfoModal').modal('show');
				};
				//DATE FORMATS
				var startOfWeek = moment().startOf('isoWeek');
				var endOfWeek = moment().endOf('isoWeek');
				var date = new Date();
				var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
				var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

				$scope.getMonthPatientsInfo = function () {
					$scope.monthRegisteredPatients = [];
					angular.forEach(trendsPatientsTable, function (sub) {
						if (sub.status == "Active" && moment(sub.joinDate, "DD-MM-YYYY").isSameOrAfter(firstDay) && moment(sub.joinDate, "DD-MM-YYYY").isSameOrBefore(lastDay)) {
							$scope.monthRegisteredPatients.push(sub);
						}
					});
					$('#showMonthPatientTrendsInfoModal').modal('show');
				};
				$scope.getWeekPatientsInfo = function () {
					$scope.weekRegisteredPatients = [];
					angular.forEach(trendsPatientsTable, function (sub) {
						if (sub.status == "Active" && moment(sub.joinDate, "DD-MM-YYYY").isSameOrAfter(startOfWeek) && moment(sub.joinDate, "DD-MM-YYYY").isSameOrBefore(endOfWeek)) {
							$scope.weekRegisteredPatients.push(sub);
						}
					});
					$('#showWeekPatientTrendsInfoModal').modal('show');
				};
				$scope.getMonthActivePatientsInfo = function () {
					$scope.monthActivePatients = [];
					angular.forEach(trendsPatientsTable, function (sub) {
						if (sub.status == "Active" && moment(sub.lastLogin, "DD-MM-YYYY").isSameOrAfter(firstDay) && moment(sub.lastLogin, "DD-MM-YYYY").isSameOrBefore(lastDay)) {
							$scope.monthActivePatients.push(sub);
						}
					});
					$('#showMonthActivePatientModal').modal('show');
				};
				$scope.getWeekActivePatientsInfo = function () {
					$scope.weekActivePatients = [];
					angular.forEach(trendsPatientsTable, function (sub) {
						if (sub.status == "Active" && moment(sub.lastLogin, "DD-MM-YYYY").isSameOrAfter(startOfWeek) && moment(sub.lastLogin, "DD-MM-YYYY").isSameOrBefore(endOfWeek)) {
							$scope.weekActivePatients.push(sub);
						}
					});
					$('#showWeekActivePatientModal').modal('show');
				};

				$scope.goToPatientProfile = function (patientId, modalId) {
					$(modalId).modal('hide');
					$state.go('patientProfile', {
						id: patientId //selectedItem and id is defined
					});
				}

				//***
				var registerDates = [];
				var prevMonthCount = 0;
				var registerTrends = function () {
					$scope.dates = [];
					$scope.regDates = {};
					var count = 0;
					var groupedDates = [];
					var points = [];
					$scope.regDates.total = 0;
					$scope.regDates.week = 0;
					$scope.regDates.month = 0;
					var date = new Date();
					var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
					var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
					var startOfWeek = moment().startOf('isoWeek');
					var endOfWeek = moment().endOf('isoWeek');
					var prevMonthStart = moment(firstDay).subtract(1, 'months').startOf('month').format('YYYY-MM-DD');
					var prevMonthEnd = moment(firstDay).subtract(1, 'months').endOf('month').format('YYYY-MM-DD');
					$http.get('/ihspdc/rest/getRegisteredPatientsTrends').then(function (response, err) {
						if (!err) {
							$scope.regDates.total = response.data.length;
							angular.forEach(response.data, function (joinDate) {
								var momentDate = moment(joinDate, "DD-MM-YYYY");
								if (momentDate.isSameOrAfter(startOfWeek) && momentDate.isSameOrBefore(endOfWeek)) {
									$scope.regDates.week += 1;
								}
								if (momentDate.isSameOrAfter(firstDay) && momentDate.isSameOrBefore(lastDay)) {
									$scope.regDates.month += 1;
								}
								if (momentDate.isSameOrAfter(moment(prevMonthStart).format('YYYY-MM-DD')) && momentDate.isSameOrBefore(moment(prevMonthEnd).format('YYYY-MM-DD'))) {
									prevMonthCount += 1;
								}
								$scope.dates.push(momentDate);
							});
							$scope.dates.sort(function (a, b) {
								return a - b;
							});
							$scope.dates = _.groupBy($scope.dates, "_i");
							angular.forEach($scope.dates, function (dateObj) {
								groupedDates.push(dateObj);
							});
							angular.forEach(groupedDates, function (date) {
								count += date.length;
								registerDates.push(date[0]._i);
								points.push(count);
							});
							var registerPercentage = $scope.regDates.month - prevMonthCount;
							if (registerPercentage > 0) {
								if (registerPercentage != 1) {
									$scope.regPercentage = registerPercentage + " Patients higher than the last month";
								} else {
									$scope.regPercentage = registerPercentage + " Patient higher than the last month";
								}
							}
							else {
								if (registerPercentage != -1) {
									$scope.regPercentage = Math.abs(registerPercentage) + " Patients lower than the last month";
								}
								else {
									$scope.regPercentage = Math.abs(registerPercentage) + " Patient lower than the last month";
								}
							}
							$scope.subscribedJson = {
								"type": "area",
								"plotarea": {
									"margin": "15 0 0 0"
								},
								"scaleX": {
									"visible": "false",
								},

								"scaleY": {
									"visible": "false",
								},
								"plot": {
									"tooltip": {
										"text": " %data-dates - %v",
										"decimals": 0,
										"shadow": 0,
										"border-radius": 5
									},
									"data-dates": registerDates
								},
								"series": [{
									"values": points,
									"background-color": "#EDC19E",
									"line-color": "#D35204",
									"alpha-area": 0.3,
									"marker": {
										"background-color": "#D35204",
									}
								}]
							};
						}
					});
				}
				registerTrends();
				//***
				//***
				var activeDates = [];
				var prevMonthActiveCount = 0;
				var activeTrends = function () {
					$scope.activeDates = [];
					$scope.actDates = {};
					var count = 0;
					var groupedDates = [];
					var activePoints = [];
					$scope.actDates.total = 0;
					$scope.actDates.week = 0;
					$scope.actDates.month = 0;
					var date = new Date();
					var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
					var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
					var startOfWeek = moment().startOf('isoWeek');
					var endOfWeek = moment().endOf('isoWeek');
					var prevMonthStart = moment(firstDay).subtract(1, 'months').startOf('month').format('YYYY-MM-DD');
					var prevMonthEnd = moment(firstDay).subtract(1, 'months').endOf('month').format('YYYY-MM-DD');
					$http.get('/ihspdc/rest/getActivePatientsTrends').then(function (response, err) {
						if (!err) {
							$scope.actDates.total = response.data.length;
							angular.forEach(response.data, function (lastLogin) {
								var momentDate = moment(lastLogin, "DD-MM-YYYY");
								if (momentDate.isSameOrAfter(startOfWeek) && momentDate.isSameOrBefore(endOfWeek)) {
									$scope.actDates.week += 1;
								}
								if (momentDate.isSameOrAfter(firstDay) && momentDate.isSameOrBefore(lastDay)) {
									$scope.actDates.month += 1;
								}
								if (momentDate.isSameOrAfter(moment(prevMonthStart).format('YYYY-MM-DD')) && momentDate.isSameOrBefore(moment(prevMonthEnd).format('YYYY-MM-DD'))) {
									prevMonthActiveCount += 1;
								}
								$scope.activeDates.push(momentDate);
							});
							$scope.activeDates.sort(function (a, b) {
								return a - b;
							});
							$scope.activeDates = _.groupBy($scope.activeDates, "_i");
							angular.forEach($scope.activeDates, function (dateObj) {
								groupedDates.push(dateObj);
							});
							angular.forEach(groupedDates, function (date) {
								count += date.length;
								activeDates.push(date[0]._i);
								activePoints.push(count);
							});
							var activePercentage = $scope.actDates.month - prevMonthActiveCount;
							if (activePercentage > 0) {
								if (activePercentage != 1) {
									$scope.actPercentage = activePercentage + " Patients higher than the last month";
								} else {
									$scope.actPercentage = activePercentage + " Patient higher than the last month";
								}
							}
							else {
								if (activePercentage != -1) {
									$scope.actPercentage = Math.abs(activePercentage) + " Patients lower than the last month";
								}
								else {
									$scope.actPercentage = Math.abs(activePercentage) + " Patient lower than the last month";
								}
							}
							$scope.activeJson = {
								"type": "area",
								"plotarea": {
									"margin": "15 0 0 0"
								},
								"scaleX": {
									"visible": "false",
								},

								"scaleY": {
									"visible": "false",
								},
								"plot": {
									"tooltip": {
										"text": " %data-dates - %v",
										"decimals": 0,
										"shadow": 0,
										"border-radius": 5
									},
									"data-dates": activeDates
								},
								"series": [{
									"values": activePoints,
									"background-color": "#EDC19E",
									"line-color": "#D35204",
									"alpha-area": 0.3,
									"marker": {
										"background-color": "#D35204",
									}
								}]
							};
						}
					});
				}
				activeTrends();
				//***

				$scope.sendPrivateMessage = function (opCode) {
					$rootScope.selectedPatientsList = [];
					$rootScope.selectedPatientsList.push(opCode);
					$state.go('sendMessageStep2');
				}

				$scope.searchPatient = function (opCode) {
					if (opCode.length >= 4) {
						var getpat = '/ihspdc/rest/getPatientDetails?opCode='
						getpat += opCode;
						$http.get(getpat).then(function (response, err) {
							if (!err && response.data != "") {
								$scope.loader = false;
								$scope.activePatientsList = response.data;
								$scope.values = [];
								if ($rootScope.Auth.hasResourceRole('coord')) {
									if ($rootScope.Auth.tokenParsed.preferred_username.toUpperCase() == $scope.activePatientsList.invitedBy.id.toUpperCase() && $scope.activePatientsList.status == "Active") {
										$scope.values.push($scope.activePatientsList);
									}
								}
								else {
									angular.forEach($scope.activePatientsList.doctors, function (docs) {
										if ($rootScope.Auth.tokenParsed.preferred_username.toUpperCase() == docs.docId.toUpperCase() && $scope.activePatientsList.status == "Active") {
											$scope.values.push($scope.activePatientsList);
										}
									});
								}
								console.log("Patient" + $scope.values);
							} else {
								console.log('Error in searching patient');
							}
						});
					} else if (opCode.length == 0) {
						$scope.getActivePatientsList(0);
					}
				}

				if ($rootScope.Auth.hasResourceRole('coord')) {
					patientService.getCountOfPatientsByCoordId($rootScope.Auth.tokenParsed.preferred_username, function (err, response) {
						if (!err && response != null) {
							var page = 0;
							$scope.pagesNeeded = [];
							if (response % 10 == 0) {
								page = $scope.values.length / 10;
							}
							else if (response % 10 != 0 && response <= 10) {
								page = 1;
							}
							else {
								page = Math.floor(response / 10) + 1;
							}
							for (i = 0; i < page; i++) {
								$scope.pagesNeeded.push(i + 1);
							}
						}
						if ($scope.pagesNeeded.length > 0) {
							$scope.getActivePatientsList(0, 1);
						}
					});
				}
				else {
					patientService.getPatientCount(function (err, response) {
						var page = 0;
						$scope.pagesNeeded = [];
						if (response % 10 == 0) {
							page = $scope.values.length / 10;
						}
						else if (response % 10 != 0 && response <= 10) {
							page = 1;
						}
						else {
							page = Math.floor(response / 10) + 1;
						}
						for (i = 0; i < page; i++) {
							$scope.pagesNeeded.push(i + 1);
						}

						if ($scope.pagesNeeded.length > 0) {
							$scope.getActivePatientsList(0, 1);
						}
					});
				}


				//Patients list getPatientsList
				$scope.getActivePatientsList = function (getNext, pageNo) {
					if (pageNo > 0 && pageNo <= $scope.pagesNeeded.length) {
						$scope.pageIn = pageNo;
						if ($rootScope.Auth.hasResourceRole('coord')) {
							$scope.loader = true;
							var getpat = '/ihspdc/rest/activePatients/by/CoordId?getNext=';
							getpat += getNext;
							getpat += "&id=";
							getpat += $rootScope.Auth.tokenParsed.preferred_username;
							$http.get(getpat).then(function (response, err) {
								if (!err) {
									$scope.loader = false;
									$scope.values = _.sortBy(response.data, 'patient.statusOfPrescription');
									console.log($scope.values);
									console.log("List of Active Patients");
								}
								else {
									console.log('Error in showing active patient list');
								}
							});
						}
						else if ($rootScope.Auth.hasResourceRole('doctor')) {
							$scope.loader = true;
							var getpat = '/ihspdc/rest/activePatientsDoctor?getNext=';
							getpat += getNext;
							getNext = "&id=";
							getNext += $rootScope.Auth.tokenParsed.preferred_username;
							$http.get(getpat).then(function (response, err) {
								if (!err) {
									$scope.loader = false;
									$scope.values = _.sortBy(response.data, 'patient.statusOfPrescription').reverse();
									$scope.values = [];
									angular.forEach(response.data, function (pat) {
										angular.forEach(pat.doctors, function (docs) {
											if ($rootScope.Auth.tokenParsed.preferred_username.toUpperCase() == docs.docId.toUpperCase()) {
												$scope.values.push(pat);
											}
										});
									});
									console.log($scope.values);
									console.log("List of Active Patients");
								}
								else {
									console.log('Error in showing active patient list');
								}
							});
						}
					}
				}

				// Checking Duplicate Function
				$scope.checkDuplicateInObject = function (propertyName, inputArray) {
					var seenDuplicate = false,
						testObject = {};

					inputArray.map(function (item) {
						var itemPropertyName = item[propertyName];
						if (itemPropertyName in testObject) {
							testObject[itemPropertyName].duplicate = true;
							item.duplicate = true;
							seenDuplicate = true;
						}
						else {
							testObject[itemPropertyName] = item;
							delete item.duplicate;
						}
					});
					return testObject;
				}


				$scope.isArrayEmpty = function (arrVal) {
					return arrVal.length > 0;
				}

				function getDayWiseAppointment(appointments) {
					var today = moment().format("YYYY-MM-DD");
					var tomorrow = moment().add(1, 'days').format("YYYY-MM-DD");
					var startOfWeek = moment().startOf('isoWeek');
					var endOfWeek = moment().endOf('isoWeek');
					angular.forEach(appointments, function (app) {
						if (moment(app.appDate, "DD-MM-YYYY").isSame(today)) {
							$scope.todayAppointments.push(app);
						}
						else if (moment(app.appDate, "DD-MM-YYYY").isSame(tomorrow)) {
							$scope.tomorrowAppointments.push(app);
						}
						else if (moment(app.appDate, "DD-MM-YYYY").isSameOrAfter(startOfWeek) &&
							moment(app.appDate, "DD-MM-YYYY").isSameOrBefore(endOfWeek) &&
							moment(app.appDate, "DD-MM-YYYY").isSame(today) != true &&
							moment(app.appDate, "DD-MM-YYYY").isSame(tomorrow) != true && (moment(app.appDate, "DD-MM-YYYY") - moment() > 0)) {
							$scope.weekAppointments.push(app);
						}
					});
				}

				//UPCOMING APPOINTMENTS
				$scope.upcomingAppointments = function () {
					$scope.todayAppointments = [];
					$scope.tomorrowAppointments = [];
					$scope.weekAppointments = [];
					if ($rootScope.Auth.hasResourceRole('doctor')) {
						patientService.getDoctorUpComingAppointments($rootScope.Auth.tokenParsed.preferred_username, function (err, response) {
							if (!err) {
								getDayWiseAppointment(response.data);
							}
						});
					} else {
						patientService.getAllAppointments(function (err, response) {
							if (!err) {
								console.log(response);
								getDayWiseAppointment(response.data);
							}
						})
					}
				}
				$scope.upcomingAppointments();


			} else if ($rootScope.Auth && ($rootScope.Auth.hasResourceRole('superadmin'))) {
				$state.go('superadmin', { id: $rootScope.Auth.tokenParsed.preferred_username });
			} else if ($rootScope.Auth && ($rootScope.Auth.hasResourceRole('marketing'))) {
				$state.go('sendMessageStep1');
			}
		}
	]);

