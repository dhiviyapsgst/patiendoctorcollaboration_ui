(function () {
	'use strict';
	var App = angular.module('app');
	App.directive('dirAddPrescription', dirAddPrescription);
	dirAddPrescription.$inject = ['$rootScope', '$compile'];

	function dirAddPrescription($rootScope, $compile) {
		return {
			restrict: 'EA',
			templateUrl: 'assets/views/prescriptions/AddPrescription.html',
			controller: 'AddPrescriptionController',
			replace: true,
			scope: true,
			link: function (scope, element) {
			}

		};
	}


})();

App.controller(
	'AddPrescriptionController',
	[
		'$scope',
		'$rootScope',
		'$http',
		'$localStorage',
		'$state',
		'$stateParams',
		'$window', '$timeout',
		function AddPrescriptionController($scope, $rootScope,
			$http, $localStorage, $state, $stateParams,
			$window, $timeout) {
			// if($stateParams.f==true)

			$scope.list = [];
			//save template
			var isnew = "false";
			$scope.templateName = {};
			$scope.registerTemplate = function (form, tempToSave) {
				console.log(tempToSave.tName);
				if (!form.validate()) {
					event.preventDefault();
					event.stopPropagation();
				}
				else {
					if (isnew) {
						$scope.saveTemplate(tempToSave.tName);
					}
				}
			}

			$scope.validationOptions = {
				debug: false,
				errorClass: "invalid-feedback",
				errorElement: "div",
				rules: {
					tempName: {
						required: true,
					}
				},
				messages: {
					tempName: {
						required: "Template Name Cannot Be Empty",
					}
				},
				highlight: function (element, errorClass) {
					$(element).removeClass(errorClass);
				}
			}

			$scope.saveTemplate = function (tName) {

				$('#templateModal').modal('hide');
				console.log(tName);
				var newTemplate = {};
				newTemplate.labTests = [];
				var selTests = [];
				$scope.messagePres = true;
				newTemplate.templateName = tName; // assigning name of template

				angular.forEach($rootScope.finalUseArr, function (test) {
					var eachLabTest = {};
					if (test.labTest) {
						eachLabTest.testCode = test.labTest.testCode;
						eachLabTest.testName = test.labTest.testName;
						eachLabTest.unit = test.labTest.unit;
						eachLabTest.gender = test.labTest.gender;
						eachLabTest.group = {};
						eachLabTest.group.id = test.labTest.group.id;
						eachLabTest.group.groupName = test.labTest.group.groupName;
						eachLabTest.group.groupRank = test.labTest.group.groupRank;
						selTests.push(eachLabTest);
					}
					else {
						eachLabTest.testCode = test.testCode;
						eachLabTest.testName = test.testName;
						eachLabTest.unit = test.unit;
						eachLabTest.gender = test.gender;
						eachLabTest.group = {};
						eachLabTest.group.id = test.group.id;
						eachLabTest.group.groupName = test.group.groupName;
						eachLabTest.group.groupRank = test.group.groupRank;
						selTests.push(eachLabTest);
					}
				});
				newTemplate.labTests = selTests;
				newTemplate.doctor = {};
				if (($rootScope.Auth.hasResourceRole('doctor'))) {
					angular.forEach($scope.prescritedDoctor, function (aDoc) {
						if (aDoc.docId.toUpperCase() == $rootScope.Auth.tokenParsed.preferred_username.toUpperCase()) {
							newTemplate.doctor = aDoc;
						}
					});
				}
				console.log(newTemplate);
				$http({
					url: '/ihspdc/rest/savetemplate',
					data: newTemplate,
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					}
				}).then(function (response) {
					console.log("success of template saved");
					$scope.throwPresSuccess(tName + " has been saved successfuly");
					document.getElementById("tempName").value = "";
					getAllTemplates = '/ihspdc/rest/gettemplate';
					$rootScope.labTemplates = [];
					$http.get(getAllTemplates).then(function (response, err) {
						if (!err) {
							console.log("getTemplates");
							if (($rootScope.Auth.hasResourceRole('doctor'))) {
								angular.forEach(JSON.parse(JSON.stringify((response.data))), function (temp) {
									if (temp.doctor.docId.toUpperCase() == $rootScope.Auth.tokenParsed.preferred_username.toUpperCase()) {
										$rootScope.labTemplates.push(temp);
										temp.Selected = false;
									}
								});
							} else {
								angular.forEach(JSON.parse(JSON.stringify((response.data))), function (temp) {
									$rootScope.labTemplates.push(temp);
									temp.Selected = false;
								});
							}
						}
					});
				}, function (error) {
					console.log("error");
				});

			}//end of save template

			// to open addprescription collapse
			$scope.opencollapse = function () {
				$(function () {
					$('#datetimepicker1').datetimepicker({ useCurrent: false, minDate: moment().add(1, 'days') });
				})
				$('#addpres').show();
				$('#addprestrigger').hide();
			}
			//to close addprescription collapse
			$scope.closecollapse = function () {
				$scope.pres = {};
				$rootScope.finSelLabTestArr = [];
				$rootScope.finalUseArr = [];
				$rootScope.unCheckAll();
				$rootScope.UnCheckAllGroup();
				$('#addpres').hide();
				$('#addprestrigger').show();
			}

			//prescription current date
			$scope.currdate = moment().format("DD-MM-YYYY");
			//current date seted


			//execute while press prescription tab
			$scope.$on('medEvent', function (e) {
				console.log("WORKING while press prescription tab");
				$scope.showLoadMorePres = 0;
				// to call and retrieve the particular medication list based on the pat
				getmedDates = '/ihspdc/rest/getdate?patientId='
				getmedDates += $stateParams.id;
				$scope.datesofmed = [];
				$http.get(getmedDates).then(
					function (response, err) {
						if (!err) {
							console.log(response);

							$scope.date = response.data;
							for (i = 0; i < $scope.date.length; i++) {
								$scope.datesofmed[i] = moment($scope.date[i], 'DD-MM-YYYY hh:mm:ss a').format('MMMM Do YYYY, h:mm:ss A');
							}

							if (response.data.length > 0) {
								$scope.medList(0);
								if (response.data.length > 5) {
									$scope.showLoadMorePres = 1;
								}
							} else {
								if ($rootScope.Auth.hasResourceRole('coord') || $rootScope.Auth.hasResourceRole('patient'))
									$scope.ifNoPres = "No prescription available";
							}
						}
						$scope.getPendingPrescription();
					});


			});


			//implementation of loadmore
			var noMorePres = 0;
			$scope.nodeLimitPres = 5;// initially shows 5 Nodes.
			$scope.loadMorePres = function () {
				if ($scope.datesofmed.length >= $scope.nodeLimitPres) {
					if (noMorePres != 1) {
						if ($scope.datesofmed.length >= ($scope.nodeLimitPres + 5)) {
							$scope.nodeLimitPres += 5;
						}
						else {
							$scope.nodeLimitPres = $scope.datesofmed.length;
							noMorePres = 1;
							$scope.showLoadMorePres = 0;
						}
					}
				} else {
					$scope.showLoadMorePres = 0;
				}
			}


			$scope.checkDrugdays = function (drugDays, index) {
				console.log(drugDays);
				if (parseInt(drugDays) > 365 || parseInt(drugDays) < 0) {
					$scope.throwPresError("Days should be within 365 days");
					document.getElementById("noOfDays" + index).value = "";
				}
				else {
					$rootScope.hideAlert();
				}
			}


			// Start of prescription function
			$scope.meddates = function () {
				//$scope.$on('medEvent', function(e){
				console.log("WORKING while after saving prescription once");
				$scope.showLoadMorePres = 0;
				// to call and retrieve the particular medication list based on the pat
				getmedDates = '/ihspdc/rest/getdate?patientId='
				getmedDates += $stateParams.id;
				$scope.datesofmed = [];
				$http.get(getmedDates).then(
					function (response, err) {
						if (!err) {
							console.log(response);

							$scope.date = response.data;
							for (i = 0; i < $scope.date.length; i++) {
								$scope.datesofmed[i] = moment($scope.date[i], 'DD-MM-YYYY hh:mm:ss a').format('MMMM Do YYYY, h:mm:ss A');
							}

							if (response.data.length > 0) {
								$scope.medList(0);
								if (response.data.length > 5) {
									$scope.showLoadMorePres = 1;
								}
							} else {
								if ($rootScope.Auth.hasResourceRole('coord') || $rootScope.Auth.hasResourceRole('patient'))
									$scope.ifNoPres = "No prescription available - Please consult your doctor";
							}
						}
					});
			};

			$scope.getDoctorInfo = function () {
				var doctorDetails = '/ihspdc/rest/getDoc?patientId='; //PatientAssociatedDoctors
				doctorDetails += $stateParams.id;
				$http.get(doctorDetails).then(
					function (response, err) {
						if (!err) {
							$scope.prescritedDoctor = response.data;
						} else {
							console.log('Error');
						}
					});
			}
			$scope.getDoctorInfo();

			//IF ANY PENDING PRESCRIPTION LOADED IN CURRENT TO SIGN-OFF
			$scope.pendPres = false;
			$scope.getPendingPrescription = function () {
				var pendingPrescription = '/ihspdc/rest/getPendingPrescription?opCode=';
				pendingPrescription += $stateParams.id;
				$http.get(pendingPrescription).then(
					function (response, err) {
						if (!err && response.data.length == 1) {
							$scope.visitId = response.data[0].visitId;
							$scope.pendPres = true;
							$scope.copyToCurrent(response.data[0]);
						} else {
							console.log('Error');
						}
					});
			};

			var getAllTemplates = '/ihspdc/rest/gettemplate';
			$http.get(getAllTemplates).then(function (response, err) {
				if (!err) {
					console.log("getTemplates");
					$rootScope.labTemplates = response.data;
				}
			});

			$scope.checkTemplateExist = function (tempName) {
				$scope.tExist = "";
				var newOne = 1;
				if ($scope.labTemplates.length == 0) {
					isnew = true;
				}
				else if (tempName.tName.length >= 3) {
					angular.forEach($scope.labTemplates, function (temp) {
						if (newOne == 1) {
							if (temp.templateName.toUpperCase() == tempName.tName.toUpperCase()) {
								$scope.tExist = "Template Name Already Exist";
								isnew = false;
								newOne = 0;
							}
							else {
								isnew = true;
								$scope.tExist = "";
							}
						}
					});
				}
			}


			//onload ends
			$scope.pres = {};
			$scope.pres.values = [{ no: " " }];
			$scope.add = function () {
				$scope.pres.values.push({});
			};
			// to delete row in drugs
			$scope.remove = function (index) {
				$scope.pres.values.splice(index, 1);
			};

			var patientInfo = '/ihspdc/rest/displayPatientDetails?patientId='
			patientInfo += $stateParams.id;
			$http.get(patientInfo).then(
				function (response, err) {
					if (!err) {
						console.log(response);
						if (response.data != null) {
							$scope.patientData = response.data;
						}
					} else {
						console.log('Error');

					}
				});

			//set QuantityType for Dosage
			$scope.setQuantityType = function (type, index) {
				console.log(type);
				index.unit = type;
			}


			$scope.validnum = function (a) {
				return ((a >= 0) && (a <= 20));
			}


			//Repeat Every 
			$scope.repeatDays = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30];
			$scope.repeatWeeks = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
			$scope.repeatMonths = [0, 1, 2, 3, 4, 5, 6];
			// $scope.daysToWeekMonth =  function(days) {
			// 	var month =0,week=0,day=0, noNext = 0 , term = "";
			// 	if(days % 30 == 0 && noNext == 0) {term="month"; month = days / 30 ; noNext=1;}
			// 	else if( noNext == 0 && days % 7 == 0){ term="week"; week = days / 7 ; noNext = 1; }
			// 	else if(noNext == 0){ term="day"; day = days / 1}
			// 	}
			//  console.log("Here I call" + $scope.daysToWeekMonth(35));

			//set Frequency for labTests
			$scope.setFrequency = function (freq, index) {
				console.log(freq);
				index.freqObtained = freq;
			}
			//finLabTestToBeSaved=[];
			$scope.removelabtest = function (index) {
				$rootScope.finalUseArr.splice(index, 1);
				console.log($rootScope.finalUseArr);
			};

			var prescriptionValidated = false;
			$scope.prescriptionValidation = function (pres, advisedTests) {
				var success = 1; // 1 denotes way to go!
				if (pres.values.length < 1) {
					$scope.throwPresError("Drug is required");
				}
				else if (pres.values.length >= 1) {
					for (var i = 0; i < pres.values.length; i++) {
						if (!pres.values[i].medName || !pres.values[i].noOfDays || !pres.values[i].relationToFood || !pres.values[i].dose || !pres.values[i].morning || !pres.values[i].afternoon || !pres.values[i].evening || !pres.values[i].night) {
							success = 0;
							break;
						}
					}
				}
				if (success == 0) {
					$scope.throwPresError("Mandatory fields are missing");
				}
				else {
					prescriptionValidated = true;
					$('#saveModalofpres').modal('show');
				}
			}

			$scope.saveprescription = function (pres, advisedTests, value) {
				if (prescriptionValidated) {
					$('#addpres').hide();
					$('#addprestrigger').show();
					console.log(advisedTests);
					prescriptionresult = {};
					prescriptionresult.dateOfVisit = $scope.currdate;
					prescriptionresult.opCode = $stateParams.id;
					prescriptionresult.symptoms = pres.patientSymptoms;
					prescriptionresult.clinicalObservations = pres.patientObservations;
					prescriptionresult.labTests = [];
					//ADDING PRIMARY KEY TO OBJECTS TO UPDATE OF CO-ORDINATOR PRESCRIPTION WHILE DOCTOR APPROVE
					if ($scope.pendPres) {
						//PRESCRIBED TEST FORMATTING
						//BY DEFAULT DAYS AS REPEAT FREQUENCY
						prescriptionresult.labTests = [];
						angular.forEach(advisedTests, function (lab) {
							var test = {};
							test.labTest = {};
							test.labTest.testCode = lab.testCode;
							test.labTest.testName = lab.testName;
							test.labTest.unit = lab.unit;
							test.notes = lab.notes;
							test.repeatFrequency = lab.freqObtained;
							test.repeatFrequency = lab.freqObtained;
							test.repeatDuration = lab.repeatDuration;
							if (lab.plabId) {
								test.plabId = lab.plabId;
							}
							prescriptionresult.labTests.push(test);
						});
						//MEDICATION LIST FORMATTING
						//BY DEFAULT MG AS QUANTITY
						prescriptionresult.medicationList = [];
						for (var i = 0; i < pres.values.length; i++) {
							if (!pres.values[i].unit) {
								pres.values[i].unit = "mg";
							}
							if (pres.values[i].medId) {
								prescriptionresult.medicationList.push({
									"medId": pres.values[i].medId,
									"medName": pres.values[i].medName,
									"notes": pres.values[i].notes,
									"relationToFood": pres.values[i].relationToFood,
									"noOfDays": pres.values[i].noOfDays,
									"medDate": $scope.currdate,
									"dose": pres.values[i].dose,
									"unit": pres.values[i].unit,
									"patient": $scope.patientData,
									"dosage": [
										{
											"dosageId": pres.values[i].morningDosageId,
											"timeOfDosage": "M",
											"quantity": pres.values[i].morning,
											"medication": { "patient": $scope.patientData }
										},
										{
											"dosageId": pres.values[i].afternoonDosageId,
											"timeOfDosage": "A",
											"quantity": pres.values[i].afternoon,
											"medication": { "patient": $scope.patientData }
										},
										{
											"dosageId": pres.values[i].eveningDosageId,
											"timeOfDosage": "E",
											"quantity": pres.values[i].evening,
											"medication": { "patient": $scope.patientData }
										},
										{
											"dosageId": pres.values[i].nightDosageId,
											"timeOfDosage": "N",
											"quantity": pres.values[i].night,
											"medication": { "patient": $scope.patientData }
										}

									],

								});
							}
							else {
								prescriptionresult.medicationList.push({
									"medName": pres.values[i].medName,
									"notes": pres.values[i].notes,
									"relationToFood": pres.values[i].relationToFood,
									"noOfDays": pres.values[i].noOfDays,
									"medDate": $scope.currdate,
									"dose": pres.values[i].dose,
									"unit": pres.values[i].unit,
									"patient": $scope.patientData,
									"dosage": [
										{
											"dosageId": pres.values[i].morningDosageId,
											"timeOfDosage": "M",
											"quantity": pres.values[i].morning,
											"medication": { "patient": $scope.patientData }
										},
										{
											"dosageId": pres.values[i].afternoonDosageId,
											"timeOfDosage": "A",
											"quantity": pres.values[i].afternoon,
											"medication": { "patient": $scope.patientData }
										},
										{
											"dosageId": pres.values[i].eveningDosageId,
											"timeOfDosage": "E",
											"quantity": pres.values[i].evening,
											"medication": { "patient": $scope.patientData }
										},
										{
											"dosageId": pres.values[i].nightDosageId,
											"timeOfDosage": "N",
											"quantity": pres.values[i].night,
											"medication": { "patient": $scope.patientData }
										}

									],

								});
							}
						}
						console.log(prescriptionresult.medicationList);
					}
					//NON-PENDING
					else {
						//BY DEFAULT DAYS AS REPEAT FREQUENCY
						angular.forEach(advisedTests, function (finaltest) {
							console.log(finaltest.freqObtained);
							prescriptionresult.labTests.push({ "notes": finaltest.notes, "repeatFrequency": finaltest.freqObtained, "repeatDuration": finaltest.repeatDuration, "labTest": { "testCode": finaltest.testCode, "testName": finaltest.testName, "gender": finaltest.gender, "unit": finaltest.unit } });
						});
						console.log(prescriptionresult.labTests);
						//MEDICATION LIST FORMATTING
						prescriptionresult.medicationList = [];
						for (var i = 0; i < pres.values.length; i++) {
							if (!pres.values[i].unit) {
								pres.values[i].unit = "mg";
							}
							prescriptionresult.medicationList.push({
								"medName": pres.values[i].medName,
								"notes": pres.values[i].notes,
								"relationToFood": pres.values[i].relationToFood,
								"noOfDays": pres.values[i].noOfDays,
								"medDate": $scope.currdate,
								"dose": pres.values[i].dose,
								"unit": pres.values[i].unit,
								"patient": $scope.patientData,
								"dosage": [
									{
										"timeOfDosage": "M",
										"quantity": pres.values[i].morning,
										"medication": { "patient": $scope.patientData }
									},
									{
										"timeOfDosage": "A",
										"quantity": pres.values[i].afternoon,
										"medication": { "patient": $scope.patientData }
									},
									{
										"timeOfDosage": "E",
										"quantity": pres.values[i].evening,
										"medication": { "patient": $scope.patientData }
									},
									{
										"timeOfDosage": "N",
										"quantity": pres.values[i].night,
										"medication": { "patient": $scope.patientData }
									}

								],

							});
						}
					}
					prescriptionresult.nextVisit = $("#datetimepicker1").val();
					prescriptionresult.patient = {};
					prescriptionresult.patient = $scope.patientData;
					prescriptionresult.patient.nextVisit = prescriptionresult.nextVisit;
					prescriptionresult.doctor = {};
					if (($rootScope.Auth.hasResourceRole('doctor'))) {
						angular.forEach($scope.prescritedDoctor, function (aDoc) {
							if (aDoc.docId.toUpperCase() == $rootScope.Auth.tokenParsed.preferred_username.toUpperCase()) {
								prescriptionresult.doctor = aDoc;
							}
						});
						//IF DOCTOR PRESCRIBING
						prescriptionresult.status = "Approved";
						prescriptionresult.patient.statusOfPrescription = prescriptionresult.status;
						$scope.pendPres = false;
					} else {
						//IF COORDINATOR PRESCRIBING
						prescriptionresult.doctor = $scope.prescritedDoctor[0]; // NEED TO FIX - WHO IS THE DOCTOR IF CO-ORDINATOR PRESCRIBED MEDICINE BEFOR APPROVAL
						prescriptionresult.status = "Pending";
						prescriptionresult.patient.statusOfPrescription = prescriptionresult.status;
					}
					if (value == 1) {
						//DOCTOR APPROVING THE CO-ORDINATOR PRESCRIPTION TO PATIENT
						prescriptionresult.visitId = $scope.visitId;
					}
					$http({
						url: '/ihspdc/rest/saveprescription',
						data: prescriptionresult,
						method: 'POST',
						headers: {
							'Content-Type': 'application/json'
						}
					}).then(function (response) {
						// console.log("success of prescription");
						// $scope.throwPresSuccess("Prescription has been successfully saved");
						// $scope.meddates();
						// console.log(response);
					}, function () {
						$rootScope.unCheckAll();
						$rootScope.UnCheckAllGroup();
						if (prescriptionresult.status == "Pending") {
							$scope.throwPresSuccess("Prescription added, Waiting for Doctor Approval");
						}
						else {
							$scope.throwPresSuccess("Prescription has been successfully saved");
						}
						console.log("error_block-- Prescription");
						prescriptionresult = {};
						$scope.meddates();
					});
				}
			}//END OF SAVING PRESCRIPTION


			$scope.toShowTemplateModal = function () {
				console.log($rootScope.finalUseArr);
				var len = $rootScope.finalUseArr.length;

				isnew = false;
				$scope.tExist = "";
				// typeof $rootScope.finalUseArr !== 'undefined'
				if (len > 0) {
					$('#templateModal').modal('show');
				}
				else {
					$scope.messagePres = true;
					$scope.throwPresError("No test to save as template");
				}
			}

			$scope.resetTemplate = function () {
				isnew = false;
				document.getElementById("tName").value = "";
				$scope.existTemp = "";
				$scope.tExist = "";
			}

			$rootScope.resetprescription = function () {
				$scope.pres.values = [];
				$scope.finSelLabTest = {};
			}


			//COPYTOCURRENT
			$scope.copyToCurrent = function (prescription) {
				console.log(prescription);
				prescription = JSON.parse(JSON.stringify(prescription));
				$scope.pres = {};
				//MAKING JSON STRUCTURE TO LOAD DATA
				$scope.pres.patientSymptoms = prescription.symptoms;
				$scope.pres.patientObservations = prescription.clinicalObservations;
				$scope.pres.currdate = $scope.currdate;
				if ($scope.pendPres) {
					$scope.pres.nextVisit = prescription.nextVisit;
					// MEDICATION LIST DATA JSON
					$scope.pres.values = [];
					var medicationList = [];
					angular.forEach(prescription.medicationList, function (med, index) {
						var medicine = {};
						medicine.medName = med.medName;
						medicine.medId = med.medId;
						medicine.dose = med.dose;
						medicine.unit = med.unit;
						medicine.noOfDays = med.noOfDays;
						medicine.notes = med.notes;
						medicine.relationToFood = med.relationToFood;
						angular.forEach(med.dosage, function (timeOftaken) {
							if (timeOftaken.timeOfDosage == 'M') {
								medicine.morningDosageId = timeOftaken.dosageId;
								medicine.morning = timeOftaken.quantity;
							}
							if (timeOftaken.timeOfDosage == 'A') {
								medicine.afternoonDosageId = timeOftaken.dosageId;
								medicine.afternoon = timeOftaken.quantity;
							}
							if (timeOftaken.timeOfDosage == 'E') {
								medicine.eveningDosageId = timeOftaken.dosageId;
								medicine.evening = timeOftaken.quantity;
							}
							if (timeOftaken.timeOfDosage == 'N') {
								medicine.nightDosageId = timeOftaken.dosageId;
								medicine.night = timeOftaken.quantity;
							}
						});
						medicationList.push(medicine);
					});
					$scope.pres.values = medicationList;
					//ADVISED LAB TEST JSON
					$rootScope.finalUseArr = [];
					angular.forEach(prescription.labTests, function (lab) {
						var test = {};
						test.group = {};
						test.testCode = lab.labTest.testCode;
						test.testName = lab.labTest.testName;
						test.unit = lab.labTest.unit;
						test.group = lab.labTest.group;
						test.notes = lab.notes;
						test.plabId = lab.plabId;
						test.freqObtained = lab.repeatFrequency;
						test.repeatDuration = lab.repeatDuration;
						$rootScope.finalUseArr.push(test);
					});
				}
				else {
					// MEDICATION LIST DATA JSON
					$scope.pres.values = [];
					var medicationList = [];
					angular.forEach(prescription.medicationList, function (med, index) {
						var medicine = {};
						medicine.medName = med.medName;
						medicine.dose = med.dose;
						medicine.unit = med.unit;
						medicine.noOfDays = med.noOfDays;
						medicine.notes = med.notes;
						medicine.relationToFood = med.relationToFood;
						angular.forEach(med.dosage, function (timeOftaken) {
							if (timeOftaken.timeOfDosage == 'M') {
								medicine.morning = timeOftaken.quantity;
							}
							if (timeOftaken.timeOfDosage == 'A') {
								medicine.afternoon = timeOftaken.quantity;
							}
							if (timeOftaken.timeOfDosage == 'E') {
								medicine.evening = timeOftaken.quantity;
							}
							if (timeOftaken.timeOfDosage == 'N') {
								medicine.night = timeOftaken.quantity;
							}
						});
						medicationList.push(medicine);
					});
					$scope.pres.values = medicationList;
					//ADVISED LAB TEST JSON
					$rootScope.finalUseArr = [];
					angular.forEach(prescription.labTests, function (lab) {
						var test = {};
						test.group = {};
						test.testCode = lab.labTest.testCode;
						test.testName = lab.labTest.testName;
						test.unit = lab.labTest.unit;
						test.group = lab.labTest.group;
						test.notes = lab.notes;
						test.freqObtained = lab.repeatFrequency;
						test.repeatDuration = lab.repeatDuration;
						$rootScope.finalUseArr.push(test);
					});
				}
				$scope.opencollapse();
				$("html, body").animate({ scrollTop: 0 }, 2000);
			}

			//Alerts For Prescription
			$scope.throwPresError = function (msg) {
				$rootScope.hideAlert();
				$("html, body").animate({ scrollTop: 0 }, 200);
				$rootScope.errMsg = true;
				$rootScope.errorMsg = msg;
				jQuery("#errId").focus();
				jQuery("#errId").show();
			}
			$scope.throwPresSuccess = function (msg) {
				$rootScope.hideAlert();
				$("html, body").animate({ scrollTop: 0 }, 200);
				$rootScope.sucMsg = true;
				$rootScope.successMsg = msg;
				jQuery("#successId").focus();
				jQuery("#successId").show();
			}


			var autoExpand = function (field) {
				// Reset field height
				field.style.height = 'inherit';
				// Get the computed styles for the element
				var computed = window.getComputedStyle(field);
				// Calculate the height
				var height = parseInt(computed.getPropertyValue('border-top-width'), 10)
					+ parseInt(computed.getPropertyValue('padding-top'), 10)
					+ field.scrollHeight
					+ parseInt(computed.getPropertyValue('padding-bottom'), 10)
					+ parseInt(computed.getPropertyValue('border-bottom-width'), 10);

				field.style.height = height - 10 + 'px';
			};
			document.addEventListener('input', function (event) {
				if (event.target.tagName.toLowerCase() !== 'textarea') return;
				autoExpand(event.target);
			}, false);

			//PRINT PRISCRIPTION
			$scope.getPrintOfPres = function (index) {
				getPres = '/ihspdc/rest/getpreslabtestbydate?opCode='
				getPres += $stateParams.id;
				getPres += '&datetime=';
				getPres += moment($scope.date[index], 'DD-MM-YYYY hh:mm:ss a').format('YYYY-MM-DD hh:mm:ss A');
				$http.get(getPres).then(
					function (response, err) {
						if (!err) {
							var printData = response.data;
							console.log(printData);
							$http({
								url: '/ihspdc/rest/eprescription',
								data: printData[0],
								method: 'POST',
								headers: {
									'Content-Type': 'application/json'
								}
							}).then(function (response) {
								$scope.throwPresSuccess(" Prescription printed successfully!");
								// $window.open('/ihspdc/rest/eprescription', "_blank");
								// $window.open('E://epres2.pdf', "_blank");
							}, function (error) {
								console.log("error occured ! print prescription function");
							}
							);
						}
					});
			}



			$scope.testShowTable = false;
			//particular prescription based on opcode and date                            
			$scope.medList = function (index) {
				getmedlist = '/ihspdc/rest/getpreslabtestbydate?opCode='
				getmedlist += $stateParams.id;
				getmedlist += '&datetime=';
				getmedlist += moment($scope.date[index], 'DD-MM-YYYY hh:mm:ss a').format('YYYY-MM-DD hh:mm:ss A');
				console.log("index");
				console.log(getmedlist);
				console.log(index);
				$http.get(getmedlist).then(
					function (response, err) {
						if (!err) {
							console.log(response);
							$scope.list["dets" + index] = response.data[0];
							console.log("med list got");
							console.log($scope.list["dets" + index]);
						}
					});
			}

		}//End of prescription function
	]);

