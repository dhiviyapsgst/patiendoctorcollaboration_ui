App.controller('diraddlabtestCtrl', ['$scope', '$rootScope', '$http', '$localStorage', '$state', '$stateParams', '$window',
    function diraddlabtestCtrl($scope, $rootScope, $http, $localStorage, $state,
        $stateParams, $window) {
        url1 = '/ihspdc/rest/getpatientlabtest?opCode=';
        url1 += $stateParams.id;
        console.log('Prabhu');
        console.log(url1);
        $http.get(url1).then(function (response, err) {
            if (!err) {
                console.log('response got!');
                console.log(response.data);
                $rootScope.labtestgrpdef = response.data;
            }
        });

        $rootScope.labtestgrpdef = {};
        $rootScope.labtestgrpdef.labTests = [];
        // date-picker watch 

        $scope.GlobalTestDateTime = {};
        $scope.$watch('$scope.GlobalTestDateTime', function () {
            console.log("@watch");
            $rootScope.labtestgrpdef.labTests.forEach(function (eachtest) {
                eachtest.testDateTime = $scope.GlobalTestDateTime;
                console.log("changed watch");
            });
        });

        $scope.savefunc = function (labgrpres) {

            console.log("Saving LabResults...!");
            console.log(labgrpres);
            var TotalLabResults = [];

            labgrpres.labTests.forEach(function (test) {
                var PatientLabRes = {};
                PatientLabRes.labTest = {};
                PatientLabRes.patient = {};
                PatientLabRes.visit = {};
                PatientLabRes.labTest.testCode = test.labTest.testCode;
                PatientLabRes.labTest.testName = test.labTest.testName;
                PatientLabRes.labTest.gender = test.labTest.gender;
                PatientLabRes.labTest.unit = test.labTest.unit;
                PatientLabRes.labTest.group = {};
                PatientLabRes.labTest.group.labGroup = {};
                PatientLabRes.labTest.group.labGroup.id = test.labTest.group.id;
                PatientLabRes.labTest.group.labGroup.groupName = test.labTest.group.groupName;
                PatientLabRes.labTest.group.labGroup.groupRank = test.labTest.group.groupRank;
                PatientLabRes.testValue = test.testValue;
                PatientLabRes.testDateTime = test.testDateTime;
                PatientLabRes.patient.opCode = $stateParams.id;
                PatientLabRes.visit_id = labgrpres.visitId;
                TotalLabResults.push(PatientLabRes);
            });
            console.log(TotalLabResults);
            //send save request
            var request = {
                url: '/ihspdc/rest/savelabresult',
                data: TotalLabResults,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                }
            };
            $http(request);
        }
    }

]);