// Create our angular module
var App = angular.module('app', ['ngStorage', 'ngRoute', 'ui.router',
	'ui.bootstrap', 'oc.lazyLoad', 'ngSanitize', 'angular-flatpickr', 'ui.select', 'ngValidate', 'ngTagsInput']);

// Router configuration
App.config([
	'$stateProvider',
	'$urlRouterProvider',
	function ($stateProvider, $urlRouterProvider) {
		$urlRouterProvider.otherwise('/patient');
		$stateProvider.state(
			'patient',
			{
				url: '/patient',
				templateUrl: 'assets/views/dashboards/coordinator_dashboard.html',
				controller: 'PatientsListCtrl',
				resolve: {
					deps: [
						'$ocLazyLoad',
						function ($ocLazyLoad) {
							return $ocLazyLoad
								.load({
									insertBefore: '#css-bootstrap',
									serie: true,
									files: [
										'bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
										'bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css',
										'bower_components/jquery-validation/dist/jquery.validate.min.js',
										'bower_components/jquery-validation/dist/additional-methods.js',
										'bower_components/select2/dist/js/select2.full.min.js',
										'bower_components/select2/dist/css/select2.min.css',
										'bower_components/zingchart/client/zingchart.min.js',
										'bower_components/moment/min/moment.min.js',
										'bower_components/ZingChart-AngularJS/src/zingchart-angularjs.js',
										'bower_components/dropzone/dist/min/dropzone.min.css',
										'bower_components/dropzone/dist/min/dropzone.min.js'
									]

								});
						}]
				}
			})



			.state('labCrud', {
				url: '/patient/labCrud/add/labTests',
				controller: 'labCrudCtrl',
				templateUrl: 'assets/views/labTestMasters/lab_crud.html',
				resolve: {
					deps: [
						'$ocLazyLoad',
						function ($ocLazyLoad) {
							return $ocLazyLoad
								.load({
									insertBefore: '#css-bootstrap',
									serie: true,
									files: [
										'bower_components/jquery-validation/dist/jquery.validate.min.js',
										'bower_components/jquery-validation/dist/additional-methods.js']
								});
						}]
				}
			})

			.state('labCrud_testgroups', {
				url: '/patient/labCrud/add/testGroups',
				controller: 'labCrud_GroupsCtrl',
				templateUrl: 'assets/views/labTestMasters/lab_crud_testgroup.html',
				resolve: {
					deps: [
						'$ocLazyLoad',
						function ($ocLazyLoad) {
							return $ocLazyLoad
								.load({
									insertBefore: '#css-bootstrap',
									serie: true,
									files: [
										'bower_components/jquery-validation/dist/jquery.validate.min.js',
										'bower_components/jquery-validation/dist/additional-methods.js']
								});
						}]
				}
			})

			.state('singlepatientOnboard', {
				controller: 'singleOnBoardCtrl',
				url: '/add/patient',
				templateUrl: 'assets/views/onBoarding/onBoardPage.html',
				resolve: {
					deps: [
						'$ocLazyLoad',
						function ($ocLazyLoad) {
							return $ocLazyLoad
								.load({
									insertBefore: '#css-bootstrap',
									serie: true,
									files: [

										'bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
										'bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css',
										'bower_components/jquery-validation/dist/jquery.validate.min.js',
										'bower_components/jquery-validation/dist/additional-methods.js',
										'bower_components/angular-ui-select/dist/select.min.js',
										'bower_components/angular-sanitize/angular-sanitize.min.js',
										// 'bower_components/select2/dist/js/select2.full.min.js',
										// 'bower_components/select2/dist/css/select2.min.css',
										'bower_components/moment/min/moment.min.js',
									]

								});
						}]
				}
			})

			.state('bookappointment', {
				url: '/patient/appointment/new',
				templateUrl: 'assets/views/appointments/book_appointment.html',
				controller: 'bookAppointmentCtrl',
				resolve: {
					deps: [
						'$ocLazyLoad',
						function ($ocLazyLoad) {
							return $ocLazyLoad
								.load({
									insertBefore: '#css-bootstrap',
									serie: true,
									files: [
										'bower_components/jquery-validation/dist/jquery.validate.min.js',
										'bower_components/jquery-validation/dist/additional-methods.js'
									]
								});
						}]
				}
			})

			.state('pastBookedAppointments', {
				url: '/patient/appointment/booked_Details',
				templateUrl: 'assets/views/appointments/appointmentManage.html',
				controller: 'manageAppointmentCtrl',
				resolve: {
					deps: [
						'$ocLazyLoad',
						function ($ocLazyLoad) {
							return $ocLazyLoad
								.load({
									insertBefore: '#css-bootstrap',
									serie: true,
									files: [
										'bower_components/jquery-validation/dist/jquery.validate.min.js',
										'bower_components/jquery-validation/dist/additional-methods.js'
									]
								});
						}]
				}
			})

			.state('reschduleAppointment', {
				url: '/patient/appointment/Re_Schdule',
				templateUrl: 'assets/views/appointments/reschduleAppointment.html',
				controller: 'reschduleAppointmentCtrl',
				resolve: {
					deps: [
						'$ocLazyLoad',
						function ($ocLazyLoad) {
							return $ocLazyLoad
								.load({
									insertBefore: '#css-bootstrap',
									serie: true,
									files: [
										'bower_components/jquery-validation/dist/jquery.validate.min.js',
										'bower_components/jquery-validation/dist/additional-methods.js'
									]
								});
						}]
				}
			})

			.state('sendMessageStep1', {
				url: '/patient/sendMessage/step1',
				templateUrl: 'assets/views/messaging/patient_messaging_step1.html',
				controller: 'sendMessageCtrl',
				resolve: {
					deps: [
						'$ocLazyLoad',
						function ($ocLazyLoad) {
							return $ocLazyLoad
								.load({
									insertBefore: '#css-bootstrap',
									serie: true,
									files: [
										'bower_components/jquery-validation/dist/jquery.validate.min.js',
										'bower_components/jquery-validation/dist/additional-methods.js'
									]
								});
						}]
				}
			})

			.state('sendMessageStep2', {
				url: '/patient/sendMessage/step2/:id',
				templateUrl: 'assets/views/messaging/patient_messaging_step2.html',
				controller: 'sendMessageStep2Ctrl',
				resolve: {
					deps: ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load({
							insertBefore: '#css-bootstrap',
							serie: true,
							files: [
								'bower_components/summernote/dist/summernote.css',
								'bower_components/summernote/dist/summernote.min.js',
								'bower_components/dropzone/dist/min/dropzone.min.css',
								'bower_components/dropzone/dist/min/dropzone.min.js',
								'bower_components/angular-ui-select/dist/select.min.js',
								'bower_components/angular-sanitize/angular-sanitize.min.js',
								'bower_components/moment/min/moment.min.js',

							]
						});
					}
					]
				}
			})

			.state('viewMessage', {
				url: '/patient/viewMessages/:id/:from',
				templateUrl: 'assets/views/messaging/view_messages.html',
				controller: 'viewMessageCtrl',
			})

			.state('doctorOnBoard', {
				controller: 'doctorOnBoardCtrl',
				url: '/add/doctor',
				templateUrl: 'assets/views/onBoarding/doctorOnBoardPage.html',
				resolve: {
					deps: [
						'$ocLazyLoad',
						function ($ocLazyLoad) {
							return $ocLazyLoad
								.load({
									insertBefore: '#css-bootstrap',
									serie: true,
									files: [
										'bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
										'bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css',
										'bower_components/jquery-validation/dist/jquery.validate.min.js',
										'bower_components/jquery-validation/dist/additional-methods.js',
										'bower_components/angular-ui-select/dist/select.min.js',
										'bower_components/angular-sanitize/angular-sanitize.min.js',
										'bower_components/moment/min/moment.min.js',
										'bower_components/jt.timepicker/jquery.timepicker.min.js',
										'bower_components/jt.timepicker/jquery.timepicker.css'
									]
								});
						}]
				}
			})

			.state('coordinator_master', {
				controller: 'coordinatorMaster',
				url: '/add/coordinator',
				templateUrl: 'assets/views/onBoarding/coordinator_master.html',
				resolve: {
					deps: [
						'$ocLazyLoad',
						function ($ocLazyLoad) {
							return $ocLazyLoad
								.load({
									insertBefore: '#css-bootstrap',
									serie: true,
									files: [
										'bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
										'bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css',
										'bower_components/jquery-validation/dist/jquery.validate.min.js',
										'bower_components/jquery-validation/dist/additional-methods.js',
										'bower_components/angular-ui-select/dist/select.min.js',
										'bower_components/angular-sanitize/angular-sanitize.min.js',
										'bower_components/moment/min/moment.min.js',
									]

								});
						}]
				}
			})

			.state('department_master', {
				controller: 'departmentMaster',
				url: '/add/department',
				templateUrl: 'assets/views/onBoarding/department_master.html',
				resolve: {
					deps: [
						'$ocLazyLoad',
						function ($ocLazyLoad) {
							return $ocLazyLoad
								.load({
									insertBefore: '#css-bootstrap',
									serie: true,
									files: [
										'bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
										'bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css',
										'bower_components/jquery-validation/dist/jquery.validate.min.js',
										'bower_components/jquery-validation/dist/additional-methods.js',
										'bower_components/angular-ui-select/dist/select.min.js',
										'bower_components/moment/min/moment.min.js',
									]

								});
						}]
				}
			})

			.state('patientOnboard', {
				controller: 'onBoardCtrl',
				url: '/patient',
				templateUrl: 'assets/views/hisWayOnboard/patient_onboarding_step1.html',
				files: [
					'bower_components/select2/dist/js/select2.full.js',
					'bower_components/select2/dist/css/select2.css',
					'bower_components/datatables/media/css/jquery.dataTables.min.css',
					'bower_components/datatables/media/js/jquery.dataTables.min.js'
				]

			})

			.state('patientOnboard2', {
				controller: 'onBoardCtrl',
				url: '/patient',
				templateUrl: 'assets/views/hisWayOnboard/patient_onboarding_step2.html',
			})

			.state('patientOnboard3', {
				controller: 'onBoardCtrl',
				url: '/patient',
				templateUrl: 'assets/views/hisWayOnboard/patient_onboarding_step3.html',
			})

			.state(
				'patientProfile',
				{
					url: '/patientProfile/:id',
					templateUrl: 'assets/views/patient_profile.html',
					controller: 'PatientProfileCtrl',
					resolve: {
						deps: [
							'$ocLazyLoad',
							function ($ocLazyLoad) {
								return $ocLazyLoad
									.load({
										insertBefore: '#css-bootstrap',
										serie: true,
										files: [
											'bower_components/moment/min/moment.min.js',
											'bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
											'bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css',
											'bower_components/flatpickr/dist/flatpickr.min.css',
											'bower_components/jquery-validation/dist/jquery.validate.min.js',
											'bower_components/jquery-validation/dist/additional-methods.js',
											'bower_components/angular-ui-select/dist/select.min.js',
											'bower_components/angular-sanitize/angular-sanitize.min.js',
											'bower_components/zingchart/client/zingchart.min.js',
											'bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
											'bower_components/ZingChart-AngularJS/src/zingchart-angularjs.js',
											'bower_components/dropzone/dist/min/dropzone.min.css',
											'bower_components/dropzone/dist/min/dropzone.min.js',
											'bower_components/jpkleemans-angular-validate/src/angular-validate.js',
											'bower_components/angular-flatpickr/dist/ng-flatpickr.min.js',
											'bower_components/flatpickr/dist/flatpickr.min.js'
										]

									});
							}]
					}
				})

			.state(
				'superadmin',
				{
					url: '/superadmin/:id',
					templateUrl: 'assets/views/masters/superAdmin.html',
					controller: 'superAdminCtrl',
					resolve: {
						deps: [
							'$ocLazyLoad',
							function ($ocLazyLoad) {
								return $ocLazyLoad
									.load({
										insertBefore: '#css-bootstrap',
										serie: true,
										files: [
											'bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
											'bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css',
											'bower_components/jquery-validation/dist/jquery.validate.min.js',
											'bower_components/jquery-validation/dist/additional-methods.js',
											'bower_components/select2/dist/js/select2.full.min.js',
											'bower_components/select2/dist/css/select2.min.css',
											'bower_components/zingchart/client/zingchart.min.js',
											'bower_components/moment/min/moment.min.js',
											'bower_components/ZingChart-AngularJS/src/zingchart-angularjs.js',
											'bower_components/dropzone/dist/min/dropzone.min.css',
											'bower_components/dropzone/dist/min/dropzone.min.js'
										]
									});
							}]
					}
				})


			// .state(
			// 	'patientProfile.prescription',
			// 	{
			// 		url: '/prescription/:f',
			// 		controller: 'AddPrescriptionController',
			// 		template: '<dir-add-prescription> </dir-add-prescription>'
			// 	})

			.state(
				'addLabResults',
				{
					url: '/historicalresults/:id',
					templateUrl: 'assets/views/labResults/add_lab_results.html',
					controller: 'HistoricalLabResultCtrl',
					resolve: {
						deps: [
							'$ocLazyLoad',
							function ($ocLazyLoad) {
								return $ocLazyLoad
									.load({
										insertBefore: '#css-bootstrap',
										serie: true,
										files: [
											'bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css',
											'bower_components/select2/dist/css/select2.min.css',
											'bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
											'bower_components/moment/min/moment.min.js',
											'bower_components/select2/dist/js/select2.full.min.js',
											'bower_components/dropzone/dist/min/dropzone.min.css',
											'bower_components/dropzone/dist/min/dropzone.min.js',
											'bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js',
											'bower_components/jquery-auto-complete/jquery.auto-complete.min.js',
											'bower_components/jquery.tagsinput/src/jquery.tagsinput.js']
									});
							}]
					}
				})


			.state(
				'viewAllNotifications',
				{
					templateUrl: 'assets/views/notifications/view_notifications.html',
					controller: 'viewAllNotificationCtrl',
					resolve: {
						deps: [
							'$ocLazyLoad',
							function ($ocLazyLoad) {
								return $ocLazyLoad
									.load({
										insertBefore: '#css-bootstrap',
										serie: true,
										files: [
											'bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css',
											'bower_components/select2/dist/css/select2.min.css',
											'bower_components/moment/min/moment.min.js',
											'bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
											'bower_components/select2/dist/js/select2.full.min.js',
											'bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js',
											'bower_components/jquery-auto-complete/jquery.auto-complete.min.js']
									});
							}]
					}
				})

			.state(
				'notification',
				{
					templateUrl: 'assets/views/partials/base_header_patients.html',
					controller: 'HeaderCtrl',
					resolve: {
						deps: [
							'$ocLazyLoad',
							function ($ocLazyLoad) {
								return $ocLazyLoad
									.load({
										insertBefore: '#css-bootstrap',
										serie: true,
										files: [
											'bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css',
											'bower_components/select2/dist/css/select2.min.css',
											'bower_components/moment/min/moment.min.js',
											'bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
											'bower_components/select2/dist/js/select2.full.min.js',
											'bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js',
											'bower_components/jquery-auto-complete/jquery.auto-complete.min.js',
											'bower_components/jquery.tagsinput/src/jquery.tagsinput.js']
									});
							}]
					}
				})

			.state(
				'uiBlocksDraggable',
				{
					url: '/ui/blocks-draggable',
					templateUrl: 'assets/views/ui_blocks_draggable.html',
					resolve: {
						deps: [
							'$ocLazyLoad',
							function ($ocLazyLoad) {
								return $ocLazyLoad
									.load({
										insertBefore: '#css-bootstrap',
										serie: true,
										files: ['bower_components/jquery-ui/jquery-ui.min.js']
									});
							}]
					}
				})

			.state(
				'login',
				{
					url: '/login',
					templateUrl: 'assets/views/login.html',
					resolve: {
						deps: [
							'$ocLazyLoad',
							function ($ocLazyLoad) {
								return $ocLazyLoad
									.load({
										insertBefore: '#css-bootstrap',
										serie: true,
										files: ['bower_components/jquery-ui/jquery-ui.min.js']
									});
							}]
					}
				})

			.state(
				'launch',
				{
					url: '/launch',
					templateUrl: 'assets/views/launchpad.html',
					resolve: {
						deps: [
							'$ocLazyLoad',
							function ($ocLazyLoad) {
								return $ocLazyLoad
									.load({
										insertBefore: '#css-bootstrap',
										serie: true,
										files: ['bower_components/jquery-ui/jquery-ui.min.js']
									});
							}]
					}
				})

			.state(
				'event',
				{
					url: '/event',
					templateUrl: 'assets/views/event.html',
					controller: 'FormsPickersMoreCtrl',
					resolve: {
						deps: [
							'$ocLazyLoad',
							function ($ocLazyLoad) {
								return $ocLazyLoad
									.load({
										insertBefore: '#css-bootstrap',
										serie: true,
										files: [
											'assets/css/ies_attendance.css',
											'bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css',
											'bower_components/select2/dist/css/select2.min.css',
											// 'bower_components/select2/select2-bootstrap.min.css',
											// not
											// found
											// in
											// bower
											'bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
											'bower_components/moment/min/moment.min.js',
											'bower_components/select2/dist/js/select2.full.min.js',
											'bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js',
											'bower_components/jquery-auto-complete/jquery.auto-complete.min.js',
											'bower_components/jquery.tagsinput/src/jquery.tagsinput.js']
									});
							}]
					}
				})

			.state(
				'eventCalendar',
				{
					url: '/components/calendar',
					templateUrl: 'assets/views/event_calendar.html',
					controller: 'CompCalendarCtrl',
					resolve: {
						deps: [
							'$ocLazyLoad',
							function ($ocLazyLoad) {
								return $ocLazyLoad
									.load({
										insertBefore: '#css-bootstrap',
										serie: true,
										files: [
											'bower_components/fullcalendar/dist/fullcalendar.min.css',
											'bower_components/moment/min/moment.min.js',
											'bower_components/fullcalendar/dist/fullcalendar.min.js',
											'bower_components/fullcalendar/dist/gcal.min.js',
											'bower_components/jquery-ui/jquery-ui.min.js',

										]
									});
							}]
					}
				})

			.state(
				'assocCrud',
				{
					url: '/event',
					templateUrl: 'assets/views/associations_crud.html',
				})

			.state(
				'eventDetail',
				{
					url: '/event',
					templateUrl: 'assets/views/event_detail.html',
					controller: 'FormsPickersMoreCtrl',
					resolve: {
						deps: [
							'$ocLazyLoad',
							function ($ocLazyLoad) {
								return $ocLazyLoad
									.load({
										insertBefore: '#css-bootstrap',
										serie: true,
										files: [
											'assets/css/ies_attendance.css',
											'bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css',
											'bower_components/select2/dist/css/select2.min.css',
											// 'bower_components/select2/select2-bootstrap.min.css',
											// not
											// found
											// in
											// bower
											'bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
											'bower_components/moment/min/moment.min.js',
											'bower_components/select2/dist/js/select2.full.min.js',
											'bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js',
											'bower_components/jquery-auto-complete/jquery.auto-complete.min.js',
											'bower_components/jquery.tagsinput/src/jquery.tagsinput.js']
									});
							}]
					}
				})

			.state(
				'eventCreate',
				{
					url: '/event',
					templateUrl: 'assets/views/event_create.html',
					resolve: {
						deps: [
							'$ocLazyLoad',
							function ($ocLazyLoad) {
								return $ocLazyLoad
									.load({
										insertBefore: '#css-bootstrap',
										serie: true,
										files: [
											'bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css',
											'bower_components/select2/dist/css/select2.min.css',
											'bower_components/dropzone/dist/min/dropzone.min.css',
											'bower_components/summernote/dist/summernote.css',
											'bower_components/summernote/dist/summernote.min.js',
											'bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
											'bower_components/moment/min/moment.min.js',
											'bower_components/select2/dist/js/select2.full.min.js',
											'bower_components/dropzone/dist/min/dropzone.min.js'

										]
									});
							}]
					}
				})

			.state(
				'uiActivity',
				{
					url: '/ui/activity',
					templateUrl: 'assets/views/ui_activity.html',
					controller: 'UiActivityCtrl',
					resolve: {
						deps: [
							'$ocLazyLoad',
							function ($ocLazyLoad) {
								return $ocLazyLoad
									.load({
										insertBefore: '#css-bootstrap',
										serie: true,
										files: [
											'bower_components/sweetalert2/dist/sweetalert2.min.css',
											'bower_components/remarkable-bootstrap-notify/bootstrap-notify.min.js',
											'bower_components/es6-promise/es6-promise.auto.min.js',
											'bower_components/sweetalert2/dist/sweetalert2.js']
									});
							}]
					}
				})
			.state('uiTabs', {
				url: '/ui/tabs',
				templateUrl: 'assets/views/ui_tabs.html'
			})

			.state(
				'uiModalsTooltips',
				{
					url: '/ui/modals-tooltips',
					templateUrl: 'assets/views/ui_modals_tooltips.html'
				})
			.state('tablesTools', {
				url: '/tables/tools',
				templateUrl: 'assets/views/tables_tools.html'
			})
			.state(
				'tablesDatatables',
				{
					url: '/tables/datatables',
					templateUrl: 'assets/views/tables_datatables.html',
					controller: 'TablesDatatablesCtrl',
					resolve: {
						deps: [
							'$ocLazyLoad',
							function ($ocLazyLoad) {
								return $ocLazyLoad
									.load({
										insertBefore: '#css-bootstrap',
										serie: true,
										files: [
											'bower_components/datatables/media/css/jquery.dataTables.min.css',
											'bower_components/datatables/media/js/jquery.dataTables.min.js']
									});
							}]
					}
				})
			.state(
				'formsPickersMore',
				{
					url: '/forms/pickers-more',
					templateUrl: 'assets/views/forms_pickers_more.html',
					controller: 'FormsPickersMoreCtrl',
					resolve: {
						deps: [
							'$ocLazyLoad',
							function ($ocLazyLoad) {
								return $ocLazyLoad
									.load({
										insertBefore: '#css-bootstrap',
										serie: true,
										files: [
											'bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css',
											'bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
											'bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css',
											'bower_components/select2/dist/css/select2.min.css',
											// 'bower_components/select2/select2-bootstrap.min.css',
											// not
											// found
											// in
											// bower
											'bower_components/jquery-auto-complete/jquery.auto-complete.css',
											'bower_components/ion.rangeSlider/css/ion.rangeSlider.css',
											'bower_components/ion.rangeSlider/css/ion.rangeSlider.skinHTML5.css',
											'bower_components/dropzone/dist/min/dropzone.min.css',
											'bower_components/jquery.tagsinput/src/jquery.tagsinput.css',

											'bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
											'bower_components/moment/min/moment.min.js',
											'bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js',
											'bower_components/bootstrap-maxlength/src/bootstrap-maxlength.js',
											'bower_components/select2/dist/js/select2.full.min.js',
											'bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js',
											'bower_components/jquery-auto-complete/jquery.auto-complete.min.js',
											'bower_components/ion.rangeSlider/js/ion.rangeSlider.min.js',
											'bower_components/dropzone/dist/min/dropzone.min.js',
											'bower_components/jquery.tagsinput/src/jquery.tagsinput.js']
									});
							}]
					}
				})

			.state(
				'formsValidation',
				{
					url: '/forms/validation',
					templateUrl: 'assets/views/forms_validation.html',
					controller: 'FormsValidationCtrl',
					resolve: {
						deps: [
							'$ocLazyLoad',
							function ($ocLazyLoad) {
								return $ocLazyLoad
									.load({
										insertBefore: '#css-bootstrap',
										serie: true,
										files: [
											'bower_components/select2/dist/css/select2.min.css',

											// 'bower_components/select2/select2-bootstrap.min.css',
											// //
											// not
											// found
											// in
											// bower
											'bower_components/select2/dist/js/select2.full.min.js',
											'bower_components/jquery-validation/dist/jquery.validate.min.js',
											'bower_components/jquery-validation/dist/additional-methods.js']
									});
							}]
					}
				})
			.state(
				'formsWizard',
				{
					url: '/forms/wizard',
					templateUrl: 'assets/views/forms_wizard.html',
					controller: 'FormsWizardCtrl',
					resolve: {
						deps: [
							'$ocLazyLoad',
							function ($ocLazyLoad) {
								return $ocLazyLoad
									.load({
										insertBefore: '#css-bootstrap',
										serie: true,
										files: [
											'bower_components/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js',
											'bower_components/jquery-validation/dist/jquery.validate.min.js',]
									});
							}]
					}
				})
			.state(
				'compCharts',
				{
					url: '/components/charts',
					templateUrl: 'assets/views/comp_charts.html',
					controller: 'CompChartsCtrl',
					resolve: {
						deps: [
							'$ocLazyLoad',
							function ($ocLazyLoad) {
								return $ocLazyLoad
									.load({
										insertBefore: '#css-bootstrap',
										serie: true,
										files: [
											'assets/js/plugins/sparkline/jquery.sparkline.min.js',
											'bower_components/requirejs/require.js',
											'bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js',
											'bower_components/Chart.js/Chart.js',
											'bower_components/flot/jquery.flot.js',
											'bower_components/flot/jquery.flot.pie.js',
											'bower_components/flot/jquery.flot.stack.js',
											'bower_components/flot/jquery.flot.resize.js']
									});
							}]
					}
				})
			.state(
				'compCalendar',
				{
					url: '/components/calendar',
					templateUrl: 'assets/views/comp_calendar.html',
					controller: 'CompCalendarCtrl',
					resolve: {
						deps: [
							'$ocLazyLoad',
							function ($ocLazyLoad) {
								return $ocLazyLoad
									.load({
										insertBefore: '#css-bootstrap',
										serie: true,
										files: [
											'bower_components/fullcalendar/dist/fullcalendar.min.css',
											'bower_components/moment/min/moment.min.js',
											'bower_components/fullcalendar/dist/fullcalendar.min.js',
											'bower_components/fullcalendar/dist/gcal.min.js',
											'bower_components/jquery-ui/jquery-ui.min.js',

										]
									});
							}]
					}
				})
			.state(
				'compSliders',
				{
					url: '/components/sliders',
					templateUrl: 'assets/views/comp_sliders.html',
					resolve: {
						deps: [
							'$ocLazyLoad',
							function ($ocLazyLoad) {
								return $ocLazyLoad
									.load({
										insertBefore: '#css-bootstrap',
										serie: true,
										files: [
											'bower_components/slick-carousel/slick/slick.css',
											'bower_components/slick-carousel/slick/slick-theme.css',
											'bower_components/slick-carousel/slick/slick.js']
									});
							}]
					}
				})
			.state(
				'compScrolling',
				{
					url: '/components/scrolling',
					templateUrl: 'assets/views/comp_scrolling.html'
				})
			.state(
				'compSyntaxHighlighting',
				{
					url: '/components/syntax-highlighting',
					templateUrl: 'assets/views/comp_syntax_highlighting.html',
					controller: 'CompSyntaxHighlightingCtrl',
					resolve: {
						deps: [
							'$ocLazyLoad',
							function ($ocLazyLoad) {
								return $ocLazyLoad
									.load({
										insertBefore: '#css-bootstrap',
										serie: true,
										files: [
											'bower_components/highlightjs/styles/github-gist.css',
											'bower_components/highlightjs/highlight.pack.min.js']
									});
							}]
					}
				})
			.state(
				'compRating',
				{
					url: '/components/rating',
					templateUrl: 'assets/views/comp_rating.html',
					controller: 'CompRatingCtrl',
					resolve: {
						deps: [
							'$ocLazyLoad',
							function ($ocLazyLoad) {
								return $ocLazyLoad
									.load({
										insertBefore: '#css-bootstrap',
										serie: true,
										files: ['bower_components/raty/lib/jquery.raty.js']
									});
							}]
					}
				})
			.state(
				'compTreeview',
				{
					url: '/components/treeview',
					templateUrl: 'assets/views/comp_treeview.html',
					controller: 'CompTreeviewCtrl',
					resolve: {
						deps: [
							'$ocLazyLoad',
							function ($ocLazyLoad) {
								return $ocLazyLoad
									.load({
										insertBefore: '#css-bootstrap',
										serie: true,
										files: [
											'bower_components/bootstrap-treeview/dist/bootstrap-treeview.min.css',
											'bower_components/bootstrap-treeview/dist/bootstrap-treeview.min.js']
									});
							}]
					}
				})
	}]);

// Tooltips and Popovers configuration
App.config(['$uibTooltipProvider', function ($uibTooltipProvider) {
	$uibTooltipProvider.options({
		appendToBody: true
	});
}]);

// Custom UI helper functions
App.factory(
	'uiHelpers',
	function () {
		return {

			// Handles #main-container height resize to push footer
			// to the bottom of the page
			uiHandleMain: function () {
				var lMain = jQuery('#main-container');
				var hWindow = jQuery(window).height();
				var hHeader = jQuery('#header-navbar')
					.outerHeight();
				var hFooter = jQuery('#page-footer').outerHeight();

				if (jQuery('#page-container').hasClass(
					'header-navbar-fixed')) {
					lMain.css('min-height', hWindow - hFooter);
				} else {
					lMain.css('min-height', hWindow
						- (hHeader + hFooter));
				}
			},
			// Handles transparent header functionality
			uiHandleHeader: function () {
				var lPage = jQuery('#page-container');

				if (lPage.hasClass('header-navbar-fixed')
					&& lPage
						.hasClass('header-navbar-transparent')) {
					jQuery(window)
						.on(
							'scroll',
							function () {
								if (jQuery(this)
									.scrollTop() > 20) {
									lPage
										.addClass('header-navbar-scroll');
								} else {
									lPage
										.removeClass('header-navbar-scroll');
								}
							});
				}
			},

			// Handles page loader functionality
			uiLoader: function (mode) {
				var lBody = jQuery('body');
				var lpageLoader = jQuery('#page-loader');

				if (mode === 'show') {
					if (lpageLoader.length) {
						lpageLoader.fadeIn(250);
					} else {
						lBody
							.prepend('<div id="page-loader"></div>');
					}
				} else if (mode === 'hide') {
					if (lpageLoader.length) {
						lpageLoader.fadeOut(250);
					}
				}
			},
			// Handles blocks API functionality
			uiBlocks: function (block, mode, button) {
				// Set default icons for fullscreen and content
				// toggle buttons
				var iconFullscreen = 'si si-size-fullscreen';
				var iconFullscreenActive = 'si si-size-actual';
				var iconContent = 'icon-arrow-up32';
				var iconContentActive = 'icon-arrow-down32';

				if (mode === 'init') {
					// Auto add the default toggle icons
					switch (button.data('action')) {
						case 'fullscreen_toggle':
							button
								.html('<i class="'
									+ (button
										.closest('.block')
										.hasClass(
											'block-opt-fullscreen') ? iconFullscreenActive
										: iconFullscreen)
									+ '"></i>');
							break;
						case 'content_toggle':
							button
								.html('<i class="'
									+ (button
										.closest('.block')
										.hasClass(
											'block-opt-hidden') ? iconContentActive
										: iconContent)
									+ '"></i>');
							break;
						default:
							return false;
					}
				} else {
					// Get block element
					var elBlock = (block instanceof jQuery) ? block
						: jQuery(block);

					// If element exists, procceed with blocks
					// functionality
					if (elBlock.length) {
						// Get block option buttons if exist (need
						// them to update their icons)
						var btnFullscreen = jQuery(
							'[data-js-block-option][data-action="fullscreen_toggle"]',
							elBlock);
						var btnToggle = jQuery(
							'[data-js-block-option][data-action="content_toggle"]',
							elBlock);

						// Mode selection
						switch (mode) {
							case 'fullscreen_toggle':
								elBlock
									.toggleClass('block-opt-fullscreen');

								// Enable/disable scroll lock to block
								if (elBlock
									.hasClass('block-opt-fullscreen')) {
									jQuery(elBlock)
										.scrollLock('enable');
								} else {
									jQuery(elBlock).scrollLock(
										'disable');
								}

								// Update block option icon
								if (btnFullscreen.length) {
									if (elBlock
										.hasClass('block-opt-fullscreen')) {
										jQuery('i', btnFullscreen)
											.removeClass(
												iconFullscreen)
											.addClass(
												iconFullscreenActive);
									} else {
										jQuery('i', btnFullscreen)
											.removeClass(
												iconFullscreenActive)
											.addClass(
												iconFullscreen);
									}
								}
								break;
							case 'fullscreen_on':
								elBlock
									.addClass('block-opt-fullscreen');

								// Enable scroll lock to block
								jQuery(elBlock).scrollLock('enable');

								// Update block option icon
								if (btnFullscreen.length) {
									jQuery('i', btnFullscreen)
										.removeClass(iconFullscreen)
										.addClass(
											iconFullscreenActive);
								}
								break;
							case 'fullscreen_off':
								elBlock
									.removeClass('block-opt-fullscreen');

								// Disable scroll lock to block
								jQuery(elBlock).scrollLock('disable');

								// Update block option icon
								if (btnFullscreen.length) {
									jQuery('i', btnFullscreen)
										.removeClass(
											iconFullscreenActive)
										.addClass(iconFullscreen);
								}
								break;
							case 'content_toggle':
								elBlock.toggleClass('block-opt-hidden');

								// Update block option icon
								if (btnToggle.length) {
									if (elBlock
										.hasClass('block-opt-hidden')) {
										jQuery('i', btnToggle)
											.removeClass(
												iconContent)
											.addClass(
												iconContentActive);
									} else {
										jQuery('i', btnToggle)
											.removeClass(
												iconContentActive)
											.addClass(iconContent);
									}
								}
								break;
							case 'content_hide':
								elBlock.addClass('block-opt-hidden');

								// Update block option icon
								if (btnToggle.length) {
									jQuery('i', btnToggle).removeClass(
										iconContent).addClass(
											iconContentActive);
								}
								break;
							case 'content_show':
								elBlock.removeClass('block-opt-hidden');

								// Update block option icon
								if (btnToggle.length) {
									jQuery('i', btnToggle).removeClass(
										iconContentActive)
										.addClass(iconContent);
								}
								break;
							case 'refresh_toggle':
								elBlock
									.toggleClass('block-opt-refresh');

								// Return block to normal state if the
								// demostration mode is on in the
								// refresh option button -
								// data-action-mode="demo"
								if (jQuery(
									'[data-js-block-option][data-action="refresh_toggle"][data-action-mode="demo"]',
									elBlock).length) {
									setTimeout(
										function () {
											elBlock
												.removeClass('block-opt-refresh');
										}, 2000);
								}
								break;
							case 'state_loading':
								elBlock.addClass('block-opt-refresh');
								break;
							case 'state_normal':
								elBlock
									.removeClass('block-opt-refresh');
								break;
							case 'close':
								elBlock.hide();
								break;
							case 'open':
								elBlock.show();
								break;
							default:
								return false;
						}
					}
				}
			}
		};
	});

// Run our App
App.run(function ($rootScope, uiHelpers) {
	// Access uiHelpers easily from all controllers
	$rootScope.helpers = uiHelpers;
	$rootScope.url = "/ihspdc/rest/";

	// On window resize or orientation change resize #main-container & Handle
	// scrolling
	var resizeTimeout;

	jQuery(window).on('resize orientationchange', function () {
		clearTimeout(resizeTimeout);

		resizeTimeout = setTimeout(function () {
			$rootScope.helpers.uiHandleMain();
		}, 150);
	});
});

// Application Main Controller
App.controller('AppCtrl', ['$scope', '$localStorage', '$window',
	function ($scope, $localStorage, $window) {
		// Template Settings
		$scope.webapp = {
			version: '3.1', // Template version
			localStorage: false, // Enable/Disable local storage
			settings: {

				headerFixed: true
				// Enables fixed header
			}
		};

		// If local storage setting is enabled
		if ($scope.webapp.localStorage) {
			// Save/Restore local storage settings
			if ($scope.webapp.localStorage) {
				if (angular.isDefined($localStorage.webappSettings)) {
					$scope.webapp.settings = $localStorage.webappSettings;
				} else {
					$localStorage.webappSettings = $scope.webapp.settings;
				}
			}

			// Watch for settings changes
			$scope.$watch('webapp.settings', function () {
				// If settings are changed then save them to localstorage
				$localStorage.webappSettings = $scope.webapp.settings;
			}, true);
		}

		// When view content is loaded
		$scope.$on('$viewContentLoaded', function () {
			// Hide page loader
			$scope.helpers.uiLoader('hide');

			// Resize #main-container
			$scope.helpers.uiHandleMain();
		});
	}]);


App.service('patientService', patientService);
patientService.$inject = ['$http', '$rootScope'];
function patientService($http, $rootScope) {
	this.getPatientsList = function (callback) {
		var responsePromise = $http({
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
			url: '/ihspdc/rest/activePatients?getNext=0'
		});
		responsePromise.then(function (responseData) {
			callback(null, responseData);
		}, function (error) {
			callback(error, null);
		});
	}

	this.getAllLabResults = function (opCode, callback) {
		var allLabResults = '/ihspdc/rest/getAllLabResults?patientId='
		allLabResults += opCode;
		var responsePromise = $http({
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
			url: allLabResults
		});
		responsePromise.then(function (response) {
			callback(null, response);
		}, function (error) {
			callback(error, null);
		});
	}

	this.getAllVitalResults = function (opCode, callback) {
		var allVitalResults = '/ihspdc/rest/getAllVitalResults?opCode='
		allVitalResults += opCode;
		var responsePromise = $http({
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
			url: allVitalResults
		});
		responsePromise.then(function (response) {
			callback(null, response);
		}, function (error) {
			callback(error, null);
		});
	}

	this.getFullPatientsList = function (callback) {
		var fullPatients = '/ihspdc/rest/getAllPatients'
		var responsePromise = $http({
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
			url: fullPatients
		});
		responsePromise.then(function (responseData) {
			callback(null, responseData);
		}, function (error) {
			callback(error, null);
		});
	}

	this.getDepartmentAssociatedPatients = function (departmentList, callback) {
		var Count = 0;
		var opCodes = [];
		angular.forEach(departmentList, function (dep) {
			var getCodDetail = '/ihspdc/rest/departmentPatients?depId='
			getCodDetail += dep.id;
			var responsePromise = $http({
				method: 'GET',
				headers: { 'Content-Type': 'application/json' },
				url: getCodDetail
			});
			responsePromise.then(function (response) {
				Count += 1;
				angular.forEach(response.data, function (op) {
					opCodes.push(op);
				})
				if (Count == departmentList.length) {
					callback(null, opCodes);
				}
			}, function (error) {
				callback(error, null);
			});
		});
	}

	//APPOINTMENT BOOKINg API'S
	this.isSlotAvaillable = function (date, timeSlot, callback) {
		var url3 = '/ihspdc/rest/findSlotAvaillable?date='
		url3 += date;
		url3 += "&timeSlot=";
		url3 += timeSlot;
		var responsePromise = $http({
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
			url: url3
		});
		responsePromise.then(function (responseData) {
			callback(null, responseData);
		}, function (error) {
			callback(null, error);
		});
	}

	this.getAllAppointments = function (callback) {
		var url3 = '/ihspdc/rest/getAllAppointments'
		var responsePromise = $http({
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
			url: url3
		});
		responsePromise.then(function (responseData) {
			callback(null, responseData);
		}, function (error) {
			callback(null, error);
		});
	}

	this.getDoctorUpComingAppointments = function (doctorId, callback) {
		var newUrl = '/ihspdc/rest/getAppointments/doctor/id?id=' + doctorId;
		var responsePromise = $http({
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
			url: newUrl
		});
		responsePromise.then(function (response) {
			callback(null, response);
		}, function (error) {
			callback(null, error);
		});
	}
	//END OF APPOINTMENT BOOKINGS

	this.getSenderInfo = function (senderId, callback) {
		if ($rootScope.Auth.hasResourceRole('coord')) {
			var getCodDetail = '/ihspdc/rest/coordDetails?codId='
			getCodDetail += senderId;
			var responsePromise = $http({
				method: 'GET',
				headers: { 'Content-Type': 'application/json' },
				url: getCodDetail
			});
			responsePromise.then(function (responseData) {
				callback(null, responseData);
			}, function (error) {
				callback(error, null);
			});
		}
		else if ($rootScope.Auth.hasResourceRole('doctor')) {
			var getDocDetail = '/ihspdc/rest/doctorDetails?docId='
			getDocDetail += senderId;
			var responsePromise = $http({
				method: 'GET',
				headers: { 'Content-Type': 'application/json' },
				url: getDocDetail
			});
			responsePromise.then(function (responseData) {
				callback(null, responseData);
			}, function (error) {
				callback(error, null);
			});
		}
		else if ($rootScope.Auth.hasResourceRole('patient')) {
			var patientDetails = '/ihspdc/rest/displayPatientDetails?patientId='
			patientDetails += senderId;
			var responsePromise = $http({
				method: 'GET',
				headers: { 'Content-Type': 'application/json' },
				url: patientDetails
			});
			responsePromise.then(function (responseData) {
				callback(null, responseData);
			}, function (error) {
				callback(error, null);
			});
		}
	}

	this.getConsultationDates = function (opCode, callback) {
		var consultDates = '/ihspdc/rest/getConsultDates?patientId='
		consultDates += opCode;
		var responsePromise = $http({
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
			url: consultDates
		});
		responsePromise.then(function (responseData) {
			callback(null, responseData);
		}, function (error) {
			callback(error, null);
		});
	}

	this.getRadiologyDates = function (opCode, callback) {
		var radialDates = '/ihspdc/rest/getRadiologyDates?patientId='
		radialDates += opCode;
		var responsePromise = $http({
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
			url: radialDates
		});
		responsePromise.then(function (responseData) {
			callback(null, responseData);
		}, function (error) {
			callback(error, null);
		});
	}

	this.getAllPatientCount = function (callback) {
		var patientDetails = '/ihspdc/rest/getAllPatientCount'
		var responsePromise = $http({
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
			url: patientDetails
		});
		responsePromise.then(function (responseData) {
			callback(null, responseData.data[0]);
		}, function (error) {
			callback(error, null);
		});
	}

	this.getCountOfPatientsByCoordId = function (id, callback) {
		var url = '/ihspdc/rest/activePatients/Count/by/CoordId?id=' + id;
		var responsePromise = $http({
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
			url: url
		});
		responsePromise.then(function (responseData) {
			callback(null, responseData.data[0]);
		}, function (error) {
			callback(error, null);
		});
	}

	this.getPatientCount = function (callback) {
		var patientDetails = '/ihspdc/rest/getPatientCount'
		var responsePromise = $http({
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
			url: patientDetails
		});
		responsePromise.then(function (responseData) {
			callback(null, responseData.data[0]);
		}, function (error) {
			callback(error, null);
		});
	}

	this.searchPatientByField = function (byField, searchTerm, callback) {
		if (byField == "1") {
			var url = '/ihspdc/rest/getPatientDetails?opCode=' + searchTerm;
			$http.get(url).then(function (response, err) {
				if (!err && response.data != "") {
					if ($rootScope.Auth.hasResourceRole('coord')) {
						if ($rootScope.Auth.tokenParsed.preferred_username.toUpperCase() == response.data.invitedBy.id.toUpperCase()) {
							callback(null, response);
						}
					}
					else {
						angular.forEach(response.data.doctors, function (docs) {
							if ($rootScope.Auth.tokenParsed.preferred_username.toUpperCase() == docs.docId.toUpperCase()) {
								callback(null, response);
							}
						});
					}
				} else {
					console.log('Error in searching patient');
				}
			});
		}
		else if (byField == "2" && searchTerm.length >= 10) {
			var url = '/ihspdc/rest/getPatient/by/mobile?mobileNo=' + searchTerm;
			var finalResponse = [];
			$http.get(url).then(function (response, err) {
				if (!err && response.data != "") {
					if ($rootScope.Auth.hasResourceRole('coord')) {
						angular.forEach(response.data, function (data) {
							if ($rootScope.Auth.tokenParsed.preferred_username.toUpperCase() == data.invitedBy.id.toUpperCase()) {
								finalResponse.push(data);
							}
						});
						callback(null, finalResponse);
					}
					else {
						angular.forEach(response.data, function (data) {
							angular.forEach(data.doctors, function (docs) {
								if ($rootScope.Auth.tokenParsed.preferred_username.toUpperCase() == docs.docId.toUpperCase()) {
									finalResponse.push(data);
								}
							});
						});
						callback(null, finalResponse);
					}
				} else {
					console.log('Error in searching patient');
				}
			});
		}
		else if (byField == "3") {
			var url = '/ihspdc/rest/getPatient/by/name?patientName=' + searchTerm;
			$http.get(url).then(function (response, err) {
				if (!err && response.data != "") {
					if ($rootScope.Auth.hasResourceRole('coord')) {
						if ($rootScope.Auth.tokenParsed.preferred_username.toUpperCase() == response.data.invitedBy.id.toUpperCase()) {
							callback(null, response);
						}
					}
					else {
						angular.forEach(response.data.doctors, function (docs) {
							if ($rootScope.Auth.tokenParsed.preferred_username.toUpperCase() == docs.docId.toUpperCase()) {
								callback(null, response);
							}
						});
					}
				} else {
					console.log('Error in searching patient');
				}
			});
		}
	}
}

App.service('otherServices', otherServices);
otherServices.$inject = ['$http', '$rootScope'];
function otherServices($http, $rootScope) {

	this.getDepartmentCount = function (callback) {
		var departmentCount = '/ihspdc/rest/getDepartmentCount'
		var responsePromise = $http({
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
			url: departmentCount
		});
		responsePromise.then(function (responseData) {
			callback(null, responseData.data[0]);
		}, function (error) {
			callback(error, null);
		});
	}

	this.getCoordinatorCount = function (callback) {
		var coordinatorCount = '/ihspdc/rest/getCoordinatorCount'
		var responsePromise = $http({
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
			url: coordinatorCount
		});
		responsePromise.then(function (responseData) {
			callback(null, responseData.data[0]);
		}, function (error) {
			callback(error, null);
		});
	}

	this.getDoctorCount = function (callback) {
		var doctorCount = '/ihspdc/rest/getDoctorCount'
		var responsePromise = $http({
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
			url: doctorCount
		});
		responsePromise.then(function (responseData) {
			callback(null, responseData.data[0]);
		}, function (error) {
			callback(error, null);
		});
	}

	this.getAllCoordsList = function (callback) {
		var pathUrl = '/ihspdc/rest/getCoordsList'
		var responsePromise = $http({
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
			url: pathUrl
		});
		responsePromise.then(function (response) {
			callback(null, response.data);
		}, function (error) {
			callback(error, null);
		});
	}

	this.getDeptDoctors = function (depId, callback) {
		var url = '/ihspdc/rest/doctorsDep?depId=' + depId
		var responsePromise = $http({
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
			url: url
		});
		responsePromise.then(function (response) {
			callback(null, response);
		}, function (error) {
			callback(error, null);
		});
	}

	this.getDeptList = function (callback) {
		var pathUrl = '/ihspdc/rest/getDepartmentsList'
		var responsePromise = $http({
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
			url: pathUrl
		});
		responsePromise.then(function (response) {
			callback(null, response);
		}, function (error) {
			callback(error, null);
		});
	}

	this.getAllDocList = function (callback) {
		var pathUrl = '/ihspdc/rest/getDoctorsList'
		var responsePromise = $http({
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
			url: pathUrl
		});
		responsePromise.then(function (response) {
			callback(null, response.data);
		}, function (error) {
			callback(error, null);
		});
	}
}

App.filter('html', ['$sce', function ($sce) {
	return function (text) {
		return $sce.trustAsHtml(text);
	};
}])

App.service('messageService', messageService);
messageService.$inject = ['$http', '$rootScope'];
function messageService($http, $rootScope) {
	this.viewAllMessages = function (messageLength, limitRecords, callback) {
		var messageQuery = '/ihspdc/rest/getMessages?id='
		messageQuery += $rootScope.Auth.tokenParsed.preferred_username.toUpperCase();
		messageQuery += '&skipRecords='
		messageQuery += messageLength;
		messageQuery += '&limitRecords='
		messageQuery += limitRecords;
		console.log(messageQuery);
		var responsePromise = $http({
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
			url: messageQuery
		});
		responsePromise.then(function (responseData) {
			var messageDate = {};
			messageDate.data = [];
			angular.forEach(responseData.data, function (msg) {
				messageDate.data.push(JSON.parse(msg));
			});
			callback(null, messageDate);
		}, function (error) {
			callback(error, null);
		});
	}

	this.deleteByMessageId = function (messageId, callback) {
		var deleteUrl = '/ihspdc/rest/deleteMessage?id='
		deleteUrl += messageId;
		var responsePromise = $http({
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
			url: deleteUrl
		});
		responsePromise.then(function (responseData) {
			callback(null, responseData);
		}, function (error) {
			callback(error, null);
		});
	}

}
// Header Controller
App.controller('HeaderCtrl', ['$scope', '$localStorage', '$window',
	function ($scope, $localStorage, $window) {
		// When view content is loaded
		$scope.$on('$includeContentLoaded', function () {
			// Transparent header functionality

		});
	}]);

//initialize keycloak and bootstrap angular
(function () {
	'use strict';
	angular.element(document).ready(function () {
		var keycloakEnabled = true;
		var keycloak = new Keycloak(window.keycloakConfig);
		//register listeners
		registerAuthListeners(keycloak);
		if (keycloakEnabled) {
			keycloak.init({
				onLoad: 'login-required',
				checkLoginIframe: true,
				checkLoginIframeInterval: 5,
				responseMode: 'fragment'
			}).success(function (authenticated) {
				angular.module('app').factory('Auth', function () {
					return keycloak;
				});
				angular.bootstrap(document, ['app']);
			}).error(function () {
				window.location.reload();
			});
		} else {
			angular.module('app').factory('Auth', function () {
				var keycloak = {};
				keycloak.tokenParsed = {
					email: 'admin@psg',
					preferred_username: '1520',
					name: 'Admin',
					userRoles: {
						'coord': 1
					}
				};
				keycloak.hasResourceRole = function (role) {
					return (role == "coord");
				};
				keycloak.principal = keycloak.tokenParsed;
				return keycloak;
			});
			angular.bootstrap(document, ['app']);
		}
	});//end document ready

	function registerAuthListeners(Auth) {
		Auth.onReady = function () {
			console.log("Adapter is initialized");
		};

		Auth.onAuthSuccess = function () {
			console.log("User is successfully authenticated");
			loadRootScopeWithUserProfile(Auth);
		};

		Auth.onAuthError = function () {
			console.log("Error during authentication");
		};

		Auth.onAuthRefreshSuccess = function () {
			//Called when the token is refreshed.
			console.log("Auth refresh success");
		};

		Auth.onAuthRefreshError = function () {
			//Called if there was an error while trying to refresh the token.
			console.log("Auth refresh error");
		};

		Auth.onAuthLogout = function () {
			//Called if the user is logged out (will only be called if the session status iframe is enabled, or in Cordova mode).
			console.log("Auth logout successfully");
		};

		Auth.onTokenExpired = function () {
			//Called when the access token is expired. 
			//If a refresh token is available the token can be refreshed with updateToken, or 
			//in cases where it is not (that is, with implicit flow) you can redirect to login screen to obtain a new access token.
			console.log("Token expired");
		};
	}

	//store / cache user profile in rootScope
	function loadRootScopeWithUserProfile(Auth) {
		Auth.principal = Auth.tokenParsed;
		Auth.principal.userRoles = {};
		Auth.principal.userEntitlements = getEntitlements(Auth, rolesToEntitlements);
		Auth.principal.userStates = getStates(Auth, roleToStates);
		console.log(JSON.stringify(Auth.principal));
	}

	function getEntitlements(Auth, allRoles) {
		var userEntitlements = {};
		Object.getOwnPropertyNames(allRoles).forEach(function (role) {
			if (Auth.principal.resource_access["pdc_client"].roles.indexOf(role) >= 0) {
				//get entitlements for the current role
				console.log("fetching entitlements for role " + role);
				var roleWithEntitlements = allRoles[role];
				Auth.principal.userRoles[role] = true;
				roleWithEntitlements.forEach(function (e) {
					//check each entitlement if present in userEntitlements and add it to the array if not present
					if (!userEntitlements[e]) {
						console.log("Adding entitlement to user " + e);
						userEntitlements[e] = true;
					}
				});
			}
		});
		console.log(userEntitlements);
		return userEntitlements;
	}

	//get the states for the role
	function getStates(Auth, allRoles) {
		var userStates = {};
		Object.getOwnPropertyNames(allRoles).forEach(function (role) {
			if (Auth.principal.realm_access.roles.indexOf(role) >= 0) {
				//get entitlements for the current role
				console.log("fetching states for role " + role);
				var roleWithStates = allRoles[role];
				roleWithStates.forEach(function (e) {
					//check each state if present in userState and add it to the array if not present
					if (!userStates[e]) {
						console.log("Adding state to user " + e);
						userStates[e] = true;
					}
				});
			}
		});
		console.log(userStates);
		return userStates;
	}

	var rolesToEntitlements =
	{
		"patient": [
			"appstat",
			"configurations"
		]

	};

	//added entitlements (key) and state(value)
	//all these states used in current example come under application sales 
	var roleToStates =
	{

		"SYS_ADMIN": [
			"appstat",
			"configurations"
		],

	};

	angular.module('app').constant("rolesToEntitlements", rolesToEntitlements);
	angular.module('app').constant("roleToStates", roleToStates);

	//initialize and login to keycloak as well as attach event handlers
	angular.module('app').run(['$rootScope', '$location', 'Auth', 'rolesToEntitlements', 'roleToStates', runKeycloak]);
	function runKeycloak($rootScope, $location, Auth, rolesToEntitlements, roleToStates) {
		$rootScope.Auth = Auth;

		//login to keycloak
		$rootScope.$on("event:auth-loginRequired", function () {
			console.log("Event auth-loginRequired acquired")
			var loginOptions = {
				redirectUri: window.location,
				prompt: "none",
				maxAge: 3600,
				loginHint: "",
				action: "login",
				locale: "en"
			};
			console.log(createLoginUrl(loginOptions));
			Auth.login(loginOptions);
		});
	}//end init

})();//end function

